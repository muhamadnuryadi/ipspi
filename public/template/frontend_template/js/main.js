$('.headerMenu').click(function() {
    var $this = $(this);
    if ($this.hasClass('is-open')) {
        $this.removeClass('is-open').addClass('is-openOut');
    } else {
        $this.addClass('is-open').removeClass('is-openOut');
    }
});

$('#customFile1').on('change',function(){
    let fileName = $(this).val().split('\\').pop(); 
    $(this).nextAll('.custom-file-label').addClass("selected").html(fileName);
})

$(document).ready(function(){
  $('.ipspi-latest-news').each(function(){  
    var highestBox = 0;
    $('.article-item', this).each(function(){
      if($(this).height() > highestBox) {
        highestBox = $(this).height(); 
      }
    });  
    $('.article-item',this).height(highestBox);
  }); 
  $('.article-detail').each(function(){  
    var highestBox = 0;
    $('.article-item', this).each(function(){
      if($(this).height() > highestBox) {
        highestBox = $(this).height(); 
      }
    });  
    $('.article-item',this).height(highestBox);
  }); 
  $('.related-article').each(function(){  
    var highestBox = 0;
    $('.related-list', this).each(function(){
      if($(this).height() > highestBox) {
        highestBox = $(this).height(); 
      }
    });  
    $('.related-list',this).height(highestBox);
  });
  $('.ipspi-faq').each(function(){  
    var highestBox = 0;
    $('.card-header', this).each(function(){
      if($(this).height() > highestBox) {
        highestBox = $(this).height(); 
      }
    });  
    $('.card-header',this).height(highestBox);
  }); 
 // Add minus icon for collapse element which is open by default
  $(".collapse.show").each(function(){
    $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
  });

    // Toggle plus minus icon on show hide of collapse element
  $(".collapse").on('show.bs.collapse', function(){
    $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
  }).on('hide.bs.collapse', function(){
    $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
  });
});


 $('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  autoplay: true,
  autoplaySpeed: 3000,
});
$('.feature-nav').slick({
  slidesToShow: 4,
  asNavFor: '.slider-for',
  dots: false,
  centerMode: false,
  vertical: true,
  focusOnSelect: true
});
 // On before slide change match active thumbnail to current slide
 $('.slider-for').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
 	var mySlideNumber = nextSlide;
 	$('.feature-nav .slick-slide').removeClass('nav-current');
 	$('.feature-nav .slick-slide').eq(mySlideNumber).addClass('nav-current');
});
$('.mitra').slick({
  slidesToShow: 5,
  slidesToScroll: 1,
  arrows: true,
  autoplay: false,
  autoplaySpeed: 4000,
  responsive: [
  {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
