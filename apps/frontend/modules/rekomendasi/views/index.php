<link href="<?php echo $this->webconfig['back_base_template']; ?>js/toastr/toastr.css" rel="stylesheet" type="text/css" />
<script src="<?php echo $this->webconfig['back_base_template']; ?>js/toastr/toastr.js" type="text/javascript"></script>
<section class="head-wrap">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1 class="head-title">Surat Rekomendasi</h1>
      </div>
    </div>
  </div>
</section>
<?php if(isset($error_hash) && count($error_hash) > 0){ ?>
    <?php foreach($error_hash as $inp_err){ ?>
        <script type="text/javascript">
        jQuery(document).ready(function($) {
            toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
            });
        </script>
    <?php } ?>
<?php } ?>
<?php if(isset($success)){ ?>
    <script type="text/javascript">
    jQuery(document).ready(function($) {
        toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
        });
    </script>
<?php } ?>
<section class="content">
  <div class="container">
    <div class="row">
      <!-- <div class="col-md-3"></div> -->
      <div class="col-md-12">
        <?php //echo json_encode($this->session->userdata());?>
        <div class="content-wrapper p-5">
          <div class="text-center">
            <div class="heading">Surat Rekomendasi</div>
          </div>
          <div class="viewer mt-5">
          <object data="<?php echo $this->webconfig['frontend_template']; ?>images/rekomendasi.pdf" type="application/pdf" width="100%" height="600px">
            <iframe width="100%" height="600" name="iframe_a" src="<?php echo $this->webconfig['media-server-files'].$lists[0]['files'];?>">
              This browser does not support PDFs. Please download the PDF to view it: <a href="<?php echo $this->webconfig['media-server-files'].$lists[0]['files'];?>">Download PDF</a>
            </iframe>
          </object>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>