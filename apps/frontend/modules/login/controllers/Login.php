<?php
class Login extends CI_Controller{
    var $webconfig;
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->sharedModel('LoginModel');
        
        $this->load->sharedModel('AboutModel');
        $this->about = $this->AboutModel->listData();
        $this->load->sharedModel('InformasiModel');
        $this->informasi = $this->InformasiModel->listData();
        $this->load->sharedModel('ConfigurationModel');
        $this->configuration = $this->ConfigurationModel->listData();
        $this->load->sharedModel('RegulationModel');
        $this->regulation = $this->RegulationModel->listData();
        
        $this->webconfig = $this->config->item('webconfig');
        $this->module_name = $this->lang->line('login');
    }
    function index(){
        $tdata = array();

        if($this->LoginModel->isLoggedIn()){
            redirect('admincms/members');
        }
        
        if ($_POST) {
            
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $check = $this->LoginModel->doLogin($email,$password);
            
            switch ($check){
                case 'failed_input':
                        $tdata['email'] = $this->input->post('email');
                        $tdata['password'] = $this->input->post('password');
                        $tdata['error_message'] = $this->lang->line('error_login_failed_input');
                    break;
                case 'failed_login':
                        $tdata['email'] = $this->input->post('email');
                        $tdata['password'] = $this->input->post('password');
                        $tdata['error_message'] = $this->lang->line('error_login_failed_login');
                    break;
                case 'banded':
                        $tdata['email'] = $this->input->post('email');
                        $tdata['password'] = $this->input->post('password');
                        $tdata['error_message'] = $this->lang->line('error_member_banded');
                    break;
                case 'non_activation':
                        $tdata['email'] = $this->input->post('email');
                        $tdata['password'] = $this->input->post('password');
                        $tdata['error_message'] = $this->lang->line('error_member_non_active');
                    break;
                case 'success_login':
                        $tdata['success'] = TRUE;
                    break;
            }
            
            if(isset($tdata['success']) && $tdata['success'] == '1'){
                redirect('admincms/members');
            }else{
                
                ## LOAD LAYOUT ##
                $ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
                $this->load->sharedView('template', $ldata);        
            }
        }else{
            ## LOAD LAYOUT ##
            $ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }

    function logout(){
        $this->LoginModel->doLogout();
        if($this->session->userdata('role') == 3){
            redirect('login');
        }else{
            redirect(base_url().'login');
        }
    }
}