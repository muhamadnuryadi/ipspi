 <?php if(isset($error_message) && $error_message != ''){ ?>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      toastr.error("<?php echo $error_message; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
    });
  </script>
<?php } ?>
<?php if(isset($success)){ ?>
    <script type="text/javascript">
    jQuery(document).ready(function($) {
        toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
        });
    </script>
<?php } ?>
 <section class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-6 offset-md-3">
        <div class="content-wrapper text-center p-5">
          <div class="heading">Login</div>
          <form method="post" action="<?php echo base_url(); ?>login">
            <div class="form-group">
              <label for="formEmail">Alamat Email</label>
              <input type="email" class="form-control" id="formEmail" name="email" value="<?php echo isset($email)?$email:'';?>">
            </div>
            <div class="form-group">
              <label for="formPassword">Password</label>
              <input type="password" class="form-control" id="formPassword" name="password" value="<?php echo isset($password)?$password:'';?>">
            </div>
            <div class="text-center">
              <button type="submit" class="btn btn-lg btn-green px-5 ">Masuk</button>
            </div>
          </form>
          <div class="forgot-txt">Lupa Password? <a href="<?php echo base_url(); ?>forgot">Klik Disini</a></div>
          <div class="announcement-box sm">
            <div class="row">
              <div class="col">
                <h2>Pengumuman</h2>
                <p>Pengumuman untuk seluruh <strong>Anggota IPSPI</strong> yang sudah terdaftar sebagai anggota namun <strong>tidak bisa login</strong>, diharapkan untuk melakukan <strong>registasi ulang</strong> dengan mengklik tombol berikut</p>
                <a href="<?php echo base_url().'daftarulang'?>" class="btn btn-lg btn-green my-1">Registrasi Ulang</a>
              </div>
            </div>
          </div>

          <h4>Belum punya akun? Registrasi sekarang</h4>
          <a href="<?php echo base_url(); ?>register" class="btn btn-blue px-5 mt-3">Daftar</a>
        </div>
      </div>
    </div>
  </div>
</section>