<?php if(isset($error_hash) && count($error_hash) > 0){ ?>
    <?php foreach($error_hash as $inp_err){ ?>
        <script type="text/javascript">
        jQuery(document).ready(function($) {
            toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
            });
        </script>
    <?php } ?>
<?php } ?>
<?php if(isset($success)){ ?>
    <script type="text/javascript">
    $(document).ready(function($) {
        toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
        });
    </script>
<?php } ?>
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-6 offset-md-3">
        <div class="content-wrapper text-center p-5">
          <div class="heading">Lupa Password</div>
          <p>Kami akan mengirimkan link ke alamat email untuk mengembalikan password anda. Link tersebut akan berlaku selama 1x24 jam.</p>
          <form name="forgotform" method="post" action="<?php echo base_url(); ?>forgot">
            <div class="form-group">
              <label for="formEmail">Alamat Email</label>
              <input name="email" type="email" class="form-control" id="formEmail">
            </div>
            <div class="text-center">
              <button type="submit" class="btn btn-lg btn-green px-5">Reset Password</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>