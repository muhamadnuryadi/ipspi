<?php if(isset($error_hash) && count($error_hash) > 0){ ?>
    <?php foreach($error_hash as $inp_err){ ?>
        <script type="text/javascript">
        jQuery(document).ready(function($) {
            toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
            });
        </script>
    <?php } ?>
<?php } ?>
<?php if(isset($success)){ ?>
    <script type="text/javascript">
    $(document).ready(function($) {
        toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
        setTimeout(function(){window.location.href='<?php echo base_url(); ?>login'},3000);
    });
    </script>
<?php } ?>
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-6 offset-md-3">
        <div class="content-wrapper text-center p-5">
          <div class="heading">Reset Password</div>
          <p>Silahkan masukan password baru anda.</p>
          <form name="forgotform" method="post" action="">
            <div class="form-group">
                <label for="exampleNewPassword">New Password</label>
                <input type="password" class="form-control" id="exampleNewPassword" name="password" value="<?php echo isset($password)?$password:'';?>">
            </div>
            <div class="form-group">
                <label for="exampleConfrimNewPassword">Confrim New Password</label>
                <input type="password" class="form-control" id="exampleConfrimNewPassword" name="password_1" value="<?php echo isset($password_1)?$password_1:'';?>">
            </div>
            <div class="text-center">
              <button type="submit" class="btn btn-lg btn-green px-5">Reset Password</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>