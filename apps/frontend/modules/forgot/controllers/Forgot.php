<?php
class Forgot extends CI_Controller{
    var $webconfig;
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->helper('phpmailer');
        $this->load->sharedModel('LoginModel');
        $this->load->sharedModel('AboutModel');
        $this->about = $this->AboutModel->listData();
        $this->load->sharedModel('InformasiModel');
        $this->informasi = $this->InformasiModel->listData();
        $this->load->sharedModel('ConfigurationModel');
        $this->configuration = $this->ConfigurationModel->listData();
        $this->load->sharedModel('RegulationModel');
        $this->regulation = $this->RegulationModel->listData();
        
        $this->webconfig = $this->config->item('webconfig');
        $this->module_name = $this->lang->line('forgot');
    }
    function index(){
        if($this->LoginModel->isLoggedIn()){
            redirect(base_url().'dashboard');
        }
        ## LOAD TEMPLATE ##
        $tdata = array();
        
        ## LOAD LAYOUT ##   
        $webconfig = $this->config->item('webconfig');
        
        require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
        if ($_POST) {

            $validator = new FormValidator();
            $validator->addValidation("email","req",$this->lang->line('error_empty_email_login'));
            if($validator->ValidateForm())
            {
                $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
                $new_password = '';
                for ($i = 0; $i < 10; $i++) {
                    $new_password .= $characters[rand(0, strlen($characters) - 1)];
                }
                
                $doUpdate = $this->LoginModel->changePass(array(
                                                                'email'=>$this->input->post('email')
                                                                ,'new_password'=>$new_password
                                                          ));
                if($doUpdate == 'empty'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
                }else if($doUpdate == 'failed'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
                }else if($doUpdate == 'email_notfound'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_email_notfound'));
                }else if($doUpdate == 'not_activate'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_not_activate'));
                }else if($doUpdate == 'failed_send'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_failed_send'));
                }else if($doUpdate == 'success'){
                    $tdata['success'] = $this->lang->line('msg_success_send_reset_password');
                }

                ## LOAD LAYOUT ##
                $ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
                $this->load->sharedView('template', $ldata);
            }else{
                $tdata['email'] = $this->input->post('email');
                $tdata['error_hash'] = $validator->GetErrors();

                ## LOAD LAYOUT ##
                $ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
                $this->load->sharedView('template', $ldata);
            }       
            
        }else{
            ## LOAD LAYOUT ##
            $ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }

    public function reset($key = '')
    {
        $tdata = $fdata = array();
        $this->pagename = "Reset Password";
        if($key == ''){
            redirect(base_url().'forgot');
        }

        $doValidate = $this->LoginModel->checkkey($key);
        if(count($doValidate) == '0'){
            redirect(base_url().'forgot');
        }
        
        if(isset($doValidate['vernumber']) && $doValidate['vernumber'] != $key){
            redirect(base_url().'forgot');
        }
        
        require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
        if($_POST)
        {
            
            $validator = new FormValidator();

            $validator->addValidation("password","req",$this->lang->line('error_empty_password_members'));
            $validator->addValidation("password","minlen=5",$this->lang->line('error_password_5_members'));
            $validator->addValidation("password_1","req",$this->lang->line('error_empty_repeat_password_members'));
            $validator->addValidation("password_1","eqelmnt=password",$this->lang->line('error_empty_retype_password_true_users'));
            
            if($validator->ValidateForm())
            {
                $id = $doValidate['id'];
                $vernumber = $doValidate['vernumber'];
                $doInsert = $this->LoginModel->resetPassword(array(
                                                                'id'=>$id
                                                                ,'password'=>$this->input->post('password')
                                                                ,'password_1'=>$this->input->post('password_1')
                                                          ));
                if($doInsert == 'empty'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
                }else if($doInsert == 'failed'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
                }else if($doInsert == 'notsame'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_notsame'));
                }else if($doInsert == 'success'){
                    $tdata['success'] = $this->lang->line('msg_success_update_password');
                    // redirect(base_url().'login');
                }

                ## LOAD LAYOUT ##
                $ldata['content'] = $this->load->view($this->router->class.'/reset',$tdata, true);
                $this->load->sharedView('template', $ldata);
            }
            else
            {
                $tdata['error_hash'] = $validator->GetErrors();

                ## LOAD LAYOUT ##
                $ldata['content'] = $this->load->view($this->router->class.'/reset',$tdata, true);
                $this->load->sharedView('template', $ldata);
            }
        }else{
            ## LOAD LAYOUT ##
            $ldata['content'] = $this->load->view($this->router->class.'/reset',$tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }

    public function success_reset()
    {
        $tdata = array();
        $this->pagename = "Succes Reset Password";
        ## LOAD LAYOUT ##
        $ldata['content'] = $this->load->view($this->router->class.'/success_reset',$tdata, true);
        $this->load->sharedView('login', $ldata);
    }
}