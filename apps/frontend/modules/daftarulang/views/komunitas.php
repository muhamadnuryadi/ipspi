<script type="text/javascript">
  $(document).ready(function() {
    $('.rows2').hide();
    $('.rows3').hide();

    $('.addform2').click(function () {
      $('.rows2').show();
      $('.addform2').hide();
    });
    $('.addform3').click(function () {
      $('.rows3').show();
      $('.addform3').hide();
    });
    $('.delform3').click(function () {
      $('.rows3').hide();
      $('.addform3').show();
    });
    $('.delform2').click(function () {
      $('.rows2').hide();
      $('.addform2').show();
    });
  });
</script>
<section class="register-step">
  <div class="container">
    <div class="row">
      <div class="col-12 text-center">
        <div class="heading mt-5">Registrasi</div>
        <div class="step-wrap">
          <div class="step-list">
            <div class="step-list-item">1</div>
            <div class="step-list-desc">Profil</div>
          </div>
          <div class="step-list ">
            <div class="step-list-item">2</div>
            <div class="step-list-desc">Pendidikan</div>
          </div>
          <div class="step-list ">
            <div class="step-list-item">3</div>
            <div class="step-list-desc">Pekerjaan</div>
          </div>
          <div class="step-list">
            <div class="step-list-item">4</div>
            <div class="step-list-desc">Praktek Peksos</div>
          </div>
          <div class="step-list">
            <div class="step-list-item">5</div>
            <div class="step-list-desc">Penghargaan</div>
          </div>
          <div class="step-list">
            <div class="step-list-item">6</div>
            <div class="step-list-desc">Sertifikasi</div>
          </div>
          <div class="step-list">
            <div class="step-list-item">7</div>
            <div class="step-list-desc">Referensi</div>
          </div>
          <div class="step-list ">
            <div class="step-list-item">8</div>
            <div class="step-list-desc">Organisasi Profesi</div>
          </div>
          <div class="step-list active">
            <div class="step-list-item">9</div>
            <div class="step-list-desc">Komunitas Internet</div>
          </div>
          <div class="step-list">
            <div class="step-list-item">10</div>
            <div class="step-list-desc">Resume Keanggotaan</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php //echo json_encode($this->session->userdata()); ?>
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <div class="content-wrapper p-5">
          <h3 class="text-center">KOMUNITAS INTERNET</h3>
          <hr class="line my-4">
          <form name="form" method="POST" action="<?php echo base_url().$this->router->class; ?>/komunitas" enctype="multipart/form-data" class="form-reg">

          <?php if(count($this->session->userdata('komunitas')) > '0'){ ?>
          <?php $i = 0; foreach ($this->session->userdata('komunitas') as $key => $value){ $i++;?>
            <div class="form-list"><?php echo $i;?></div>
            <div class="form-row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="formEducation">Nama<span>*</span></label>
                  <input type="text" class="form-control" id="formInstitution" name="nama[]" value="<?php echo $value['nama'];?>" >
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="formEducation">Alamat<span>*</span></label>
                  <input type="text" class="form-control" id="formInstitution" name="alamat[]" value="<?php echo $value['alamat'];?>" >
                </div>
              </div>
            </div>
            <a href="#" class="btn btn-red"><i class="far fa-trash-alt"></i> Hapus</a>
            <hr class="line my-4">
          <?php } ?>
          <?php } else { ?>
            <div class="form-list">1</div>
            <div class="form-row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="formEducation">Nama<span>*</span></label>
                  <input type="text" class="form-control" id="formInstitution" name="nama[]">
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="formEducation">Alamat<span>*</span></label>
                  <input type="text" class="form-control" id="formInstitution" name="alamat[]">
                </div>
              </div>
            </div>
           
            <hr class="line my-4">
            <div class="text-center">
              <a href="#" class="btn-txt addform2">+ Tambah Form Data Komunitas</a>
            </div>
            <hr class="line my-4">
            
            <div class="rows2">
              <div class="form-list">2</div>
              <div class="form-row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="formEducation">Nama</label>
                    <input type="text" class="form-control" id="formInstitution" name="nama[]">
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="formEducation">Alamat</label>
                    <input type="text" class="form-control" id="formInstitution" name="alamat[]">
                  </div>
                </div>
              </div>
              <a href="#" class="btn btn-red delform2"><i class="far fa-trash-alt"></i> Hapus</a>
              <div class="text-center">
                <a href="#" class="btn-txt addform3">+ Tambah Form Data Komunitas</a>
              </div>
              <hr class="line my-4">
            </div>
            
            <div class="rows3">
              <div class="form-list">3</div>
              <div class="form-row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="formEducation">Nama</label>
                    <input type="text" class="form-control" id="formInstitution" name="nama[]">
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="formEducation">Alamat</label>
                    <input type="text" class="form-control" id="formInstitution" name="alamat[]">
                  </div>
                </div>
              </div>
              <a href="#" class="btn btn-red delform3"><i class="far fa-trash-alt"></i> Hapus</a>
            </div>
            
            <?php } ?>
            
            
            <div class="addition-info">* Wajib diisi / required</div>
            <hr class="line my-3">
            <!-- <div class="text-center">
              <a href="#" class="btn-txt">+ Tambah Form Data Komunitas Internet</a>
            </div>
            <hr class="line mt-3 mb-5"> -->
            
            
            <div class="form-row">
              <div class="col-6">
                <div class="text-left">
                  <a href="<?php echo base_url(); ?>daftarulang/profesi" class="btn btn-lg btn-grey px-5 ">Kembali</a>
                </div>
              </div>
              <div class="col-6">
                <div class="text-right">
                  <button type="submit" class="btn btn-lg btn-green px-5 ">Lanjut</button>
                </div>
              </div>
            </div>
          </form>
      
        </div>
      </div>
    </div>
  </div>
</section>