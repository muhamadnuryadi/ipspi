<link href="<?php echo $this->webconfig['back_base_template']; ?>js/toastr/toastr.css" rel="stylesheet" type="text/css" />
<script src="<?php echo $this->webconfig['back_base_template']; ?>js/toastr/toastr.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
      $('#customFile1').change(function(){
          // console.log($(this)[0].files[0].name);
          $('.label1').text($(this)[0].files[0].name);
      })

      $('#customFile2').change(function(){
          // console.log($(this)[0].files[0].name);
          $('.label2').text($(this)[0].files[0].name);
      })

      $('#customFile3').change(function(){
          // console.log($(this)[0].files[0].name);
          $('.label3').text($(this)[0].files[0].name);
      })
  })

</script>
<section class="register-step">
  <div class="container">
    <div class="row">
      <div class="col-12 text-center">
        <div class="heading mt-5">Registrasi</div>
        <div class="step-wrap">
          <div class="step-list">
            <div class="step-list-item">1</div>
            <div class="step-list-desc">Profil</div>
          </div>
          <div class="step-list ">
            <div class="step-list-item">2</div>
            <div class="step-list-desc">Pendidikan</div>
          </div>
          <div class="step-list ">
            <div class="step-list-item">3</div>
            <div class="step-list-desc">Pekerjaan</div>
          </div>
          <div class="step-list">
            <div class="step-list-item">4</div>
            <div class="step-list-desc">Praktek Peksos</div>
          </div>
          <div class="step-list">
            <div class="step-list-item">5</div>
            <div class="step-list-desc">Penghargaan</div>
          </div>
          <div class="step-list">
            <div class="step-list-item">6</div>
            <div class="step-list-desc">Sertifikasi</div>
          </div>
          <div class="step-list">
            <div class="step-list-item">7</div>
            <div class="step-list-desc">Referensi</div>
          </div>
          <div class="step-list">
            <div class="step-list-item">8</div>
            <div class="step-list-desc">Organisasi Profesi</div>
          </div>
          <div class="step-list">
            <div class="step-list-item">9</div>
            <div class="step-list-desc">Komunitas Internet</div>
          </div>
          <div class="step-list active">
            <div class="step-list-item">10</div>
            <div class="step-list-desc">Resume Keanggotaan</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php //echo json_encode($this->session->userdata()); ?>
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <div class="content-wrapper p-5">
          <h3 class="text-center">RESUME KEANGOTAAN</h3>
          <hr class="line my-4">
          <form name="form" method="POST" action="<?php echo base_url().$this->router->class; ?>/resume" enctype="multipart/form-data" class="form-reg">
            <input type="hidden" name="idx">
            <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
                <?php foreach($error_hash as $inp_err){ ?>
                    <script type="text/javascript">
                    jQuery(document).ready(function($) {
                        toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                        });
                    </script>
                <?php } ?>
            <?php } ?>
            <div class="head-table">1. Profil</div>
              <table class="table table-sm table-form">
                <tbody>
                  <tr>
                    <th style="width: 30%">Nama Lengkap</th>
                    <td style="width: 70%"><?php echo $this->session->userdata('nama');?> <?php echo $this->session->userdata('gelar');?></td>
                  </tr>
                  <tr>
                    <th>Jenis Kelamin</th>
                    <td><?php echo gendertext($this->session->userdata('jk'));?></td>
                  </tr>
                  <tr>
                    <th>Tempat Lahir</th>
                    <td><?php echo $this->session->userdata('tmptlahir');?></td>
                  </tr>
                  <tr>
                    <th>Tanggal Lahir</th>
                    <td><?php echo $this->session->userdata('tgllahir');?></td>
                  </tr>
                  <tr>
                    <th>Status Perkawinan</th>
                    <td><?php echo perkawinantext($this->session->userdata('statuskawin'));?></td>
                  </tr>
                  <tr>
                    <th>Jumlah Anak</th>
                    <td><?php echo $this->session->userdata('jumanak');?></td>
                  </tr>
                  <tr>
                    <th>No KTP / NIK</th>
                    <td><?php echo $this->session->userdata('ktp');?></td>
                  </tr>
                  <tr>
                    <th>Alamat Kantor</th>
                    <td><?php echo $this->session->userdata('alamatkantor');?></td>
                  </tr>
                  <tr>
                    <th>Telepon Kantor</th>
                    <td><?php echo $this->session->userdata('telpkantor');?></td>
                  </tr>
                  <tr>
                    <th>Fax Kantor</th>
                    <td><?php echo $this->session->userdata('faxkantor');?></td>
                  </tr>
                  <tr>
                    <th>Alamat Rumah</th>
                    <td><?php echo $this->session->userdata('alamatrumah');?></td>
                  </tr>
                  <tr>
                    <th>Kabupaten / Kota</th>
                    <td><?php echo getwilayah($this->session->userdata('id_kota'));?></td>
                  </tr>
                  <tr>
                    <th>Provinsi</th>
                    <td><?php echo getwilayah($this->session->userdata('id_propinsi'));?></td>
                  </tr>
                  <tr>
                    <th>Telepon</th>
                    <td><?php echo $this->session->userdata('telp');?></td>
                  </tr>
                  <tr>
                    <th>No. Handphone</th>
                    <td><?php echo $this->session->userdata('hp');?></td>
                  </tr>
                </tbody>
              </table>
              
              <div class="head-table">2. Pendidikan</div>
              <?php if(count($this->session->userdata('pendidikan')) > '0'){ ?>
              <?php $i = 0; foreach ($this->session->userdata('pendidikan') as $key => $value){ $i++;?>
                <?php if($value['tingkat'] != '' || $value['pt'] != '' || $value['jurusan'] != '' || $value['gelar'] != '' || $value['thn_lulus'] != ''){ ?>
                  <table class="table table-sm table-form">
                    <thead>
                      <tr>
                        <th colspan="2">Pendidikan <?php echo $i;?></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th style="width: 30%">Tingkat Pendidikan</th>
                        <td style="width: 70%">
                          <?php 
                          if (isset($tingkat) && count($tingkat) > 0) {
                              foreach ($tingkat as $key2 => $value2) {
                                  if ($value['tingkat'] == $value2['id']) {
                                      echo $value2['name'];
                                  }
                              }
                          }
                          
                          ?>
                        </td>
                      </tr>

                      <tr>
                        <th style="width: 30%">Jenis Pendidikan</th>
                        <td style="width: 70%">
                          <?php 
                          foreach ($this->webconfig['pendidikan_jenis'] as $key2 => $value2) {
                              if ($value['jenis_pendidikan'] == $key2) {
                                  echo $value2;
                              }
                          }
                          ?>
                        </td>
                      </tr>

                      <tr>
                        <th>Nama Perguruan Tinggi</th>
                        <td><?php echo $value['pt'];?></td>
                      </tr>
                      <tr>
                        <th>Jurusan</th>
                        <td><?php echo $value['jurusan'];?></td>
                      </tr>
                      <tr>
                        <th>Gelar</th>
                        <td><?php echo $value['gelar'];?></td>
                      </tr>
                      <tr>
                        <th>Tahun Lulus</th>
                        <td><?php echo $value['thn_lulus'];?></td>
                      </tr>
                    </tbody>
                  </table>
                <?php }?>
              <?php } ?>
              <?php } ?>
              
              <div class="head-table">3. Pekerjaan</div>
              <?php if(count($this->session->userdata('pekerjaan')) > '0'){ ?>
              <?php $i = 0; foreach ($this->session->userdata('pekerjaan') as $key => $value){ $i++;?>
                <?php if($value['nama'] != '' || $value['jabatan'] != '' || $value['mulai'] != '' || $value['akhir'] != ''){ ?>
                  <table class="table table-sm table-form">
                    <thead>
                      <tr>
                        <th colspan="2">Pekerjaan <?php echo $i;?></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th style="width: 30%">Jenis Pekerjaan</th>
                        <td style="width: 70%">
                            <?php 
                            if (isset($jenis_pekerjaan) && count($jenis_pekerjaan) > 0) {
                                foreach ($jenis_pekerjaan as $key2 => $value2) {
                                    if ($value['jenis_pekerjaan'] == $value2['id']) {
                                        echo $value2['name'];
                                    }
                                }
                            }
                            ?>
                        </td>
                      </tr>

                      <tr>
                        <th style="width: 30%">Jenis Instansi</th>
                        <td style="width: 70%">
                            <?php 
                            if (isset($jenis_instansi) && count($jenis_instansi) > 0) {
                                foreach ($jenis_instansi as $key2 => $value2) {
                                    if ($value['jenis_instansi'] == $value2['id']) {
                                        echo $value2['name'];
                                    }
                                }
                            }
                            ?>
                        </td>
                      </tr>

                      <tr>
                        <th style="width: 30%">Nama Instansi</th>
                        <td style="width: 70%"><?php echo $value['nama'];?></td>
                      </tr>
                      <tr>
                        <th>Jabatan</th>
                        <td><?php echo $value['jabatan'];?></td>
                      </tr>
                      <tr>
                        <th>Mulai Tahun</th>
                        <td><?php echo $value['mulai'];?></td>
                      </tr>
                      <tr>
                        <th>Berakhir Tahun</th>
                        <td><?php echo $value['akhir'];?></td>
                      </tr>
                    </tbody>
                  </table>
                <?php } ?>
              <?php } ?>
              <?php } ?>
              
              <div class="head-table">4. Praktek Peksos</div>
              <?php if(count($this->session->userdata('praktek')) > '0'){ ?>
              <?php $i = 0; foreach ($this->session->userdata('praktek') as $key => $value){ $i++;?>
                <?php if($value['setting'] != '' || $value['tahun'] != '' || $value['jabatan'] != '' || $value['peran'] != '' || $value['keahlian'] != ''){ ?>
                  <table class="table table-sm table-form">
                    <thead>
                      <tr>
                        <th colspan="2">Praktek <?php echo $i;?></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th style="width: 30%">Bidang/Setting Praktik</th>
                        <td style="width: 70%">
                          <?php 
                          foreach ($this->webconfig['bidang_setting'] as $key2 => $value2) {
                              if ($value['setting'] == $key2) {
                                  echo $value2;
                              }
                          }
                          // echo $value['setting'];
                          ?>
                        </td>
                      </tr>
                      <tr>
                        <th>Tahun</th>
                        <td><?php echo $value['tahun'];?></td>
                      </tr>
                      <tr>
                        <th>Jabatan/Posisi</th>
                        <td><?php echo $value['jabatan'];?></td>
                      </tr>
                      <tr>
                        <th>Peran Utama</th>
                        <td>
                            <?php 
                            foreach ($this->webconfig['peran_utama'] as $key2 => $value2) {
                                if ($value['peran'] == $key2) {
                                    echo $value2;
                                }
                            }
                            
                            ?>
                        </td>
                      </tr>
                      <tr>
                        <th>Jenis Keahlian</th>
                        <td>
                            <?php 
                            foreach ($this->webconfig['jenis_keahlian'] as $key2 => $value2) {
                                if ($value['keahlian'] == $key2) {
                                    echo $value2;
                                }
                            }
                            
                            ?>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                <?php } ?>
              <?php } ?>
              <?php } ?>
              
              <div class="head-table">5. Penghargaan</div>
              <?php if(count($this->session->userdata('penghargaan')) > '0'){ ?>
              <?php $i = 0; foreach ($this->session->userdata('penghargaan') as $key => $value){ $i++;?>
                <?php if($value['nama'] != '' || $value['instansi'] != '' || $value['tahun'] != ''){ ?>
                  <table class="table table-sm table-form">
                    <thead>
                      <tr>
                        <th colspan="2">Penghargaan <?php echo $i;?></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th style="width: 30%">Nama Penghargaan</th>
                        <td style="width: 70%"><?php echo $value['nama'];?></td>
                      </tr>
                      <tr>
                        <th>Instansi Pemberi</th>
                        <td><?php echo $value['instansi'];?></td>
                      </tr>
                      <tr>
                        <th>Tahun</th>
                        <td><?php echo $value['tahun'];?></td>
                      </tr>
                    </tbody>
                  </table>
                <?php } ?>
              <?php } ?>
              <?php } ?>
              
              <div class="head-table">6. Sertifikasi</div>
              <?php if(count($this->session->userdata('sertifikasi')) > '0'){ ?>
              <?php $i = 0; foreach ($this->session->userdata('sertifikasi') as $key => $value){ $i++;?>
                <?php if($value['nama'] != '' || $value['lembaga'] != '' || $value['tahun'] != ''){ ?>
                  <table class="table table-sm table-form">
                    <thead>
                      <tr>
                        <th colspan="2">Sertifikasi <?php echo $i;?></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th style="width: 30%">Nama Sertifikasi</th>
                        <td style="width: 70%"><?php echo $value['nama'];?></td>
                      </tr>

                      <tr>
                        <th style="width: 30%">Jenis Sertifikasi</th>
                        <td style="width: 70%">
                          <?php 
                            foreach ($this->webconfig['sertifikasi_tipe'] as $key2 => $value2) {
                                if ($value['jenis_sertifikasi'] == $key2) {
                                    echo $value2;
                                }
                            }
                            ?>
                        </td>
                      </tr>
                      <tr>
                        <th>Lembaga yang Mengeluarkan</th>
                        <td><?php echo $value['lembaga'];?></td>
                      </tr>
                      <tr>
                        <th>Tahun</th>
                        <td><?php echo $value['tahun'];?></td>
                      </tr>
                    </tbody>
                  </table>
                <?php } ?>
              <?php } ?>
              <?php } ?>
              
              <div class="head-table">7. Referensi</div>
              <?php if(count($this->session->userdata('referensi')) > '0'){ ?>
              <?php $i = 0; foreach ($this->session->userdata('referensi') as $key => $value){ $i++;?>
                <?php if($value['nama'] != '' || $value['jabatan'] != '' || $value['telp'] != ''){ ?>
                  <table class="table table-sm table-form">
                    <thead>
                      <tr>
                        <th colspan="2">Referensi <?php echo $i;?></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th style="width: 30%">Nama</th>
                        <td style="width: 70%"><?php echo $value['nama'];?></td>
                      </tr>
                      <tr>
                        <th>Jabatan</th>
                        <td><?php echo $value['jabatan'];?></td>
                      </tr>
                      <tr>
                        <th>No. Telepon</th>
                        <td><?php echo $value['telp'];?></td>
                      </tr>
                    </tbody>
                  </table>
                <?php } ?>
              <?php } ?>
              <?php } ?>
              
              <div class="head-table">8. Organisasi Profesi</div>
              <?php if(count($this->session->userdata('profesi')) > '0'){ ?>
              <?php $i = 0; foreach ($this->session->userdata('profesi') as $key => $value){ $i++;?>
                <?php if($value['nama'] != '' || $value['tahun'] != '' || $value['keterangan'] != ''){ ?>
                  <table class="table table-sm table-form">
                    <thead>
                      <tr>
                        <th colspan="2">Organisasi <?php echo $i;?></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th style="width: 30%">Nama Organisasi Profesi</th>
                        <td style="width: 70%"><?php echo $value['nama'];?></td>
                      </tr>
                      <tr>
                        <th>Tahun Bergabung</th>
                        <td><?php echo $value['tahun'];?></td>
                      </tr>
                      <tr>
                        <th>Keterangan</th>
                        <td><?php echo $value['keterangan'];?></td>
                      </tr>
                    </tbody>
                  </table>
                <?php } ?>
              <?php } ?>
              <?php } ?>
              
              <div class="head-table">9. Komunitas Internet</div>
              <?php if(count($this->session->userdata('komunitas')) > '0'){ ?>
              <?php $i = 0; foreach ($this->session->userdata('komunitas') as $key => $value){ $i++;?>
                <?php if($value['nama'] != '' || $value['alamat'] != ''){ ?>
                  <table class="table table-sm table-form">
                    <thead>
                      <tr>
                        <th colspan="2">Komunitas <?php echo $i;?></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th style="width: 30%">Nama Komunitas</th>
                        <td style="width: 70%"><?php echo $value['nama'];?></td>
                      </tr>
                      <tr>
                        <th>Alamat</th>
                        <td><?php echo $value['alamat'];?></td>
                      </tr>
                    </tbody>
                  </table>
                <?php } ?>
              <?php } ?>
              <?php } ?>

              <div class="head-table" style="margin-bottom:12px; ">Upload File</div>
              <div class="form-group">
                <label>Upload Foto / Scan KTP<span>*</span></label>
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFile1" name="path_ktp" value="<?php echo isset($path_ktp)?$path_ktp:''; ?>" required>
                  <label class="custom-file-label label1" for="customFile1">Upload File</label>
                </div>
                <div class="mt-3"><small>Maksimum file 1Mb</small></div>
              </div>
              <div class="form-group">
                <label>Upload Pas Foto<span>*</span></label>
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFile2" name="foto" value="<?php echo isset($foto)?$foto:''; ?>" required>
                  <label class="custom-file-label label2" for="customFile2">Upload File</label>
                </div>
                <div class="mt-3"><small>Foto formal dengan background biru seukuran pas foto. Maksimum file 1Mb</small></div>
              </div>

              <div class="form-group">
                <label>Upload Ijazah<span>*</span></label>
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFile3" name="path_ijazah" value="<?php echo isset($path_ijazah)?$path_ijazah:''; ?>" required>
                  <label class="custom-file-label label3" for="customFile3">Upload File</label>
                </div>
                <!-- <div class="mt-3"><small>Foto formal dengan background biru seukuran pas foto. Maksimum file 1Mb</small></div> -->
              </div>
              
              <div class="form-check text-center mt-5">
                <input class="form-check-input" type="checkbox" value="1" id="defaultCheck1" name="faq">
                <label class="form-check-label" for="defaultCheck1" style="font-size: 13px;">
                  Dengan menekan tombol daftar, saya menyetujui <a href="kebijakan-privasi.html" target="_blank">Kebijakan</a> dan <a href="ketentuan-layanan.html" target="_blank">Ketentuan</a> IPSPI
                </label>
              </div>


            <hr class="line mt-4 mb-5">
            
            
            <div class="form-row">
              <div class="col-6">
                <div class="text-left">
                  <a href="<?php echo base_url(); ?>daftarulang/komunitas" class="btn btn-lg btn-grey px-5 ">Kembali</a>
                </div>
              </div>
              <div class="col-6">
                <div class="text-right">
                  <button type="submit" class="btn btn-lg btn-green px-5 ">Daftar</button>
                </div>
              </div>
            </div>
          </form>
      
        </div>
      </div>
    </div>
  </div>
</section>