<link href="<?php echo $this->webconfig['back_base_template']; ?>js/toastr/toastr.css" rel="stylesheet" type="text/css" />
<script src="<?php echo $this->webconfig['back_base_template']; ?>js/toastr/toastr.js" type="text/javascript"></script>
<section class="head-wrap">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1 class="head-title">Kontak</h1>
      </div>
    </div>
  </div>
</section>
<?php if(isset($error_hash) && count($error_hash) > 0){ ?>
    <?php foreach($error_hash as $inp_err){ ?>
        <script type="text/javascript">
        jQuery(document).ready(function($) {
            toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
            });
        </script>
    <?php } ?>
<?php } ?>
<?php if(isset($success)){ ?>
    <script type="text/javascript">
    jQuery(document).ready(function($) {
        toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
        });
    </script>
<?php } ?>
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <div class="mt-4"></div>
        <h4>IKATAN PEKERJA SOSIAL PROFESIONAL INDONESIA</h4>
        <table class="table table-borderless contact-info">
          <tr>
            <td width="100px"><strong>Alamat</strong></td>
            <td><strong>:</strong></td>
            <td><?php echo isset($this->configuration['address'])?$this->configuration['address']:'';?></td>
          </tr>
          <tr>
            <td><strong>Email</strong></td>
            <td><strong>:</strong></td>
            <td><?php echo isset($this->configuration['email_contact'])?$this->configuration['email_contact']:'';?></td>
          </tr>
          <tr>
            <td><strong>Telepon</strong></td>
            <td><strong>:</strong></td>
            <td><?php echo isset($this->configuration['phone'])?$this->configuration['phone']:'';?></td>
          </tr>
          <tr>
            <td><strong>Telepon 2</strong></td>
            <td><strong>:</strong></td>
            <td><?php echo isset($this->configuration['phone_2'])?$this->configuration['phone_2']:'';?></td>
          </tr>
          <tr>
            <td><strong>Fax</strong></td>
            <td><strong>:</strong></td>
            <td><?php echo isset($this->configuration['fax'])?$this->configuration['fax']:'';?></td>
          </tr>
        </table>
        <h4>PETA LOKASI</h4>
        <div class="maps mb-5">
          <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15865.732664881656!2d106.858078!3d-6.206454!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6112ffbcdfbc150d!2sIkatan+Pekerja+Sosial+Profesional+Indonesia+(IPSPI)!5e0!3m2!1sen!2sid!4v1538016418206" width="100%" height="299" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="content-wrapper p-5">
          <div class="text-center">
            <div class="heading">Kontak Kami</div>
          </div>
          <form method="post" action="<?php echo base_url(); ?>contact">
            <div class="form-group">
              <label for="formNama">Nama</label>
              <input type="text" class="form-control" id="formNama" name="name" required>
            </div>
            <div class="form-group">
              <label for="formEmail">Email</label>
              <input type="email" class="form-control" id="formEmail" name="email" required>
            </div>
            <div class="form-group">
              <label for="formSubjek">Subjek</label>
              <input type="text" class="form-control" id="formSubjek" name="subject" required>
            </div>
            <div class="form-group">
              <label for="formPesan">Pesan</label>
              <textarea class="form-control" id="formPesan" rows="5" name="message" required></textarea>
            </div>
            <div class="text-center">
              <button type="submit" class="btn btn-lg btn-green px-5 ">Kirim</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>