<?php
class Contact extends CI_Controller{
    var $webconfig;
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->sharedModel('ContactModel');

        $this->load->sharedModel('AboutModel');
        $this->about = $this->AboutModel->listData();
        $this->load->sharedModel('InformasiModel');
        $this->informasi = $this->InformasiModel->listData();
        $this->load->sharedModel('ConfigurationModel');
        $this->configuration = $this->ConfigurationModel->listData();
        $this->load->sharedModel('RegulationModel');
        $this->regulation = $this->RegulationModel->listData();
        
        $this->webconfig = $this->config->item('webconfig');
        $this->module_name = $this->lang->line('home');
    }
    function index(){
        $tdata = array();
        
        if($_POST){
            $doInsert = $this->ContactModel->entriData(array(
                                                            'name'=>$this->input->post('name')
                                                            ,'email'=>$this->input->post('email')
                                                            ,'subject'=>$this->input->post('subject')
                                                            ,'message'=>$this->input->post('message')
                                                      ));

            if($doInsert == 'empty'){
                $tdata['error_hash'] = array('error' => $this->lang->line('error_empty_form'));
            }else if($doInsert == 'failed'){
                $tdata['error_hash'] = array('error' => $this->lang->line('error_system'));
            }else if($doInsert == 'success'){
                $tdata['success'] = $this->lang->line('message_success');
            }
            if($doInsert !='success'){
                $tdata['name']     = $this->input->post('name');
                $tdata['email']     = $this->input->post('email');
                $tdata['subject']     = $this->input->post('subject');
                $tdata['message']     = $this->input->post('message');
            }
            $ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
            $this->load->sharedView('template', $ldata);
            
        }else{
            ## LOAD LAYOUT ##
            $ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }
}