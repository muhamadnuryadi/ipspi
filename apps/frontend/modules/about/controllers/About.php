<?php
class About extends CI_Controller{
    var $webconfig;
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->sharedModel('BeritaModel');

        $this->load->sharedModel('AboutModel');
        $this->about = $this->AboutModel->listData();
        $this->load->sharedModel('InformasiModel');
        $this->informasi = $this->InformasiModel->listData();
        $this->load->sharedModel('ConfigurationModel');
        $this->configuration = $this->ConfigurationModel->listData();
        $this->load->sharedModel('RegulationModel');
        $this->regulation = $this->RegulationModel->listData();
        
        $this->webconfig = $this->config->item('webconfig');
        $this->module_name = $this->lang->line('about');
    }
    function index(){
        $tdata = array();
        $tdata['lists'] = $this->BeritaModel->listData(array(
                                                                'limit' => 6
                                                            ));
        $ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
        $this->load->sharedView('template', $ldata);
    }
    
    function read($id = 0){
        $tdata = array();
        $tdata['lists'] = $this->AboutModel->listData(array('id' => $id));
        
        $ldata['content'] = $this->load->view($this->router->class.'/detail',$tdata, true);
        $this->load->sharedView('template', $ldata);
    }
}