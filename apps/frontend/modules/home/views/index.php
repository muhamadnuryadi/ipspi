  <script type="text/javascript">
function unduhFile(id) {
    $.ajax({
        url: '<?php echo base_url(); ?>downloads/checkFile/'+id,
        dataType: 'json',
        success: function(data) {
            if(data.is_exist == true) {
                window.open('<?php echo base_url(); ?>downloads/files/'+id,'_self');
            } else {
                alert('<?php echo $this->lang->line('file_not_exist'); ?>');
            }
        }
    }); return false;
}
function searchThis(){
  var frm = document.searchform;
  var name = frm.name.value;
  if(name == ''){ name = '-'; }
  window.location = "<?php echo base_url(); ?>berita/searchdata/"+encode_js(name.replace(/\s/g,'%20').trim());
}
</script>
  <section class="ipspi-featured">
    <div class="row no-gutters">
      <div class="col-md-6">
        <div class="slider slider-for">
          <?php if(isset($listsfeatured) && count($listsfeatured) > 0) {?>
          <?php foreach ($listsfeatured as $key => $listdata) { ?>
            <div>
              <div class="featured-item feature-big">
                <a href="<?php echo base_url().'berita/read/'.$listdata['id'].'/'.$listdata['slug']; ?>">
                  <img src="<?php echo thumb_image($listdata['path'],'658x496', 'berita'); ?>" alt="News" class="img-fluid">
                  <div class="featured-overlay">
                    <div class="featured-meta">
                      <div class="ft-date"><i class="far fa-clock"></i><?php echo $listdata['datepublish'];?></div>
                      <div class="ft-author"><i class="far fa-user"></i>IPSPI</div>
                      <div class="ft-title head1"><?php echo $listdata['title'];?></div>
                      <p><?php echo $listdata['description'];?></p>
                      <button href="berita-detail.html" class="btn btn-border">Selengkapnya</button>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          <?php } ?>
          <?php } ?>
        </div>
      </div>
      <div class="col-md-6">
        <div class="row no-gutters">
          <div class="col-sm-12">
            <div class="slider feature-nav">
              <?php if(isset($listsfeatured) && count($listsfeatured) > 0) {?>
              <?php foreach ($listsfeatured as $key => $listdata) { ?>
               <?php
                  $classed = '';
                  $dataid = '';
                  if ($key == 0) { 
                    $classed = "nav-current";
                  }else{
                    $dataid = 'data-id='.$key;
                  }
               ?>
              <div class="feature-item-nav <?php echo $classed;?>" <?php echo $dataid;?>>  
                <div class="meta-nav">
                  <div class="d-flex flex-row">
                      <div class="bd-highlight"><div class="number"><?php echo $key+1;?></div></div>
                      <div class="bd-highlight">
                        <div class="ft-date"><i class="far fa-clock"></i><?php echo $listdata['datepublish'];?></div>
                          <div class="ft-author"><i class="far fa-user"></i>IPSPI</div>
                          <div class="ft-head"><?php echo $listdata['title'];?></div>
                      </div>
                  </div>
                </div>
              </div>
              <?php } ?>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="ipspi-search">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <form id="searchform" name="searchform" class="form-inline form-search" action="" method="post" onsubmit="searchThis();return false;">
            <input class="form-control search-large" name="name" type="search" placeholder="Pencarian.." aria-label="Search">
            <button class="btn btn-search" onclick="searchThis();return false;" type="submit">CARI</button>
            <input type="submit" style="display:none;">
          </form>
        </div>
      </div>
    </div>
  </section>
  
  <section class="ipspi-latest-news">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="announcement-box">
            <div class="row">
              <div class="col-md-9">
                <h2>Pengumuman</h2>
                <p>Pengumuman untuk seluruh <strong>Anggota IPSPI</strong> yang sudah terdaftar sebagai anggota namun <strong>tidak bisa login</strong>, diharapkan untuk melakukan <strong>registasi ulang</strong> dengan mengklik tombol berikut</p>
              </div>
              <div class="col-md-3 d-flex justify-content-center align-items-center">
                <a href="<?php echo base_url().'daftarulang'?>" class="btn btn-lg btn-green my-3">Registrasi Ulang</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="text-center">
       <div class="heading">Berita Terbaru</div>
      </div>
      <div class="row">
        <?php if(isset($lists) && count($lists) > 0) {?>
        <?php foreach ($lists as $listdata) { ?>
        <div class="col-sm-4">
          <div class="article-item" style="height: 458px;">
            <a href="<?php echo base_url().'berita/read/'.$listdata['id'].'/'.$listdata['slug']; ?>"><img src="<?php echo thumb_image($listdata['path'],'350x263', 'berita'); ?>" alt="News 04" class="img-fluid img-article"></a>
            <div class="article-meta">
              <div class="ar-date"><i class="far fa-clock"></i><?php echo $listdata['datepublish'];?></div>
              <div class="ar-author"><i class="far fa-user"></i>IPSPI</div>
              <div class="ar-title head3"><a href="<?php echo base_url().'berita/read/'.$listdata['id'].'/'.$listdata['slug']; ?>"><?php echo $listdata['title'];?></a></div>
              <div class="ar-desc"><?php echo cutting_words(stripcslashes($listdata['description']), 100);?></div>
            </div>
          </div>
        </div>
        <?php } ?>
        <?php } ?>
        
      </div>
      <div class="row">

      </div>
      <div class="text-center">
        <a href="<?php echo base_url(); ?>berita" class="btn btn-lg btn-blue">Lihat Semua</a>
      </div>
    </div>
  </section>

  <section>
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="text-center">
              <div class="heading">Mitra Kami</div>
            </div>
            <div class="mitra">
              <div class="mitra-item d-flex align-items-center justify-content-center"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/mitra/mitra1.png" alt="Mitra 1" /></div>
              <div class="mitra-item d-flex align-items-center justify-content-center"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/mitra/mitra2.png" alt="Mitra 2" /></div>
              <div class="mitra-item d-flex align-items-center justify-content-center"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/mitra/mitra3.jpg" alt="Mitra 3" /></div>
              <div class="mitra-item d-flex align-items-center justify-content-center"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/mitra/mitra4.png" alt="Mitra 4" /></div>
              <div class="mitra-item d-flex align-items-center justify-content-center"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/mitra/mitra5.png" alt="Mitra 5" /></div>
              <div class="mitra-item d-flex align-items-center justify-content-center"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/mitra/mitra6.jpg" alt="Mitra 6" /></div>
              <div class="mitra-item d-flex align-items-center justify-content-center"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/mitra/mitra7.png" alt="Mitra 7" /></div>
              <div class="mitra-item d-flex align-items-center justify-content-center"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/mitra/mitra8.png" alt="Mitra 8" /></div>
              <div class="mitra-item d-flex align-items-center justify-content-center"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/mitra/mitra9.jpg" alt="Mitra 9" /></div>
            </div>
          </div>
        </div>
      </div>
  </section>

  <section class="ipspi-info">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="info-wrap">
            <div class="info-title">Kebijakan IPSPI</div>
            <ul class="info-list">
              <?php if(isset($listkebijakan) && count($listkebijakan) > 0) {?>
              <?php foreach ($listkebijakan as $key => $listdata) { ?>
              <li><a href="javascript:void(0)" onclick="unduhFile(<?php echo $listdata['id']; ?>)"><i class="far fa-file"></i><?php echo $listdata['name'];?></a></li>
              <?php } ?>
              <?php } ?>
            </ul>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="info-wrap">
            <div class="info-title">Unduh Materi</div>
            <ul class="info-list">
              <?php if(isset($listmateri) && count($listmateri) > 0) {?>
              <?php foreach ($listmateri as $key => $listdata) { ?>
              <li><a href="javascript:void(0)" onclick="unduhFile(<?php echo $listdata['id']; ?>)"><i class="far fa-file"></i><?php echo $listdata['name'];?></a></li>
              <?php } ?>
              <?php } ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>