<script type="text/javascript">
  jQuery(document).ready(function($) {
    jQuery("#listData").load('<?php echo base_url().$this->router->class; ?>/getAllData/<?php echo $id_provinsi;?>');
  });
  function searchThis(){
    var frm = document.searchform;
    var name = frm.name.value;
    if(name == ''){ name = '-'; }
    jQuery("#listData").load("<?php echo base_url().$this->router->class; ?>/searchdata/"+encode_js(name.replace(/\s/g,'%20')));
  }
</script>
<section class="head-wrap text-left">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="dpd-profile">
          <div class="dpd-profile-image"><img src="<?php echo thumb_image($wilayah[0]['files'],'304x300', 'dpd'); ?>" alt="IPSPI DPD Jawa Barat"></div>
          <div class="dpd-profile-name">IPSPI DPD <?php echo isset($wilayah[0]['nama'])?$wilayah[0]['nama']:'';?></div>
        </div>
      </div>
      <div class="col-md-6">
        <table class="table table-borderless table-heading">
          <tr>
            <td><strong>Ketua</strong></td>
            <td><?php echo isset($wilayah[0]['ketua'])?$wilayah[0]['ketua']:'';?></td>
          </tr>
          <tr>
            <td><strong>Alamat</strong></td>
            <td><?php echo isset($wilayah[0]['alamat'])?$wilayah[0]['alamat']:'';?></td>
          </tr>
          <tr>
            <td><strong>Email</strong></td>
            <td><?php echo isset($wilayah[0]['email'])?$wilayah[0]['email']:'';?></td>
          </tr>
          <tr>
            <td><strong>Telepon</strong></td>
            <td><?php echo isset($wilayah[0]['telp'])?$wilayah[0]['telp']:'';?></td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</section>

<section class="ipspi-search">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <form class="form-inline form-search">
          <input class="form-control search-large" type="search" placeholder="Pencarian.." aria-label="Search">
          <button class="btn btn-search" type="submit">CARI</button>
        </form>
      </div>
    </div>
  </div>
</section>

<div id="listData">
  <center>
    <img src='<?php echo $this->webconfig['back_images']; ?>ajax_loading.gif' align='middle' style="margin:5px;" />
  </center>
</div>
