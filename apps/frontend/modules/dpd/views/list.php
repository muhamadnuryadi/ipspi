<section class="ipspi-latest-news">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
      <?php if(isset($lists) && count($lists) > 0) {?>
      <?php $i = $start_no; foreach ($lists as $key => $listdata) { $i++; ?>
        
          <?php if($key == 0) {?>
          <div class="row">
            <div class="col-12">
              <div class="featured-item featured-news">
                <a href="<?php echo base_url().'berita/read/'.$listdata['id'].'/'.$listdata['slug']; ?>">
                  <img src="<?php echo thumb_image($listdata['path'],'658x496', 'berita'); ?>" alt="News 06" class="img-fluid">
                  <div class="featured-overlay">
                    <div class="featured-meta">
                      <div class="ft-date"><i class="far fa-clock"></i><?php echo $listdata['datepublish'];?></div>
                      <div class="ft-author"><i class="far fa-user"></i>IPSPI</div>
                      <div class="ft-title head2"><?php echo $listdata['title'];?></div>
                      <div class="ft-desc"><?php echo stripcslashes($listdata['description']);?></div>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
        <?php } else { ?>

          <div class="row">
            <div class="col-sm-6">
              <div class="article-item">
                <a href="<?php echo base_url().'berita/read/'.$listdata['id'].'/'.$listdata['slug']; ?>"><img src="<?php echo thumb_image($listdata['path'],'350x263', 'berita'); ?>" alt="News" class="img-fluid img-article"></a>
                <div class="article-meta">
                  <div class="ar-date"><i class="far fa-clock"></i><?php echo $listdata['datepublish'];?></div>
                  <div class="ar-author"><i class="far fa-user"></i>IPSPI</div>
                  <div class="ar-title head3"><a href="<?php echo base_url().'berita/read/'.$listdata['id'].'/'.$listdata['slug']; ?>"><?php echo $listdata['title'];?></a></div>
                  <div class="ar-desc"><?php echo stripcslashes($listdata['description']);?></div>
                </div>
              </div>
            </div>
          </div>

      <?php } ?>
      <?php } ?>

      <?php } else{ ?>
        <div class="col-sm-12">
          <center>Tidak Ada Data</center>
        </div>
      <?php } ?>
        <div class="mb-5"></div>
        </div>
        <div class="col-lg-4">
          <div class="widget-container">
            <div class="widget-title">Daftar DPD IPSPI</div>
            <div class="widget-wrap">
              <ul class="item-list">
                <?php foreach ($this->DpdModel->listData() as $key => $value) { ?>
                  <li><a href="<?php echo base_url(); ?>dpd/wilayah/<?php echo $value['id'];?>">Provinsi <?php echo $value['nama'];?></a></li>
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
      
    </div>
    
    <nav aria-label="page">
      <ul class="pagination justify-content-center">
        <?php echo isset($pagination)?$pagination:""; ?>
        <!-- <li class="page-item disabled">
          <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
          </a>
        </li>
        <li class="page-item active"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item"><a class="page-link" href="#">100</a></li>
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
          </a>
        </li> -->
      </ul>
    </nav>
    
  </div>
</section>