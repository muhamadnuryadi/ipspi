<section class="ipspi-search">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <form class="form-inline form-search">
          <input class="form-control search-large" type="search" placeholder="Pencarian.." aria-label="Search">
          <button class="btn btn-search" type="submit">CARI</button>
        </form>
      </div>
    </div>
  </div>
</section>

<section class="article-detail">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <div class="article-meta">
            <div class="ar-date"><i class="far fa-clock"></i><?php echo $lists[0]['datepublish'];?></div>
            <div class="ar-author"><i class="far fa-user"></i>IPSPI</div>
        </div>
        <h1><?php echo stripcslashes($lists[0]['title']);?></h1>
        <div class="article-detail-img">
          <img src="<?php echo thumb_image($lists[0]['path'],'658x496', 'berita'); ?>" alt="Article Image" class="img-fluid">
        </div>
        <article>
          <?php echo stripcslashes($lists[0]['body']);?>
        </article>

        <div class="related-article">
          <div class="row">
            <div class="col-12">
              <div class="heading-alt">Berita Lainnya</div>
            </div>
          </div>
          <!-- <div class="row">
            <div class="col-sm-4">
              <div class="related-list">
                <a href="berita-detail.html">
                  <img src="images/news-05.jpg" alt="News 05" class="img-fluid">
                  <div class="related-meta-date">21 Februari 2018</div>
                  <div class="related-meta-title">Menteri Sosial RI Dukung RUU Praktik Pekerjaan Sosial</div>
                </a>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="related-list">
                <a href="berita-detail.html">
                  <img src="images/news-07.jpg" alt="News 07" class="img-fluid">
                  <div class="related-meta-date">31 Januari 2018</div>
                  <div class="related-meta-title">Kunjungan Kerja Komisi VIII DPR RI diterima DPD IPSPI Jayapura</div>
                </a>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="related-list">
                <a href="berita-detail.html">
                  <img src="images/news-06.jpg" alt="News 06" class="img-fluid">
                  <div class="related-meta-date">01 Februari 2018</div>
                  <div class="related-meta-title">Praktik Pekerja Sosial dalam Kekasih Juara</div>
                </a>
              </div>
            </div>
          </div> -->
        </div>

      </div>
      <div class="col-lg-4">
        <div class="widget-container">
          <div class="widget-title">Berita Terbaru</div>
          <!-- <div class="widget-wrap">
            <div class="latest-item">
              <div class="latest-item-img">
                <a href="berita-detail.html"><img src="images/news-04.jpg" alt="News 04"></a>
              </div>
              <div class="latest-item-meta">
                <div class="meta-date">22 Maret 2018</div>
                <div class="meta-title">
                  <a href="berita-detail.html">World Social Work Day 2018</a>
                </div>
              </div>
            </div>
            <div class="latest-item">
              <div class="latest-item-img">
                <a href="berita-detail.html"><img src="images/news-05.jpg" alt="News 05"></a>
              </div>
              <div class="latest-item-meta">
                <div class="meta-date">21 Februari 2018</div>
                <div class="meta-title">
                  <a href="berita-detail.html">Menteri Sosial RI Dukung RUU Praktik Pekerjaan Sosial</a>
                </div>
              </div>
            </div>
            <div class="latest-item">
              <div class="latest-item-img">
                <a href="berita-detail.html"><img src="images/news-06.jpg" alt="News 06"></a>
              </div>
              <div class="latest-item-meta">
                <div class="meta-date">01 Februari 2018</div>
                <div class="meta-title">
                  <a href="berita-detail.html">Praktik Pekerja Sosial dalam Kekasih Juara</a>
                </div>
              </div>
            </div>
            <div class="latest-item">
              <div class="latest-item-img">
                <a href="berita-detail.html"><img src="images/news-07.jpg" alt="News 07"></a>
              </div>
              <div class="latest-item-meta">
                <div class="meta-date">31 Januari 2018</div>
                <div class="meta-title">
                  <a href="berita-detail.html">Kunjungan Kerja Komisi VIII DPR RI diterima DPD IPSPI Jayapura</a>
                </div>
              </div>
            </div>
            <div class="latest-item">
              <div class="latest-item-img">
                <a href="berita-detail.html"><img src="images/news-08.jpg" alt="News 08"></a>
              </div>
              <div class="latest-item-meta">
                <div class="meta-date">31 Januari 2018</div>
                <div class="meta-title">
                  <a href="berita-detail.html">Panja Pekerjaan Sosial Komisi VIII Dorong Perlindungan Hukum bagi Pekerja Sosial</a>
                </div>
              </div>
            </div>
          </div> -->
        </div>
      </div>
    </div>
  </div>
</section>
