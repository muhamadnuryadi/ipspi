  <script type="text/javascript">
function unduhFile(id) {
    $.ajax({
        url: '<?php echo base_url(); ?>downloads/checkFile/'+id,
        dataType: 'json',
        success: function(data) {
            if(data.is_exist == true) {
                window.open('<?php echo base_url(); ?>downloads/files/'+id,'_self');
            } else {
                alert('<?php echo $this->lang->line('file_not_exist'); ?>');
            }
        }
    }); return false;
}
function searchThis(){
  var frm = document.searchform;
  var name = frm.name.value;
  if(name == ''){ name = '-'; }
  window.location = "<?php echo base_url(); ?>berita/searchdata/"+name.replace(/\s/g,'%20');
}
</script>
<section class="head-wrap">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1 class="head-title">FAQ</h1>
      </div>
    </div>
  </div>
</section>
  
<section class="ipspi-faq">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="accordion">
            <div class="card-faq">
              <div class="card-header d-flex align-items-center" id="head-faq-1">
                <div class="ttl-faq d-flex" data-toggle="collapse" data-target="#faq-1">
                    <div><i class="fa fa-plus"></i></div>
                    <div>Kalimat pembuka live chat</div>
                </div>
              </div>
              <div id="faq-1" class="collapse" aria-labelledby="head-faq-1">
                <div class="card-body-faq">
                  <p>Halo, saat ini kami sedang Offline. Silakan kirimkan pesan kepada kami, dan kami akan merespon pesan Anda saat kami Online kembali. Isi nama, email, dan ketikan pesan disini</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="accordion">
            <div class="card-faq">
              <div class="card-header d-flex align-items-center" id="head-faq-2">
                <div class="ttl-faq d-flex" data-toggle="collapse" data-target="#faq-2">
                    <div><i class="fa fa-plus"></i></div>
                    <div>Apa itu IPSPI.</div>
                </div>
              </div>
              <div id="faq-2" class="collapse" aria-labelledby="head-faq-2">
                <div class="card-body-faq">
                  <p>IPSPI adalah organisasi Perkumpulan mewadahi Pekerja Sosial Profesional di seluruh Indonesia</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="accordion">
            <div class="card-faq">
              <div class="card-header d-flex align-items-center" id="head-faq-3">
                <div class="ttl-faq d-flex" data-toggle="collapse" data-target="#faq-3">
                    <div><i class="fa fa-plus"></i></div>
                    <div>Apakah IPSPI ada di wilayah saya.?</div>
                </div>
              </div>
              <div id="faq-3" class="collapse" aria-labelledby="head-faq-3">
                <div class="card-body-faq">
                  <p>IPSPI memiliki dewan pengurus daerah disetiap provinsi diseluruh Indonesia.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="accordion">
            <div class="card-faq">
              <div class="card-header d-flex align-items-center" id="head-faq-4">
                <div class="ttl-faq d-flex" data-toggle="collapse" data-target="#faq-4">
                    <div><i class="fa fa-plus"></i></div>
                    <div>Bagaiamana cara saya bergabung di IPSPI.?</div>
                </div>
              </div>
              <div id="faq-4" class="collapse" aria-labelledby="head-faq-4">
                <div class="card-body-faq">
                  <p>Silahkan klik daftar pada kolom kanan atas pada halaman home website ini.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="accordion">
            <div class="card-faq">
              <div class="card-header d-flex align-items-center" id="head-faq-5">
                <div class="ttl-faq d-flex" data-toggle="collapse" data-target="#faq-5">
                    <div><i class="fa fa-plus"></i></div>
                    <div>Apa keuntungan saya bergabung di IPSPI.?</div>
                </div>
              </div>
              <div id="faq-5" class="collapse" aria-labelledby="head-faq-5">
                <div class="card-body-faq">
                  <ul>
                    <li>IPSPI Mewadahi Pekerja Sosial Profesional di Indonesia</li> 
                    <li>IPSPI Mengeluarkan tanda keanggotaan Pekerja Sosial Profesional Indonesia</li> 
                    <li>IPSPI Memberikan rekomendasi uji kompetensi dan izin praktik (lisensi) bagi Pekerja Sosial Profesional.</li>
                    <li>IPSPI Memberikan rekomendasi akreditasi lembaga pendidikan pekerjaan sosial</li> 
                    <li>IPSPI Memperjuangkan hak dan kepentingan Pekerja Sosial Profesional dalam melakukan tugas pelayanan pekerjaan sosial.</li> 
                    <li>IPSPI Memberikan perlindungan kepada Pekerja Sosial Profesional dan penerima layanan Pekerjaan Sosial</li> 
                    <li>IPSPI Menetapkan standar praktik Pekerjaan Sosial</li> 
                    <li>IPSPI Meningkatkan kualitas Pekerjaan Sosial melalui kerjasama antara anggota maupun dengan organisasi keilmuan dan profesi lainnya di dalam maupun di luar negeri</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="accordion">
            <div class="card-faq">
              <div class="card-header d-flex align-items-center" id="head-faq-6">
                <div class="ttl-faq d-flex" data-toggle="collapse" data-target="#faq-6">
                    <div><i class="fa fa-plus"></i></div>
                    <div>Siapa saja yang dapat bergabung di IPSPI.?</div>
                </div>
              </div>
              <div id="faq-6" class="collapse" aria-labelledby="head-faq-6">
                <div class="card-body-faq">
                  <ul>
                    <li>Warga Negara Indonesia lulusan Sarjana Perguruan Tinggi Pekerjaan Sosial / Ilmu Kesejahteraan Sosial</li> 
                    <li>Siswa atau mahasiswa bidang kesejahteraan atau pekerjaan sosial</li> 
                    <li>calon,asisten atau praktisi pekerjaan sosial yang bukan lulusan pendidikan tinggi perguruan kesejahteraan atau pekerjaan sosial</li>
                    <li>Individu yang karena kepakaran,tugas dan fungsinya dianggap memberikan sumbangan yang signifikan pada profesi Pekerjaan Sosial</li> 
                    <li>Pekerja sosial asing</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="accordion">
            <div class="card-faq">
              <div class="card-header d-flex align-items-center" id="head-faq-7">
                <div class="ttl-faq d-flex" data-toggle="collapse" data-target="#faq-7">
                    <div><i class="fa fa-plus"></i></div>
                    <div>Apa saja yang perlu saya perhatikan sebelum mendaftarkan diri di IPSPI.?</div>
                </div>
              </div>
              <div id="faq-7" class="collapse" aria-labelledby="head-faq-7">
                <div class="card-body-faq">
                  <ul>
                    <li>Pastikan internet anda dalam kondisi stabil</li> 
                    <li>Pastikan setiap berkas penunjang telah di scan dan siap upload.</li> 
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="accordion">
            <div class="card-faq">
              <div class="card-header d-flex align-items-center" id="head-faq-8">
                <div class="ttl-faq d-flex" data-toggle="collapse" data-target="#faq-8">
                    <div><i class="fa fa-plus"></i></div>
                    <div>Berkas apa saja yang perlu saya persiapkan untuk bergabung.?</div>
                </div>
              </div>
              <div id="faq-8" class="collapse" aria-labelledby="head-faq-8">
                <div class="card-body-faq">
                  <ul>
                    <li>Kartu tanda penduduk</li> 
                    <li>Ijazah</li> 
                    <li>Berkas penunjang (Sertifikat penghargaan, Sertifikat sertifikasi, dan lainnya)</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="accordion">
            <div class="card-faq">
              <div class="card-header d-flex align-items-center" id="head-faq-9">
                <div class="ttl-faq d-flex" data-toggle="collapse" data-target="#faq-9">
                    <div><i class="fa fa-plus"></i></div>
                    <div>Berapa lama masa keanggotaan ipspi berlaku.?</div>
                </div>
              </div>
              <div id="faq-9" class="collapse" aria-labelledby="head-faq-9">
                <div class="card-body-faq">
                  <ul>
                    <li>Keanggotaan IPSPI berlaku selama 3 tahun.</li> 
                    <li>Apabila keanggotaan telah habis dapat diperpanjang melalui halaman profil anda.</li> 
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="accordion">
            <div class="card-faq">
              <div class="card-header d-flex align-items-center" id="head-faq-10">
                <div class="ttl-faq d-flex" data-toggle="collapse" data-target="#faq-10">
                    <div><i class="fa fa-plus"></i></div>
                    <div>Apakah IPSPI memiliki informasi lowongan pekerjaan?</div>
                </div>
              </div>
              <div id="faq-10" class="collapse" aria-labelledby="head-faq-10">
                <div class="card-body-faq">
                  <p>Iya, salah satu fungsi IPSPI dalam menjalin relasi antar lembaga dan pelayanan-pelayan pekerjaan sosial, dalam dan luar negri salah satunya diwujudkan melalui kerjasama bersama mitra dalam informasi lowongan pekerjaan dari mitra-mitra yang dimiliki IPSPI</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="accordion">
            <div class="card-faq">
              <div class="card-header d-flex align-items-center" id="head-faq-11">
                <div class="ttl-faq d-flex" data-toggle="collapse" data-target="#faq-11">
                    <div><i class="fa fa-plus"></i></div>
                    <div>Bagaimana cara mengakses informasi layanan lowongan pekerjaan yang dimiliki IPSPI.?</div>
                </div>
              </div>
              <div id="faq-11" class="collapse" aria-labelledby="head-faq-11">
                <div class="card-body-faq">
                  <ul>
                    <li>Pastikan anda telah terdaftar sebagai anggota IPSPI aktif pada website ini.</li> 
                    <li>Login pada akun anda.</li>
                    <li>Pilihlah navbar lowongan pekerjaan</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="accordion">
            <div class="card-faq">
              <div class="card-header d-flex align-items-center" id="head-faq-12">
                <div class="ttl-faq d-flex" data-toggle="collapse" data-target="#faq-12">
                    <div><i class="fa fa-plus"></i></div>
                    <div>Apakah ipspi memiliki perpustakaan tulisan atau jurnal nasional.?</div>
                </div>
              </div>
              <div id="faq-12" class="collapse" aria-labelledby="head-faq-12">
                <div class="card-body-faq">
                  <p>Iya, IPSPI sebagai wadah yang memfasilitasi peningkatan kulitas SDM pada bidang literasi pekerjaan sosial dengan adanya fitur jurnal digital.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="accordion">
            <div class="card-faq">
              <div class="card-header d-flex align-items-center" id="head-faq-13">
                <div class="ttl-faq d-flex" data-toggle="collapse" data-target="#faq-13">
                    <div><i class="fa fa-plus"></i></div>
                    <div>Apakah ipspi memiliki fitur surat rekomendasi otomatis.?</div>
                </div>
              </div>
              <div id="faq-13" class="collapse" aria-labelledby="head-faq-13">
                <div class="card-body-faq">
                  <ul>
                    <li>Iya, IPSPI telah memiliki fitur pembuatan surat rekomendasi otomatis yang dapat dinikmati oleh setiap anggota apabila telah terdaftar sebagai anggota aktif pada website ini.</li> 
                    <li>Masuklah pada halaman login</li>
                    <li>Masuk pada halaman profil lalu klik surat rekomendasai otomatis</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="accordion">
            <div class="card-faq">
              <div class="card-header d-flex align-items-center" id="head-faq-14">
                <div class="ttl-faq d-flex" data-toggle="collapse" data-target="#faq-14">
                    <div><i class="fa fa-plus"></i></div>
                    <div>Bagaimana cara saya dapat memasukan tulisan atau jurnal saya.?</div>
                </div>
              </div>
              <div id="faq-14" class="collapse" aria-labelledby="head-faq-14">
                <div class="card-body-faq">
                  <ul>
                    <li>Daftarkan diri anda pada webiste ini.</li> 
                    <li>Login menggunakan akun yang telah anda daftarkan.</li>
                    <li>Masuklah pada halamn profile anda lalu pilih halaman jurnal.</li>
                    <li>Upload lah jurnal yang anda miliki pada halaman tersebut</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="accordion">
            <div class="card-faq">
              <div class="card-header d-flex align-items-center" id="head-faq-15">
                <div class="ttl-faq d-flex" data-toggle="collapse" data-target="#faq-15">
                    <div><i class="fa fa-plus"></i></div>
                    <div>Bagaimana cara mendaftarakan diri untuk sertifikasi pekerja sosial.</div>
                </div>
              </div>
              <div id="faq-15" class="collapse" aria-labelledby="head-faq-15">
                <div class="card-body-faq">
                  <ul>
                    <li>Pastikan anda telah mendaftarkan diri sebagai anggota pada web ini.</li> 
                    <li>Lakukan login, lalu unduhlah berkas surat rekomendasi yang bisa anda dapatkan setelah login dan membuka profil akun anda.</li>
                    <li>Ajukanlah surat permintaan sertifikasi ke lsps.indonesia@yahoo.com</li>
                    <li>Pihak lsps akan membalas email anda dengan jadwal sertifikasi, lokasi, dan informasi lainnya. (balasan akan menyesuaikan secara rentang waktu tergantung pada jumlah pengaju, maka dari itu seluruh pengaju diharap menunggu hingga email dibalas oleh pihak LSPS)</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="accordion">
            <div class="card-faq">
              <div class="card-header d-flex align-items-center" id="head-faq-16">
                <div class="ttl-faq d-flex" data-toggle="collapse" data-target="#faq-16">
                    <div><i class="fa fa-plus"></i></div>
                    <div>Bagaimana cara memiliki izin praktik pekerja sosial secara mandiri.?</div>
                </div>
              </div>
              <div id="faq-16" class="collapse" aria-labelledby="head-faq-16">
                <div class="card-body-faq">
                  <ul>
                    <li>Pastikan anda telah terdaftar secara resmi sebagai anggota IPSPI</li> 
                    <li>Pastikan anda telah memiliki kompetensi sebagai pekerja sosial tersertifikasi oleh lembaga sertifikasi resmi seperti LSPS.</li>
                    <li>Ajukanlah permohonan rekomendasi praktik pekerjaan sosial kepada pihak IPSPI untuk selanjutnya merekomendasikan anda kepada pihak LSPS untuk bersurat terkait perizinan praktik anda secara tertulis kepada pihak Kementrian Sosial Republik Indonesia.</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>