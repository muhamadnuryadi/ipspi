<?php

class Webservice extends CI_Controller {

    var $webconfig;

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('date');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->sharedModel('MembersModel');
        $this->load->helper('phpmailer');
        $this->webconfig = $this->config->item('webconfig');
        // $this->module_name = $this->lang->line('wilayah');
    }

    function send_email_notification() {
        $getMember = $this->MembersModel->getMemberExpired();
        if (count($getMember) > 0) {
            foreach ($getMember as $key => $value) {
                $email = $value['email'];
                $name = $value['nama'];

                $message = $this->load->view($this->router->class.'/email_notification', array(
                                                                                "email" => $email
                                                                                ,"name" => $name
                                                                            ), TRUE);

                $result = send_mail(array(
                                'email_tujuan' => $email
                                ,'email_tujuan_text' => 'Ipspi Admin'
                                ,'subject' => 'Notifikasi Status Anggota Tetap IPSPI akan segera berakhir'
                                ,'setfrom_email' => SENDER_EMAIL
                                ,'body' => $message
                            ));

            }
        }
    }

    function getnik(){
        $nik = $this->input->post('nik');
        $getNik = $this->MembersModel->getNik(array('nik'=>$nik));
        if ($getNik == 'success') {
            echo "success";
        }else{
            echo "failed";
        }
    }

    function UpdateStatusExpired(){
        
    }
}
