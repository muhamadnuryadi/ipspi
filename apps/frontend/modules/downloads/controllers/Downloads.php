<?php
class Downloads extends CI_Controller{
	var $webconfig;
	function __construct(){
		parent::__construct();	
		$this->load->database();
		$this->load->helper('url');		
		$this->webconfig = $this->config->item('webconfig');
        $this->load->sharedModel('MateriModel');
	}
	function files($id = ''){

		$media_files_path = $this->webconfig['media-path-files'];
		$files = $this->MateriModel->listDataDownload(array('id' => $id));
		$file_path = $media_files_path.$files[0]['files'];
		
		// Must be fresh start
		  if( headers_sent() )
			die('Headers Sent');

		  // Required for some browsers
		  if(ini_get('zlib.output_compression'))
			ini_set('zlib.output_compression', 'Off');

		  // File Exists
		  if( file_exists($file_path) ){
		   
			// Parse Info / Get Extension
			$fsize = filesize($file_path);
			$path_parts = pathinfo($file_path);
			$ext = strtolower($path_parts["extension"]);
		   
			// Determine Content Type
			switch ($ext) {
			  case "pdf": $ctype="application/pdf"; break;
			  case "exe": $ctype="application/octet-stream"; break;
			  case "zip": $ctype="application/zip"; break;
			  case "doc": $ctype="application/msword"; break;
			  case "xls": $ctype="application/vnd.ms-excel"; break;
			  case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
			  case "gif": $ctype="image/gif"; break;
			  case "png": $ctype="image/png"; break;
			  case "jpeg":
			  case "jpg": $ctype="image/jpg"; break;
			  default: $ctype="application/force-download";
			}

			header("Pragma: public"); // required
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false); // required for certain browsers
			header("Content-Type: ".$ctype);
			header("Content-Disposition: attachment; filename=\"".basename($file_path)."\";" );
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: ".$fsize);
			@ob_end_clean(); flush(); 
			readfile( $file_path );

		  } else {
				die('File Not Found'); 
		}
	}
	function checkFile($id = 0) {
		$media_files_path = $this->webconfig['media-path-files'];
		
		$files = $this->MateriModel->listDataDownload(array('id' => $id));
		
		$data = array();
		if(isset($files[0]['files']) && $files[0]['files'] != "") {
			if (file_exists($media_files_path.$files[0]['files'])) {
				$data['is_exist'] = true;
			} else {
				$data['is_exist'] = false;
			}
		} else {
			$data['is_exist'] = false;
		}

		$this->output
				    ->set_content_type('application/json')
				    ->set_output(json_encode($data));
	}
}