<?php
class Payment extends CI_Controller{
    var $webconfig;
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->sharedModel('MembersModel');
        $this->load->sharedModel('WilayahModel');
        $this->load->sharedModel('ConfigurationModel');
        $this->configuration = $this->ConfigurationModel->listData();
        $this->webconfig = $this->config->item('webconfig');
        $this->module_name = 'Payment';
    }
    function index(){
        
    }
    
    function notification(){
        if ($this->webconfig['payment_production'] == true) {
            $server_key = $this->webconfig['midtrans_server_key_production'];
        }else{
            $server_key = $this->webconfig['midtrans_server_key_sandbox'];
        }

        $params = array('server_key' => $server_key, 'production' => $this->webconfig['payment_production']);
        $this->load->library('midtrans');
        $this->midtrans->config($params);

        $json_result = file_get_contents('php://input');
        $result = json_decode($json_result);
        if($result){
            $notif = $this->midtrans->status($result->order_id);
            $transaction = $notif->transaction_status;
            $type = $notif->payment_type;
            $order_id = $notif->order_id;
            $fraud = $notif->fraud_status;
            
            if ($transaction == 'capture') {
                // For credit card transaction, we need to check whether transaction is challenge by FDS or not
                if ($type == 'credit_card'){
                    if($fraud == 'challenge'){
                        // TODO set payment status in merchant's database to 'Challenge by FDS'
                        // TODO merchant should decide whether this transaction is authorized or not in MAP
                        echo "Transaction order_id: " . $order_id ." is challenged by FDS";
                    } else {
                        // TODO set payment status in merchant's database to 'Success'
                        echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
                    }
                }
            }else if ($transaction == 'settlement' && $type != 'credit_card'){
                // TODO set payment status in merchant's database to 'Settlement'
                // echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
                $updatepayment = $this->MembersModel->updatePayment(array(
                    'email'=>$notif->custom_field1
                    ,'order_id'=>$notif->order_id
                    ,'settlement_time'=>$notif->settlement_time
                    ,'transaction_status'=>$notif->transaction_status
                    ,'payment_type'=>$notif->payment_type
                ));

                $getnoanggota = $this->generateAnggota($notif->custom_field1);
                $this->MembersModel->updateNoAnggota(array(
                    'email'=>$notif->custom_field1
                    ,'no_anggota'=>$getnoanggota
                ));



            } else if($transaction == 'pending'){
                // TODO set payment status in merchant's database to 'Pending'
                echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
            } else if ($transaction == 'deny') {
                // TODO set payment status in merchant's database to 'Denied'
                echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
            }
        }
    }

    public function generateAnggota($email){
        $tdata = array();
        $member = $this->MembersModel->listData(array('email'=>$email));
        
        $id_member = $member[0]['id'];
        $id_provinsi = isset($member[0]['id_propinsi'])?$member[0]['id_propinsi']:'';
        $id_kota = isset($member[0]['id_kota'])?$member[0]['id_kota']:'';
        $no_anggota_from_db = $member[0]['no_anggota'];
        $created_at = isset($member[0]['created_at'])?$member[0]['created_at']:'';
        
        // //get date join
        if($created_at != ''){
            $month = date('m',strtotime($created_at));
            $year = substr(date('Y',strtotime($created_at)), 2) ;
        }else{
            if($fotomember != ''){
                $expfoto = explode('/', $fotomember);
                $month = $expfoto[1];
                $year = substr($expfoto[0], 2);
            } else if ($pathijazahmember != '') {
                $expfoto = explode('/', $pathijazahmember);
                $month = $expfoto[1];
                $year = substr($expfoto[0], 2);
            } else if ($pathktpmember != ''){
                $expfoto = explode('/', $pathktpmember);
                $month = $expfoto[1];
                $year = substr($expfoto[0], 2);
            } else {
                $month = date('m');
                $year = date('Y');
            }
        }

        //auto no anggota
        $allmember = $this->MembersModel->getLastNoAnggota();
        $newnumb = max(array_column($allmember, 'lastnumb'));
        $newno_anggota = $newnumb + 1;

        //untuk get kode wilayah
        $getKodeProvinsi = $this->WilayahModel->listData(array('id'=>$id_provinsi));
        $getKodeKota = $this->WilayahModel->listData(array('id'=>$id_kota));

        //untuk get pendidikan sosial dan non sosial/jenis anggota
        $pendidikan = $this->MembersModel->listDataPendidikan(array('id'=>$id_member));
        $pendidikanterakhir = end($pendidikan);
        $jenis_pendidikan = $pendidikanterakhir['jenis'];
        
        if ($jenis_pendidikan == 2) {//jika sosial
            $jenis_anggota = 1;
        }else{
            $jenis_anggota = 2;
        }

        $kodeprovinsi = $tdata['kodeprovinsi'] = isset($getKodeProvinsi[0]['kode'])?$getKodeProvinsi[0]['kode']:'';
        $kodekota = $tdata['kodekota'] = isset($getKodeKota[0]['kode'])?$getKodeKota[0]['kode']:'';

        $no_anggota_generate = $kodeprovinsi.'.'.$kodekota.'.'.$month.$year.'.'.$jenis_anggota.'.'.$newno_anggota;
        
        if($no_anggota_from_db == ''){
            return $no_anggota_generate;
        }else{
            return $no_anggota_from_db;
        }
    }
}