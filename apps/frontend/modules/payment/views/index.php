<section class="ipspi-featured">
    <div class="row no-gutters">
      <div class="col-md-6">
        <div class="featured-item">
          <a href="berita-detail.html">
            <img src="<?php echo $this->webconfig['frontend_template']; ?>images/news-01.jpg" alt="News 01" class="img-fluid">
            <div class="featured-overlay">
              <div class="featured-meta">
                <div class="ft-date"><i class="far fa-clock"></i>18 Agustus 2018</div>
                <div class="ft-author"><i class="far fa-user"></i>IPSPI</div>
                <div class="ft-title head1">IPSPI Berikan Bantuan Bagi Korban Gempa di Lombok</div>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-md-6">
        <div class="row no-gutters">
          <div class="col-sm-12">
            <div class="featured-item">
              <a href="berita-detail.html">
                <img src="<?php echo $this->webconfig['frontend_template']; ?>images/news-02.jpg" alt="News 02" class="img-fluid">
                <div class="featured-overlay">
                  <div class="featured-meta">
                    <div class="ft-date"><i class="far fa-clock"></i>03 Juni 2018</div>
                    <div class="ft-author"><i class="far fa-user"></i>IPSPI</div>
                    <div class="ft-title head2">Proses Perkembangan Penyusunan Undang Undang Praktik Pekerjaan Sosial</div>
                  </div>
                </div>
              </a>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="featured-item">
              <a href="berita-detail.html">
                <img src="<?php echo $this->webconfig['frontend_template']; ?>images/news-03.jpg" alt="News 03" class="img-fluid">
                <div class="featured-overlay">
                  <div class="featured-meta">
                    <div class="ft-date"><i class="far fa-clock"></i>16 April 2018</div>
                    <div class="ft-author"><i class="far fa-user"></i>IPSPI</div>
                    <div class="ft-title head2">Temu Dengar Pendapat tentang Program PPPA dengan Lembaga Profesi dan Dunia Usaha</div>
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="ipspi-search">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <form class="form-inline form-search">
            <input class="form-control search-large" type="search" placeholder="Pencarian.." aria-label="Search">
            <button class="btn btn-search" type="submit">CARI</button>
          </form>
        </div>
      </div>
    </div>
  </section>
  
  <section class="ipspi-info">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="info-wrap">
            <div class="info-title">Kebijakan IPSPI</div>
            <ul class="info-list">
              <li><a href="#"><i class="far fa-file"></i>Rencana Strategis IPSPI</a></li>
              <li><a href="#"><i class="far fa-file"></i>Sistem Keanggotaan</a></li>
              <li><a href="#"><i class="far fa-file"></i>Kode Etik IPSPI 2016</a></li>
              <li><a href="#"><i class="far fa-file"></i>Anggaran Dasar</a></li>
              <li><a href="#"><i class="far fa-file"></i>Anggaran Rumah Tangga</a></li>
              <li><a href="#"><i class="far fa-file"></i>Mekanisme Pendaftaran Anggota IPSPI</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="info-wrap">
            <div class="info-title">Unduh Materi</div>
            <ul class="info-list">
              <li><a href="#"><i class="far fa-file"></i>Materi Diskusi RUU Peksos</a></li>
              <li><a href="#"><i class="far fa-file"></i>Materi SWD 2016</a></li>
              <li><a href="#"><i class="far fa-file"></i>Rapat Dengan Komisi Perlindungan Anak Indonesia (KPAI)</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="ipspi-latest-news">
    <div class="container">
      <div class="text-center">
       <div class="heading">Berita Terbaru</div>
      </div>
      <div class="row">
        <?php if(isset($lists) && count($lists) > 0) {?>
        <?php foreach ($lists as $listdata) { ?>
        <div class="col-sm-4">
          <div class="article-item">
            <a href="<?php echo base_url().'berita/read/'.$listdata['id'].'/'.$listdata['slug']; ?>"><img src="<?php echo thumb_image($listdata['path'],'600x338', 'berita'); ?>" alt="News 04" class="img-fluid img-article"></a>
            <div class="article-meta">
              <div class="ar-date"><i class="far fa-clock"></i><?php echo $listdata['datepublish'];?></div>
              <div class="ar-author"><i class="far fa-user"></i>IPSPI</div>
              <div class="ar-title head3"><a href="<?php echo base_url().'berita/read/'.$listdata['id'].'/'.$listdata['slug']; ?>"><?php echo $listdata['title'];?></a></div>
              <div class="ar-desc"><?php echo stripcslashes($listdata['description']);?></div>
            </div>
          </div>
        </div>
        <?php } ?>
        <?php } ?>
        
      </div>
      <div class="row">

      </div>
      <div class="text-center">
        <a href="berita.html" class="btn btn-lg btn-blue">Lihat Semua</a>
      </div>
    </div>
  </section>