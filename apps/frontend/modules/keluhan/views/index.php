<link href="<?php echo $this->webconfig['back_base_template']; ?>js/toastr/toastr.css" rel="stylesheet" type="text/css" />
<script src="<?php echo $this->webconfig['back_base_template']; ?>js/toastr/toastr.js" type="text/javascript"></script>
<section class="head-wrap">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1 class="head-title">Keluhan</h1>
      </div>
    </div>
  </div>
</section>
<?php if(isset($error_hash) && count($error_hash) > 0){ ?>
    <?php foreach($error_hash as $inp_err){ ?>
        <script type="text/javascript">
        jQuery(document).ready(function($) {
            toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
            });
        </script>
    <?php } ?>
<?php } ?>
<?php if(isset($success)){ ?>
    <script type="text/javascript">
    jQuery(document).ready(function($) {
        toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
        });
    </script>
<?php } ?>
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">
        <div class="content-wrapper p-5">
          <div class="text-center">
            <div class="heading">Form Keluhan</div>
          </div>
          <form method="post" action="<?php echo base_url(); ?>keluhan">
            <div class="form-group">
              <label for="formNama">Nama</label>
              <input type="text" class="form-control" id="formNama" name="name" required>
            </div>
            <div class="form-group">
              <label for="formEmail">No Anggota</label>
              <input type="text" class="form-control" id="formEmail" name="no_anggota" required>
            </div>
            <div class="form-group">
              <label for="formSubjek">Telp</label>
              <input type="text" class="form-control" id="formSubjek" name="telp" required>
            </div>
            <div class="form-group">
              <label for="formPesan">Keluhan</label>
              <textarea class="form-control" id="formPesan" rows="5" name="message" required></textarea>
            </div>
            <div class="text-center">
              <button type="submit" class="btn btn-lg btn-green px-5 ">Kirim</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>