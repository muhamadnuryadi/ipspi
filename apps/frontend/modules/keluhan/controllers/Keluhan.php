<?php
class Keluhan extends CI_Controller{
    var $webconfig;
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->sharedModel('KeluhanModel');

        $this->load->sharedModel('AboutModel');
        $this->about = $this->AboutModel->listData();
        $this->load->sharedModel('InformasiModel');
        $this->informasi = $this->InformasiModel->listData();
        $this->load->sharedModel('ConfigurationModel');
        $this->configuration = $this->ConfigurationModel->listData();
        $this->load->sharedModel('RegulationModel');
        $this->regulation = $this->RegulationModel->listData();
        
        $this->webconfig = $this->config->item('webconfig');
        $this->module_name = $this->lang->line('home');
    }
    function index(){
        redirect(base_url());
        $tdata = array();
        
        if($_POST){
            $doInsert = $this->KeluhanModel->entriData(array(
                                                            'name'=>$this->input->post('name')
                                                            ,'no_anggota'=>$this->input->post('no_anggota')
                                                            ,'telp'=>$this->input->post('telp')
                                                            ,'message'=>$this->input->post('message')
                                                      ));

            if($doInsert == 'empty'){
                $tdata['error_hash'] = array('error' => $this->lang->line('error_empty_form'));
            }else if($doInsert == 'failed'){
                $tdata['error_hash'] = array('error' => $this->lang->line('error_system'));
            }else if($doInsert == 'success'){
                $tdata['success'] = $this->lang->line('message_success');
            }
            if($doInsert !='success'){
                $tdata['name']     = $this->input->post('name');
                $tdata['no_anggota']     = $this->input->post('no_anggota');
                $tdata['telp']     = $this->input->post('telp');
                $tdata['message']     = $this->input->post('message');
            }
            $ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
            $this->load->sharedView('template', $ldata);
            
        }else{
            ## LOAD LAYOUT ##
            $ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }
}