<?php
class Loker extends CI_Controller{
    var $webconfig;
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
        $this->load->library('session');
        $this->load->library('pagination');
        // $this->load->sharedModel('MateriModel');
        $this->load->sharedModel('LokerModel');
        $this->load->sharedModel('AboutModel');
        $this->about = $this->AboutModel->listData();
        $this->load->sharedModel('InformasiModel');
        $this->informasi = $this->InformasiModel->listData();
        $this->load->sharedModel('ConfigurationModel');
        $this->configuration = $this->ConfigurationModel->listData();
        $this->load->sharedModel('RegulationModel');
        $this->regulation = $this->RegulationModel->listData();
        $this->load->sharedModel('ConfigurationModel');
        $this->configuration = $this->ConfigurationModel->listData();
        

        $this->webconfig = $this->config->item('webconfig');
        $this->module_name = $this->lang->line('home');
    }

    function index(){
        redirect(base_url().'loker/listing');
    }

    function listing($page = 0){
        $tdata = array();
        $all_data = $this->LokerModel->listDataCount();
        
        $cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = 9;
        $cfg_pg ['uri_segment'] = 3;

        $cfg_pg['first_link'] = '&laquo;';
        $cfg_pg['first_tag_close'] = '</li>';
        $cfg_pg['first_tag_open'] = '<li class="page-item page-link">';

        $cfg_pg['last_link'] = '&raquo;';
        $cfg_pg['last_tag_open'] = '<li class="page-item page-link">';
        $cfg_pg['last_tag_close'] = '</li>';

        $cfg_pg['next_link'] = '&gt;';
        $cfg_pg['next_tag_open'] = '<li class="page-item page-link">';
        $cfg_pg['next_tag_close'] = '</li>';

        $cfg_pg['prev_link'] = '&lt;';
        $cfg_pg['prev_tag_open'] = '<li class="page-item page-link">';
        $cfg_pg['prev_tag_close'] = '</li>';

        $cfg_pg['num_tag_open'] = '<li class="page-item page-link">';
        $cfg_pg['num_tag_close'] = '</li>';

        $cfg_pg['cur_tag_open'] = '<li class="page-item page-link active"><span>';      
        $cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
        $listdata["pagination"] = $this->pagination->create_links();

        $start_no = 0;
        if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
            $start_no = $this->uri->segment($cfg_pg ['uri_segment']);
        }
        
        $listdata["start_no"] = $start_no;

        $start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
        $start_idx = $start_idx < 0?0:$start_idx;

        $listdata['lists'] = $this->LokerModel->listData(array(
                                                                'start'  => $start_idx
                                                                ,'limit' => $cfg_pg['per_page']
                                                            ));
        
        $ldata['content'] = $this->load->view($this->router->class.'/index',$listdata, true);
        $this->load->sharedView('template', $ldata);
    }
    
    function read($slug = '') {
        $tdata = array();
        $tdata['detail'] = $this->LokerModel->listData(array('slug' => $slug));
        $id_exclude = isset($tdata['detail'][0]['id'])?$tdata['detail'][0]['id']:'';
        $tdata['loker_lainnya'] = $this->LokerModel->listData(array('id_exclude' => $id_exclude, 'limit' => 5));
        
        $ldata['content'] = $this->load->view($this->router->class.'/detail',$tdata, true);
        $this->load->sharedView('template', $ldata);
    }

    public function loker1(){
        $tdata = array();
        
        $ldata['content'] = $this->load->view($this->router->class.'/indexloker1',$tdata, true);
            $this->load->sharedView('template', $ldata);
    }
    
    public function loker2(){
        $tdata = array();
        
        $ldata['content'] = $this->load->view($this->router->class.'/indexloker2',$tdata, true);
            $this->load->sharedView('template', $ldata);
    }
    
    public function loker3(){
        $tdata = array();
        
        $ldata['content'] = $this->load->view($this->router->class.'/indexloker3',$tdata, true);
            $this->load->sharedView('template', $ldata);
    }
    
}