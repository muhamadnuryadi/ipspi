  <script type="text/javascript">
function unduhFile(id) {
    $.ajax({
        url: '<?php echo base_url(); ?>downloads/checkFile/'+id,
        dataType: 'json',
        success: function(data) {
            if(data.is_exist == true) {
                window.open('<?php echo base_url(); ?>downloads/files/'+id,'_self');
            } else {
                alert('<?php echo $this->lang->line('file_not_exist'); ?>');
            }
        }
    }); return false;
}
function searchThis(){
  var frm = document.searchform;
  var name = frm.name.value;
  if(name == ''){ name = '-'; }
  window.location = "<?php echo base_url(); ?>berita/searchdata/"+name.replace(/\s/g,'%20');
}
</script>
<section class="head-wrap">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1 class="head-title">Lowongan Pekerjaan</h1>
      </div>
    </div>
  </div>
</section>
  
<section class="ipspi-latest-news">
  <div class="container">
    <div class="row">
      <?php 
      if (isset($lists) && count($lists) > 0) {
        foreach ($lists as $key => $value) {
          ?>
          <div class="col-sm-4">
            <div class="article-item">
              <a href="<?php echo base_url(); ?>loker/read/<?php echo isset($value['slug'])?$value['slug']:''; ?>"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker 01" class="img-fluid img-article"></a>
              <div class="article-meta">
                <div class="ar-date"><i class="far fa-clock"></i><?php echo isset($value['date_posted'])?date_min_format($value['date_posted']):''; ?></div>
                <div class="ar-author"><i class="far fa-user"></i>IPSPI</div>
                <div class="ar-title head3"><a href="<?php echo base_url(); ?>loker/read/<?php echo isset($value['slug'])?$value['slug']:''; ?>"><?php echo isset($value['title'])?$value['title']:''; ?></a></div>
                <?php 
                /*<div class="ar-desc">Membuka lamaran pekerjaan menjadi Konselor Adiksi (3 orang)dan Pekerja Sosial (2 orang) yang akan bertugas di LSM yg selama ini bergerak di isu HIV/AIDS, public policy, narkoba dan development community / society yaitu Yayasan Sadar Hati. </div>*/
                ?>
              </div>
            </div>
          </div>
          <?php
        }
      }
      ?>
      

    </div>
    <div class="clearfix"></div>
    <nav aria-label="page">
      <ul class="pagination justify-content-center">
        <?php echo isset($pagination)?$pagination:""; ?>
      </ul>
    </nav>
  </div>
</section>