<section class="article-detail">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <div class="article-item-detail">
            <div class="article-meta-detail">
                <div class="ar-date"><i class="far fa-clock"></i><?php echo isset($detail[0]['date_posted'])?date_min_format($detail[0]['date_posted']):''; ?></div>
                <div class="ar-author"><i class="far fa-user"></i>IPSPI</div>
            </div>
            <h1><?php echo isset($detail[0]['title'])?clean_str($detail[0]['title']):''; ?></h1>
            <div class="article-detail-img text-center">
              <img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Article Image" class="img-fluid" style="max-width: 50%;">
            </div>
            <article>
              <?php echo isset($detail[0]['body'])?clean_str($detail[0]['body']):''; ?>
            </article>
         </div>
        <?php 
        /*
        <div class="related-article">
          <div class="row">
            <div class="col-12">
              <div class="heading-alt">Lowongan kerja Lainnya</div>
            </div>
          </div>
          <div class="row">
            <!-- <div class="col-sm-4">
              <div class="related-list">
                <a href="<?php echo base_url(); ?>loker/loker1">
                  <img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker" class="img-fluid">
                  <div class="related-meta-date">21 Februari 2018</div>
                  <div class="related-meta-title">Menteri Sosial RI Dukung RUU Praktik Pekerjaan Sosial</div>
                </a>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="related-list">
                <a href="<?php echo base_url(); ?>loker/loker2">
                  <img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker" class="img-fluid">
                  <div class="related-meta-date">31 Januari 2018</div>
                  <div class="related-meta-title">Kunjungan Kerja Komisi VIII DPR RI diterima DPD IPSPI Jayapura</div>
                </a>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="related-list">
                <a href="<?php echo base_url(); ?>loker/loker3">
                  <img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker" class="img-fluid">
                  <div class="related-meta-date">01 Februari 2018</div>
                  <div class="related-meta-title">Praktik Pekerja Sosial dalam Kekasih Juara</div>
                </a>
              </div>
            </div> -->
          </div>
        </div>
        */
        ?>
        
        
      </div>
      <div class="col-lg-4">
        <div class="widget-container">
          <div class="widget-title">Lowongan Kerja Terbaru</div>
          <div class="widget-wrap">
            <?php 
            if (isset($loker_lainnya) && count($loker_lainnya) > 0) {
              foreach ($loker_lainnya as $key => $value) {
                ?>
                <div class="latest-item d-flex">
                  <div class="latest-loker-img">
                    <a href="<?php echo base_url(); ?>loker/read/<?php echo isset($value['slug'])?$value['slug']:''; ?>"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker"></a>
                  </div>
                  <div class="latest-item-meta">
                    <div class="meta-date"><?php echo isset($value['date_posted'])?date_min_format($value['date_posted']):''; ?></div>
                    <div class="meta-title">
                      <a href="<?php echo base_url(); ?>loker/read/<?php echo isset($value['slug'])?$value['slug']:''; ?>"><?php echo isset($value['title'])?clean_str($value['title']):''; ?></a>
                    </div>
                  </div>
                </div>
                <?php
              }
            }
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>