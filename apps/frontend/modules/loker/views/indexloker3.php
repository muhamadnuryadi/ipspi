  <script type="text/javascript">
function unduhFile(id) {
    $.ajax({
        url: '<?php echo base_url(); ?>downloads/checkFile/'+id,
        dataType: 'json',
        success: function(data) {
            if(data.is_exist == true) {
                window.open('<?php echo base_url(); ?>downloads/files/'+id,'_self');
            } else {
                alert('<?php echo $this->lang->line('file_not_exist'); ?>');
            }
        }
    }); return false;
}
function searchThis(){
  var frm = document.searchform;
  var name = frm.name.value;
  if(name == ''){ name = '-'; }
  window.location = "<?php echo base_url(); ?>berita/searchdata/"+name.replace(/\s/g,'%20');
}
</script>
<section class="ipspi-search">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <!-- <form class="form-inline form-search">
            <input class="form-control search-large" type="search" placeholder="Pencarian.." aria-label="Search">
            <button class="btn btn-search" type="submit">CARI</button>
          </form> -->
        </div>
      </div>
    </div>
  </section>
  
<section class="article-detail">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <div class="article-item-detail">
            <div class="article-meta-detail">
                <div class="ar-date"><i class="far fa-clock"></i>21 Agustus 2019</div>
                <div class="ar-author"><i class="far fa-user"></i>IPSPI</div>
            </div>
            <h1>Kementerian Sosial RI Membutuhkan 16.902 Orang untuk Pendamping PKH</h1>
            <div class="article-detail-img">
              <img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Article Image" class="img-fluid">
            </div>
            <article>
              <p>SRIPOKU.COM - Kementerian Sosial (Kemensos) Republik Indonesia bulan ini membuka pendaftaran rekrutmen pendamping Program Keluarga Harapan (PKH) yang menyediakan 16.092 lowongan.</p>
              <p>Kemensos membutuhkan banyak pendamping PKH baru pada 2018 sebab jumlah tenaga yang ada sekarang tidak bisa mengimbangi penambahan 4 juta peserta PKH.</p>
              <p>Data Kemensos mencatat lowongan 16.092 pendamping PKH itu terbagi dalam sejumlah kategori penempatan.</p>
              <p>Sebanyak 14.227 lowongan dibutuhkan untuk posisi Pendamping Sosial dan Pendamping Sosial PKH Akses.</p>
              <p>Lalu, posisi Pekerja Sosial Supervisor memerlukan 877 orang.</p>
              <p>Sementara untuk posisi Administrator Database, tersedia 607 lowongan.</p>
              <p>Terakhir, Kemensos membutuhkan Asisten Pendamping sebanyak 172 orang bagi Tenaga Kesejahteraan Sosial Kecamatan (TKSK).</p>
              <p>Selain itu, Kemensos juga akan merekrut koordinator kabupaten/kota sebanyak 193 orang, koordinator wilayah sebanyak 9 orang serta 7 orang koordinator regional.</p>
              <p>​​Jadwal pendaftaran online untuk rekrutmen ini telah resmi dibuka Kemensos sejak 9 Oktober 2017 dan ditutup pada 18 Oktober 2017 pukul 23.59 WIB.</p>
              <p>Berikut ini Daftar Kualifikasi Lowongan Pendamping PKH Kemensos 2017​.</p>
              <p> A. Pekerja Sosial Supervisor (877 orang) Keterangan: Berkedudukan di Kabupaten/Kota setempat Kualifikasi:</p>
              <ol>
                <li>Minimal D.IV/S1 Ilmu Kesejahteraan Sosial/Pekerja Sosial</li>
                <li>Diutamakan pengalaman menjadi pendamping sosial PKH denggn latar belakang pendidikan DIV/S1 Ilmu Kesejahteraan Sosial/Pekerja Sosial</li>
                <li>Mempunyai pengalaman praktek pekerjaan sosial</li>
                <li>Menguasai MS Office</li>
                <li>Usia maksimum 45 tahun.</li>
              </ol>
              <p> B. Pendamping Sosial (12.214 orang) Keterangan: Berkedudukan di Kecamatan setempat Kualifikasi:</p>
              <ol>
                <li>Pendidikan D.III/ D.IV/Sarjana Pekerjaan Sosial/Kesejahteraan Sosial/Sarjana di bidang ilmu-ilmu sosial terapan. Diutamakan yang pernah mengikuti pelatihan dan/atau berpengalaman praktek di bidang pendampingan sosial/fasilitator pemberdayaan masyarakat, rehabilitasi sosial, perlindungan sosial, jaminan sosial, dan program penanggulangan kemiskinan lainnya</li>
                <li>Diutamakan bertempat tinggal di wilayah kecamatan lokasi pelaksanaan PKH (sesuai alamat tinggal/domisili saat ini).</li>
                <li>Menguasai MS Office</li>
                <li>Usia maksimum 35 tahun.</li>
              </ol>
              <p> C. Pendamping Sosial PKH Akses (2.013 orang) Keterangan: Berkedudukan di Kecamatan Setempat Kualifikasi:</p>
              <ol>
                <li>Pendidikan minimal tingkat SMA, diutamakan lulusan SMK Pekerjaan Sosial (SMKPS), diutamakan yang pernah mengikuti pelatihan dan/atau pengalaman praktek pekerjaan sosial di berbagai bidang pelayanan kesejahteraan sosial dan tenaga kesejahteraan sosial kecamatan (TKSK) minimal 1 tahun</li>
                <li>Diutamakan bertempat tinggal di wilayah kecamatan lokasi pelaksanaan PKH (sesuai alamat tinggal/domisili saat ini)</li>
                <li>Mampu berkomunikasi dengan masyarakat kampung secara baik dan memahami karakteritik masyarakat serta adat istiadat setempat;</li>
                <li>Bersedia ditempatkan di distrik-distrik yang jauh dari ibukota kabupaten.</li>
                <li>Usia maksimum 45 tahun.</li>
              </ol>
              <p> D. Asisten Pendamping Sosial (172 orang) Keterangan: Berkedudukan di Kecamatan setempat Kualifikasi:</p>
              <ol>
                <li>Pengalaman Kerja sebagai Tenaga Kesejahteraan Sosial Kecamatan (TKSK) minimal 1 tahun dan memiliki pendidikan minimal SMA/Sederajat;</li>
                <li>Diutamakan bertempat tinggal di wilayah kecamatan lokasi pelaksanaan PKH (sesuai alamat tinggal/domisili saat ini)</li>
                <li>Mampu berkomunikasi dengan masyarakat kampung secara baik dan memahami karakteristik masyarakat serta adat istiadat setempat</li>
                <li>Usia maksimum 45 tahun.</li>
              </ol>
              <p> E. Asisten Pendamping Sosial (172 orang) Keterangan: Berkedudukan di Kecamatan setempat Kualifikasi:</p>
              <ol>
                <li>Pendidikan Diploma/Sarjana di bidang Ilmu Komputer/ Informatika/Statistika dan rumpun ilmu Sains dan Teknologi diutamakan yang pernah mengikuti pelatihan dan/atau pengalaman praktek di bidang komputer/ pengolahan data dan internet</li>
                <li>Diutamakan bertempat tinggal di wilayah kecamatan di kabupaten/kota lokasi pelaksanaan PKH (sesuai alamat tinggal/domisili saat ini)</li>
                <li>Usia maksimum 35 tahun.</li>
              </ol>
              <p> F. Koordinator Regional (7 orang) Keterangan: Berkedudukan di Jawa Timur, Nusa Tenggara Timur, Sulawesi Tenggara, Sulawesi Tengah, Maluku Utara, Papua dan Sumatera Utara Kualifikasi:</p>
              <ol>
                <li>Memiliki pengalaman kerja sebagai Koordinator Wilayah PKH minimal 2 tahun</li>
                <li>Memiliki pengalaman kerja sebagai Koordinator Kab/Kota PKH minimal 4 tahun</li>
                <li>Memiliki pengalaman kerja sebagai Operator PKH Provinsi/ Kab/Kota minimal 5 tahun</li>
                <li>Memiliki hasil evaluasi kinerja baik dan rekomendasi dari Dinas/Instansi Sosial Provinsi</li>
                <li> Membuat surat pernyataan:
                  <ul>
                    <li>Bersedia mengikuti proses seleksi Koordinator Regional yang diadakan oleh Direktorat Jaminan Sosial Keluarga;</li>
                    <li>Bersedia ditempatkan di seluruh regional;</li>
                    <li>Bersedia bekerja penuh waktu dan tidak terikat pekerjaan dengan pihak lain</li>
                  </ul>
                </li>
                <li>Berdomisili di lokasi yang membutuhkan Koordinator Regional.</li>
              </ol>
              <p> G. Koordinator Kabupaten/Kota (193 orang) Keterangan:​ ​Berkedudukan di Kab/Kota setempat</p>
              <ol>
                <li>Memiliki pengalaman kerja sebagai Pendamping Sosial minimal selama 5 tahun.</li>
                <li>Memiliki pengalaman kerja sebagai Operator minimal selama 5 tahun.</li>
                <li>Berkinerja baik berdasarkan penilaian kinerja Dinas Sosial Kabupaten/Kota dan belum pernah mendapatkan Surat Peringatan (SP) dalam 4 (empat) bulan terakhir.</li>
                <li> Membuat surat pernyataan:
                  <ul>
                    <li>Bersedia mengikuti proses seleksi Koordinator Kabupaten/kota yang diadakan oleh Direktorat Jaminan Sosial Keluarga;</li>
                    <li>Bersedia bekerja penuh waktu dan tidak terikat pekerjaan dengan pihak lain</li>
                  </ul>
                </li>
                <li>Mendapatkan rekomendasi dari Dinas Sosial Kabupaten/Kota.</li>
                <li>Berdomisili di kabupaten/Kota yang dibutuhkan.</li>
              </ol>
              <p> Adapun persyaratan ​Lengkap ​Pelamar Lowongan Pendamping PKH Kemensos 2017​​ ialah :</p>
              <ol>
                <li>Tidak berkedudukan sebagai CPNS/PNS/TNI/Polri;</li>
                <li>Siap dan bersedia bekerja purna waktu (full time);</li>
                <li>Bukan pengurus, anggota, dan atau berafiliasi Partai Politik (isi formulir pernyataan yang tersedia)</li>
                <li>Tidak pernah atau sedang tersangkut kasus hukum baik pidana maupun perdata, dibuktikan SKCK; </li>
                <li>Memiliki pendidikan sesuai prasyarat jabatan yang dibuktikan dengan ijasah terlegalisir;</li>
                <li>Usia miminal 19 tahun maksimal 45 (empat puluh lima) tahun pada bulan Oktober 2017;</li>
                <li>Bebas dari narkoba dan zat adiktif lainnya;</li>
                <li>Sehat jasmani dan rohani;</li>
                <li>Tidak terikat kontrak kerja dengan pihak lain;</li>
                <li>Bersedia menandatangani Pakta Integritas apabila terpilih;</li>
                <li>Mengikuti seluruh tahapan seleksi.</li>
              </ol>
              <p> Tata Cara Pendaftaran Lowongan Pendamping PKH Kemensos 2017 :</p>
              <ol>
                <li>Pendaftaran online pada 9-18 Oktober 2017</li>
                <li>Pendaftaran online dapat dilakukan melalui aplikasi berbasis android dengan nama Seleksi SDM PKH 2017 yang dapat diunduh di google play store.</li>
                <li>Pelamar melakukan login pada aplikasi “Seleksi SDM PKH 2017” dengan menggunakan NIK.</li>
                <li>Pelamar mengisi form pendaftaran secara lengkap yang terdiri dari; Data domisili, Data pendidikan Data pengalaman kerja, Data sertifikat (bila ada), Data penghargaan (bila ada).</li>
                <li>Pelamar mengunggah; Foto KTP asli dan atau surat keterangan domisili, Foto ijazah asli terakhir.</li>
              </ol>
            </article>
        </div>
        
        <div class="related-article">
          <div class="row">
            <div class="col-12">
              <div class="heading-alt">Lowongan kerja Lainnya</div>
            </div>
          </div>
          <div class="row">
            <!-- <div class="col-sm-4">
              <div class="related-list">
                <a href="<?php echo base_url(); ?>loker/loker1">
                  <img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker" class="img-fluid">
                  <div class="related-meta-date">21 Februari 2018</div>
                  <div class="related-meta-title">Menteri Sosial RI Dukung RUU Praktik Pekerjaan Sosial</div>
                </a>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="related-list">
                <a href="<?php echo base_url(); ?>loker/loker2">
                  <img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker" class="img-fluid">
                  <div class="related-meta-date">31 Januari 2018</div>
                  <div class="related-meta-title">Kunjungan Kerja Komisi VIII DPR RI diterima DPD IPSPI Jayapura</div>
                </a>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="related-list">
                <a href="<?php echo base_url(); ?>loker/loker3">
                  <img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker" class="img-fluid">
                  <div class="related-meta-date">01 Februari 2018</div>
                  <div class="related-meta-title">Praktik Pekerja Sosial dalam Kekasih Juara</div>
                </a>
              </div>
            </div> -->
          </div>
        </div>
        
      </div>
      <div class="col-lg-4">
        <div class="widget-container">
          <div class="widget-title">Lowongan Kerja Terbaru</div>
          <div class="widget-wrap">
            <div class="latest-item d-flex">
              <div class="latest-loker-img">
                <a href="<?php echo base_url(); ?>loker/loker1"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker"></a>
              </div>
              <div class="latest-item-meta">
                <div class="meta-date">21 Agustus 2019</div>
                <div class="meta-title">
                  <a href="<?php echo base_url(); ?>loker/loker1">Dicari 3 Orang untuk menjadi Konselor Adiksi dan 2 orang Pekerja Sosial</a>
                </div>
              </div>
            </div>
            <div class="latest-item d-flex">
              <div class="latest-loker-img">
                <a href="<?php echo base_url(); ?>loker/loker2"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker"></a>
              </div>
              <div class="latest-item-meta">
                <div class="meta-date">21 Agustus 2019</div>
                <div class="meta-title">
                  <a href="<?php echo base_url(); ?>loker/loker2">KEMENSOS RI membuka Rekrutmen Koordinator Tenaga Kesejahteraan Sosial (TKS)</a>
                </div>
              </div>
            </div>
            <div class="latest-item d-flex">
              <div class="latest-loker-img">
                <a href="<?php echo base_url(); ?>loker/loker3"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker"></a>
              </div>
              <div class="latest-item-meta">
                <div class="meta-date">21 Agustus 2019</div>
                <div class="meta-title">
                  <a href="<?php echo base_url(); ?>loker/loker3">Kementerian Sosial RI Membutuhkan 16.902 Orang untuk Pendamping PKH</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>