  <script type="text/javascript">
function unduhFile(id) {
    $.ajax({
        url: '<?php echo base_url(); ?>downloads/checkFile/'+id,
        dataType: 'json',
        success: function(data) {
            if(data.is_exist == true) {
                window.open('<?php echo base_url(); ?>downloads/files/'+id,'_self');
            } else {
                alert('<?php echo $this->lang->line('file_not_exist'); ?>');
            }
        }
    }); return false;
}
function searchThis(){
  var frm = document.searchform;
  var name = frm.name.value;
  if(name == ''){ name = '-'; }
  window.location = "<?php echo base_url(); ?>berita/searchdata/"+name.replace(/\s/g,'%20');
}
</script>
<section class="ipspi-search">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <!-- <form class="form-inline form-search">
          <input class="form-control search-large" type="search" placeholder="Pencarian.." aria-label="Search">
          <button class="btn btn-search" type="submit">CARI</button>
        </form> -->
      </div>
    </div>
  </div>
</section>
  
<section class="article-detail">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <div class="article-item-detail">
            <div class="article-meta-detail">
                <div class="ar-date"><i class="far fa-clock"></i>21 Agustus 2019</div>
                <div class="ar-author"><i class="far fa-user"></i>IPSPI</div>
            </div>
            <h1>KEMENSOS RI membuka Rekrutmen Koordinator Tenaga Kesejahteraan Sosial (TKS)</h1>
            <div class="article-detail-img">
              <img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Article Image" class="img-fluid">
            </div>
            <article>
              <p>Kementrian Sosial Republik Indonesia membuka kesempatan bagi Warga Negara Republik Indonesia yang memiliki integritas dan komitmen tinggi untuk menjadi Koordinator Tenaga Kesejahteraan Sosial Kabupaten/Kota di seluruh Wilayah Indonesia (514 Kabupaten/Kota)</p>
              <p>A. Posisi yang akan diisi melalui seleksi online adalah Koordinator Tenaga Kesejahteraan sosial Kabupaten/Kota, formasi di 514 Kabupaten/Kota seluruh wilayah Indoneisa.</p>
              <p>B. Persyaratan Pendaftaran :</p>
              <ol>
                <li>Pendidikan minimal DIV/S1 Pekerjaan Sosial/Kesejateraan Sosial, Sarjana Bidang Ilmu-ilmu Sosial Terapan yang dibuktikan dengan foto copy Ijazah dilegalisir.</li>
                <li>Mendaftar di kabupaten/Kota sesuai domisili.</li>
                <li>Mampu menggunakan MS Office dan Internet.</li>
                <li>Usia minimal 25Tahun, Maksimal 40 Tahun pada bulan Oktober 2017.</li>
                <li>Di utamakan Tenaga Kesejahteraan Sosial Kecamatan atau Pendamping sosial yang:
                  <ul>
                    <li> Berkinerja baik berdasarkan penilaian kinerja dari DInas Sosial Kabupaten/Kota</li>
                    <li>belum pernah mendapatkan surat Peringatan dalam empat bulan terakhir</li>
                    <li>Mendapatkan rekomendasi dari kepala Dinas soaial kabupaten/Kota</li>
                  </ul>
                </li>
                <li>Bukan CPNS/PNS/TNI/POLRI</li>
                <li>BUkan pengurus, anggota, dan atau berafilisiasi parti politik</li>
                <li>Tidak pernah atau tidak sedang tersangkut kasus hukum pidana yang di buktikan dengan SKCK.</li>
                <li>Bebas dari narkoba dan zat adiktif lainya.</li>
                <li>Tidak terikat kontrak kerja dengan pihak lain</li>
                <li>Membuat surat pernyataan:
                  <ul>
                    <li>Bersedia mengikti proses seleksi koordinator tenada kesejahteraan sosial Kabupaten/Kota yang diadakan oleh panitia Seleksi Kementrian Sosial RI</li>
                    <li>Bersedia bekejra penuh waktu dan tidak terkait dengan pihak lain</li>
                    <li>Bersedia menandatangani pakta integritas apabila terpilih</li>
                  </ul>
                </li>
              </ol>
              <p>C Tata Cara Pendaftaran:</p>
              <ol>
                <li>Pendaftran onlinr dibuka pada tanggal 12 Oktober 2017 pukul 09.00 WIB dan ditutup pada tanggal 18 Oktober 2017 pukul 23.59 WIB</li>
                <li>Pendaftarandapat dilakukan melalui aplikasi berbaiss andriod dengan nama Seleksi SDM PKH 2017 yang dapat di unduh / di updaate melalui Google Playstore mulai tanggal 12 oktober 2017.</li>
                <li>Pelamar melakukan login pada alikasi "Seleksi SDM PKH 2017" dengan menggunakan NIK</li>
                <li>Pelamar mengisi form pendaftaran secara lngkap terdiri dari:
                  <ul>
                    <li>Data Domisili</li>
                    <li>Data pendidikan terakhir</li>
                    <li>Data pengalaman kerja</li>
                    <li>Data Sertifikat</li>
                    <li>Data penghargaan</li>
                  </ul>
                </li>
                <li>Pelamar mengunggah :
                  <ul>
                    <li>Foto KTP asli dan atau surat keterangan domisili</li>
                    <li>Foto ijazah asli terakhir dilegalisir</li>
                  </ul>
                </li>
              </ol>
              <p>D. Tahapan Seleksi :</p>
              <li>Seleksi Administrasi adalah seleksi yang dilakukan berdasarkan hasil verifikasidokumen pelamar yang telah diunggah</li>
              <li>Seleksi Kompetensi bidang menggunakan metode tes tertulis. atempat dan waktu akan di informasikan kemudian. Seluruh peserta seleksi wajibmembawa papan jalan dan pensil 2B.</li>
              <p>Keyentuan Kelulusan:</p>
              <ol>
                <li>Seleksi administasi: ketentuan seleksi administrasi dilakukan berdasarkan hasil verifikasi dokumen</li>
                <li>Seleksi kompetensi bidang pelamar dinyatakan lulus kompetensi bidang jika memenuhi syarat minimal kelulusan bidang.</li>
              </ol>
              <p>E. Pengumuman kelulusan:</p>
              <ol>
                <li>pengumuman hasil seleksi administrasi akan dikonfirmasikan melalui email dan notifikasi epada masing- masing pelamarr yang dinyatakan lulus.</li>
                <li>Peserts yang dinyatakan lulus seleksi adminsitasi akan dipanggil mengikuti tahap selanjutnya, untuk mengikuti seleksi kompetensi Bidang dengan menunjukan bukti kelulusan, berupa email dan nitifikasi dan membawa seluruh berkas diunggah.</li>
                <li>Peserta yang di nyatakan lulus seleksi kompetensi bidang akan diinformasikan melalui email dan notifikasi.</li>
              </ol>
              <p>F. Ketentuan Lain</p>
              <ol>
                <li>Bagi pelamar yang tidak memenuhi persyaratan agar tidak mengajukan lamaran</li>
                <li>Berkas lamaran yang ltelah masuk menjadi milik panitia seleksi kementrian sosial RI</li>
                <li>Panitia seleksu kementrian sosial RI hanya memprose berkas plemar yang telah lengkap</li>
                <li>Seluruh proses seleksi koordintaor tenaga kesejeahteraan sosial kabupaten/Kota tisak di pungut biaya apapun</li>
                <li>Panitia seleksi kementrian sosial RI tidak bertanggung jawab atas pgutuan atau tawaran serupa oleh oknum-oknum yang mengatasnamakan Kementrian sosial RI atau Panitia seleksi</li>
                <li>Apabia di kemudian  hari di ketahui pelamar memeberikan data/keterangan tidak benar, maka panitia seleksi kementrian sosial RI erhak membatalkan hasil seleksi.</li>
                <li>Kelalain akiat tidak mengikuti perkembangan informasi menjadi tanngung jawab pelamr</li>
                <li>Seluruh keputusan panitia Seleksi kemntrian osial RI adalah mutlak dan tidak dapa diganggu gugat.</li>
              </ol>
            </article>
         </div>
        
        <div class="related-article">
          <div class="row">
            <div class="col-12">
              <div class="heading-alt">Lowongan kerja Lainnya</div>
            </div>
          </div>
          <div class="row">
            <!-- <div class="col-sm-4">
              <div class="related-list">
                <a href="<?php echo base_url(); ?>loker/loker1">
                  <img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker" class="img-fluid">
                  <div class="related-meta-date">21 Februari 2018</div>
                  <div class="related-meta-title">Menteri Sosial RI Dukung RUU Praktik Pekerjaan Sosial</div>
                </a>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="related-list">
                <a href="<?php echo base_url(); ?>loker/loker2">
                  <img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker" class="img-fluid">
                  <div class="related-meta-date">31 Januari 2018</div>
                  <div class="related-meta-title">Kunjungan Kerja Komisi VIII DPR RI diterima DPD IPSPI Jayapura</div>
                </a>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="related-list">
                <a href="<?php echo base_url(); ?>loker/loker3">
                  <img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker" class="img-fluid">
                  <div class="related-meta-date">01 Februari 2018</div>
                  <div class="related-meta-title">Praktik Pekerja Sosial dalam Kekasih Juara</div>
                </a>
              </div>
            </div> -->
          </div>
        </div>
        
      </div>
      <div class="col-lg-4">
        <div class="widget-container">
          <div class="widget-title">Lowongan Kerja Terbaru</div>
          <div class="widget-wrap">
            <div class="latest-item d-flex">
              <div class="latest-loker-img">
                <a href="<?php echo base_url(); ?>loker/loker1"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker"></a>
              </div>
              <div class="latest-item-meta">
                <div class="meta-date">21 Agustus 2019</div>
                <div class="meta-title">
                  <a href="<?php echo base_url(); ?>loker/loker1">Dicari 3 Orang untuk menjadi Konselor Adiksi dan 2 orang Pekerja Sosial</a>
                </div>
              </div>
            </div>
            <div class="latest-item d-flex">
              <div class="latest-loker-img">
                <a href="<?php echo base_url(); ?>loker/loker2"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker"></a>
              </div>
              <div class="latest-item-meta">
                <div class="meta-date">21 Agustus 2019</div>
                <div class="meta-title">
                  <a href="<?php echo base_url(); ?>loker/loker2">KEMENSOS RI membuka Rekrutmen Koordinator Tenaga Kesejahteraan Sosial (TKS)</a>
                </div>
              </div>
            </div>
            <div class="latest-item d-flex">
              <div class="latest-loker-img">
                <a href="<?php echo base_url(); ?>loker/loker3"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker"></a>
              </div>
              <div class="latest-item-meta">
                <div class="meta-date">21 Agustus 2019</div>
                <div class="meta-title">
                  <a href="<?php echo base_url(); ?>loker/loker3">Kementerian Sosial RI Membutuhkan 16.902 Orang untuk Pendamping PKH</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>