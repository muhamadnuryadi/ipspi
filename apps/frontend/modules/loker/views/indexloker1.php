  <script type="text/javascript">
function unduhFile(id) {
    $.ajax({
        url: '<?php echo base_url(); ?>downloads/checkFile/'+id,
        dataType: 'json',
        success: function(data) {
            if(data.is_exist == true) {
                window.open('<?php echo base_url(); ?>downloads/files/'+id,'_self');
            } else {
                alert('<?php echo $this->lang->line('file_not_exist'); ?>');
            }
        }
    }); return false;
}
function searchThis(){
  var frm = document.searchform;
  var name = frm.name.value;
  if(name == ''){ name = '-'; }
  window.location = "<?php echo base_url(); ?>berita/searchdata/"+name.replace(/\s/g,'%20');
}
</script>
<section class="ipspi-search">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <!-- <form class="form-inline form-search">
            <input class="form-control search-large" type="search" placeholder="Pencarian.." aria-label="Search">
            <button class="btn btn-search" type="submit">CARI</button>
          </form> -->
        </div>
      </div>
    </div>
  </section>
  
<section class="article-detail">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <div class="article-item-detail">
            <div class="article-meta-detail">
                <div class="ar-date"><i class="far fa-clock"></i>16 Agustus 2019</div>
                <div class="ar-author"><i class="far fa-user"></i>IPSPI</div>
            </div>
            <h1>Dicari 3 Orang untuk menjadi Konselor Adiksi dan 2 orang Pekerja Sosial</h1>
            <div class="article-detail-img">
              <img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Article Image" class="img-fluid">
            </div>
            <article>
              <p>Membuka lamaran pekerjaan menjadi Konselor Adiksi (3 orang)dan Pekerja Sosial (2 orang) yang akan bertugas di LSM yg selama ini bergerak di isu HIV/AIDS, public policy, narkoba dan development community / society yaitu Yayasan Sadar Hati. </p>
              <p>Kelengkapan administrasi:</p>
              <ol>
                <li>Curriculum Vitae (CV)</li>
                <li>Ijazah Terakhir. Minimal SLTA (fc)</li>
                <li>Sertifikat Pelatihan Napza (OJT) atau keterangan dari lembaga (fc)</li>
                <li>Foto 3x4 = 3</li>
                <li>Foto copy KTP</li>
                <li>Rekening BRI (fc)</li>
                <li>NPWP (fc) (fc)</li>
                <li>Surat keterangan sehat (non komunitas)</li>
                <li>SKCK (non komunitas)</li>
              </ol>
              <p>Program tersebut dicanangkan dengan tujuan untuk merespon semakin meluasnya peristiwa kekerasan yang dialami oleh perempuan dan anak. Dalam hal ini, KPPPA tengah giat menggalang partisipasi kepada seluruh komponen masyarakat untuk bersama-sama mendukung pelaksanaan program pemberdayaan perempuan dan perlindungan anak.</p>
              <p>Lamaran ditujukan kepada Direktur RSKP Napza. Dikumpulkan oleh Sadar Hati. </p>
              <p>Untuk pengajuan lamaran bisa datang ke kantor Yayasan Sadar Hati, Jl. Kunta Bhaswara 2, No 27 - Malang</p>
              <p>No telp: 0341- 357498</p>
              <p>Contak person:</p>
              <p>Ibnu Satar : 087864817072</p>
              <p>Rizal : 085733081727</p>
            </article>
         </div>
        
        <div class="related-article">
          <div class="row">
            <div class="col-12">
              <div class="heading-alt">Lowongan kerja Lainnya</div>
            </div>
          </div>
          <div class="row">
            <!-- <div class="col-sm-4">
              <div class="related-list">
                <a href="<?php echo base_url(); ?>loker/loker1">
                  <img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker" class="img-fluid">
                  <div class="related-meta-date">21 Februari 2018</div>
                  <div class="related-meta-title">Menteri Sosial RI Dukung RUU Praktik Pekerjaan Sosial</div>
                </a>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="related-list">
                <a href="<?php echo base_url(); ?>loker/loker2">
                  <img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker" class="img-fluid">
                  <div class="related-meta-date">31 Januari 2018</div>
                  <div class="related-meta-title">Kunjungan Kerja Komisi VIII DPR RI diterima DPD IPSPI Jayapura</div>
                </a>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="related-list">
                <a href="<?php echo base_url(); ?>loker/loker3">
                  <img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker" class="img-fluid">
                  <div class="related-meta-date">01 Februari 2018</div>
                  <div class="related-meta-title">Praktik Pekerja Sosial dalam Kekasih Juara</div>
                </a>
              </div>
            </div> -->
          </div>
        </div>
        
      </div>
      <div class="col-lg-4">
        <div class="widget-container">
          <div class="widget-title">Lowongan Kerja Terbaru</div>
          <div class="widget-wrap">
            <div class="latest-item d-flex">
              <div class="latest-loker-img">
                <a href="<?php echo base_url(); ?>loker/loker1"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker"></a>
              </div>
              <div class="latest-item-meta">
                <div class="meta-date">21 Agustus 2019</div>
                <div class="meta-title">
                  <a href="<?php echo base_url(); ?>loker/loker1">Dicari 3 Orang untuk menjadi Konselor Adiksi dan 2 orang Pekerja Sosial</a>
                </div>
              </div>
            </div>
            <div class="latest-item d-flex">
              <div class="latest-loker-img">
                <a href="<?php echo base_url(); ?>loker/loker2"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker"></a>
              </div>
              <div class="latest-item-meta">
                <div class="meta-date">21 Agustus 2019</div>
                <div class="meta-title">
                  <a href="<?php echo base_url(); ?>loker/loker1">KEMENSOS RI membuka Rekrutmen Koordinator Tenaga Kesejahteraan Sosial (TKS)</a>
                </div>
              </div>
            </div>
            <div class="latest-item d-flex">
              <div class="latest-loker-img">
                <a href="<?php echo base_url(); ?>loker/loker3"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/featured-img.jpg" alt="Loker"></a>
              </div>
              <div class="latest-item-meta">
                <div class="meta-date">21 Agustus 2019</div>
                <div class="meta-title">
                  <a href="<?php echo base_url(); ?>loker/loker3">Kementerian Sosial RI Membutuhkan 16.902 Orang untuk Pendamping PKH</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>