<?php
class Jurnal extends CI_Controller{
    var $webconfig;
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->sharedModel('JurnalModel');

        $this->load->sharedModel('AboutModel');
        $this->about = $this->AboutModel->listData();
        $this->load->sharedModel('InformasiModel');
        $this->informasi = $this->InformasiModel->listData();
        $this->load->sharedModel('ConfigurationModel');
        $this->configuration = $this->ConfigurationModel->listData();
        $this->load->sharedModel('RegulationModel');
        $this->load->sharedModel('WilayahModel');
        $this->regulation = $this->RegulationModel->listData();

        $this->webconfig = $this->config->item('webconfig');
        $this->module_name = $this->lang->line('home');
    }
    
    function index($page = 0){
        redirect(base_url().'jurnal/listing');
    }

    function listing($page = 0){
        $listdata = array();
        $listdata['listprovinsi'] = $this->WilayahModel->listpropinsi();

        $all_data = $this->JurnalModel->listDataCount();
        $cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = 10;
        $cfg_pg ['uri_segment'] = 3;

        $cfg_pg['first_link'] = '&laquo;';
        $cfg_pg['first_tag_close'] = '</li>';
        $cfg_pg['first_tag_open'] = '<li class="page-item page-link">';

        $cfg_pg['last_link'] = '&raquo;';
        $cfg_pg['last_tag_open'] = '<li class="page-item page-link">';
        $cfg_pg['last_tag_close'] = '</li>';

        $cfg_pg['next_link'] = '&gt;';
        $cfg_pg['next_tag_open'] = '<li class="page-item page-link">';
        $cfg_pg['next_tag_close'] = '</li>';

        $cfg_pg['prev_link'] = '&lt;';
        $cfg_pg['prev_tag_open'] = '<li class="page-item page-link">';
        $cfg_pg['prev_tag_close'] = '</li>';

        $cfg_pg['num_tag_open'] = '<li class="page-item page-link">';
        $cfg_pg['num_tag_close'] = '</li>';

        $cfg_pg['cur_tag_open'] = '<li class="page-item page-link active"><span>';
        $cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
        $listdata["pagination"] = $this->pagination->create_links();

        $start_no = 0;
        if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
            $start_no = $this->uri->segment($cfg_pg ['uri_segment']);
        }

        $listdata["start_no"] = $start_no;

        $start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
        $start_idx = $start_idx < 0?0:$start_idx;

        $listdata['lists'] = $this->JurnalModel->listData(array(
                                                                'start'  => $start_idx
                                                                ,'limit' => $cfg_pg['per_page']
                                                            ));

        $ldata['content'] = $this->load->view($this->router->class.'/index',$listdata, true);
        $this->load->sharedView('template', $ldata);
    }

    function getAllData($page = 0){
        
        $all_data = $this->JurnalModel->listDataCount();
        $cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = 9;
        $cfg_pg ['uri_segment'] = 3;

        $cfg_pg['first_link'] = '&laquo;';
        $cfg_pg['first_tag_close'] = '</li>';
        $cfg_pg['first_tag_open'] = '<li>';

        $cfg_pg['last_link'] = '&raquo;';
        $cfg_pg['last_tag_open'] = '<li>';
        $cfg_pg['last_tag_close'] = '</li>';

        $cfg_pg['next_link'] = '&gt;';
        $cfg_pg['next_tag_open'] = '<li>';
        $cfg_pg['next_tag_close'] = '</li>';

        $cfg_pg['prev_link'] = '&lt;';
        $cfg_pg['prev_tag_open'] = '<li>';
        $cfg_pg['prev_tag_close'] = '</li>';

        $cfg_pg['num_tag_open'] = '<li>';
        $cfg_pg['num_tag_close'] = '</li>';

        $cfg_pg['cur_tag_open'] = '<li class="active"><span>';
        $cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
        $listdata["pagination"] = $this->pagination->create_links();

        $start_no = 0;
        if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
            $start_no = $this->uri->segment($cfg_pg ['uri_segment']);
        }

        $listdata["start_no"] = $start_no;

        $start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
        $start_idx = $start_idx < 0?0:$start_idx;

        $listdata['lists'] = $this->JurnalModel->listData(array(
                                                                'start'  => $start_idx
                                                                ,'limit' => $cfg_pg['per_page']
                                                            ));
        
        $this->load->view($this->router->class.'/list', $listdata);
    }
    function searchdata($judul = 'judul',$penulis = '',$id_provinsi = '',$tahun = ''){
        if($judul == '-'){$judul = '';}
        if($penulis == '-'){$penulis = '';}
        if($id_provinsi == '-'){$id_provinsi = '';}
        if($tahun == '-'){$tahun = '';}
        $listdata['listprovinsi'] = $this->WilayahModel->listpropinsi();
        # PAGINATION #
        $all_data = $this->JurnalModel->filterDataCount(array(
                                                    'judul'=>trim(urldecode($judul))
                                                    ,'penulis'=>trim(urldecode($penulis))
                                                    ,'id_provinsi'=>trim(urldecode($id_provinsi))
                                                    ,'tahun'=>trim(urldecode($tahun))
                                                    ));
        $cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/".$this->uri->segment(3)."/"."/".$this->uri->segment(4)."/"."/".$this->uri->segment(5)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = 1;
        $cfg_pg ['uri_segment'] = 6;

        $cfg_pg['first_link'] = '&laquo;';
        $cfg_pg['first_tag_close'] = '</li>';
        $cfg_pg['first_tag_open'] = '<li>';

        $cfg_pg['last_link'] = '&raquo;';
        $cfg_pg['last_tag_open'] = '<li>';
        $cfg_pg['last_tag_close'] = '</li>';

        $cfg_pg['next_link'] = '&gt;';
        $cfg_pg['next_tag_open'] = '<li>';
        $cfg_pg['next_tag_close'] = '</li>';

        $cfg_pg['prev_link'] = '&lt;';
        $cfg_pg['prev_tag_open'] = '<li>';
        $cfg_pg['prev_tag_close'] = '</li>';

        $cfg_pg['num_tag_open'] = '<li>';
        $cfg_pg['num_tag_close'] = '</li>';

        $cfg_pg['cur_tag_open'] = '<li class="active"><span>';
        $cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
        $listdata["pagination"] = $this->pagination->create_links();

        $start_no = 0;
        if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
            $start_no = $this->uri->segment($cfg_pg ['uri_segment']);
        }

        $listdata["start_no"] = $start_no;

        $start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
        $start_idx = $start_idx < 0?0:$start_idx;

        $listdata['lists'] = $this->JurnalModel->filterData(array(
                                                                'start'  => $start_idx
                                                                ,'limit' => $cfg_pg['per_page']
                                                                ,'judul' => trim(urldecode($judul))
                                                                ,'penulis'=>trim(urldecode($penulis))
                                                                ,'tahun'=>trim(urldecode($tahun))
                                                                ,'id_provinsi'=>trim(urldecode($id_provinsi))
                                                            ));

        
        $ldata['content'] = $this->load->view($this->router->class.'/index',$listdata, true);
        $this->load->sharedView('template', $ldata);
    }
    
    function read($id = '0') {
        $tdata = array();
        $tdata['lists'] = $this->JurnalModel->listData(array('id' => $id));
        
        $ldata['content'] = $this->load->view($this->router->class.'/detail',$tdata, true);
        $this->load->sharedView('template', $ldata);
    }
}