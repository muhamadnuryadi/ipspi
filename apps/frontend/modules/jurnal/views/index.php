<script type="text/javascript">
  jQuery(document).ready(function($) {
    // jQuery("#listData").load('<?php echo base_url().$this->router->class; ?>/getAllData/');
  });
  function searchThis(){
    var frm = document.searchform;
    var judul = frm.judul.value;
    var penulis = frm.penulis.value;
    var id_provinsi = frm.id_provinsi.value;
    var tahun = frm.tahun.value;
    if(judul == ''){ judul = '-'; }
    if(penulis == ''){ penulis = '-'; }
    if(id_provinsi == ''){ id_provinsi = '-'; }
    if(tahun == ''){ tahun = '-'; }
    window.location = "<?php echo base_url().$this->router->class; ?>/searchdata/"+encode_js(judul.replace(/\s/g,'%20'))+'/'+encode_js(penulis.replace(/\s/g,'%20'))+'/'+id_provinsi.replace(/\s/g,'%20')+'/'+tahun.replace(/\s/g,'%20');
  }
  function resetThis(){
    var frm = document.searchform;
    var judul = frm.judul.value;
    var penulis = frm.penulis.value;
    var id_provinsi = frm.id_provinsi.value;
    var tahun = frm.tahun.value;
    
    judul = '-';
    penulis = '-';
    id_provinsi = '-';
    tahun = '-';
    window.location = "<?php echo base_url().$this->router->class; ?>/searchdata/"+encode_js(judul.replace(/\s/g,'%20'))+'/'+encode_js(penulis.replace(/\s/g,'%20'))+'/'+id_provinsi.replace(/\s/g,'%20')+'/'+tahun.replace(/\s/g,'%20');
  }
</script>
<section class="head-wrap">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1 class="head-title">Jurnal</h1>
      </div>
    </div>
  </div>
</section>

<section class="ipspi-search">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <form name="searchform" class="form form-search" action="" method="post" onsubmit="searchThis();return false;">
          <div class="row half-gutters">
            <div class="col-lg-3 col-md-3 col-6">
              <input name="judul" type="text" class="form-control search-large" placeholder="Judul Jurnal">
            </div>
            <div class="col-lg-3 col-md-3 col-6">
              <input name="penulis" type="text" class="form-control search-large" placeholder="Nama Penulis">
            </div>
            <div class="col-lg-3 col-md-2 col-6">
              <select name="id_provinsi" class="form-control search-large">
                  <option value="">-- Pilih DPD --</option>
                  <?php 
                    foreach ($this->DpdModel->listData() as $key => $value) {
                        ?>
                        <option value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
                        <?php
                    }
                  ?>
              </select>
            </div>
            <div class="col-lg-2 col-md-2 col-6">
              <select name="tahun" class="form-control search-large">
                <option value="">Tahun Publikasi</option>
                <?php 
                for ($i=2014; $i <= date('Y') ; $i++) { 
                    ?><option value="<?php echo $i ?>"><?php echo $i ?></option> <?php
                }
                ?>
              </select>
            </div>
            <div class="col-lg-1 col-md-2 col-12">
              <button class="btn btn-green btn-block" onclick="searchThis();return false;" type="submit">CARI</button>
            </div>
            <!-- <div class="col-lg-1 col-md-2 col-12">
              <button class="btn btn-green btn-block" onclick="resetThis();return false;" type="submit">RESET</button>
            </div> -->
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

<div id="listData">
    <section class="content">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="content-wrapper">
                <table class="table table-responsive jurnal-list">
                  <thead>
                    <tr>
                      <th style="width: 35%">Judul Jurnal</th>
                      <th style="width: 16%">Nama Penulis</th>
                      <th style="width: 15%">DPD</th>
                      <th style="width: 10%">Tahun Publikasi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(isset($lists) && count($lists) > 0) {?>
                    <?php $i = $start_no; foreach ($lists as $listdata) { $i++; ?>
                    <tr>
                      <td>
                        <div class="jurnal-list-title">
                          <a href="<?php echo base_url().'jurnal/read/'.$listdata['id'].'/'.$listdata['slug']; ?>"><?php echo $listdata['title'];?></a>
                        </div>
                        <div class="jurnal-list-desc"><?php echo stripcslashes($listdata['description']);?></div>
                      </td>
                      <td><?php echo isset($listdata['id_user'])?usersdata($listdata['id_user']):'' ;?></td>
                      <td><?php echo isset($listdata['provinsi']['nama'])?$listdata['provinsi']['nama']:''; ?></td>
                      <td><?php echo date('d/m/Y', strtotime($listdata['datepublish']));?></td>
                    </tr>
                    <?php } ?>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              
              <nav aria-label="page" class="mt-5">
                <ul class="pagination justify-content-center">
                    <?php echo isset($pagination)?$pagination:""; ?>
                </ul>
                
                <!-- <ul class="pagination justify-content-center">
                  <li class="page-item disabled">
                    <a class="page-link" href="#" aria-label="Previous">
                      <span aria-hidden="true">&laquo;</span>
                      <span class="sr-only">Previous</span>
                    </a>
                  </li>
                  <li class="page-item active"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">100</a></li>
                  <li class="page-item">
                    <a class="page-link" href="#" aria-label="Next">
                      <span aria-hidden="true">&raquo;</span>
                      <span class="sr-only">Next</span>
                    </a>
                  </li>
                </ul> -->
              </nav>
              
            </div>
          </div>
        </div>
      </section>
</div>