<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="content-wrapper">
          <table class="table table-responsive jurnal-list">
            <thead>
              <tr>
                <th style="width: 50%">Judul Jurnal</th>
                <th style="width: 15%">Nama Penulis</th>
                <th style="width: 20%">DPD</th>
                <th style="width: 15%">Tahun Publikasi</th>
              </tr>
            </thead>
            <tbody>
              <?php if(isset($lists) && count($lists) > 0) {?>
              <?php $i = $start_no; foreach ($lists as $listdata) { $i++; ?>
              <tr>
                <td>
                  <div class="jurnal-list-title">
                    <a href="<?php echo base_url().'jurnal/read/'.$listdata['id'].'/'.$listdata['slug']; ?>"><?php echo $listdata['title'];?></a>
                  </div>
                  <div class="jurnal-list-desc"><?php echo stripcslashes($listdata['description']);?></div>
                </td>
                <td>Admin</td>
                <td>DPD IPSPI Jawa Barat</td>
                <td><?php echo $listdata['datepublish'];?></td>
              </tr>
              <?php } ?>
              <?php } ?>
            </tbody>
          </table>
        </div>
        
        <nav aria-label="page" class="mt-5">
          <?php echo isset($pagination)?$pagination:""; ?>
          <!-- <ul class="pagination justify-content-center">
            <li class="page-item disabled">
              <a class="page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
              </a>
            </li>
            <li class="page-item active"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">100</a></li>
            <li class="page-item">
              <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
              </a>
            </li>
          </ul> -->
        </nav>
        
      </div>
    </div>
  </div>
</section>