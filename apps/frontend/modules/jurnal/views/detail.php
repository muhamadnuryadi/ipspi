<script type="text/javascript">
  jQuery(document).ready(function($) {
    // jQuery("#listData").load('<?php echo base_url().$this->router->class; ?>/getAllData/');
  });
  function searchThis(){
    var frm = document.searchform;
    var judul = frm.judul.value;
    var penulis = frm.penulis.value;
    var id_provinsi = frm.id_provinsi.value;
    var tahun = frm.tahun.value;
    if(judul == ''){ judul = '-'; }
    if(penulis == ''){ penulis = '-'; }
    if(id_provinsi == ''){ id_provinsi = '-'; }
    if(tahun == ''){ tahun = '-'; }
    window.location = "<?php echo base_url().$this->router->class; ?>/searchdata/"+judul.replace(/\s/g,'%20')+'/'+penulis.replace(/\s/g,'%20')+'/'+id_provinsi.replace(/\s/g,'%20')+'/'+tahun.replace(/\s/g,'%20');
  }
  function resetThis(){
    var frm = document.searchform;
    var judul = frm.judul.value;
    var penulis = frm.penulis.value;
    var id_provinsi = frm.id_provinsi.value;
    var tahun = frm.tahun.value;
    
    judul = '-';
    penulis = '-';
    id_provinsi = '-';
    tahun = '-';
    window.location = "<?php echo base_url().$this->router->class; ?>/searchdata/"+judul.replace(/\s/g,'%20')+'/'+penulis.replace(/\s/g,'%20')+'/'+id_provinsi.replace(/\s/g,'%20')+'/'+tahun.replace(/\s/g,'%20');
  }
</script>
<section class="head-wrap">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1 class="head-title">Jurnal</h1>
      </div>
    </div>
  </div>
</section>
<section class="ipspi-search">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <form name="searchform" class="form form-search" action="" method="post" onsubmit="searchThis();return false;">
          <div class="row half-gutters">
            <div class="col-lg-2 col-md-3 col-6">
              <input name="judul" type="text" class="form-control search-large" placeholder="Judul Jurnal">
            </div>
            <div class="col-lg-3 col-md-3 col-6">
              <input name="penulis" type="text" class="form-control search-large" placeholder="Nama Penulis">
            </div>
            <div class="col-lg-3 col-md-2 col-6">
              <select name="id_provinsi" class="form-control search-large">
                  <option value="">-- Pilih DPD --</option>
                  <?php 
                      foreach ($this->DpdModel->listData() as $key => $value) {
                          ?>
                          <option value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
                          <?php
                      }
                  ?>
              </select>
            </div>
            <div class="col-lg-2 col-md-2 col-6">
              <select name="tahun" class="form-control search-large">
                <option value="">Tahun Publikasi</option>
                <?php 
                for ($i=2014; $i <= date('Y') ; $i++) { 
                    ?><option value="<?php echo $i ?>"><?php echo $i ?></option> <?php
                }
                ?>
              </select>
            </div>
            <div class="col-lg-1 col-md-2 col-12">
              <button class="btn btn-green btn-block" onclick="searchThis();return false;" type="submit">CARI</button>
            </div>
            <div class="col-lg-1 col-md-2 col-12">
              <button class="btn btn-green btn-block" onclick="resetThis();return false;" type="submit">RESET</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="content-wrapper p-5">
          <h3><?php echo stripcslashes($lists[0]['title']);?></h3>
          <div class="jurnal-meta">
            <div class="jurnal-avatar">
              <img src="<?php echo $this->webconfig['frontend_template']; ?>images/default-avatar.png" alt="default-avatar">
            </div>
            <span><?php echo usersdata($lists[0]['id_user']);?></span>
            <span>IPSPI DPD Provinsi <?php echo $lists[0]['provinsi']['nama'];?></span>
          </div>
          <hr class="line mt-4">
          <p>Tanggal Publikasi : <?php echo date('d/m/Y', strtotime($lists[0]['datepublish']));?></p>
          <p><strong>ABSTRAK</strong></p>
          <p><?php echo stripcslashes($lists[0]['body']);?></p>
        </div>
        
        <div class="viewer mt-5">
          <object data="<?php echo $this->webconfig['media-server-files'].$lists[0]['files'];?>" type="application/pdf" width="100%" height="600px">
            <iframe width="100%" height="600" name="iframe_a" src="<?php echo $this->webconfig['media-server-files'].$lists[0]['files'];?>">
              This browser does not support PDFs. Please download the PDF to view it: <a href="<?php echo $this->webconfig['media-server-files'].$lists[0]['files'];?>">Download PDF</a>
            </iframe>
          </object>
        </div>
          
      </div>
    </div>
  </div>
</section>

