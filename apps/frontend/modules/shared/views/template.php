<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <?php 
  $meta_title = isset($this->configuration['site_title'])?stripslashes(htmlentities($this->configuration['site_title'])):'';
  ?>
  <title><?php echo ($this->module_name != "")?stripslashes(htmlentities($this->module_name)):$meta_title; ?> <?php echo isset($this->configuration['site_title'])?' | '.stripslashes(htmlentities($this->configuration['site_title'])):' | Site Title'?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta property="og:title" content="<?php echo ($this->module_name)?stripslashes(htmlentities($this->module_name)):''; ?> <?php echo isset($this->configuration['site_title'])?' | '.stripslashes(htmlentities($this->configuration['site_title'])):' | Site Title'?>">

  <?php if(isset($page_desc) && $page_desc != ''){ ?>
  <meta name="description" content="<?php echo isset($this->configuration['meta_description'])?$this->configuration['meta_description']:'Description'?>" />
  <meta property="og:description" content="<?php echo isset($this->configuration['meta_description'])?$this->configuration['meta_description']:'Description'?>">
  <?php } ?>

  <meta name="keyword" content="<?php echo isset($this->configuration['meta_keyword'])?$this->configuration['meta_keyword']:'IPSPI'?>" />

  <?php if(isset($ogimg) && $ogimg != ''){ ?>
  <meta property="og:image" content="<?php echo $ogimg; ?>"/>
  <?php }else{ ?>
  <meta property="og:image" content="<?php echo $this->webconfig['frontend_template']; ?>images/IPSPI_logo.jpg"/>
  <?php } ?>

  <?php 
    $stringurl = $_SERVER['HTTP_HOST'].parse_url( $_SERVER['REQUEST_URI'], PHP_URL_PATH );
    if(substr($stringurl, -1) == '/') {
      $stringurl = substr($stringurl, 0, -1);
    }else{
      $stringurl = $stringurl;
    }
  ?>
  <link rel="canonical" href="http://<?php echo $stringurl; ?>" />


  <link rel="apple-touch-icon" href="<?php echo $this->webconfig['frontend_template']; ?>images/favicon.png">
  <link rel="icon" href="<?php echo $this->webconfig['frontend_template']; ?>images/favicon.png">
  <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo $this->webconfig['frontend_template']; ?>css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $this->webconfig['frontend_template']; ?>css/fontawesome.min.css">
  <link rel="stylesheet" href="<?php echo $this->webconfig['frontend_template']; ?>css/normalize.css">
  <link href="<?php echo $this->webconfig['back_base_template']; ?>js/toastr/toastr.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo $this->webconfig['frontend_template']; ?>css/slick/slick.css" rel="stylesheet">
  <link href="<?php echo $this->webconfig['frontend_template']; ?>css/slick/slick-theme.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo $this->webconfig['frontend_template']; ?>css/main.css">

  <script src="<?php echo $this->webconfig['back_base_template']; ?>js/jquery.min.js" type="text/javascript"></script>
  <script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/modernizr-3.6.0.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <!-- <script>window.jQuery || document.write('<script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
  <script src="<?php echo $this->webconfig['frontend_template']; ?>js/plugins.js"></script>
  <script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/slick/slick.min.js"></script>
  <script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/bootstrap.min.js"></script>
  <script src="<?php echo $this->webconfig['frontend_template']; ?>js/main.js"></script> -->

  <script>window.jQuery || document.write('<script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
  <script type="text/javascript">
      function encode_js(str){str = (str + '').toString();return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '%20').replace(/\s/g,'%20').replace(/%2F/g,'-').replace(/%/g,'%25');}
    </script>
</head>

<body>
  <!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->

  <header class="fixed-top">
    <div class="top-border"></div>
    <nav class="navbar navbar-expand-lg navbar-light bg-white">
      <div class="container">
      <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo $this->webconfig['frontend_template']; ?>images/IPSPI_logo.jpg" alt="IPSPI Logo" class="img-logo"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsMenu" aria-controls="navbarsMenu" aria-expanded="false" aria-label="Toggle navigation">
        <div class="headerMenu">
            <div class="headerMenu-items">
              <div class="headerMenu-item headerMenu-item-1"></div>
              <div class="headerMenu-item headerMenu-item-2"></div>
              <div class="headerMenu-item headerMenu-item-3"></div>
            </div>
          </div>
      </button>

      <div class="collapse navbar-collapse" id="navbarsMenu">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url(); ?>">Beranda <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown-about" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tentang</a>
            <div class="dropdown-menu" aria-labelledby="dropdown-about">
              <?php if(count($this->about) > 0){?>
              <?php foreach ($this->about as $key => $listdata) {?>
              <a class="dropdown-item" href="<?php echo base_url().'about/read/'.$listdata['id'].'/'.$listdata['slug']; ?>"><?php echo stripcslashes(cutting_words($listdata['name'],'50')); ?></a>
              <?php } ?>
              <?php } ?>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>berita/listing">Berita</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>jurnal">Jurnal</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown-dpd" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">DPD</a>
            <div class="dropdown-menu tab-dpd dropdown-menu-right" aria-labelledby="dropdown-dpd">
              <?php foreach ($this->DpdModel->listData() as $key => $value) { ?>
                <a class="dropdown-item" href="<?php echo base_url(); ?>dpd/wilayah/<?php echo $value['id'];?>">Provinsi <?php echo ucwords(strtolower($value['nama']));?></a>
              <?php } ?>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown-anggota" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Keangotaan</a>
            <div class="dropdown-menu" aria-labelledby="dropdown-anggota">
              <?php if(count($this->informasi) > 0){?>
              <?php foreach ($this->informasi as $key => $listdata) {?>
              <a class="dropdown-item" href="<?php echo base_url().'informasi/read/'.$listdata['id'].'/'.$listdata['slug']; ?>"><?php echo stripcslashes(cutting_words($listdata['title'],'50')); ?></a>
              <?php } ?>
              <?php } ?>
              <a class="dropdown-item" href="<?php echo base_url().'informasi/' ?>dataanggota">Data Anggota</a>
              <?php 
              /*
              <a class="dropdown-item" href="<?php echo base_url(); ?>keluhan">Keluhan</a>
              */
              ?>
              <?php if($this->session->userdata('userid') != ''){?>
              
              <?php } ?>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>loker">Karir</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>faq">FAQ</a>
          </li>

          <li class="nav-item sp-button">
            <a class="nav-link sp-menu menu-green" href="<?php echo base_url(); ?>login">Masuk</a>
          </li>
          <li class="nav-item sp-button">
            <?php if($this->session->userdata('role') == '1' || $this->session->userdata('role') == ''){?>
              <a class="nav-link sp-menu menu-blue" href="<?php echo base_url(); ?>register">Daftar</a>
            <?php }else if($this->session->userdata('role') == '3'){ ?>
              <a class="nav-link sp-menu menu-blue" href="<?php echo base_url(); ?>login/logout">Logout</a>
            <?php } ?>
          </li>
        </ul>
        
      </div>
      </div>
    </nav>
  </header>
  
  <?php echo isset($content)?$content:''; ?>

  <footer class="footer-home">
    <div class="top-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-3">
            <img src="<?php echo $this->webconfig['frontend_template']; ?>images/IPSPI_logo_white.png" class="footer-logo" width="118" height="52">
            <p><?php echo isset($this->configuration['meta_description'])?$this->configuration['meta_description']:'';?></p>
          </div>
          <div class="col-sm-3">
            <div class="footer-menu-title">Tentang</div>
            <ul class="footer-menu">
              <?php if(count($this->about) > 0){?>
              <?php foreach ($this->about as $key => $listdata) {?>
              <li><a href="<?php echo base_url().'about/read/'.$listdata['id'].'/'.$listdata['slug']; ?>"><?php echo stripcslashes(cutting_words($listdata['name'],'50')); ?></a></li>
              <?php } ?>
              <?php } ?>
            </ul>
          </div>
          <div class="col-sm-3">
            <div class="footer-menu-title">Keangotaan</div>
            <ul class="footer-menu">
              <?php if(count($this->informasi) > 0){?>
              <?php foreach ($this->informasi as $key => $listdata) {?>
              <li><a href="<?php echo base_url().'informasi/read/'.$listdata['id'].'/'.$listdata['slug']; ?>"><?php echo stripcslashes(cutting_words($listdata['title'],'50')); ?></a></li>
              <?php } ?>
              <?php } ?>
              <?php /*<li><a href="<?php echo base_url(); ?>faq">FAQ</a></li>*/ ?>
              <li><a href="<?php echo base_url(); ?>contact">Kontak</a></li>
            </ul>
          </div>
          <div class="col-sm-3">
            <div class="footer-menu-title">Hubungi Kami</div>
            <div class="mt-3"></div>
            <strong>Alamat :</strong>
            <p><?php echo isset($this->configuration['address'])?$this->configuration['address']:'';?></p>
            <p>
              <strong>Email :</strong> <?php echo isset($this->configuration['email_contact'])?$this->configuration['email_contact']:'';?><br/>
              <strong>Telefon :</strong> <?php echo isset($this->configuration['phone'])?$this->configuration['phone']:'';?><br/>
              <strong>Fax :</strong> <?php echo isset($this->configuration['fax'])?$this->configuration['fax']:'';?>
            </p>
          </div>
        </div>
      </div>
    </div>

    <div class="bottom-footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-2 col-md-12">
            <ul class="social-icon">
              <li><a href="<?php echo isset($this->configuration['fb_url'])?$this->configuration['fb_url']:'';?>" target="_blank"><span class="fab fa-facebook-f"></span></a></li>
              <li><a href="<?php echo isset($this->configuration['twitter_url'])?$this->configuration['twitter_url']:'';?>" target="_blank"><span class="fab fa-twitter"></span></a></li>
              <li><a href="<?php echo isset($this->configuration['instagram_url'])?$this->configuration['instagram_url']:'';?>" target="_blank"><span class="fab fa-instagram"></span></a></li>
              <li><a href="<?php echo isset($this->configuration['youtube_url'])?$this->configuration['youtube_url']:'';?>" target="_blank"><span class="fab fa-youtube"></span></a></li>
            </ul>
          </div>
          <div class="col-lg-4 col-md-12">
            <div class="footer-menu">
              <?php if(count($this->regulation) > 0){?>
              <?php foreach ($this->regulation as $key => $listdata) {?>
              <span><a href="<?php echo base_url().'regulation/read/'.$listdata['id'].'/'.$listdata['slug']; ?>"><?php echo stripcslashes(cutting_words($listdata['name'],'20')); ?></a></span>
              <?php } ?>
              <?php } ?>
            </div>
          </div>
          <div class="col-lg-6 col-md-12">
            <div class="copyright">Copyright © 2018. Ikatan Pekerja Sosial Profesional Indonesia. All Right Reserved.</div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  
  <script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/chart.bundle.min.js"></script>
  <script src="<?php echo $this->webconfig['back_base_template']; ?>js/toastr/toastr.js" type="text/javascript"></script>
  <script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/modernizr-3.6.0.min.js"></script>
  <!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> -->
  <script src="<?php echo $this->webconfig['frontend_template']; ?>js/plugins.js"></script>
  <script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/slick/slick.min.js"></script>
  <script src="<?php echo $this->webconfig['frontend_template']; ?>js/vendor/bootstrap.min.js"></script>
  <script src="<?php echo $this->webconfig['frontend_template']; ?>js/main.js"></script>
  <!--Start of Tawk.to Script-->
  <script type="text/javascript">
  var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
  (function(){
  var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
  s1.async=true;
  s1.src='https://embed.tawk.to/5d5b68f077aa790be32fbc19/default';
  s1.charset='UTF-8';
  s1.setAttribute('crossorigin','*');
  s0.parentNode.insertBefore(s1,s0);
  })();
  </script>
  <!--End of Tawk.to Script-->
</body>

</html>
