<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class BeritaModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "berita";
	}
	
	public function filterData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional  = "";
		$rest  = "ORDER BY id DESC";
		if($name !=''){
			$conditional .= "AND title LIKE '%".$this->db->escape_str($name)."%'";
		}
		if($this->session->userdata('role') != 0){
			if($this->session->userdata('role') == 1){
				$conditional .= "";
			}else if($this->session->userdata('role') == 2){
				$conditional .= "AND id_provinsi = '".$this->session->userdata('userprovinsi')."'";
			}else if($this->session->userdata('role') == 3){
				$conditional .= "AND id_user = '".$this->session->userdata('userid')."'";
			}
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE (1=1)
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function filterDataCount($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$conditional = '';
		$rest  = "ORDER BY id DESC";
		if($name !=''){
			$conditional .= "AND title LIKE '%".$this->db->escape_str($name)."%'";
		}
		if($this->session->userdata('role') != 0){
			if($this->session->userdata('role') == 1){
				$conditional .= "";
			}else if($this->session->userdata('role') == 2){
				$conditional .= "AND id_provinsi = '".$this->session->userdata('userprovinsi')."'";
			}else if($this->session->userdata('role') == 3){
				$conditional .= "AND id_user = '".$this->session->userdata('userid')."'";
			}
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE (1=1)
			".$conditional."
			".$rest."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$id_provinsi = isset($params["id_provinsi"])?$params["id_provinsi"]:'';
		$expid = isset($params["expid"])?$params["expid"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id_provinsi != '') {
			$conditional .= "AND id_provinsi = '".$id_provinsi."'";
		}

		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($expid != '') {
			$conditional .= "AND id NOT IN (".$expid.")";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE status = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listDataCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$id_provinsi = isset($params["id_provinsi"])?$params["id_provinsi"]:'';
		$expid = isset($params["expid"])?$params["expid"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id_provinsi != '') {
			$conditional .= "AND id_provinsi = '".$id_provinsi."'";
		}
		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}
		

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE status = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listDataFeatured($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE status = 1 and featured = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}
    
}