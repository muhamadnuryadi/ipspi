<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class LoginModel extends CI_Model {
	var $module, $CI;
	function __construct() {	 
		parent::__construct();
		$this->CI =& get_instance(); 
		$this->CI->load->library('session');		
		$this->module = "Authentification";
	}
	public function doLogin($email = '', $password = ''){
		if($email == '' || $password == ''){
			return 'failed_input';
		}
		
		$password = md5 ($password);
		$member = $this->checkLogin($email,$password);
		
		if(count($member) > 0){
			if($member['status'] == 0){
				return 'non_activation';
			}
			if($member['status'] == 1){
				$this->sessionCreate($member['id']);
				
				writeLog(array('module' => $this->module, 'details' => "Login to Application"));
				return 'success_login';
			}
		}else{
			return 'failed_login';
		}
	}
	public function dologin_confirmation($key = ''){
		if($key == ''){
			return 'failed_input';
		}
		
		$member = $this->checkLogin_confirmation($key);

		if(count($member) > 0){
			if($member['status'] == 0){
				return 'non_activation';
			}
			if($member['status'] == 1){
				$this->sessionCreate($member['id']);
				writeLog(array('module' => $this->module, 'details' => "Login to Application"));
				return 'success_login';
			}
		}else{
			return 'failed_login';
		}
	}
	public function doLogout(){
		writeLog(array('module' => $this->module, 'details' => "Member Logout from Application"));

		$this->session->unset_userdata('iduser');
		$this->session->unset_userdata('userid');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('role');
		$this->session->unset_userdata('userprovinsi');
		$this->session->unset_userdata('userkota');
		## DESTROY ##
		// $this->cart->destroy();
	}
	public function isLoggedIn(){
		if($this->session->userdata('iduser') != ''
			&& $this->session->userdata('email') != ''
			&& $this->session->userdata('name') != ''
		){
			$validCheked = $this->__validSession($this->session->userdata('email'),$this->session->userdata('iduser'));
			if($validCheked > 0){
				return true;
			}else{
				return false;
			}
		}else {
			return false;
		}
	}
	
	private function sessionCreate($memberid){
		$this->db->from('members');
		$this->db->where('id', $memberid);
		$user = $this->db->get()->first_row('array');
		$user = $this->getMemberAccess($user);
		
		# STORE SESSION #
		$this->session->set_userdata('iduser', $user['id']);
		$this->session->set_userdata('userid', $user['email']);
		$this->session->set_userdata('email', $user['email']);
		$this->session->set_userdata('name', $user['nama']);
		$this->session->set_userdata('role', 3);
		$this->session->set_userdata('userpropinsi', $user['id_propinsi']);
		$this->session->set_userdata('userkota', $user['id_kota']);
		$this->session->set_userdata('avatar', $user['path_avatar']);
		
		if (isset($user['member_access']) && count($user['member_access']) > 0 ) {
			if (isset($user['member_access']['date_expired']) && $user['member_access']['date_expired'] != '' && strtotime($user['member_access']['date_expired']) >= strtotime(date('Y-m-d H:i:s'))) {
				if ($user['member_access']['status'] == 1) {
					$this->session->set_userdata('status_access', 1);
				}
				$this->session->set_userdata('date_expired', $user['member_access']['date_expired']);
			}
		}
	}

	private function getMemberAccess($result){
		if (isset($result['id']) && $result['id'] != '') {
			$result['member_access'] = $this->db->query("
				SELECT * FROM members_access WHERE status = 1 AND id_member = '".$this->db->escape_str($result['id'])."' ORDER BY id DESC
			")->first_row('array');
		}
		return $result;
	}

	private function checkLogin($email = '', $password = ''){
		$this->db->select('id, email, password, status');
		$this->db->from('members');
		$this->db->where('email', $email);
		$this->db->where('password', $password);
		$result = $this->db->get()->first_row('array');
		return $result;
	}
	private function checkLogin_confirmation($key = ''){
		$this->db->select('id, email, password,status');
		$this->db->from('members');
		$this->db->where('vernumber', $key);
		$result = $this->db->get()->first_row('array');
		return $result;
	}
	private function __validSession($email, $memberid){
		$this->db->select('id, email');
		$this->db->from('members');
		$this->db->where('email', $email);
		$this->db->where('id', $memberid);
		
		$result = $this->db->get()->first_row('array');
		return $result;
	}
	public function changePass($params=array()){
		$email = isset($params["email"])?$params["email"]:'';
		$new_password = isset($params["new_password"])?$params["new_password"]:'';

		$member = $this->checkemail($email);
		
		if(isset($member) && count($member) == 0){
			return 'email_notfound';
		}
		if(isset($member) && count($member) > 0){
			if($member[0]['status'] == 0){
				return 'not_activate';
			}
		}
		
		/*SEND EMAIL*/
		$send_email = $this->send_email_newpass_member($email, $new_password,$member[0]['nama']);
		
		if($send_email == '0'){return 'failed_send';}

		$data = array(
		        'vernumber' => $this->db->escape_str($new_password)
		);

		$this->db->where('email', $email);
		$doUpdate = $this->db->update('members', $data);
		if($doUpdate){
			$member = $this->checkemail($email);
			$this->session->set_userdata('idreg', $member[0]['id']);
			// writeLog(array('module' => $this->module, 'details' => "Change Member Password"));
			return 'success';
		}else{
			return 'failed';
		}
	}
	private function checkemail($email = ''){
		$this->db->select('id, email, status, nama');
		$this->db->from('members');
		$this->db->where('email', $email);
		$result = $this->db->get()->result_array();
		return $result;
	}

	public function UpdateRegData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$code = isset($params["code"])?$params["code"]:'';
		
		$member = $this->getMember($id);
		if(isset($member['vernumber']) && $member['vernumber'] != $code){
			return 'failed_code';
		}else{
			return 'success';
		}
	}
	public function getMember($id = 0){
		$this->db->from('members');
		$this->db->where('id', $id);
		$result = $this->db->get()->first_row('array');
		return $result;
	}
	public function send_email_newpass_member($email, $new_password, $name)
	{
		$message = $this->load->view($this->router->class.'/email_forgot_password', array(
																					"email" => $email
																					,"name" => $name
																					,"new_password" => $new_password
																				), TRUE);

		$result = send_mail(array(
						'email_tujuan' => $email
						,'email_tujuan_text' => $this->lang->line('email_members_register_newpassword')
						,'subject' => $this->lang->line('email_members_register_newpassword')
						,'setfrom_email' => SENDER_EMAIL
						,'body' => $message
					));
		
		return $result;
	}

	public function checkkey($vernumber = ''){
		$this->db->from('members');
		$this->db->where('vernumber', $vernumber);
		$result = $this->db->get()->first_row('array');
		return $result;
	}

	public function resetPassword($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$password = isset($params["password"])?$params["password"]:'';
		
		$sql_user = "password = '".$this->db->escape_str(md5($password))."' ";
		
		$doUpdate = $this->db->query("
		UPDATE members
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			$this->db->query("
			UPDATE members
			SET
				vernumber = ''
			WHERE
				id = ".$id."
			");
			return 'success';
		}else{
			return 'failed';
		}
	}
}
?>