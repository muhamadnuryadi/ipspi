<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class InformasiModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "informasi";
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY position ASC";

		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE status = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listDataCount($params=array()){
		$q = $this->db->query("
			SELECT
				*
			FROM
				wilayah
			WHERE id_parent = 0
		");
		$result = $q->result_array();
		$result = $this->getLoopMemberProv($result, 'jumlah', 'members');
		return $result;
	}

	private function getLoopMemberProv($loopdata, $fielddata, $tablename){
		if(count($loopdata) > 0){
			for($i = 0; $i < count($loopdata); $i++){
				$loopdata[$i][$fielddata] = $this->getSQLMemberProv($loopdata[$i]['id'], $tablename);
			}
		}else{
			$loopdata = array();
		}
		return $loopdata;
	}

	public function getSQLMemberProv($iddata, $tablename){
		$q = $this->db->query("
			SELECT
					id,
					id_propinsi,
					count(id) AS total
			FROM ".$tablename."
			WHERE id_propinsi = '".$iddata."' AND status = '1'
			GROUP BY id_propinsi
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listDataMember($params=array()){
		$q = $this->db->query("
			SELECT 
			    COUNT(id) AS total
			FROM
			    members
			WHERE
			    1 = 1
			GROUP BY status
		");
		$result = $q->result_array();
		return $result;
	}

	public function listMemberPekerjaan($params=array()){
		$q = $this->db->query("
			SELECT
				*
			FROM
				pekerjaan
			WHERE 1=1
		");
		$result = $q->result_array();
		$result = $this->getLoopMemberPekerjaan($result, 'jumlah', 'members_pekerjaan');
		return $result;
	}

	private function getLoopMemberPekerjaan($loopdata, $fielddata, $tablename){
		if(count($loopdata) > 0){
			for($i = 0; $i < count($loopdata); $i++){
				$loopdata[$i][$fielddata] = $this->getSQLMemberPekerjaan($loopdata[$i]['id'], $tablename);
			}
		}else{
			$loopdata = array();
		}
		return $loopdata;
	}

	public function getSQLMemberPekerjaan($iddata, $tablename){
		$q = $this->db->query("
			SELECT
					id,
					id_pekerjaan,
					count(id) AS total
			FROM ".$tablename."
			WHERE id_pekerjaan = '".$iddata."'
			GROUP BY id_pekerjaan
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listMemberInstansi(){
		$arr_result = array();

		$q = $this->db->query("
			SELECT id,name FROM instansi
			");
		$arr_instansi = $q->result_array();
		//select member instansi
		$q2 = $this->db->query("
			SELECT id,id_members,id_instansi FROM members_pekerjaan GROUP BY id_members ORDER BY id DESC
			");
		$member_instansi = $q2->result_array();
		if (count($arr_instansi) > 0 && count($member_instansi) > 0) {
			foreach ($arr_instansi as $key => $value) {
				$arr_result[$value['name']] = 0;
				foreach ($member_instansi as $key2 => $value2) {
					if ($value['id'] == $value2['id_instansi']) {
						$arr_result[$value['name']] = $arr_result[$value['name']]+1;
					}
				}
			}
		}
		
		return $arr_result;
	}

	public function listMemberPendidikan($params=array()){
		$q = $this->db->query("
			SELECT
				*
			FROM
				tingkat
			WHERE 1=1
		");
		$result = $q->result_array();
		$result = $this->getLoopMemberPendidikan($result, 'jumlah', 'members_pendidikan');
		return $result;
	}

	private function getLoopMemberPendidikan($loopdata, $fielddata, $tablename){
		if(count($loopdata) > 0){
			for($i = 0; $i < count($loopdata); $i++){
				$loopdata[$i][$fielddata] = $this->getSQLMemberPendidikan($loopdata[$i]['id'], $tablename);
			}
		}else{
			$loopdata = array();
		}
		return $loopdata;
	}

	public function getSQLMemberPendidikan($iddata, $tablename){
		$q = $this->db->query("
			SELECT
					id,
					tingkat,
					count(id) AS total
			FROM ".$tablename."
			WHERE tingkat = '".$iddata."'
			GROUP BY tingkat
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listMemberSertifikasi(){
		$arr_result = array();

		$q2 = $this->db->query("
			SELECT id,id_members,tipe FROM members_sertifikasi GROUP BY id_members ORDER BY id DESC
			");
		$arr_sertifikasi = $q2->result_array();
		$arr_hitung = array();
		if (count($arr_sertifikasi) > 0) {
			foreach ($this->webconfig['sertifikasi_tipe'] as $key => $value) {
				$arr_hitung[$value] = 0;
				foreach ($arr_sertifikasi as $key2 => $value2) {
					if ($value2['tipe'] == $key ) {
						$arr_hitung[$value] = $arr_hitung[$value]+1;
					}
				}
			}
		}

		return $arr_hitung;

	}

	public function listMemberKota($params=array()){
		$q = $this->db->query("
			SELECT
				*
			FROM
				wilayah
			WHERE id_parent != 0
		");
		$result = $q->result_array();
		$result = $this->getLoopMemberKota($result, 'jumlah', 'members');
		return $result;
	}

	private function getLoopMemberKota($loopdata, $fielddata, $tablename){
		if(count($loopdata) > 0){
			for($i = 0; $i < count($loopdata); $i++){
				$loopdata[$i][$fielddata] = $this->getSQLMemberKota($loopdata[$i]['id'], $tablename);
			}
		}else{
			$loopdata = array();
		}
		return $loopdata;
	}

	public function getSQLMemberKota($iddata, $tablename){
		$q = $this->db->query("
			SELECT
					id,
					id_kota,
					count(id) AS total
			FROM ".$tablename."
			WHERE id_kota = '".$iddata."' AND status = '1'
			GROUP BY id_kota
		");
		$result = $q->first_row('array');
		return $result;
	}
}