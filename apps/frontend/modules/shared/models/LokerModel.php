<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class LokerModel extends CI_Model{
	var $ci;
	function __construct() {	 
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "loker";		
	}
	
	public function listData($params=array()){
		$slug = isset($params["slug"])?$params["slug"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$id_exclude = isset($params["id_exclude"])?$params["id_exclude"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY date_posted DESC";

		if($slug != '') {
			$conditional .= " AND slug = '".$slug."'";
		}

		if($id_exclude != '') {
			$conditional .= " AND id != '".$id_exclude."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE status = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listDataCount($params=array()){
		$slug = isset($params["slug"])?$params["slug"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$id_exclude = isset($params["id_exclude"])?$params["id_exclude"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($slug != '') {
			$conditional .= " AND slug = '".$slug."'";
		}
		
		if($id_exclude != '') {
			$conditional .= " AND id != '".$id_exclude."'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE status = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}
}