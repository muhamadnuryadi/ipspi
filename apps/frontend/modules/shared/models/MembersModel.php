<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MembersModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->ci->load->helper('phpmailer');
		$this->maintablename = "members";
		$this->table_members_pendidikan = "members_pendidikan";
		$this->table_members_pekerjaan = "members_pekerjaan";
		$this->table_members_praktek = "members_praktek";
		$this->table_members_penghargaan = "members_penghargaan";
		$this->table_members_sertifikasi = "members_sertifikasi";
		$this->table_members_referensi = "members_referensi";
		$this->table_members_profesi = "members_profesi";
		$this->table_members_komunitas = "members_komunitas";
		$this->table_payments = "payments";
	}
	public function entriData($params=array()){
		$nama = $this->session->userdata('nama');
		$jk = $this->session->userdata('jk');
        $username = $this->session->userdata('username');
        $email = $this->session->userdata('email');
        $password = $this->session->userdata('password');
        $tgllahir = $this->session->userdata('tgllahir');
        $jumanak = $this->session->userdata('jumanak');
        $ktp = $this->session->userdata('ktp');
        $alamatkantor = $this->session->userdata('alamatkantor');
        $telpkantor = $this->session->userdata('telpkantor');
        $faxkantor = $this->session->userdata('faxkantor');
        $alamatrumah = $this->session->userdata('alamatrumah');
        $id_propinsi = $this->session->userdata('id_propinsi');
        $id_kota = $this->session->userdata('id_kota');
        $telp = $this->session->userdata('telp');
        $hp = $this->session->userdata('hp');
        $statuskawin = $this->session->userdata('statuskawin');
        $status = '1';
		
        $exist_email = $this->__CheckEmailExist($email);
		if($exist_email > 0){return 'exist_email';}

		/*RAMDOM CODE*/
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
	    $getcode = '';
	    for ($i = 0; $i < 10; $i++) {
	        $getcode .= $characters[rand(0, strlen($characters) - 1)];
	    }

		/*SEND EMAIL*/
		$send_email = $this->send_email_code_member($email, $nama, $getcode);

		if($send_email == '0'){return 'failed_send';}

        if ($_FILES['path_ktp']['size']<>0){
			$arr_path = explode('.', $_FILES['path_ktp']['name']);
			$extension_thumb = strtoupper(end($arr_path));

			if (!(($extension_thumb == 'JPG') || ($extension_thumb == 'JPEG') || ($extension_thumb == 'PNG') || ($extension_thumb == 'PDF')))
			{
				return 'failed_ext';
			}else if($_FILES['path_ktp']['size'] > $this->webconfig['max_file_size'] * 1000000){
				return 'failed_filesize';
			}else{


				# UPLOAD #
				$pathdir = date("Y/m/d");
	            $thepath = $this->webconfig['media-path-files'].$pathdir."/";
	            $exp = explode("/",$thepath);
	            $way = '';
	            foreach($exp as $n){
	                $way .= $n.'/';
	                @mkdir($way);
	            }

				## FILENAME ##
	            $pos = strripos($_FILES['path_ktp']['name'], '.');
	            if($pos === false){
	                $ordinary_name = $_FILES['path_ktp']['name'];
	            }else{
	                $ordinary_name = substr($_FILES['path_ktp']['name'], 0, $pos);
	            }

	            $name_upload = date("Y_m_d-H_i_s")."_".md5($ordinary_name);
	            $name_upload_c = $name_upload.'.'.strtolower($extension_thumb);

				$result = was_upload_file('path_ktp', $name_upload, false, $thepath."/", array(), array(), array());

				$path1 = date("Y/m/d")."/".$name_upload_c;
				$data['path_ktp'] = $path1;
			}
		} else {
			return 'empty_ktp';
		}

		if ($_FILES['foto']['size']<>0){
			$arr_path = explode('.', $_FILES['foto']['name']);
			$extension_thumb = strtoupper(end($arr_path));

			if (!(($extension_thumb == 'JPG') || ($extension_thumb == 'JPEG') || ($extension_thumb == 'PNG') || ($extension_thumb == 'PDF')))
			{
				return 'failed_ext';
			}else if($_FILES['foto']['size'] > $this->webconfig['max_file_size'] * 1000000){
				return 'failed_filesize';
			}else{


				# UPLOAD #
				$pathdir = date("Y/m/d");
	            $thepath = $this->webconfig['media-path-files'].$pathdir."/";
	            $exp = explode("/",$thepath);
	            $way = '';
	            foreach($exp as $n){
	                $way .= $n.'/';
	                @mkdir($way);
	            }

				## FILENAME ##
	            $pos = strripos($_FILES['foto']['name'], '.');
	            if($pos === false){
	                $ordinary_name = $_FILES['foto']['name'];
	            }else{
	                $ordinary_name = substr($_FILES['foto']['name'], 0, $pos);
	            }

	            $name_upload = date("Y_m_d-H_i_s")."_".md5($ordinary_name);
	            $name_upload_c = $name_upload.'.'.strtolower($extension_thumb);

				$result = was_upload_file('foto', $name_upload, false, $thepath."/", array(), array(), array());

				$path1 = date("Y/m/d")."/".$name_upload_c;
				$data['foto'] = $path1;
			}
		} else {
			return 'empty_foto';
		}

		if ($_FILES['path_ijazah']['size']<>0){
			$arr_path = explode('.', $_FILES['path_ijazah']['name']);
			$extension_thumb = strtoupper(end($arr_path));

			if (!(($extension_thumb == 'JPG') || ($extension_thumb == 'JPEG') || ($extension_thumb == 'PNG') || ($extension_thumb == 'PDF')))
			{
				return 'failed_ext';
			}else if($_FILES['path_ijazah']['size'] > $this->webconfig['max_file_size'] * 1000000){
				return 'failed_filesize';
			}else{


				# UPLOAD #
				$pathdir = date("Y/m/d");
	            $thepath = $this->webconfig['media-path-files'].$pathdir."/";
	            $exp = explode("/",$thepath);
	            $way = '';
	            foreach($exp as $n){
	                $way .= $n.'/';
	                @mkdir($way);
	            }

				## FILENAME ##
	            $pos = strripos($_FILES['path_ijazah']['name'], '.');
	            if($pos === false){
	                $ordinary_name = $_FILES['path_ijazah']['name'];
	            }else{
	                $ordinary_name = substr($_FILES['path_ijazah']['name'], 0, $pos);
	            }

	            $name_upload = date("Y_m_d-H_i_s")."_".md5($ordinary_name);
	            $name_upload_c = $name_upload.'.'.strtolower($extension_thumb);

				$result = was_upload_file('path_ijazah', $name_upload, false, $thepath."/", array(), array(), array());

				$path1 = date("Y/m/d")."/".$name_upload_c;
				$data['path_ijazah'] = $path1;
			}
		} else {
			return 'empty_foto';
		}

		$data['nama'] = $this->db->escape_str($nama);
		$data['jk'] = $this->db->escape_str($jk);
		$data['username'] = $this->db->escape_str($username);
        $data['email'] = $this->db->escape_str($email);
        $data['password'] = md5($password);
        $data['tgllahir'] = date("Y-m-d", strtotime($tgllahir));
        $data['jumanak'] = $this->db->escape_str($jumanak);
        $data['ktp'] = $this->db->escape_str($ktp);
        $data['alamatkantor'] = $this->db->escape_str($alamatkantor);
        $data['telpkantor'] = $this->db->escape_str($telpkantor);
        $data['faxkantor'] = $this->db->escape_str($faxkantor);
        $data['alamatrumah'] = $this->db->escape_str($alamatrumah);
        $data['id_propinsi'] = $this->db->escape_str($id_propinsi);
        $data['id_kota'] = $this->db->escape_str($id_kota);
        $data['telp'] = $this->db->escape_str($telp);
        $data['hp'] = $this->db->escape_str($hp);
        $data['statuskawin'] = $this->db->escape_str($statuskawin);
        $data['status'] = $this->db->escape_str($status);
        $data['created_at'] = date('Y-m-d H:i');

		$doInsert = $this->db->insert($this->maintablename, $data);
		if($doInsert){
			$id_members = $this->db->insert_id();
			foreach ($this->session->userdata('pendidikan') as $key => $value) {
				if ($value['pt'] != '') {
					$pendidikan['tingkat'] = $this->db->escape_str($value['tingkat']);
					$pendidikan['pt'] = $this->db->escape_str($value['pt']);
					$pendidikan['jurusan_2'] = $this->db->escape_str($value['jurusan']);
					$pendidikan['gelar'] = $this->db->escape_str($value['gelar']);
					$pendidikan['thn_lulus'] = $this->db->escape_str($value['thn_lulus']);
					$pendidikan['id_members'] = $this->db->escape_str($id_members);
					$pendidikan['jenis'] = $this->db->escape_str($value['jenis_pendidikan']);
					$this->db->insert('members_pendidikan', $pendidikan);
				}

			}

			foreach ($this->session->userdata('pekerjaan') as $key => $value) {
				if ($value['nama'] != '') {
					$pekerjaan['nama'] = $this->db->escape_str($value['nama']);
					$pekerjaan['jabatan'] = $this->db->escape_str($value['jabatan']);
					$pekerjaan['mulai'] = $this->db->escape_str($value['mulai']);
					$pekerjaan['akhir'] = $this->db->escape_str($value['akhir']);
					$pekerjaan['id_pekerjaan'] = $this->db->escape_str($value['jenis_pekerjaan']);
					$pekerjaan['id_instansi'] = $this->db->escape_str($value['jenis_instansi']);
					$pekerjaan['id_members'] = $this->db->escape_str($id_members);
					$this->db->insert('members_pekerjaan', $pekerjaan);
				}
			}

			foreach ($this->session->userdata('praktek') as $key => $value) {
				$praktek['setting'] = $this->db->escape_str($value['setting']);
				$praktek['tahun'] = $this->db->escape_str($value['tahun']);
				$praktek['jabatan'] = $this->db->escape_str($value['jabatan']);
				$praktek['peran'] = $this->db->escape_str($value['peran']);
				$praktek['keahlian'] = $this->db->escape_str($value['keahlian']);
				$praktek['id_members'] = $this->db->escape_str($id_members);
				$this->db->insert('members_praktek', $praktek);
			}

			foreach ($this->session->userdata('penghargaan') as $key => $value) {
				$penghargaan['nama'] = $this->db->escape_str($value['nama']);
				$penghargaan['instansi'] = $this->db->escape_str($value['instansi']);
				$penghargaan['tahun'] = $this->db->escape_str($value['tahun']);
				$penghargaan['id_members'] = $this->db->escape_str($id_members);
				$this->db->insert('members_penghargaan', $penghargaan);
			}

			foreach ($this->session->userdata('sertifikasi') as $key => $value) {
				if ($value['nama'] != '') {
					$sertifikasi['nama'] = $this->db->escape_str($value['nama']);
					$sertifikasi['lembaga'] = $this->db->escape_str($value['lembaga']);
					$sertifikasi['tahun'] = $this->db->escape_str($value['tahun']);
					$sertifikasi['tipe'] = $this->db->escape_str($value['jenis_sertifikasi']);
					$sertifikasi['id_members'] = $this->db->escape_str($id_members);
					$this->db->insert('members_sertifikasi', $sertifikasi);
				}
			}

			foreach ($this->session->userdata('referensi') as $key => $value) {
				$referensi['nama'] = $this->db->escape_str($value['nama']);
				$referensi['jabatan'] = $this->db->escape_str($value['jabatan']);
				$referensi['telp'] = $this->db->escape_str($value['telp']);
				$referensi['id_members'] = $this->db->escape_str($id_members);
				$this->db->insert('members_referensi', $referensi);
			}

			foreach ($this->session->userdata('profesi') as $key => $value) {
				$profesi['nama'] = $this->db->escape_str($value['nama']);
				$profesi['tahun'] = $this->db->escape_str($value['tahun']);
				$profesi['keterangan'] = $this->db->escape_str($value['keterangan']);
				$profesi['id_members'] = $this->db->escape_str($id_members);
				$this->db->insert('members_profesi', $profesi);
			}

			foreach ($this->session->userdata('komunitas') as $key => $value) {
				$komunitas['nama'] = $this->db->escape_str($value['nama']);
				$komunitas['alamat'] = $this->db->escape_str($value['alamat']);
				$komunitas['id_members'] = $this->db->escape_str($id_members);
				$this->db->insert('members_komunitas', $komunitas);
			}


			return 'success';
		}else{
			return 'failed';
		}
	}

	private function __CheckEmailExist($email){
		if($email !=''){
			$db = $this->db->query("
				SELECT
					count(id) as jumlah
				FROM
					".$this->maintablename."
				WHERE email LIKE '".$this->db->escape_str($email)."'
			")->result_array();

			$jumlah = $db[0]['jumlah'];
			return $jumlah;
		}else{
			return 'failed';
		}
	}

	public function send_email_code_member($email, $name, $code)
	{
		$message = $this->load->view($this->router->class.'/email_register2', array(
																					"code" => $code
																					,"name" => $name
																				), TRUE);

		$result = send_mail(array(
						'email_tujuan' => $email
						,'email_tujuan_text' => $this->lang->line('email_members_register_verification_code')
						,'subject' => $this->lang->line('email_members_register_verification_code')
						,'setfrom_email' => SENDER_EMAIL
						,'body' => $message
					));
		return $result;
	}

	private function uploadImageProfile($width_resize, $a1, $b1, $a2, $b2) {
        $this->ci->load->helper('upload');
        if ($_FILES['pathprofile']['size']<>0){
            $data = array();

            $tmpName = $_FILES['pathprofile']['tmp_name'];
            list($width, $height, $type, $attr) = getimagesize($tmpName);
            # Rasio Ukuran Sliders #
            $rasio = $width/$width_resize;
            $extension_thumb = strtoupper(end(explode('.', $_FILES['pathprofile']['name'])));
            if (!(($extension_thumb == 'JPEG') || ($extension_thumb == 'JPG') || ($extension_thumb == 'PNG') || ($extension_thumb == 'GIF'))){
                $data['error'] = "failed_ext";
            }

            # UPLOAD #
            $pathdir = date("Y/m/d");
            $thepath = $this->webconfig['media-path-profile'].$pathdir."/";
            $thepath_create = $this->webconfig['media-path-profile'].$pathdir."/temp/";
            $exp = explode("/",$thepath_create);
            $way = '';
            foreach($exp as $n){
                $way .= $n.'/';
                @mkdir($way);
            }

            $pos = strripos($_FILES['pathprofile']['name'], '.');
            if($pos === false){
                $ordinary_name = $_FILES['pathprofile']['name'];
            }else{
                $ordinary_name = substr($_FILES['pathprofile']['name'], 0, $pos);
            }

            $name_upload = date("Y_m_d-H_i_s")."_".md5($ordinary_name);
            $name_upload_c = $name_upload.'.'.strtolower($extension_thumb);

            $result = was_upload_file('pathprofile', $name_upload, false, $thepath_create."/", array(), array(), array());

            if ($result['status'] > 0){
                $data['path'] = "";
            }else{
                $path1 = $pathdir."/".$name_upload_c;
                $data['path'] = $path1;
                if(isset($rasio) && $rasio !=''){
                    $finalcrop_resize = $this->croping_resize_profile($result,$a1, $b1, $a2, $b2,$rasio,$thepath);
                }
            }
        }else{
            $data['error'] = "empty_file";
        }

        return $data;
    }

    private function croping_resize_profile($result,$a1, $b1, $a2, $b2,$rasio,$image_path) {

        # RESIZE and CROP #
        $thepath = $this->webconfig['media-path-profile-thumb'].date("Y/m/d")."/";
        $exp = explode("/",$thepath);
        $way ='';
        foreach($exp as $n){
            $way.=$n.'/';
            @mkdir($way);
        }

        $file_name = $result['name'];
        $src_path = $image_path . 'temp/' . $file_name;

        $exp_file = explode('.', $file_name, 2);

        $des_path =  $image_path .$file_name;
        $aa1 = $a1 * $rasio;
        $bb1 = $b1 * $rasio;
        $aa2 = $a2 * $rasio;
        $bb2 = $b2 * $rasio;
        if($des_path !=''){
            $this->image_moo
                ->load($src_path)
                ->crop($aa1,$bb1,$aa2,$bb2)
                ->save($des_path);
        }

        ## RESIZE IMAGE ##
        foreach($this->webconfig['image_profile'] as $key => $value){
            $size_file = explode('x',$value);
            $file_final_name_resize = $exp_file[0].'_'.$value.'_thumb.'.$exp_file[1];
            $des_path_resize =  $thepath . $file_final_name_resize;
            $this->image_moo
                ->load($des_path)
                ->stretch($size_file[0],$size_file[1])
                ->save($des_path_resize);
        }

        /* Delete Temp File */
        if (file_exists($src_path)){
            @unlink($src_path);
        }
    }

    private function del_cropresize_profile($cfile) {
        if($cfile == "") return false;

        $explod_file = explode("/",$cfile);
        $filename = end($explod_file);
        $exp_file = explode(".",$filename);
        $exp_thumb_path = explode("/", $this->webconfig['media-path-profile-thumb'].$cfile, -1);
        $thumb_path = implode("/", $exp_thumb_path);

        foreach($this->webconfig['image_profile'] as $key => $value){
            $delete_resize = $thumb_path.'/'.$exp_file[0]."_".$value."_thumb.".$exp_file[1];
            if (file_exists($delete_resize)){
                @unlink($delete_resize);
            }
        }
    }

	public function updateData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$fullname = isset($params["fullname"])?$params["fullname"]:'';
		$birthday = isset($params["birthday"])?$params["birthday"]:'';
		$address = isset($params["address"])?$params["address"]:'';
		$postcode = isset($params["postcode"])?$params["postcode"]:'';
		$gender = isset($params["gender"])?$params["gender"]:'';
		$phone = isset($params["phone"])?$params["phone"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$password = isset($params["password"])?$params["password"]:'';
		$status = isset($params["status"])?$params["status"]:'';
		$username = isset($params["username"])?$params["username"]:'';
		$instagram = isset($params["instagram"])?$params["instagram"]:'';
		$tipe_user = isset($params["tipe_user"])?$params["tipe_user"]:'';
		$discount = isset($params["discount"])?$params["discount"]:'';
		$saldo = isset($params["saldo"])?$params["saldo"]:'';

		$exist_username = $this->__CheckUserExistEdit($id,$username);

		if($exist_username > 0){return 'exist_username';}

		$a1     = isset($params["a1"])?$params["a1"]:'';
        $b1     = isset($params["b1"])?$params["b1"]:'';
        $a2      = isset($params["a2"])?$params["a2"]:'';
        $b2      = isset($params["b2"])?$params["b2"]:'';
        $width_resize      = isset($params["width_resize"])?$params["width_resize"]:'';

        if ($_FILES['pathprofile']['size']<>0){
            $getData = $this->listData(array('id' => $id));
            if(count($getData) > 0) {
                $pfile = $this->webconfig['media-path-profile'].$getData[0]['logo_img'];
                if (file_exists($pfile)){
                    @unlink($pfile);
                }
                $delete_cropresize = $this->del_cropresize_profile($getData[0]['logo_img']);
            }
        }

        $upload_image_profile = $this->uploadImageProfile($width_resize, $a1, $b1, $a2, $b2);

        if(isset($upload_image_profile["error"])) {
            if($upload_image_profile["error"] == "failed_ext") {
                return "failed_ext";
            }
        }

        $path_profile = isset($upload_image_profile["path"])?$upload_image_profile["path"]:"";

		if($password != ''){
			if(strlen($password) < 5){
				return 'min_5';
			}else{
				$sql_user = "fullname = '".$this->db->escape_str($fullname)."',birthday = '".date("Y-m-d", strtotime($birthday))."',address = '".$this->db->escape_str($address)."',postcode = '".$this->db->escape_str($postcode)."',gender = '".$this->db->escape_str($gender)."',phone = '".$this->db->escape_str($phone)."',email = '".$this->db->escape_str($email)."',status = '".$this->db->escape_str($status)."', password = '".$this->db->escape_str(md5($password))."' , username = '".$this->db->escape_str($username)."' , instagram = '".$this->db->escape_str($instagram)."' , tipe_user = '".$this->db->escape_str($tipe_user)."' , discount = '".$this->db->escape_str($discount)."', saldo = '".$this->db->escape_str($saldo)."' ";
			}

		}else{
			$sql_user = "fullname = '".$this->db->escape_str($fullname)."',birthday = '".date("Y-m-d", strtotime($birthday))."',address = '".$this->db->escape_str($address)."',postcode = '".$this->db->escape_str($postcode)."',gender = '".$this->db->escape_str($gender)."',phone = '".$this->db->escape_str($phone)."',email = '".$this->db->escape_str($email)."',status = '".$this->db->escape_str($status)."' , username = '".$this->db->escape_str($username)."' , instagram = '".$this->db->escape_str($instagram)."' , tipe_user = '".$this->db->escape_str($tipe_user)."' , discount = '".$this->db->escape_str($discount)."', saldo = '".$this->db->escape_str($saldo)."'";
		}

		if ($_FILES['pathprofile']['size']<>0) $sql_user .= ", logo_img = '".$this->db->escape_str($path_profile)."'";

		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.", name = ".htmlentities($fullname).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteData($id){
		if($id == 0) return 'failed';

		# DELETE MEMBER PROJECT #
		$this->db->where('id',$id);
		$queproject = $this->db->get($this->maintablename);
		$resultproject = $queproject->result_array();
		if(isset($resultproject) && count($resultproject) > 0){
			foreach ($resultproject as $key => $value) {
				@unlink($this->webconfig['media-path-profile'].$value['logo_img']);
				$exp_img = explode(".", $value['logo_img']);
				foreach ($this->webconfig['image_profile'] as $key => $image_data) {
					@unlink($this->webconfig['media-path-profile-thumb'].$exp_img[0]."_".$image_data."_thumb.".$exp_img[1]);
				}
			}
		}

		$doDelete = $this->db->query("
		DELETE FROM ".$this->maintablename."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function filterData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$id_propinsi = isset($params["id_propinsi"])?$params["id_propinsi"]:'';
		$id_kota = isset($params["id_kota"])?$params["id_kota"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$tingkat = isset($params["tingkat"])?$params["tingkat"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$rest  = "ORDER BY id DESC";
		$conditional = '';
		if($name != ''){
			$conditional .= "AND nama LIKE '%".$this->db->escape_str($name)."%'";
		}
		if($id_propinsi != ''){
			$conditional .= "AND id_propinsi = '".$this->db->escape_str($id_propinsi)."'";
		}
		if($id_kota != ''){
			$conditional .= "AND id_kota = '".$this->db->escape_str($id_kota)."'";
		}
		if($email != ''){
			$conditional .= "AND email LIKE '%".$this->db->escape_str($email)."%'";
		}
		if($tingkat != ''){
			$conditional .= "AND tingkat = '".$this->db->escape_str($tingkat)."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE (1=1)
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function filterDataCount($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$id_propinsi = isset($params["id_propinsi"])?$params["id_propinsi"]:'';
		$id_kota = isset($params["id_kota"])?$params["id_kota"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$tingkat = isset($params["tingkat"])?$params["tingkat"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';

		$rest  = "ORDER BY id DESC";
		$conditional = '';
		if($name != ''){
			$conditional .= "AND nama LIKE '%".$this->db->escape_str($name)."%'";
		}
		if($id_propinsi != ''){
			$conditional .= "AND id_propinsi = '".$this->db->escape_str($id_propinsi)."'";
		}
		if($id_kota != ''){
			$conditional .= "AND id_kota = '".$this->db->escape_str($id_kota)."'";
		}
		if($email != ''){
			$conditional .= "AND email LIKE '%".$this->db->escape_str($email)."%'";
		}
		if($tingkat != ''){
			$conditional .= "AND tingkat = '".$this->db->escape_str($tingkat)."'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE (1=1)
			".$conditional."
			".$rest."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function getLastNoAnggota($params=array()){
		$conditional = '';
		$q = $this->db->query("
			SELECT
				id,no_anggota,
				SUBSTRING_INDEX(no_anggota, '.', -1) AS lastnumb
			FROM
				".$this->maintablename."
			WHERE no_anggota != ''
			".$conditional."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($email != '') {
			$conditional .= " AND email = '".$email."'";
		}

		if($id != '') {
			$conditional .= " AND id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE (1=1)
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listDataCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

	function checkname($nama){
		$q = $this->db->query("
			SELECT
				id
				,nama
			FROM
				".$this->maintablename."
			WHERE
				nama = '".$this->db->escape_str($nama)."'
		");
		$result = $q->first_row('array');
		return $result;
	}

	private function __CheckUserExist($username){
		if($username !=''){
			$db = $this->db->query("
				SELECT
					count(id) as jumlah
				FROM
					".$this->maintablename."
				WHERE username LIKE '".$this->db->escape_str($username)."'
			")->result_array();

			$jumlah = $db[0]['jumlah'];
			return $jumlah;
		}else{
			return 'failed';
		}
	}

	private function __CheckUserExistEdit($id,$username){
		$conditional = "";
		if($id !='' AND $username !=''){
			$db = $this->db->query("
				SELECT
					count(id) as jumlah
				FROM
					".$this->maintablename."
				WHERE id NOT IN (".$id.")
				AND username LIKE '".$this->db->escape_str($username)."'
			")->result_array();

			$jumlah = $db[0]['jumlah'];
			return $jumlah;
		}
	}

	public function filterSelect2($query = ""){
		$q = $this->db->query("
			SELECT
				id
				,fullname as text
			FROM
				".$this->maintablename."
			WHERE fullname LIKE '%".$query."%'
			limit 0, 10
		");
		$result = $q->result_array();
		return $result;
	}

	public function updateDataProfile($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$nama = isset($params['nama'])?$params['nama']:'';
        $username = isset($params['username'])?$params['username']:'';
        $email = isset($params['email'])?$params['email']:'';
        $password = isset($params['password'])?$params['password']:'';
        $password_1 = isset($params['password_1'])?$params['password_1']:'';
        $tgllahir = isset($params['tgllahir'])?$params['tgllahir']:'';
        $jumanak = isset($params['jumanak'])?$params['jumanak']:'';
        $alamatkantor = isset($params['alamatkantor'])?$params['alamatkantor']:'';
        $telpkantor = isset($params['telpkantor'])?$params['telpkantor']:'';
        $faxkantor = isset($params['faxkantor'])?$params['faxkantor']:'';
        $alamatrumah = isset($params['alamatrumah'])?$params['alamatrumah']:'';
        $id_propinsi = isset($params['id_propinsi'])?$params['id_propinsi']:'';
        $id_kota = isset($params['id_kota'])?$params['id_kota']:'';
        $telp = isset($params['telp'])?$params['telp']:'';
        $hp = isset($params['hp'])?$params['hp']:'';
        $status = isset($params['status'])?$params['status']:'';
        $created_at = isset($params['created_at'])?$params['created_at']:'';

		$sql_user = "
					nama = '".$this->db->escape_str($nama)."'
					,username = '".$this->db->escape_str($username)."'
					,email = '".$this->db->escape_str($email)."'
					,password = '".$this->db->escape_str($password)."'
					,tgllahir = '".date("Y-m-d", strtotime($tgllahir))."'
					,jumanak = '".$this->db->escape_str($jumanak)."'
					,alamatkantor = '".$this->db->escape_str($alamatkantor)."'
					,telpkantor = '".$this->db->escape_str($telpkantor)."'
					,faxkantor = '".$this->db->escape_str($faxkantor)."'
					,alamatrumah = '".$this->db->escape_str($alamatrumah)."'
					,id_propinsi = '".$this->db->escape_str($id_propinsi)."'
					,id_kota = '".$this->db->escape_str($id_kota)."'
					,telp = '".$this->db->escape_str($telp)."'
					,hp = '".$this->db->escape_str($hp)."'
					,status = '".$this->db->escape_str($status)."'
					,created_at = '".date("Y-m-d", strtotime($created_at))."'
					";


		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");

		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function listDataPendidikan($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id ASC";

		if($id != '') {
			$conditional = "WHERE id_members = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_members_pendidikan."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function updateDataPendidikan($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$idmember = isset($params["idmember"])?$params["idmember"]:'';
		$tingkat = isset($params["tingkat"])?$params["tingkat"]:'';
		$pt = isset($params["pt"])?$params["pt"]:'';
		$jurusan = isset($params["jurusan"])?$params["jurusan"]:'';
		$gelar = isset($params["gelar"])?$params["gelar"]:'';
		$thn_lulus = isset($params["thn_lulus"])?$params["thn_lulus"]:'';

		if (count($idmember) > 0) {
        	for ($si = 0; $si < count($idmember); $si++) {
        		$sql_detail = "
							tingkat = '".$this->db->escape_str($tingkat[$si])."'
							,pt = '".$this->db->escape_str($pt[$si])."'
							,jurusan = '".$this->db->escape_str($jurusan[$si])."'
							,gelar = '".$this->db->escape_str($gelar[$si])."'
							,thn_lulus = '".$this->db->escape_str($thn_lulus[$si])."'
						";
        		$doUpdate = $this->db->query("
		    	INSERT INTO ".$this->table_members_pendidikan."
				    (id, tingkat, pt, jurusan, gelar, thn_lulus, id_members)
				VALUES
				    (
				    	'".(isset($idmember)?$this->db->escape_str($idmember[$si]):'')."',
				    	'".(isset($tingkat)?$this->db->escape_str($tingkat[$si]):'')."',
				    	'".(isset($pt)?$this->db->escape_str($pt[$si]):'')."',
				    	'".(isset($jurusan)?$this->db->escape_str($jurusan[$si]):'')."',
				    	'".(isset($gelar)?$this->db->escape_str($gelar[$si]):'')."',
				    	'".(isset($thn_lulus)?$this->db->escape_str($gelar[$si]):'')."',
				    	'".(isset($id)?$id:'')."'
				    )
				ON DUPLICATE KEY UPDATE
				    ".$sql_detail."
				");
        	}
        }

		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteDataPendidikan($id){
		if($id == 0) return 'failed';

		$doDelete = $this->db->query("
		DELETE FROM ".$this->table_members_pendidikan."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function listDataPekerjaan($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id ASC";

		if($id != '') {
			$conditional = "WHERE id_members = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_members_pekerjaan."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function updateDataPekerjaan($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$idmember = isset($params["idmember"])?$params["idmember"]:'';
		$nama = isset($params["nama"])?$params["nama"]:'';
		$jabatan = isset($params["jabatan"])?$params["jabatan"]:'';
		$mulai = isset($params["mulai"])?$params["mulai"]:'';
		$akhir = isset($params["akhir"])?$params["akhir"]:'';

		if (count($idmember) > 0) {
        	for ($si = 0; $si < count($idmember); $si++) {
        		$sql_detail = "
							nama = '".$this->db->escape_str($nama[$si])."'
							,jabatan = '".$this->db->escape_str($jabatan[$si])."'
							,mulai = '".$this->db->escape_str($mulai[$si])."'
							,akhir = '".$this->db->escape_str($akhir[$si])."'
						";
        		$doUpdate = $this->db->query("
		    	INSERT INTO ".$this->table_members_pekerjaan."
				    (id, nama, jabatan, mulai, akhir, id_members)
				VALUES
				    (
				    	'".(isset($idmember)?$this->db->escape_str($idmember[$si]):'')."',
				    	'".(isset($nama)?$this->db->escape_str($nama[$si]):'')."',
				    	'".(isset($jabatan)?$this->db->escape_str($jabatan[$si]):'')."',
				    	'".(isset($mulai)?$this->db->escape_str($mulai[$si]):'')."',
				    	'".(isset($akhir)?$this->db->escape_str($akhir[$si]):'')."',
				    	'".(isset($id)?$id:'')."'
				    )
				ON DUPLICATE KEY UPDATE
				    ".$sql_detail."
				");
        	}
        }

		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteDataPekerjaan($id){
		if($id == 0) return 'failed';

		$doDelete = $this->db->query("
		DELETE FROM ".$this->table_members_pekerjaan."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function listDataPraktek($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id ASC";

		if($id != '') {
			$conditional = "WHERE id_members = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_members_praktek."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function updateDataPraktek($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$idmember = isset($params["idmember"])?$params["idmember"]:'';
		$setting = isset($params["setting"])?$params["setting"]:'';
		$tahun = isset($params["tahun"])?$params["tahun"]:'';
		$jabatan = isset($params["jabatan"])?$params["jabatan"]:'';
		$peran = isset($params["peran"])?$params["peran"]:'';
		$keahlian = isset($params["keahlian"])?$params["keahlian"]:'';

		if (count($idmember) > 0) {
        	for ($si = 0; $si < count($idmember); $si++) {
        		$sql_detail = "
							setting = '".$this->db->escape_str($setting[$si])."'
							,tahun = '".$this->db->escape_str($tahun[$si])."'
							,jabatan = '".$this->db->escape_str($jabatan[$si])."'
							,peran = '".$this->db->escape_str($peran[$si])."'
							,keahlian = '".$this->db->escape_str($keahlian[$si])."'
						";
        		$doUpdate = $this->db->query("
		    	INSERT INTO ".$this->table_members_praktek."
				    (id, setting, tahun, jabatan, peran, keahlian, id_members)
				VALUES
				    (
				    	'".(isset($idmember)?$this->db->escape_str($idmember[$si]):'')."',
				    	'".(isset($setting)?$this->db->escape_str($setting[$si]):'')."',
				    	'".(isset($tahun)?$this->db->escape_str($tahun[$si]):'')."',
				    	'".(isset($jabatan)?$this->db->escape_str($jabatan[$si]):'')."',
				    	'".(isset($peran)?$this->db->escape_str($peran[$si]):'')."',
				    	'".(isset($keahlian)?$this->db->escape_str($keahlian[$si]):'')."',
				    	'".(isset($id)?$id:'')."'
				    )
				ON DUPLICATE KEY UPDATE
				    ".$sql_detail."
				");
        	}
        }

		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteDataPraktek($id){
		if($id == 0) return 'failed';

		$doDelete = $this->db->query("
		DELETE FROM ".$this->table_members_praktek."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function listDataPenghargaan($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id ASC";

		if($id != '') {
			$conditional = "WHERE id_members = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_members_penghargaan."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function updateDataPenghargaan($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$idmember = isset($params["idmember"])?$params["idmember"]:'';
		$nama = isset($params["nama"])?$params["nama"]:'';
		$instansi = isset($params["instansi"])?$params["instansi"]:'';
		$tahun = isset($params["tahun"])?$params["tahun"]:'';

		if (count($idmember) > 0) {
        	for ($si = 0; $si < count($idmember); $si++) {
        		$sql_detail = "
							nama = '".$this->db->escape_str($nama[$si])."'
							,instansi = '".$this->db->escape_str($instansi[$si])."'
							,tahun = '".$this->db->escape_str($tahun[$si])."'
						";
        		$doUpdate = $this->db->query("
		    	INSERT INTO ".$this->table_members_penghargaan."
				    (id, nama, instansi, tahun, id_members)
				VALUES
				    (
				    	'".(isset($idmember)?$this->db->escape_str($idmember[$si]):'')."',
				    	'".(isset($nama)?$this->db->escape_str($nama[$si]):'')."',
				    	'".(isset($instansi)?$this->db->escape_str($instansi[$si]):'')."',
				    	'".(isset($tahun)?$this->db->escape_str($tahun[$si]):'')."',
				    	'".(isset($id)?$id:'')."'
				    )
				ON DUPLICATE KEY UPDATE
				    ".$sql_detail."
				");
        	}
        }

		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteDataPenghargaan($id){
		if($id == 0) return 'failed';

		$doDelete = $this->db->query("
		DELETE FROM ".$this->table_members_penghargaan."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function listDataSertifikasi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id ASC";

		if($id != '') {
			$conditional = "WHERE id_members = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_members_sertifikasi."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function updateDataSertifikasi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$idmember = isset($params["idmember"])?$params["idmember"]:'';
		$nama = isset($params["nama"])?$params["nama"]:'';
		$lembaga = isset($params["lembaga"])?$params["lembaga"]:'';
		$tahun = isset($params["tahun"])?$params["tahun"]:'';

		if (count($idmember) > 0) {
        	for ($si = 0; $si < count($idmember); $si++) {
        		$sql_detail = "
							nama = '".$this->db->escape_str($nama[$si])."'
							,lembaga = '".$this->db->escape_str($lembaga[$si])."'
							,tahun = '".$this->db->escape_str($tahun[$si])."'
						";
        		$doUpdate = $this->db->query("
		    	INSERT INTO ".$this->table_members_sertifikasi."
				    (id, nama, lembaga, tahun, id_members)
				VALUES
				    (
				    	'".(isset($idmember)?$this->db->escape_str($idmember[$si]):'')."',
				    	'".(isset($nama)?$this->db->escape_str($nama[$si]):'')."',
				    	'".(isset($lembaga)?$this->db->escape_str($lembaga[$si]):'')."',
				    	'".(isset($tahun)?$this->db->escape_str($tahun[$si]):'')."',
				    	'".(isset($id)?$id:'')."'
				    )
				ON DUPLICATE KEY UPDATE
				    ".$sql_detail."
				");
        	}
        }

		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteDataSertifikasi($id){
		if($id == 0) return 'failed';

		$doDelete = $this->db->query("
		DELETE FROM ".$this->table_members_sertifikasi."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function listDataReferensi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id ASC";

		if($id != '') {
			$conditional = "WHERE id_members = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_members_referensi."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function updateDataReferensi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$idmember = isset($params["idmember"])?$params["idmember"]:'';
		$nama = isset($params["nama"])?$params["nama"]:'';
		$jabatan = isset($params["jabatan"])?$params["jabatan"]:'';
		$telp = isset($params["telp"])?$params["telp"]:'';

		if (count($idmember) > 0) {
        	for ($si = 0; $si < count($idmember); $si++) {
        		$sql_detail = "
							nama = '".$this->db->escape_str($nama[$si])."'
							,jabatan = '".$this->db->escape_str($jabatan[$si])."'
							,telp = '".$this->db->escape_str($telp[$si])."'
						";
        		$doUpdate = $this->db->query("
		    	INSERT INTO ".$this->table_members_referensi."
				    (id, nama, jabatan, telp, id_members)
				VALUES
				    (
				    	'".(isset($idmember)?$this->db->escape_str($idmember[$si]):'')."',
				    	'".(isset($nama)?$this->db->escape_str($nama[$si]):'')."',
				    	'".(isset($jabatan)?$this->db->escape_str($jabatan[$si]):'')."',
				    	'".(isset($telp)?$this->db->escape_str($telp[$si]):'')."',
				    	'".(isset($id)?$id:'')."'
				    )
				ON DUPLICATE KEY UPDATE
				    ".$sql_detail."
				");
        	}
        }

		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteDataReferensi($id){
		if($id == 0) return 'failed';

		$doDelete = $this->db->query("
		DELETE FROM ".$this->table_members_referensi."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function listDataProfesi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id ASC";

		if($id != '') {
			$conditional = "WHERE id_members = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_members_profesi."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function updateDataProfesi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$idmember = isset($params["idmember"])?$params["idmember"]:'';
		$nama = isset($params["nama"])?$params["nama"]:'';
		$tahun = isset($params["tahun"])?$params["tahun"]:'';
		$keterangan = isset($params["keterangan"])?$params["keterangan"]:'';

		if (count($idmember) > 0) {
        	for ($si = 0; $si < count($idmember); $si++) {
        		$sql_detail = "
							nama = '".$this->db->escape_str($nama[$si])."'
							,tahun = '".$this->db->escape_str($tahun[$si])."'
							,keterangan = '".$this->db->escape_str($keterangan[$si])."'
						";
        		$doUpdate = $this->db->query("
		    	INSERT INTO ".$this->table_members_profesi."
				    (id, nama, tahun, keterangan, id_members)
				VALUES
				    (
				    	'".(isset($idmember)?$this->db->escape_str($idmember[$si]):'')."',
				    	'".(isset($nama)?$this->db->escape_str($nama[$si]):'')."',
				    	'".(isset($tahun)?$this->db->escape_str($tahun[$si]):'')."',
				    	'".(isset($keterangan)?$this->db->escape_str($keterangan[$si]):'')."',
				    	'".(isset($id)?$id:'')."'
				    )
				ON DUPLICATE KEY UPDATE
				    ".$sql_detail."
				");
        	}
        }

		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteDataProfesi($id){
		if($id == 0) return 'failed';

		$doDelete = $this->db->query("
		DELETE FROM ".$this->table_members_profesi."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function listDataKomunitas($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id ASC";

		if($id != '') {
			$conditional = "WHERE id_members = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_members_komunitas."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function updateDataKomunitas($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$idmember = isset($params["idmember"])?$params["idmember"]:'';
		$nama = isset($params["nama"])?$params["nama"]:'';
		$alamat = isset($params["alamat"])?$params["alamat"]:'';

		if (count($idmember) > 0) {
        	for ($si = 0; $si < count($idmember); $si++) {
        		$sql_detail = "
							nama = '".$this->db->escape_str($nama[$si])."'
							,alamat = '".$this->db->escape_str($alamat[$si])."'
						";
        		$doUpdate = $this->db->query("
		    	INSERT INTO ".$this->table_members_komunitas."
				    (id, nama, alamat, id_members)
				VALUES
				    (
				    	'".(isset($idmember)?$this->db->escape_str($idmember[$si]):'')."',
				    	'".(isset($nama)?$this->db->escape_str($nama[$si]):'')."',
				    	'".(isset($alamat)?$this->db->escape_str($alamat[$si]):'')."',
				    	'".(isset($id)?$id:'')."'
				    )
				ON DUPLICATE KEY UPDATE
				    ".$sql_detail."
				");
        	}
        }

		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteDataKomunitas($id){
		if($id == 0) return 'failed';

		$doDelete = $this->db->query("
		DELETE FROM ".$this->table_members_komunitas."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function listTingkat(){
		$query = $this->db->query("
			SELECT * FROM tingkat ORDER BY id ASC
			");
		$result = $query->result_array();
		return $result;
	}

	public function listPekerjaan(){
		$query = $this->db->query("
			SELECT * FROM pekerjaan ORDER BY id ASC
			");
		$result = $query->result_array();
		return $result;
	}

	public function listInstansi(){
		$query = $this->db->query("
			SELECT * FROM instansi ORDER BY id ASC
			");
		$result = $query->result_array();
		return $result;
	}

	public function updateNoAnggota($params = array()){
		$email = isset($params['email'])?$params['email']:'';
		$no_anggota = isset($params['no_anggota'])?$params['no_anggota']:'';

		$sql_user = "
			no_anggota = '".$this->db->escape_str($no_anggota)."'
		";

		$this->db->query("
			UPDATE ".$this->maintablename."
			SET
				".$sql_user."
			WHERE
				email = '".$this->db->escape_str($email)."'
		");

	}

	public function updatePayment($params = array()){
		$email = isset($params['email'])?$params['email']:'';
		$order_id = isset($params['order_id'])?$params['order_id']:'';
		$settlement_time = isset($params['settlement_time'])?$params['settlement_time']:'';
		$transaction_status = isset($params['transaction_status'])?$params['transaction_status']:'';
		$payment_type = isset($params['payment_type'])?$params['payment_type']:'';

		$sql_user = "
			status = '".$this->db->escape_str(1)."'
			,date_paid = '".$this->db->escape_str($settlement_time)."' 
			,payment_type = '".$this->db->escape_str($payment_type)."' 
		";

		$doUpdate = $this->db->query("
			UPDATE ".$this->table_payments."
			SET
				".$sql_user."
			WHERE
				charge_id = '".$this->db->escape_str($order_id)."'
		");

		if ($doUpdate) {
			$getMemberID = $this->db->query("
				SELECT id,nama FROM ".$this->maintablename." WHERE email = '".$this->db->escape_str($email)."'
				")->first_row('array');
			$id_member = isset($getMemberID['id'])?$getMemberID['id']:'';
			$nama = isset($getMemberID['nama'])?$getMemberID['nama']:'';
			if ($id_member != '') {
				// $this->webconfig['masa_berlaku'];
				$expired = date('Y-m-d', strtotime('+'.$this->webconfig['masa_berlaku'].' years'));
				$data['id_member'] = $this->db->escape_str($id_member);
				$data['date_start'] = $this->db->escape_str($settlement_time);
				$data['date_expired'] = $this->db->escape_str($expired);
				$data['status'] = $this->db->escape_str(1);
				
				$doInsert = $this->db->insert('members_access',$data);
				if ($doInsert) {

					$message = $this->load->sharedView('/email_success_payment', array(
																					"nama" => $nama
																					,"date_expired" => $expired
																				), TRUE);

					$result = send_mail(array(
									'email_tujuan' => $email
									,'email_tujuan_text' => "Ipspi"
									,'subject' => 'Status Keanggotaan Aktif'
									,'setfrom_email' => SENDER_EMAIL
									,'body' => $message
								));
				}
			}

			return true;
		}else{
			return false;
		}
	}

	public function getMemberExpired($params = array()){
		$result = array();
		$select = "
			SELECT 
				m.id
				,m.nama
				,m.email
				,ma.status
				,ma.date_expired
				,ma.date_start
			FROM members m INNER JOIN members_access ma 
			ON m.id = ma.id_member
			WHERE 1=1 AND DATE(ma.date_expired) = DATE(NOW()) + INTERVAL ".$this->webconfig['notif_expired_hari']." DAY
			LIMIT ".$this->webconfig['limit_member_expired']."
		";
		
		$result = $this->db->query($select)->result_array();
		return $result;
	}

	public function getNIK($params = array()){
		$nik = isset($params['nik'])?$params['nik']:'';
		$select = "
			SELECT count(id) as jumlah
			FROM ".$this->maintablename."
			WHERE ktp = '".$this->db->escape_str($nik)."'
		";
		$result = $this->db->query($select)->first_row('array');
		if (isset($result['jumlah']) && $result['jumlah'] > 0) {
			return 'failed';
		}else{
			return 'success';
		}
	}
}
