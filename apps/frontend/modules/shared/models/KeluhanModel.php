<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class KeluhanModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "keluhan";
	}
	public function entriData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$no_anggota = isset($params["no_anggota"])?$params["no_anggota"]:'';
		$telp = isset($params["telp"])?$params["telp"]:'';
		$message = isset($params["message"])?$params["message"]:'';

		
		$data ['id'] = "Null";
		$data ['name'] = $this->db->escape_str($name);
		$data ['no_anggota'] = $this->db->escape_str($no_anggota);
		$data ['telp'] = $this->db->escape_str($telp);
		$data ['message'] = $this->db->escape_str($message);
		$data ['datecreated'] = date("Y-m-d H:i");
		$data ['statusread'] = 0;
		
		$doInsert = $this->db->insert($this->maintablename, $data);
		if($doInsert){
			writeLog(array('module' => 'keluhan', 'details' => $this->lang->line('logs_entry_keluhan')." dengan nama = ".htmlentities($name).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	
}