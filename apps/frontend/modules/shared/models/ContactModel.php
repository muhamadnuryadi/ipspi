<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ContactModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "contact";
	}
	public function entriData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$subject = isset($params["subject"])?$params["subject"]:'';
		$message = isset($params["message"])?$params["message"]:'';

		
		$data ['id'] = "Null";
		$data ['name'] = $this->db->escape_str($name);
		$data ['email'] = $this->db->escape_str($email);
		$data ['subject'] = $this->db->escape_str($subject);
		$data ['message'] = $this->db->escape_str($message);
		$data ['datecreated'] = date("Y-m-d H:i");
		$data ['statusread'] = 0;
		
		$doInsert = $this->db->insert($this->maintablename, $data);
		if($doInsert){
			writeLog(array('module' => 'contact', 'details' => $this->lang->line('logs_entry_contact')." dengan nama = ".htmlentities($name).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	
}