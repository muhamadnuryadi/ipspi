<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class WilayahModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "wilayah";
	}
	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "-- ORDER BY id DESC";

		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listpropinsi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$status = isset($params["status"])?$params["status"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "-- ORDER BY id DESC";

		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($status != '') {
			$conditional .= "AND status = '".$status."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE id_parent = '0'
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listDataCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listKota($params=array()){
		$prop = isset($params["prop"])?$params["prop"]:'';
		
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE id_parent = '".$prop."'
			
		");
		
		$result = $q->result_array();
		return $result;
	}

	public function listKecamatan($params=array()){
		$city = isset($params["city"])?$params["city"]:'';
		
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE id_parent = '".$city."'
			
		");
		
		$result = $q->result_array();
		return $result;
	}

	public function listDataProvinsiCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE id_parent = '0'
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listDataProvinsi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional = "AND id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE id_parent = '0'
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function filterDataProvinsi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$name = isset($params["name"])?$params["name"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($name != '') {
			$conditional .= "AND nama LIKE '%".$this->db->escape_str($name)."%'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE id_parent = '0'
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function filterDataProvinsiCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$name = isset($params["name"])?$params["name"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($name != '') {
			$conditional .= "AND nama LIKE '%".$this->db->escape_str($name)."%'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE id_parent = '0'
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function doPublish($id){
		if($id == 0) return 'failed';
		$doPublish = $this->db->query("
			UPDATE ".$this->maintablename."
			SET 
				status = '1'
			WHERE 
				id = ".$id."
		");
		if($doPublish){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_publish_provinsi')." id = ".$id.""));		
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function doUnublish($id){
		if($id == 0) return 'failed';
		
		$doUnublish = $this->db->query("
			UPDATE ".$this->maintablename."
			SET status = '0'
			WHERE 
				id = ".$id."
		");
		if($doUnublish){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_unpublish_provinsi')." id = ".$id.""));		
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function entriData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$status = isset($params["status"])?$params["status"]:'';

		$data ['nama'] = $this->db->escape_str($name);
		$data ['status'] = $this->db->escape_str($status);
		$data ['id_parent'] = 0;
		
		$doInsert = $this->db->insert($this->maintablename, $data);
		if($doInsert){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_entry_provinsi')." dengan judul = ".htmlentities($name).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function updateData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$name = isset($params["name"])?$params["name"]:'';
		$status = isset($params["status"])?$params["status"]:'';

		$sql_user = "
					nama = '".$this->db->escape_str($name)."'
					,status = '".$this->db->escape_str($status)."'
					";

		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_provinsi')." id = ".$id.", name = ".htmlentities($name).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteData($id){
		if($id == 0) return 'failed';

		$doDelete = $this->db->query("
		DELETE FROM ".$this->maintablename."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_provinsi')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function listDpd($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$status = isset($params["status"])?$params["status"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "-- ORDER BY id DESC";

		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($status != '') {
			$conditional .= "AND status = '".$status."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE id_parent = '0' AND status = '1'
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}
}