<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class JurnalModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "jurnal";
	}
	
	public function filterData($params=array()){
		$judul = isset($params["judul"])?$params["judul"]:'';
		$penulis = isset($params["penulis"])?$params["penulis"]:'';
		$id_provinsi = isset($params["id_provinsi"])?$params["id_provinsi"]:'';
		$tahun = isset($params["tahun"])?$params["tahun"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		
		$offsetData  = "";
		$conditional  = "";
		$rest  = " ORDER BY j.id DESC";
		if($judul !=''){
			$conditional .= "AND j.title LIKE '%".$this->db->escape_str($judul)."%'";
		}

		if($penulis !=''){
			$conditional .= "AND m.nama LIKE '%".$this->db->escape_str($penulis)."%'";
		}

		if($id_provinsi !=''){
			$conditional .= "AND j.id_provinsi = '".$this->db->escape_str($id_provinsi)."'";
		}

		if($tahun !=''){
			$conditional .= "AND YEAR(j.datepublish) = '".$this->db->escape_str($tahun)."'";
		}

		if($this->session->userdata('role') != 0){
			if($this->session->userdata('role') == 1){
				$conditional .= "";
			}else if($this->session->userdata('role') == 2){
				$conditional .= "AND j.id_provinsi = '".$this->session->userdata('userprovinsi')."'";
			}else if($this->session->userdata('role') == 3){
				$conditional .= "AND j.id_user = '".$this->session->userdata('userid')."'";
			}
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				j.id
				,j.datepublish
				,j.id_user
				,j.title
				,j.slug
				,j.status
				,j.id_provinsi
				,j.role
				,j.files
				,j.path
				,j.description
				,m.nama
			FROM
				".$this->maintablename." j 
			JOIN members m
			WHERE j.id_user = m.id AND j.status = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		
		$result = $q->result_array();
		$result = $this->getProvinsi($result);
		return $result;
	}

	public function filterDataCount($params=array()){
		$judul = isset($params["judul"])?$params["judul"]:'';
		$penulis = isset($params["penulis"])?$params["penulis"]:'';
		$id_provinsi = isset($params["id_provinsi"])?$params["id_provinsi"]:'';
		$tahun = isset($params["tahun"])?$params["tahun"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$conditional = '';
		$rest  = " ORDER BY j.id DESC";
		if($judul !=''){
			$conditional .= " AND j.title LIKE '%".$this->db->escape_str($judul)."%'";
		}

		if($penulis !=''){
			$conditional .= "AND m.nama LIKE '%".$this->db->escape_str($penulis)."%'";
		}

		if($id_provinsi !=''){
			$conditional .= "AND j.id_provinsi = '".$this->db->escape_str($id_provinsi)."'";
		}		

		if($tahun !=''){
			$conditional .= "AND YEAR(j.datepublish) = '".$this->db->escape_str($tahun)."'";
		}

		if($this->session->userdata('role') != 0){
			if($this->session->userdata('role') == 1){
				$conditional .= "";
			}else if($this->session->userdata('role') == 2){
				$conditional .= " AND j.id_provinsi = '".$this->session->userdata('userprovinsi')."'";
			}else if($this->session->userdata('role') == 3){
				$conditional .= " AND j.id_user = '".$this->session->userdata('userid')."'";
			}
		}

		$q = $this->db->query("
			SELECT
				count(j.id) as jumlah
			FROM
				".$this->maintablename." j 
			JOIN members m
			WHERE j.id_user = m.id AND j.status = 1
			".$conditional."
			".$rest."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$expid = isset($params["expid"])?$params["expid"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($expid != '') {
			$conditional .= "AND id NOT IN (".$expid.")";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE status = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		$result = $this->getProvinsi($result);
		return $result;
	}

	public function listDataCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$expid = isset($params["expid"])?$params["expid"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}
		

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE status = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function getProvinsi($result){
		if (count($result) > 0) {
			foreach ($result as $key => $value) {
				if ($value['id_provinsi'] != '') {
					$result[$key]['provinsi'] = $this->db->query("
						SELECT nama FROM wilayah WHERE id = ".$this->db->escape_str($value['id_provinsi'])."
						")->first_row('array');
				}
			}
		}

		return $result;
	}

	public function listDataFeatured($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE status = 1 and featured = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}
    
}