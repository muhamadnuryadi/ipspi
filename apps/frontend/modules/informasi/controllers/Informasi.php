<?php
class Informasi extends CI_Controller{
    var $webconfig;
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->sharedModel('BeritaModel');

        $this->load->sharedModel('AboutModel');
        $this->about = $this->AboutModel->listData();
        $this->load->sharedModel('InformasiModel');
        $this->informasi = $this->InformasiModel->listData();
        $this->load->sharedModel('ConfigurationModel');
        $this->configuration = $this->ConfigurationModel->listData();
        $this->load->sharedModel('RegulationModel');
        $this->regulation = $this->RegulationModel->listData();
        
        $this->webconfig = $this->config->item('webconfig');
        $this->module_name = $this->lang->line('home');
    }
    function index(){
        $tdata = array();
        $tdata['lists'] = $this->BeritaModel->listData(array(
                                                                'limit' => 6
                                                            ));
        $ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
            $this->load->sharedView('template', $ldata);
    }
    
    function read($id = 0){
        $tdata = array();
        $tdata['lists'] = $this->InformasiModel->listData(array('id' => $id));
        if (count($tdata['lists']) == 0) {
            redirect(base_url());
        }
        $ldata['content'] = $this->load->view($this->router->class.'/detail',$tdata, true);
        $this->load->sharedView('template', $ldata);
    }

    function dataanggota(){
        $tdata = array();

        $ldata['content'] = $this->load->view($this->router->class.'/dataanggota',$tdata, true);
        $this->load->sharedView('template', $ldata);
    }

    function getChartDPD(){
        $qdata = $this->InformasiModel->listDataCount();
        $result = array();
        if (count($qdata) > 0) {
            foreach ($qdata as $key => $value) {
                $result['provinsi'][$key] = $value['nama'];
                $result['jumlah'][$key] = isset($value['jumlah']['total'])?$value['jumlah']['total']:0;;
                $result['arr_bgcolor'][$key] = 'rgba(54, 162, 235, 0.2)';
                $result['arr_bordercolor'][$key] = 'rgba(54, 162, 235, 1)';
            }
        }

        echo json_encode($result);
    }

    public function getChartaktif(){
        $qdata = $this->InformasiModel->listDataMember();
        $result = array();
        if (count($qdata) > 0) {
            foreach ($qdata as $key => $value) {
                $result['jumlah'][$key] = isset($value['total'])?$value['total']:0;;
                if ($key == 0) {
                    $result['arr_bgcolor'][$key] = 'rgba(255, 99, 132, 0.2)';
                    $result['arr_bordercolor'][$key] = 'rgba(255,99,132,1)';
                }else{
                    $result['arr_bgcolor'][$key] = 'rgba(54, 162, 235, 0.2)';
                    $result['arr_bordercolor'][$key] = 'rgba(54, 162, 235, 1)';
                }
                
            }   
        }
        echo json_encode($result);
    }

    public function getChartPekerjaan(){
        $data = $this->InformasiModel->listMemberPekerjaan();
        $result = array();
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                $result['nama'][$key] = $value['name'];
                $result['jumlah'][$key] = isset($value['jumlah']['total'])?$value['jumlah']['total']:'';
                $result['arr_bgcolor'][$key] = isset($this->webconfig['chart_bgcolor'][$key])?$this->webconfig['chart_bgcolor'][$key]:'';
                $result['arr_bordercolor'][$key] = isset($this->webconfig['chart_bordercolor'][$key])?$this->webconfig['chart_bordercolor'][$key]:'';
            }
        }

        echo json_encode($result);
    }

    public function getChartInstansi(){
        $data = $this->InformasiModel->listMemberInstansi();
        $result = array();
        if (count($data) > 0) {
            $a = 0;
            foreach ($data as $key => $value) {
                $result['nama'][$a] = $key;
                $result['jumlah'][$a] = $value;
                $result['arr_bgcolor'][$a] = isset($this->webconfig['chart_bgcolor'][$a])?$this->webconfig['chart_bgcolor'][$a]:'';
                $result['arr_bordercolor'][$a] = isset($this->webconfig['chart_bordercolor'][$a])?$this->webconfig['chart_bordercolor'][$a]:'';
                $a++;
            }
        }
        
        echo json_encode($result);
    }

    public function getChartPendidikan(){
        $data = $this->InformasiModel->listMemberPendidikan();
        $result = array();
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                $result['nama'][$key] = $value['name'];
                $result['jumlah'][$key] = isset($value['jumlah']['total'])?$value['jumlah']['total']:0;
                $result['arr_bgcolor'][$key] = isset($this->webconfig['chart_bgcolor'][$key])?$this->webconfig['chart_bgcolor'][$key]:'';
                $result['arr_bordercolor'][$key] = isset($this->webconfig['chart_bordercolor'][$key])?$this->webconfig['chart_bordercolor'][$key]:'';
            }
        }
        
        echo json_encode($result);
    }

    public function getChartSertifikasi(){
        $data = $this->InformasiModel->listMemberSertifikasi();
        $result = array();
        if (count($data) > 0) {
            $a = 0;
            foreach ($data as $key => $value) {
                $result['nama'][$a] = $key;
                $result['jumlah'][$a] = $value;
                $result['arr_bgcolor'][$a] = isset($this->webconfig['chart_bgcolor'][$a])?$this->webconfig['chart_bgcolor'][$a]:'';
                $result['arr_bordercolor'][$a] = isset($this->webconfig['chart_bordercolor'][$a])?$this->webconfig['chart_bordercolor'][$a]:'';
                $a++;
            }
        }

        echo json_encode($result);
    }

    public function getChartProvince(){
        $qdata = $this->InformasiModel->listDataCount();
        $arr_hitung = array();
        $arr_hitung_fix = array();
        $result = array();
        $hasil = array();
        if (count($qdata) > 0) {
            foreach ($qdata as $key => $value) {
                $arr_hitung[$key]['id'] = $value['id'];
                $arr_hitung[$key]['jumlah'] = isset($value['jumlah']['total'])?$value['jumlah']['total']:0;
            }
        }

        usort($arr_hitung, function($a, $b) { //urutkan berdasakran jumlah
            return $b['jumlah'] - $a['jumlah'];
        });

        if (count($arr_hitung) > 0) {
            foreach ($arr_hitung as $key => $value) {
                if ($key < 10) {
                    $arr_hitung_fix[$key] = $value;
                }
            }

            if (count($arr_hitung_fix) > 0) {
                foreach ($arr_hitung_fix as $key => $value) {
                    foreach ($qdata as $key2 => $value2) {
                        if ($value2['id'] == $value['id']) {
                            $hasil[$key] = $value2;
                        }
                    }
                }
            }
        }

        if (count($hasil) > 0) {
            foreach ($hasil as $key => $value) {
                $result['provinsi'][$key] = $value['nama'];
                $result['jumlah'][$key] = isset($value['jumlah']['total'])?$value['jumlah']['total']:0;;
                $result['arr_bgcolor'][$key] = 'rgba(54, 162, 235, 0.2)';
                $result['arr_bordercolor'][$key] = 'rgba(54, 162, 235, 1)';
            }
        }
        
        echo json_encode($result);
    }

    public function getChartKota(){
        $data = $this->InformasiModel->listMemberKota();
        $arr_hitung = array();
        $arr_hitung_fix = array();
        $result = array();
        $hasil = array();
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                $arr_hitung[$key]['id'] = $value['id'];
                $arr_hitung[$key]['jumlah'] = isset($value['jumlah']['total'])?$value['jumlah']['total']:0;
            }
        }

        usort($arr_hitung, function($a, $b) { //urutkan berdasakran jumlah
            return $b['jumlah'] - $a['jumlah'];
        });

        if (count($arr_hitung) > 0) {
            foreach ($arr_hitung as $key => $value) {
                if ($key < 10) {
                    $arr_hitung_fix[$key] = $value;
                }
            }

            if (count($arr_hitung_fix) > 0) {
                foreach ($arr_hitung_fix as $key => $value) {
                    foreach ($data as $key2 => $value2) {
                        if ($value2['id'] == $value['id']) {
                            $hasil[$key] = $value2;
                        }
                    }
                }
            }
        }

        if (count($hasil) > 0) {
            foreach ($hasil as $key => $value) {
                $result['nama'][$key] = $value['nama'];
                $result['jumlah'][$key] = isset($value['jumlah']['total'])?$value['jumlah']['total']:0;;
                $result['arr_bgcolor'][$key] = 'rgba(245, 166, 35, 0.2)';
                $result['arr_bordercolor'][$key] = 'rgba(245, 166, 35, 1)';
            }
        }
        
        echo json_encode($result);
    }
}