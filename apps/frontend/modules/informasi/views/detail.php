<section class="head-wrap">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1 class="head-title"><?php echo stripcslashes($lists[0]['title']);?></h1>
      </div>
    </div>
  </div>
</section>

<section class="content">
  <div class="container">
    <div class="row">
      <!-- <div class="col-sm-6">
        <div class="content-wrapper p-5">
          <img src="images/IPSPI_logo_big.jpg" alt="IPSPI Logo" class="img-fluid">
        </div>
      </div> -->
      <div class="col-sm-12">
        <!-- <h1>IKATAN PEKERJA SOSIAL PROFESIONAL INDONESIA</h1> -->
        <div class="content-wrapper p-5">
          <?php echo stripcslashes($lists[0]['body']);?>
        </div>
      </div>
    </div>
  </div>
</section>