<section class="head-wrap">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h1 class="head-title">Data Anggota</h1>
        </div>
      </div>
    </div>
  </section>
  
  <section class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="content-wrapper p-5">
            <h3 class="text-center mb-5">Grafik Area Anggota DPD</h3>
            <canvas id="ChartDPD" width="300" height="400"></canvas>
          </div>
          <div class="mb-5"></div>
          <div class="content-wrapper p-5">
            <h3 class="text-center mb-5">Data Anggota Aktif &amp; Non Aktif</h3>
            <canvas id="ChartActive" width="400" height="150"></canvas>
          </div>
          <div class="mb-5"></div>
          
          <div class="row">
            <div class="col-lg-6">
              <div class="content-wrapper p-5">
                <h3 class="text-center mb-5">Data 10 Teratas Domisili Provinsi Anggota IPSPI</h3>
                <canvas id="ChartProvince" width="400" height="400"></canvas>
              </div>
              <div class="mb-5"></div>
            </div>
            <div class="col-lg-6">
              <div class="content-wrapper p-5">
                <h3 class="text-center mb-5">Data 10 Teratas Domisili Kota Anggota IPSPI</h3>
                <canvas id="ChartCity" width="400" height="400"></canvas>
              </div>
              <div class="mb-5"></div>
            </div>
          </div>
          
          <div class="row">
            <div class="col-lg-6">
              <div class="content-wrapper p-5">
                <h3 class="text-center mb-5">Data Presentase Pekerjaan Anggota IPSPI</h3>
                <canvas id="ChartPekerjaan" width="400" height="400"></canvas>
              </div>
              <div class="mb-5"></div>
            </div>
            <div class="col-lg-6">
              <div class="content-wrapper p-5">
                <h3 class="text-center mb-5">Data Presentase Instansi Anggota IPSPI</h3>
                <canvas id="ChartInstansi" width="400" height="400"></canvas>
              </div>
              <div class="mb-5"></div>
            </div>
          </div>
          
          <div class="row">
            <div class="col-lg-6">
              <div class="content-wrapper p-5">
                <h3 class="text-center mb-5">Data Presentase Pendidikan Anggota IPSPI</h3>
                <canvas id="ChartPendidikan" width="400" height="400"></canvas>
              </div>
              <div class="mb-5"></div>
            </div>
            <div class="col-lg-6">
              <div class="content-wrapper p-5">
                <h3 class="text-center mb-5">Data Presentase Sertifikasi Anggota IPSPI</h3>
                <canvas id="ChartSertifikasi" width="400" height="400"></canvas>
              </div>
              <div class="mb-5"></div>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  </section>

  <script>
    $(document).ready(function(){
        //CHART DPD/PROVINSI
        var ctx = document.getElementById("ChartDPD");
        var getChartDPD = $.ajax({url: "<?php echo base_url() ?>informasi/getChartDPD", success: function(result){
            generateChartDPD(result);
        }});

        function generateChartDPD(result){
            result = $.parseJSON(result);
            var ChartDPD = new Chart(ctx, {
              type: 'horizontalBar',
              data: {
                  labels: result['provinsi'],
                  datasets: [{
                      label: 'Jumlah Anggota',
                      data: result['jumlah'],
                      backgroundColor: result['arr_bgcolor'],
                      borderColor: result['arr_bordercolor'],
                      borderWidth: 1
                  }]
              },
              options: {
                scales: {
                  xAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                  }]
                }
              }
          });
        }
        //END CHART DPD

        //CHART MEMBER AKTIF
        var ctxaktif = document.getElementById("ChartActive");
        var getChartaktif = $.ajax({url: "<?php echo base_url() ?>informasi/getChartaktif", success: function(result){
            generateChartaktif(result);
        }});

        function generateChartaktif(result){
            result = $.parseJSON(result);
            var ChartActive = new Chart(ctxaktif,{
                type: 'pie',
                data: {
                  labels: ["Non Aktif", "Aktif"],
                  datasets: [{
                      label: 'Jumlah Member',
                      data: result['jumlah'],
                      backgroundColor: result['arr_bgcolor'],
                      borderColor: result['arr_bordercolor'],
                      borderWidth: 1
                  }]
                },
            });
        }
        //END STATUS AKTIF

        //PEKERJAAN
        var ctxpekerjaan = document.getElementById("ChartPekerjaan");
        var getChartPekerjaan = $.ajax({url: "<?php echo base_url() ?>informasi/getChartPekerjaan", success: function(result){
            generateChartPekerjaan(result);
        }});
        function generateChartPekerjaan(result){
            result = $.parseJSON(result);
            // console.log(result)
            var ChartPekerjaan = new Chart(ctxpekerjaan,{
                type: 'pie',
                data: {
                  labels: result['nama'],
                  datasets: [{
                      label: 'Jumlah Member',
                      data: result['jumlah'],
                      backgroundColor: result['arr_bgcolor'],
                      borderColor: result['arr_bordercolor'],
                      borderWidth: 1
                  }]
                },
            });
        }

        //INSTANSI
        var ctxinstansi = document.getElementById("ChartInstansi");
        var getChartInstansi = $.ajax({url: "<?php echo base_url() ?>informasi/getChartInstansi", success: function(result){
            generateChartInstansi(result);
        }});

        function generateChartInstansi(result){
            result = $.parseJSON(result);
            // console.log(result['nama']);
            var ChartInstansi = new Chart(ctxinstansi,{
                type: 'pie',
                data: {
                  labels: result['nama'],
                  datasets: [{
                      label: 'Jumlah Member',
                      data: result['jumlah'],
                      backgroundColor: [
                          'rgba(54, 162, 235, 0.2)',
                          'rgba(255, 99, 132, 0.2)',
                          'rgba(245, 166, 35, 0.2)',
                          'rgba(127, 197, 0, 0.2)',
                          'rgba(189, 16, 224, 0.2)'
                      ],
                      borderColor: [
                          'rgba(54, 162, 235, 1)',
                          'rgba(255, 99, 132, 1)',
                          'rgba(245, 166, 35, 1)',
                          'rgba(127, 197, 0, 1)',
                          'rgba(189, 16, 224, 1)'
                      ],
                      borderWidth: 1
                  }]
                },
            });
        }

        //PENDIDIKAN
        var ctxpendidikan = document.getElementById("ChartPendidikan");
        var getChartPendidikan = $.ajax({url: "<?php echo base_url() ?>informasi/getChartPendidikan", success: function(result){
            generateChartPendidikan(result);
        }});

        function generateChartPendidikan(result){
            result = $.parseJSON(result);
            var ChartPendidikan = new Chart(ctxpendidikan,{
                type: 'pie',
                data: {
                  labels: result['nama'],
                  datasets: [{
                      label: 'Jumlah Member',
                      data: result['jumlah'],
                      backgroundColor: result['arr_bgcolor'],
                      borderColor: result['arr_bordercolor'],
                      borderWidth: 1
                  }]
                },
            });
        }

        //SERTIFIKASI
        var ctxsertifikasi = document.getElementById("ChartSertifikasi");
        var getChartSertifikasi = $.ajax({url: "<?php echo base_url() ?>informasi/getChartSertifikasi", success: function(result){
            generateChartSertifikasi(result);
        }});

        function generateChartSertifikasi(result){
            result = $.parseJSON(result);
            var ChartSertifikasi = new Chart(ctxsertifikasi,{
                type: 'pie',
                data: {
                  labels: result['nama'],
                  datasets: [{
                      label: 'Jumlah Member',
                      data: result['jumlah'],
                      backgroundColor: result['arr_bgcolor'],
                      borderColor: result['arr_bordercolor'],
                      borderWidth: 1
                  }]
                },
            });
        }

        //DATA PROVINSI 10 TERATAS
        var ctxprovince = document.getElementById("ChartProvince");
        var getChartProvince = $.ajax({url: "<?php echo base_url() ?>informasi/getChartProvince", success: function(result){
            generateChartProvince(result);
        }});

        function generateChartProvince(result){
            result = $.parseJSON(result);
            var ChartProvince = new Chart(ctxprovince, {
                type: 'horizontalBar',
                data: {
                    labels: result['provinsi'],
                    datasets: [{
                        label: 'Jumlah Anggota',
                        data: result['jumlah'],
                        backgroundColor: result['arr_bgcolor'],
                        borderColor: result['arr_bordercolor'],
                        borderWidth: 1
                    }]
                },
                options: {
                  scales: {
                    xAxes: [{
                      ticks: {
                          beginAtZero:true
                      }
                    }]
                  }
                }
            });
        }

        //DATA KOTA 10 TERATAS
        var ctxkota = document.getElementById("ChartCity");
        var getChartKota = $.ajax({url: "<?php echo base_url() ?>informasi/getChartKota", success: function(result){
            generateChartKota(result);
        }});

        function generateChartKota(result){
            result = $.parseJSON(result);
            var ChartCity = new Chart(ctxkota, {
                type: 'horizontalBar',
                data: {
                    labels: result['nama'],
                    datasets: [{
                        label: 'Jumlah Anggota',
                        data: result['jumlah'],
                        backgroundColor: result['arr_bgcolor'],
                        borderColor: result['arr_bordercolor'],
                        borderWidth: 1
                    }]
                },
                options: {
                  scales: {
                    xAxes: [{
                      ticks: {
                          beginAtZero:true
                      }
                    }]
                  }
                }
            });
        }
        
        
    })
    
  </script>
  
  <script>
    
  </script>