<?php
class Berita extends CI_Controller{
    var $webconfig;
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->sharedModel('BeritaModel');

        $this->load->sharedModel('AboutModel');
        $this->about = $this->AboutModel->listData();
        $this->load->sharedModel('InformasiModel');
        $this->informasi = $this->InformasiModel->listData();
        $this->load->sharedModel('ConfigurationModel');
        $this->configuration = $this->ConfigurationModel->listData();
        $this->load->sharedModel('RegulationModel');
        $this->regulation = $this->RegulationModel->listData();

        $this->webconfig = $this->config->item('webconfig');
        $this->module_name = $this->lang->line('home');
    }
    
    function index(){
        redirect(base_url().'berita/listing');
    }

    function listing($page = 0){
        $tdata = array();

        $all_data = $this->BeritaModel->listDataCount();
        $cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = 9;
        $cfg_pg ['uri_segment'] = 3;

        $cfg_pg['first_link'] = '&laquo;';
        $cfg_pg['first_tag_close'] = '</li>';
        $cfg_pg['first_tag_open'] = '<li class="page-item page-link">';

        $cfg_pg['last_link'] = '&raquo;';
        $cfg_pg['last_tag_open'] = '<li class="page-item page-link">';
        $cfg_pg['last_tag_close'] = '</li>';

        $cfg_pg['next_link'] = '&gt;';
        $cfg_pg['next_tag_open'] = '<li class="page-item page-link">';
        $cfg_pg['next_tag_close'] = '</li>';

        $cfg_pg['prev_link'] = '&lt;';
        $cfg_pg['prev_tag_open'] = '<li class="page-item page-link">';
        $cfg_pg['prev_tag_close'] = '</li>';

        $cfg_pg['num_tag_open'] = '<li class="page-item page-link">';
        $cfg_pg['num_tag_close'] = '</li>';

        $cfg_pg['cur_tag_open'] = '<li class="page-item page-link active"><span>';
        $cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
        $listdata["pagination"] = $this->pagination->create_links();

        $start_no = 0;
        if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
            $start_no = $this->uri->segment($cfg_pg ['uri_segment']);
        }

        $listdata["start_no"] = $start_no;

        $start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
        $start_idx = $start_idx < 0?0:$start_idx;

        $listdata['lists'] = $this->BeritaModel->listData(array(
                                                                'start'  => $start_idx
                                                                ,'limit' => $cfg_pg['per_page']
                                                            ));
        
        $ldata['content'] = $this->load->view($this->router->class.'/index',$listdata, true);
        $this->load->sharedView('template', $ldata);
    }
    function getAllData($page = 0){
        
        $all_data = $this->BeritaModel->listDataCount();
        $cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = 9;
        $cfg_pg ['uri_segment'] = 3;

        $cfg_pg['first_link'] = '&laquo;';
        $cfg_pg['first_tag_close'] = '</li>';
        $cfg_pg['first_tag_open'] = '<li class="page-item page-link">';

        $cfg_pg['last_link'] = '&raquo;';
        $cfg_pg['last_tag_open'] = '<li class="page-item page-link">';
        $cfg_pg['last_tag_close'] = '</li>';

        $cfg_pg['next_link'] = '&gt;';
        $cfg_pg['next_tag_open'] = '<li class="page-item page-link">';
        $cfg_pg['next_tag_close'] = '</li>';

        $cfg_pg['prev_link'] = '&lt;';
        $cfg_pg['prev_tag_open'] = '<li class="page-item page-link">';
        $cfg_pg['prev_tag_close'] = '</li>';

        $cfg_pg['num_tag_open'] = '<li class="page-item page-link">';
        $cfg_pg['num_tag_close'] = '</li>';

        $cfg_pg['cur_tag_open'] = '<li class="page-item page-link active"><span>';
        $cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
        $listdata["pagination"] = $this->pagination->create_links();

        $start_no = 0;
        if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
            $start_no = $this->uri->segment($cfg_pg ['uri_segment']);
        }

        $listdata["start_no"] = $start_no;

        $start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
        $start_idx = $start_idx < 0?0:$start_idx;

        $listdata['lists'] = $this->BeritaModel->listData(array(
                                                                'start'  => $start_idx
                                                                ,'limit' => $cfg_pg['per_page']
                                                            ));
        
        $this->load->view($this->router->class.'/list', $listdata);
    }
    function searchdata($name = 'name'){
        if($name == '-'){$name = '';}

        # PAGINATION #
        $all_data = $this->BeritaModel->filterDataCount(array('name'=>trim(urldecode($name))));
        $cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/".$this->uri->segment(3)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 4;

        $cfg_pg['first_link'] = '&laquo;';
        $cfg_pg['first_tag_close'] = '</li>';
        $cfg_pg['first_tag_open'] = '<li class="page-item page-link">';

        $cfg_pg['last_link'] = '&raquo;';
        $cfg_pg['last_tag_open'] = '<li class="page-item page-link">';
        $cfg_pg['last_tag_close'] = '</li>';

        $cfg_pg['next_link'] = '&gt;';
        $cfg_pg['next_tag_open'] = '<li class="page-item page-link">';
        $cfg_pg['next_tag_close'] = '</li>';

        $cfg_pg['prev_link'] = '&lt;';
        $cfg_pg['prev_tag_open'] = '<li class="page-item page-link">';
        $cfg_pg['prev_tag_close'] = '</li>';

        $cfg_pg['num_tag_open'] = '<li class="page-item page-link">';
        $cfg_pg['num_tag_close'] = '</li>';

        $cfg_pg['cur_tag_open'] = '<li class="page-item page-link active"><span>';
        $cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
        $listdata["pagination"] = $this->pagination->create_links();

        $start_no = 0;
        if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
            $start_no = $this->uri->segment($cfg_pg ['uri_segment']);
        }

        $listdata["start_no"] = $start_no;

        $start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
        $start_idx = $start_idx < 0?0:$start_idx;

        $listdata['lists'] = $this->BeritaModel->filterData(array(
                                                                'start'  => $start_idx
                                                                ,'limit' => $cfg_pg['per_page']
                                                                ,'name' => trim(urldecode($name))
                                                            ));
        // $this->load->view($this->router->class.'/list', $listdata);
        $ldata['content'] = $this->load->view($this->router->class.'/index',$listdata, true);
        $this->load->sharedView('template', $ldata);
    }
    
    function read($id = '0') {
        $tdata = array();
        $tdata['lists'] = $this->BeritaModel->listData(array('id' => $id));
        if (count($tdata['lists']) == 0) {
            redirect(base_url());
        }
        $tdata['newarticle'] = $this->BeritaModel->listData(array('expid' => $id, 'limit' => 5));
        $allarticle = $this->BeritaModel->listData();
        $tdata['randomarticle'] = $this->array_random_assoc($allarticle);
        
        $ldata['content'] = $this->load->view($this->router->class.'/detail',$tdata, true);
        $this->load->sharedView('template', $ldata);
    }

    function array_random_assoc($arr, $num = 3) {
        $keys = array_keys($arr);
        shuffle($keys);
        
        $r = array();
        if(count($arr) > 3){
            for ($i = 0; $i < $num; $i++) {
                $r[$keys[$i]] = $arr[$keys[$i]];
            }
        }
        
        return $r;
    }
}