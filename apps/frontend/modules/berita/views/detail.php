<script type="text/javascript">
  jQuery(document).ready(function($) {

  });
  function searchThis(){
    var frm = document.searchform;
    var name = frm.name.value;
    if(name == ''){ name = '-'; }
    window.location = "<?php echo base_url().$this->router->class; ?>/searchdata/"+name.replace(/\s/g,'%20');
  }
</script>
<section class="ipspi-search">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <form id="searchform" name="searchform" class="form-inline form-search" action="" method="post" onsubmit="searchThis();return false;">
          <input class="form-control search-large" name="name" type="search" placeholder="Pencarian.." aria-label="Search">
          <button class="btn btn-search" onclick="searchThis();return false;" type="submit">CARI</button>
          <input type="submit" style="display:none;">
        </form>
      </div>
    </div>
  </div>
</section>

<section class="article-detail">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <div class="article-item-detail">
          <div class="article-meta">
              <div class="ar-date"><i class="far fa-clock"></i><?php echo $lists[0]['datepublish'];?></div>
              <div class="ar-author"><i class="far fa-user"></i>IPSPI</div>
          </div>
          <h1><?php echo stripcslashes($lists[0]['title']);?></h1>
          <div class="article-detail-img">
            <img src="<?php echo thumb_image($lists[0]['path'],'658x496', 'berita'); ?>" alt="Article Image" class="img-fluid">
          </div>
          <article>
            <?php echo stripcslashes($lists[0]['body']);?>
          </article>
        </div>
        
        <div class="related-article">
          <div class="row">
            <div class="col-12">
              <div class="heading-alt">Berita Lainnya</div>
            </div>
          </div>
          <div class="row">
            <?php if(isset($randomarticle) && count($randomarticle) > 0){?>
              <?php foreach ($randomarticle as $key => $listdata) {?>
                <div class="col-sm-4">
                  <div class="related-list">
                    <a href="<?php echo base_url().'berita/read/'.$listdata['id'].'/'.$listdata['slug']; ?>">
                      <img src="<?php echo thumb_image($listdata['path'],'350x263', 'berita'); ?>" alt="News" class="img-fluid">
                      <div class="related-meta-date"><?php echo $listdata['datepublish'];?></div>
                      <div class="related-meta-title"><?php echo $listdata['title'];?></div>
                    </a>
                  </div>
                </div>
              <?php } ?>
            <?php } ?>
            
          </div>
        </div>
        
      </div>
      <div class="col-lg-4">
        <div class="widget-container">
          <div class="widget-title">Berita Terbaru</div>
          <div class="widget-wrap">
            <?php if(isset($newarticle) && count($newarticle) > 0){?>
              <?php foreach ($newarticle as $key => $listdata) {?>
                <div class="latest-item">
                  <div class="latest-item-img">
                    <a href="<?php echo base_url().'berita/read/'.$listdata['id'].'/'.$listdata['slug']; ?>"><img src="<?php echo thumb_image($listdata['path'],'134x100', 'berita'); ?>" alt="News"></a>
                  </div>
                  <div class="latest-item-meta">
                    <div class="meta-date"><?php echo $listdata['datepublish'];?></div>
                    <div class="meta-title">
                      <a href="<?php echo base_url().'berita/read/'.$listdata['id'].'/'.$listdata['slug']; ?>"><?php echo $listdata['title'];?></a>
                    </div>
                  </div>
                </div>
              <?php } ?>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>