<script type="text/javascript">
  jQuery(document).ready(function($) {
    // jQuery("#listData").load('<?php echo base_url().$this->router->class; ?>/getAllData/');
  });
  function searchThis(){

    var frm = document.searchform;
    var name = frm.name.value;
    if(name == ''){ name = '-'; }
    window.location = "<?php echo base_url().$this->router->class; ?>/searchdata/"+encode_js(name.replace(/\s/g,'%20').trim());
  }
</script>
<section class="head-wrap">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1 class="head-title">Berita Terbaru</h1>
      </div>
    </div>
  </div>
</section>

<section class="ipspi-search">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <form id="searchform" name="searchform" class="form-inline form-search" action="" method="post" onsubmit="searchThis();return false;">
            <input class="form-control search-large" name="name" type="search" placeholder="Pencarian.." aria-label="Search">
            <button class="btn btn-search" onclick="searchThis();return false;" type="submit">CARI</button>
            <input type="submit" style="display:none;">
          </form>
        </div>
      </div>
    </div>
</section>

<div id="listData">
    <section class="ipspi-latest-news">
        <div class="container">
          <div class="row">
            <?php if(isset($lists) && count($lists) > 0) {?>
            <?php $i = $start_no; foreach ($lists as $listdata) { $i++; ?>
            <div class="col-sm-4">
              <div class="article-item">
                <a href="<?php echo base_url().'berita/read/'.$listdata['id'].'/'.$listdata['slug']; ?>"><img src="<?php echo thumb_image($listdata['path'],'350x263', 'berita'); ?>" alt="News" class="img-fluid img-article"></a>
                <div class="article-meta">
                  <div class="ar-date"><i class="far fa-clock"></i><?php echo $listdata['datepublish'];?></div>
                  <div class="ar-author"><i class="far fa-user"></i>IPSPI</div>
                  <div class="ar-title head3"><a href="<?php echo base_url().'berita/read/'.$listdata['id'].'/'.$listdata['slug']; ?>"><?php echo $listdata['title'];?></a></div>
                  <div class="ar-desc"><?php echo stripcslashes($listdata['description']);?></div>
                </div>
              </div>
            </div>
            <?php } ?>
            <?php }else{
              ?>
              <div class="col-sm-12 text-center" style="text-align: center;">
                Berita Tidak Ditemukan
              </div>
              <?php
            } ?>
            
          </div>
          
          <nav aria-label="page">
            <ul class="pagination justify-content-center">
              <?php echo isset($pagination)?$pagination:""; ?>
              <!-- <li class="page-item disabled">
                <a class="page-link" href="#" aria-label="Previous">
                  <span aria-hidden="true">&laquo;</span>
                  <span class="sr-only">Previous</span>
                </a>
              </li>
              <li class="page-item active"><a class="page-link" href="#">1</a></li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item"><a class="page-link" href="#">100</a></li>
              <li class="page-item">
                <a class="page-link" href="#" aria-label="Next">
                  <span aria-hidden="true">&raquo;</span>
                  <span class="sr-only">Next</span>
                </a>
              </li> -->
            </ul>
          </nav>
          
        </div>
      </section>
</div>