<script type="text/javascript">
$(document).ready(function() {
    
});
</script>
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <div class="content-wrapper text-center p-5">
          <div class="icon-big"><i class="fas fa-check"></i></div>
          <div class="heading">Registrasi Berhasil</div>
          <p>Registrasi anda berhasil, Anda telah menjadi <strong>Anggota Tidak Tetap IPSPI.</strong></p>

          <p>Untuk menjadi <strong>Anggota Tetap IPSPI</strong> silahkan melakukan login dan melakukan pembayaran di halaman pembayaran.</p>
          <a href="<?php echo base_url(); ?>login" class="btn btn-green px-5 mt-3">Masuk</a>
          
        </div>
      </div>
    </div>
  </div>
</section>