<link href="<?php echo $this->webconfig['back_base_template']; ?>plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
<script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.datepicker').datepicker({
        format: "dd-mm-yyyy",
        autoclose: true
    });
    <?php  if($this->session->userdata('id_propinsi') !=''){?>
        getkota(<?php echo $this->session->userdata('id_propinsi');?>);
    <?php } ?>
    $("#id_propinsi").change(function(){
        $("#id_kota").html('');
        var arr = {prop:$(this).val()};
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>wilayah/getkota",
            data: arr,
            dataType: "json",
            success: function(data) {
                var html = '<option value="">-- Pilih Kota/Kabupaten --</option>';
                $("#id_kota").html(html);
                if(data.length > 0) {
                    $.each(data, function(key, val) {
                        $("#id_kota").append($("<option />").val(val.id).text(val.nama));
                    });   
                }
            }
        });
    });


    // jQuery("#listData").load('<?php echo base_url().$this->router->class; ?>/getProfile/');
});

function checksubmit(){
    var nik = $('#ktp').val();
    // alert(nik)
    $.ajax({
        type: 'POST',
        url: "<?php echo base_url(); ?>webservice/getnik",
        data: {nik:nik},
        success: function(data) {
            if (data == 'success') {
              $('.form-reg').submit();
            }else{
              alert('NIK sudah terdaftar');
              return false;
            }
        }
    });
    return false;
}

function getkota(id_propinsi){
    $("#id_kota").html('');
    var arr = {prop:id_propinsi};
    $.ajax({
        type: 'POST',
        url: "<?php echo base_url(); ?>wilayah/getkota",
        data: arr,
        dataType: "json",
        success: function(data) {
            var html = '<option value="">-- Pilih Kota/Kabupaten--</option>';
            $("#id_kota").html(html);
            var citydb = '<?php echo $this->session->userdata('id_kota'); ?>';
            if(data.length > 0) {
              $.each(data, function(key, val) {
                if(val.id == citydb){
                  $("#id_kota").append($("<option />").val(val.id).text(val.nama).prop('selected', true));
                }else{
                   $("#id_kota").append($("<option />").val(val.id).text(val.nama));
                }
              });
            }
        }
    });
}
</script>
<section class="register-step">
  <div class="container">
    <div class="row">
      <div class="col-12 text-center">
        <div class="heading mt-5">Registrasi</div>
        <div class="step-wrap">
          <div class="step-list active">
            <div class="step-list-item">1</div>
            <div class="step-list-desc">Profil</div>
          </div>
          <div class="step-list">
            <div class="step-list-item">2</div>
            <div class="step-list-desc">Pendidikan</div>
          </div>
          <div class="step-list">
            <div class="step-list-item">3</div>
            <div class="step-list-desc">Pekerjaan</div>
          </div>
          <div class="step-list">
            <div class="step-list-item">4</div>
            <div class="step-list-desc">Praktek Peksos</div>
          </div>
          <div class="step-list">
            <div class="step-list-item">5</div>
            <div class="step-list-desc">Penghargaan</div>
          </div>
          <div class="step-list">
            <div class="step-list-item">6</div>
            <div class="step-list-desc">Sertifikasi</div>
          </div>
          <div class="step-list">
            <div class="step-list-item">7</div>
            <div class="step-list-desc">Referensi</div>
          </div>
          <div class="step-list">
            <div class="step-list-item">8</div>
            <div class="step-list-desc">Organisasi Profesi</div>
          </div>
          <div class="step-list">
            <div class="step-list-item">9</div>
            <div class="step-list-desc">Komunitas Internet</div>
          </div>
          <div class="step-list">
            <div class="step-list-item">10</div>
            <div class="step-list-desc">Resume Keanggotaan</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <div class="content-wrapper p-5">
          <h3 class="text-center">PROFIL</h3>
          <hr class="line my-4">
          <form name="form" method="POST" action="<?php echo base_url().$this->router->class; ?>" enctype="multipart/form-data" class="form-reg">
            <div class="form-group">
              <label for="formName">Nama Lengkap<span>*</span></label>
              <input type="text" class="form-control" id="formName" name="nama" value="<?php echo $this->session->userdata('nama');?>" required>
            </div>
            <div class="form-group">
              <label>Jenis Kelamin<span>*</span></label><br/>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" id="inlineRadio1" name="jk" value="1" <?php if($this->session->userdata('jk') == 1){ echo "checked=checked"; }?> required>
                <label class="form-check-label" for="inlineRadio1" <?php if($this->session->userdata('jk') == 2){ echo "checked=checked"; }?>>Laki-Laki</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio"  id="inlineRadio2" name="jk" value="2" required>
                <label class="form-check-label" for="inlineRadio2">Perempuan</label>
              </div>
            </div>
            <div class="form-row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="formBirthPlace">Tempat Lahir<span>*</span></label>
                  <input type="text" class="form-control" id="formBirthPlace" name="tmptlahir" value="<?php echo $this->session->userdata('tmptlahir');?>" required>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="formBirthDate">Tanggal Lahir<span>*</span></label>
                  <input style="padding: 0.7rem 1.5rem;border: 1px solid #ced4da;border-radius: 20px;" type="text" class="form-control datepicker" id="formBirthDate" placeholder="dd/mm/yyyy" name="tgllahir" value="<?php echo $this->session->userdata('tgllahir');?>" required>
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="formMarriage">Status Perkawinan<span>*</span></label>
                  <select class="form-control" name="statuskawin" required>
                    <option value="">- Pilih -</option>
                    <option value="0" <?php if($this->session->userdata('statuskawin') == 0){ echo "selected"; }?>>Belum Menikah</option>
                    <option value="1" <?php if($this->session->userdata('statuskawin') == 1){ echo "selected"; }?>>Sudah Menikah</option>
                    <option value="2" <?php if($this->session->userdata('statuskawin') == 2){ echo "selected"; }?>>Duda</option>
                    <option value="3" <?php if($this->session->userdata('statuskawin') == 3){ echo "selected"; }?>>Janda</option>
                  </select>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="formChildren">Jumlah Anak</label>
                  <input type="text" class="form-control" id="formChildren" name="jumanak" value="<?php echo $this->session->userdata('jumanak');?>">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="ktp">No KTP / NIK<span>*</span></label>
              <input type="text" class="form-control" id="ktp" name="ktp" value="<?php echo $this->session->userdata('ktp');?>" required>
            </div>
            <div class="form-group">
              <label for="formAddressOffice">Alamat Kantor<span>*</span></label>
              <input type="text" class="form-control" id="formAddressOffice" name="alamatkantor" value="<?php echo $this->session->userdata('alamatkantor');?>" required>
            </div>
            <div class="form-row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="formPhoneOffice">Telepon Kantor<span>*</span></label>
                  <input type="text" class="form-control" id="formPhoneOffice" name="telpkantor" value="<?php echo $this->session->userdata('telpkantor');?>" required>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="formFaxOffice">Fax kantor</label>
                  <input type="text" class="form-control" id="formFaxOffice" name="faxkantor" value="<?php echo $this->session->userdata('faxkantor');?>">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="formAddressHome">Alamat Rumah<span>*</span></label>
              <input type="text" class="form-control" id="formAddressHome" name="alamatrumah" value="<?php echo $this->session->userdata('alamatrumah');?>" required>
            </div>
            <div class="form-row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="formProvince">Provinsi<span>*</span></label>
                  <select name="id_propinsi" class="form-control select2" id="id_propinsi" required>
                    <option value="">-- Propinsi --</option>
                    <?php foreach ($wilayah as $key => $value){ ?>
                        <option <?php echo ($this->session->userdata('id_propinsi') == $value['id'])?"selected='selected'":""; ?> value="<?php echo $value['id']; ?>"><?php echo $value['nama']; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="formCity">Kabupaten / Kota<span>*</span></label>
                  <select name="id_kota" class="form-control select2" id="id_kota" required>
                    <option value="">-- Kota --</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="formPhone">Telepon</label>
                  <input type="text" class="form-control" id="formPhone" name="telp" value="<?php echo $this->session->userdata('telp');?>">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="formMobilePhone">No. Handphone<span>*</span></label>
                  <input type="text" class="form-control" id="formMobilePhone" name="hp" value="<?php echo $this->session->userdata('hp');?>" required>
                </div>
              </div>
            </div>
            <!-- <div class="form-group">
              <label>Upload Foto / Scan KTP<span>*</span></label>
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="customFile1" name="file_ktp" required>
                <label class="custom-file-label" for="customFile1">Upload File</label>
              </div>
              <div class="mt-3"><small>Maksimum file 1Mb</small></div>
            </div>
            <div class="form-group">
              <label>Upload Pas Foto<span>*</span></label>
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="customFile2" name="file_photo" required>
                <label class="custom-file-label" for="customFile2">Upload File</label>
              </div>
              <div class="mt-3"><small>Foto formal dengan background biru seukuran pas foto. Maksimum file 1Mb</small></div>
            </div> -->
            
            <hr class="line my-5">
            
            <h3 class="text-center">Akun</h3>
            
            <div class="form-group">
              <label for="formEmail">Alamat Email<span>*</span></label>
              <input type="email" class="form-control" id="formEmail" name="email" value="<?php echo $this->session->userdata('email');?>" required>
            </div>
            <div class="form-row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="formPassword">Password<span>*</span></label>
                  <input type="password" class="form-control" id="formPassword" name="password" required>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="formPasswordConfirm">Konfirmasi Password<span>*</span></label>
                  <input type="password" class="form-control" id="formPasswordConfirm" name="password_2" required>
                </div>
              </div>
            </div>
            
            <div class="addition-info">* Wajib diisi / required</div>
            <hr class="line my-5">
            
            <div class="text-right">
              <button type="submit" class="btn btn-lg btn-green px-5 btn-lanjut" onclick="checksubmit();return false;">Lanjut</button>
              <?php /*<input type="submit" class="btn btn-lg btn-green px-5 " value="Lanjut"> */ ?>
            </div>
          </form>
      
        </div>
      </div>
    </div>
  </div>
</section>