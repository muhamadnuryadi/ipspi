<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>IPSPI EMAIL</title>
<!-- <link href="styles.css" media="all" rel="stylesheet" type="text/css" /> -->
<style type="text/css">
@media only screen and (max-width: 640px) {
  body {
    padding: 0 !important;
  }

  h1, h2, h3, h4 {
    font-weight: 800 !important;
    margin: 20px 0 5px !important;
  }

  h1 {
    font-size: 22px !important;
  }

  h2 {
    font-size: 18px !important;
  }

  h3 {
    font-size: 16px !important;
  }

  .container {
    padding: 0 !important;
    width: 100% !important;
  }

  .content {
    padding: 0 !important;
  }

  .content-wrap {
    padding: 10px !important;
  }

  .invoice {
    width: 100% !important;
  }
}
body {
    background-color: #f6f6f6;
}
body {
    -webkit-font-smoothing: antialiased;
    -webkit-text-size-adjust: none;
    width: 100% !important;
    height: 100%;
    line-height: 1.6em;
    /* line-height: 22px; */
}
* {
    margin: 0;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    box-sizing: border-box;
    font-size: 14px;
}
.body-wrap {
    background-color: #f6f6f6;
    width: 100%;
}
table td {
    vertical-align: top;
}
.container {
    display: block !important;
    max-width: 600px !important;
    margin: 0 auto !important;
    clear: both !important;
}
.content {
    max-width: 600px;
    margin: 0 auto;
    display: block;
    padding: 20px;
}
.main {
    background-color: #fff;
    border: 1px solid #e9e9e9;
    border-radius: 3px;
}
.content-wrap {
    padding: 20px;
}
.content-block {
    padding: 0 0 20px;
}
.btn-primary {
    text-decoration: none;
    color: #FFF;
    background-color: #348eda;
    border: solid #348eda;
    border-width: 10px 20px;
    line-height: 2em;
    /* line-height: 28px; */
    font-weight: bold;
    text-align: center;
    cursor: pointer;
    display: inline-block;
    border-radius: 5px;
    text-transform: capitalize;
}
.footer {
    width: 100%;
    clear: both;
    color: #999;
    padding: 20px;
}
.footer p, .footer a, .footer td {
    color: #999;
    font-size: 12px;
}
.aligncenter {
    text-align: center;
}
a {
    color: #348eda;
    text-decoration: underline;
}
p, ul, ol {
  margin-bottom: 10px;
  font-weight: normal;
}
p li, ul li, ol li {
  margin-left: 5px;
  list-style-position: inside;
}
</style>
</head>

<body itemscope itemtype="http://schema.org/EmailMessage">

<table class="body-wrap">
	<tr>
		<td></td>
		<td class="container" width="600">
			<div class="content">
				<table class="main" width="100%" cellpadding="0" cellspacing="0" itemprop="action" itemscope itemtype="">
					<tr>
						<td class="content-wrap">
							<meta itemprop="name" content="Confirm Email Member"/>
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td class="content-block">
										<center><img width="100px" src="<?php echo $this->webconfig['frontend_template']; ?>images/IPSPI_logo.jpg"></center>
									</td>
								</tr>
								<tr>
									<td class="content-block">
										<h4>Konfirmasi Pendaftaran</h4>
									</td>
								</tr>
								<tr>
									<td class="content-block">
										<p>Dear <strong><?php echo $name; ?></strong></p>
                    <!-- <p>You have just registered as a member IPSPI. Click the button below to activate your account IPSPI :</p> -->
										<p>Untuk menjadi Anggota Tetap IPSPI silahkan melakukan login dan melakukan pembayaran di halaman pembayaran.</p>
									</td>
								</tr>
								<tr>
									<td class="content-block" style="padding-bottom: 0px;">
										<p>Warm Regards,<br/> 
		                <strong>Admin Website</strong></p>
		                <p><?php echo isset($this->configuration['address'])?$this->configuration['address']:'';?></p>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				</div>
		</td>
	</tr>
</table>

</body>
</html>
