<?php
class Register extends CI_Controller{
    var $webconfig;
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->library('cart');
        $this->load->helper('phpmailer');
        $this->load->sharedModel('MembersModel');
        $this->load->sharedModel('WilayahModel');

        $this->load->sharedModel('AboutModel');
        $this->about = $this->AboutModel->listData();
        $this->load->sharedModel('InformasiModel');
        $this->informasi = $this->InformasiModel->listData();
        $this->load->sharedModel('ConfigurationModel');
        $this->configuration = $this->ConfigurationModel->listData();
        $this->load->sharedModel('RegulationModel');
        $this->regulation = $this->RegulationModel->listData();
        
        $this->webconfig = $this->config->item('webconfig');
        $this->module_name = $this->lang->line('about');
    }
    function index(){
        $tdata = array();
        $tdata['wilayah'] = $this->WilayahModel->listpropinsi();
        if($_POST){

            $this->session->set_userdata('nama',  $this->input->post('nama'));
            $this->session->set_userdata('jk',  $this->input->post('jk'));
            $this->session->set_userdata('tmptlahir',  $this->input->post('tmptlahir'));
            $this->session->set_userdata('tgllahir',  $this->input->post('tgllahir'));
            $this->session->set_userdata('statuskawin',  $this->input->post('statuskawin'));
            $this->session->set_userdata('jumanak',  $this->input->post('jumanak'));
            $this->session->set_userdata('alamatkantor',  $this->input->post('alamatkantor'));
            $this->session->set_userdata('ktp',  $this->input->post('ktp'));
            $this->session->set_userdata('telpkantor',  $this->input->post('telpkantor'));
            $this->session->set_userdata('faxkantor',  $this->input->post('faxkantor'));
            $this->session->set_userdata('alamatrumah',  $this->input->post('alamatrumah'));
            $this->session->set_userdata('id_propinsi',  $this->input->post('id_propinsi'));
            $this->session->set_userdata('id_kota',  $this->input->post('id_kota'));
            $this->session->set_userdata('telp',  $this->input->post('telp'));
            $this->session->set_userdata('hp',  $this->input->post('hp'));
            $this->session->set_userdata('email',  $this->input->post('email'));
            $this->session->set_userdata('password',  $this->input->post('password'));
            
            return redirect('register/pendidikan', $tdata, true);
        }else{
            $ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
            $this->load->sharedView('template', $ldata);
        }

    }
    
    public function pendidikan($id = 0){
        $tdata = array();
        $tdata['tingkat'] = $this->MembersModel->listTingkat();
        if($_POST){
        
        $this->session->unset_userdata('pendidikan');

        $result = array();
        foreach ($this->input->post('tingkat') as $key => $value) {
            $result[$key]['tingkat'] = $value;
            $result[$key]['pt'] = $this->input->post('pt')[$key];
            $result[$key]['jurusan'] = $this->input->post('jurusan')[$key];
            $result[$key]['gelar'] = $this->input->post('gelar')[$key];
            $result[$key]['thn_lulus'] = $this->input->post('thn_lulus')[$key];
        }

        foreach ($this->input->post('jenis_pendidikan') as $key => $value) {
            $result[$key]['jenis_pendidikan'] = $value;
        }

        $this->session->set_userdata('pendidikan', $result);

        return redirect('register/pekerjaan', $tdata, true);
            
        }else{
            $ldata['content'] = $this->load->view($this->router->class.'/pendidikan',$tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }

    public function pekerjaan($id = 0){
        $tdata = array();
        $tdata['jenis_pekerjaan'] = $this->MembersModel->listPekerjaan();
        $tdata['jenis_instansi'] = $this->MembersModel->listInstansi();

        if($_POST){
        
        $this->session->unset_userdata('pekerjaan');
        

        $result = array();
        foreach ($this->input->post('nama') as $key => $value) {
            $result[$key]['nama'] = $value;
            $result[$key]['jabatan'] = $this->input->post('jabatan')[$key];
            $result[$key]['mulai'] = $this->input->post('mulai')[$key];
            $result[$key]['akhir'] = $this->input->post('akhir')[$key];
        }
        
        foreach ($this->input->post('jenis_pekerjaan') as $key => $value) {
            $result[$key]['jenis_pekerjaan'] = $value;
        }

        foreach ($this->input->post('jenis_instansi') as $key => $value) {
            $result[$key]['jenis_instansi'] = $value;
        }

        $this->session->set_userdata('pekerjaan', $result);

        return redirect('register/praktek', $tdata, true);
            
        }else{
            $ldata['content'] = $this->load->view($this->router->class.'/pekerjaan',$tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }

    public function praktek($id = 0){
        $tdata = array();
        
        if($_POST){
        
        $this->session->unset_userdata('praktek');

        $result = array();
        foreach ($this->input->post('setting') as $key => $value) {
            $result[$key]['setting'] = $value;
            $result[$key]['tahun'] = $this->input->post('tahun')[$key];
            $result[$key]['jabatan'] = $this->input->post('jabatan')[$key];
            $result[$key]['peran'] = $this->input->post('peran')[$key];
            $result[$key]['keahlian'] = $this->input->post('keahlian')[$key];
        }
        
        $this->session->set_userdata('praktek', $result);

        return redirect('register/penghargaan', $tdata, true);
            
        }else{
            $ldata['content'] = $this->load->view($this->router->class.'/praktek',$tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }

    public function penghargaan($id = 0){
        $tdata = array();
        
        if($_POST){
        
        $this->session->unset_userdata('penghargaan');
        
        $result = array();
        foreach ($this->input->post('nama') as $key => $value) {
            $result[$key]['nama'] = $value;
            $result[$key]['instansi'] = $this->input->post('instansi')[$key];
            $result[$key]['tahun'] = $this->input->post('tahun')[$key];
        }
        
        $this->session->set_userdata('penghargaan', $result);

        return redirect('register/sertifikasi', $tdata, true);
            
        }else{
            $ldata['content'] = $this->load->view($this->router->class.'/penghargaan',$tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }

    public function sertifikasi($id = 0){
        $tdata = array();
        
        if($_POST){
        
        $this->session->unset_userdata('sertifikasi');
        
        $result = array();
        foreach ($this->input->post('nama') as $key => $value) {
            $result[$key]['nama'] = $value;
            $result[$key]['lembaga'] = $this->input->post('lembaga')[$key];
            $result[$key]['tahun'] = $this->input->post('tahun')[$key];
            $result[$key]['jenis_sertifikasi'] = $this->input->post('jenis_sertifikasi')[$key];
        }
        
        $this->session->set_userdata('sertifikasi', $result);

        return redirect('register/referensi', $tdata, true);
            
        }else{
            $ldata['content'] = $this->load->view($this->router->class.'/sertifikasi',$tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }

    public function referensi($id = 0){
        $tdata = array();
        
        if($_POST){
        
        $this->session->unset_userdata('referensi');
        
        $result = array();
        foreach ($this->input->post('nama') as $key => $value) {
            $result[$key]['nama'] = $value;
            $result[$key]['jabatan'] = $this->input->post('jabatan')[$key];
            $result[$key]['telp'] = $this->input->post('telp')[$key];
        }
        
        $this->session->set_userdata('referensi', $result);

        return redirect('register/profesi', $tdata, true);
            
        }else{
            $ldata['content'] = $this->load->view($this->router->class.'/referensi',$tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }

    public function profesi($id = 0){
        $tdata = array();
        
        if($_POST){
        
        $this->session->unset_userdata('profesi');
        
        $result = array();
        foreach ($this->input->post('nama') as $key => $value) {
            $result[$key]['nama'] = $value;
            $result[$key]['tahun'] = $this->input->post('tahun')[$key];
            $result[$key]['keterangan'] = $this->input->post('keterangan')[$key];
        }
        
        $this->session->set_userdata('profesi', $result);

        return redirect('register/komunitas', $tdata, true);
            
        }else{
            $ldata['content'] = $this->load->view($this->router->class.'/profesi',$tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }

    public function komunitas($id = 0){
        $tdata = array();
        
        if($_POST){
        $this->session->unset_userdata('komunitas');
        
        $result = array();
        foreach ($this->input->post('nama') as $key => $value) {
            $result[$key]['nama'] = $value;
            $result[$key]['alamat'] = $this->input->post('alamat')[$key];
        }
        
        $this->session->set_userdata('komunitas', $result);

        return redirect('register/resume', $tdata, true);
            
        }else{
            $ldata['content'] = $this->load->view($this->router->class.'/komunitas',$tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }

    public function resume($id = 0){
        $tdata = array();

        $tdata['tingkat'] = $this->MembersModel->listTingkat();
        $tdata['jenis_pekerjaan'] = $this->MembersModel->listPekerjaan();
        $tdata['jenis_instansi'] = $this->MembersModel->listInstansi();

        require_once(dirname(__FILE__) . '/../../../libraries/formvalidator.php');
        if($_POST){
            
            $validator = new FormValidator();
            $validator->addValidation("faq", "req", 'Silahkan menyetujui Kebijakan dan Ketentuan IPSPI');
            if ($validator->ValidateForm()) {
                $doInsert = $this->MembersModel->entriData(array(
                                                            'faq'=>$this->input->post('faq')
                                                      ));
                
                
                if($doInsert == 'empty'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
                }else if($doInsert == 'exist_email'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('error_duplicate_email'));
                }else if($doInsert == 'failed'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
                }else if($doInsert == 'not_same'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_password_not_same'));
                }else if($doInsert == 'failed_send'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_failed_send_email'));
                }else if($doInsert == 'empty_ktp'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_ktp'));
                }else if($doInsert == 'empty_foto'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_foto'));
                }else if($doInsert == 'success'){
                    $tdata['success'] = $this->lang->line('msg_success_registration');
                    return redirect('register/success_reg', $tdata, true);
                }

                $ldata['content'] = $this->load->view($this->router->class.'/resume',$tdata, true);
                $this->load->sharedView('template', $ldata);
            } else {
                
                $tdata['faq'] = $this->input->post('faq');
                $tdata['error_hash'] = $validator->GetErrors();
                
                $ldata['content'] = $this->load->view($this->router->class.'/resume',$tdata, true);
                $this->load->sharedView('template', $ldata);
            }
        }else{
            $ldata['content'] = $this->load->view($this->router->class.'/resume',$tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }

    public function confirmation($key = '')
    {
        $tdata = $fdata = array();
        $this->pagename = "Confirmation";
        
        $doValidate = $this->MembersModel->confirmation($key);
        if($doValidate == 'success'){
            $tdata['message'] = $this->lang->line('msg_success_confirmation');
            $tdata['status'] = 'success';
            // $check = $this->LoginModel->dologin_confirmation($key);
            redirect(base_url().'register/success_reg');
        }else{
            $tdata['message'] = $this->lang->line('msg_error_confirmation');
            $tdata['status'] = 'error';
            redirect(base_url().'register');
        }
    }

    public function success_reg()
    {
        $tdata = $fdata = array();
        $this->session->sess_destroy();
        $this->pagename = "Success Registration";
        // $tdata['category'] = $this->CategoryModel->listData();

        // $tdata['member'] = $this->MembersModel->detailMember($this->session->userdata('memberid'), $this->session->userdata('memberemail'));
        
        ## LOAD LAYOUT ##
        $ldata['content'] = $this->load->view($this->router->class.'/success2',$tdata, true);
        $this->load->sharedView('template', $ldata);
    }

    public function detailMember($id = 0, $email = ""){
        $q = $this->db->query("
            SELECT
                *
            FROM
                ".$this->maintablename."
            WHERE id = ".$id." AND email = '".$this->db->escape_str($email)."'
        ");
        $result = $q->first_row('array');
        return $result;
    }

    function tes_email($id = 0){
        $result = send_mail(array(
                    'email_tujuan' => 'moeh.noeryadie@gmail.com'
                    ,'email_tujuan_text' => 'sasa'
                    ,'subject' => 'sasa'
                    ,'setfrom_email' => SENDER_EMAIL
                    ,'body' => 'dssds'
                ));
    }
}