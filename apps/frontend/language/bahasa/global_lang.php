<?php
$lang['app_logo'] = "IPSPI";

$lang['login_web_title'] = "Sistem Keamanan Login";
$lang['login_username'] = "username";
$lang['login_password'] = "password";
$lang['login_btn'] = "Masuk";

$lang['master'] = "Master Data";
$lang['no_content'] = "Tidak Ada Konten";
$lang['no_data'] = "Tidak Ada Data";
$lang['breadcrumb_home'] = "Beranda";

$lang['navigation_save'] = "Simpan";
$lang['navigation_add'] = "Tambah Data";
$lang['navigation_list'] = "Daftar Data";
$lang['navigation_search'] = "Cari";
$lang['navigation_modif'] = "Ubah";
$lang['navigation_modif_breadcrumb'] = "Ubah Data";
$lang['navigation_delete'] = "Hapus";
$lang['navigation_publish'] = "Aktifkan";
$lang['navigation_unpublish'] = "Nonaktifkan";
$lang['navigation_download'] = "Download";
$lang['navigation_crop'] = "Crop";
$lang['navigation_reset_search'] = "Reset";
$lang['navigation_data_aktif'] = "Data aktif";
$lang['navigation_data_nonaktif'] = "Data tidak aktif";
$lang['navigation_kirim'] = "Kirim";

$lang['tablelist_no'] = "No.";
$lang['tablelist_option'] = "Opsi";
$lang['tablelist_choose'] = "Pilih";
$lang['tanggal_combo'] = "--Tanggal--";
$lang['bulan_combo'] = "--Bulan--";
$lang['tahun_combo'] = "--Tahun--";
$lang['label_yes'] = "Ya";
$lang['label_no'] = "Tidak";

$lang['info_notif'] = "Informasi";
$lang['error_notif'] = "Pesan Error";
$lang['success_notif'] = "Pesan Sukses";

$lang['msg_empty_send'] = "Silahkan pilih salah satu data";
$lang['msg_success_send'] = "Newsletter telah berhasil di kirim ke subscriber";
$lang['msg_empty_delete'] = "Silahkan pilih salah satu data";
$lang['msg_empty_forward'] = "Surat masuk gagal di teruskan";
$lang['msg_success_delete'] = "Data telah dihapus dari database";
$lang['msg_success_send_revisi'] = "Revisi Sudah berhasil dikirimkan";
$lang['msg_failed_revision'] = "Revisi gagal dikirimkan";
$lang['msg_success_forward'] = "Surat telah berhasil diteruskan";
$lang['msg_error_database'] = "Maaf, telah terjadi kesalahan pada database. Silahkan mencoba beberapa saat lagi, atau menghubungi Administrator.";
$lang['msg_error_empty_field'] = "Silahkan memeriksa kembali form isian Anda.";
$lang['msg_error_empty_file_image'] = "Silahkan mengunggah file gambar";
$lang['msg_error_empty_article'] = "Silahkan masukkan minimal satu artikel";
$lang['msg_success_entry'] = "Data telah ditambahkan ke dalam database.";
$lang['msg_success_update'] = "Data telah disimpan ke dalam database.";
$lang['msg_success_publish'] = "Data telah diaktifkan.";
$lang['msg_empty_publish'] = "Data gagal diaktifkan.";
$lang['msg_success_unpublish'] = "Data telah dinonaktifkan.";
$lang['msg_empty_unpublish'] = "Data gagal dinonaktifkan.";
$lang['msg_success_entry_info'] = "Data telah disimpan ke dalam database.";
$lang['msg_error_interval1_empty'] = "Silahkan mengisi tanggal awal";
$lang['msg_error_interval2_empty'] = "Silahkan mengisi tanggal akhir";
$lang['msg_error_interval'] = "Tanggal mulai tidak boleh lebih besar dari tanggal akhir";
$lang['msg_error_minimum_dimensi_gambar'] = "Silahkan memilih gambar dengan dimensi yang lebih besar";

$lang['error_username_alfanumeric_users'] = "Silahkan mengisi username berupa huruf, angka, _ dan -";

$lang['alert_forward'] = "Apakah anda yakin akan meneruskan surat ini?";
$lang['alert_ditinjau'] = "Apakah anda yakin akan mengembalikan surat ini?";
$lang['alert_delete'] = "Apakah anda yakin akan menghapus data ini?";
$lang['alert_delete_approval'] = "Apakah anda yakin akan menghapus anggota approval ini?";
$lang['alert_approval'] = "Apakah anda yakin akan approval data ini?";
$lang['alert_publish'] = "Apakah yakin akan mengaktifkan data ini?";
$lang['alert_unpublish'] = "Apakah yakin akan me-non aktifkan data ini?";
$lang['alert_ok'] = "Benar";
$lang['alert_cancel'] = "Batal";
$lang['alert_date_greater'] = "Tanggal mulai lebih besar dari tanggal akhir, silahkan tentukan tanggal kembali.";
$lang['alert_date_empty'] = "Tanggal mulai atau tanggal akhir tidak boleh kosong.";

$lang['empty_userid'] = "Tamu";
$lang['empty_name'] = "Nama Kosong";

$lang['user_menu_profile'] = "Profil";
$lang['user_menu_message'] = "Pesan";
$lang['user_menu_logout'] = "Keluar";
$lang['user_menu_visit'] = "Lihat Website";
$lang['upload_btn'] = "Pilih File";

$lang['btn_slider_gambar'] = "Crop slider";
$lang['btn_news_gambar'] = "Crop berita";
$lang['btn_free_gambar'] = "Crop bebas";
$lang['btn_product_gambar'] = "Crop produk";
$lang['add_images'] = "Tambah Gambar";


$lang['label_interval_mulai'] = "Mulai Tanggal";
$lang['label_interval_akhir'] = "Sampai Dengan";
$lang['label_btn_tambahkan'] = "Tambahkan";
$lang['label_image_exist'] = "Gambar sudah dipilih";
$lang['label_pilih_lokasi'] = "-- Pilih Lokasi --";
$lang['label_pilih_kategori'] = "-- Pilih Kategori --";
$lang['label_ekstensi_gambar'] = "File harus berekstensi : .jpg / .jpeg / .png / .gif";
$lang['label_semua_kategori'] = "Semua Kategori";
$lang['label_aktif'] = "Aktif";
$lang['label_tidak_aktif'] = "Tidak Aktif";
$lang['label_min_password'] = "Minimal 5 karakter";
$lang['label_info_icon2'] = "File harus berekstensi : .png";
$lang['label_info_icon'] = "Silahkan upload ikon maksimum lebar/width 50px.";
$lang['label_info_icon_kategori_hotel'] = "Silahkan upload ikon maksimum lebar/width 100px.";
$lang['label_icon'] = "Ikon";
$lang['label_search_result'] = "Hasil Penelusuran";
$lang['label_undifined'] = "(Tidak terdefinisi)";
$lang['label_tgl_awal'] = "Tanggal awal";
$lang['label_tgl_akhir'] = "Tanggal akhir";
$lang['label_info_namakategori'] = "Maksimal 20 karakter";
$lang['label_image_notavailable'] = "Gambar tidak tersedia";
$lang['label_summary'] = "Ringkasan";
$lang['msg_empty_summary'] = "Silahkan mengisi ringkasan";

$lang['btn_recomended'] = "Merekomendasikan";
$lang['btn_unrecomended'] = "Hapus Recomendasi";
$lang['msg_exist_name'] = "Nama yang anda masukan sudah ada";
$lang['ok'] = "OK";
$lang['cancel'] = "Batal";
$lang['name'] = "Nama";
$lang['title'] = "Title/Judul";
$lang['seo_title'] = "Title seo";
$lang['datecreated'] = "Tgl. Pembuatan";
$lang['datemodified'] = "Tgl. Perubahan";
$lang['body'] = "Isi Konten";
$lang['detail'] = "Keterangan";
$lang['description'] = "Deskripsi";
$lang['seo_description'] = "Deskripsi seo";
$lang['slug'] = "Slug";
$lang['gambar'] = "Gambar";

# MENU #
$lang['home'] = "Home";



$lang['error_duplicate_email'] = "Email sudah digunakan";
$lang['msg_error_failed_send_email'] = "Email AKtifasi gagal dikirimkan silahkan ulangi registrasi";
$lang['error_empty_code_members'] = "Silahkan mengisi kode AKtifasi";
$lang['activated_account'] = "Mengaktifkan akun";
$lang['msg_error_failed_code'] = "Kode aktifasi yang anda masukan salah";
$lang['email_members_register_verification_code'] = "Konfirmasi pendaftaran member";
$lang['msg_error_email_register'] = "Pendaftaran gagal silahkan ulangi lagi pendaftaran";
$lang['msg_success_confirmation'] = "Terima kasih, akun Anda telah diaktifkan dalam sistem Anda, untuk login silahkan klik";
$lang['msg_error_confirmation'] = "Maaf, aktivasi Anda gagal, silakan-registrate ulang akun Anda atau hubungi Administrator Anda.";
$lang['msg_success_registration'] = "Terima kasih atas pendaftaran Anda, silakan cek email Anda untuk mengaktifkan akun Anda.";
$lang['msg_error_empty_ktp'] = "Silahkan Upload KTP";
$lang['msg_error_empty_foto'] = "Silahkan Upload FOTO";
$lang['logs_entry_contact'] = "Menambahkan Contact";
$lang['logs_entry_keluhan'] = "Menambahkan Keluhan";
$lang['message_success'] = "Berhasil mengirimkan pesan";


## MODUL LOGIN
$lang['error_login_failed_login'] = "Login gagal, silahkan check email dan password";
$lang['error_login_failed_input'] = "Please fill in email and password";
$lang['error_member_banded'] = "Sorry, your account member has been banned";
$lang['error_member_non_active'] = "Maaf, akun member anda belum aktif";
$lang['email_address'] = "Email address";
$lang['email_members_forgot_password'] = "Confirm Forgot Password";
$lang['msg_error_empty_email'] = "Please fill in email";
$lang['msg_error_empty_belumterdaftar'] = "This email has not been registered yet";
$lang['email_members_forgotpassword'] = "Forgot Password";
$lang['msg_error_email_notfound'] = "Your email is not registered";
$lang['error_empty_email_login'] = "Please fill in email";
$lang['msg_error_not_activate'] = "Akun anda belum aktif";
$lang['msg_error_failed_send'] = "System failed, Please repeat in a few moments";
$lang['email_members_register_newpassword'] = "Confirm new Password";
$lang['msg_success_send_reset_password'] = "Silahkan check email anda ntuk reset password";
$lang['reset_your_password'] = "Reset Your Password";
$lang['file_not_exist'] = "File Tidak Ada";
$lang['msg_success_update_password'] = "Password Berhasil di Reset";

