<?php
if ( ! function_exists('send_email')) {
	function send_email($params = array()) {
		include_once(dirname(__FILE__)."/../libraries/phpmailer/class.phpmailer.php");
		include_once(dirname(__FILE__)."/../libraries/phpmailer/class.smtp.php");
		
		$email_tujuan = isset($params["email_tujuan"])?$params["email_tujuan"]:'';
		$email_tujuan_text = isset($params["email_tujuan_text"])?$params["email_tujuan_text"]:'';
		$body 		  = isset($params["body"])?$params["body"]:'';
		$subject 	  = isset($params["subject"])?$params["subject"]:'';
		
		$setfrom_email	  = isset($params["setfrom_email"])?$params["setfrom_email"]:'';
		$setfrom_text	  = isset($params["setfrom_text"])?$params["setfrom_text"]:'IPSPI Admin';
		$addreply_email	  = isset($params["addreply_email"])?$params["addreply_email"]:'';
		$addreply_text	  = isset($params["addreply_text"])?$params["addreply_text"]:'IPSPI Admin';
		
		$attachment	  = isset($params["attachment"])?$params["attachment"]:array();
		
		$mail             = new PHPMailer();

		//$body             = file_get_contents(base_url().'register/bodyemail/');
		
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
												   // 1 = errors and messages
												   // 2 = messages only
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->Host       = HOST_EMAIL; // sets the SMTP server
		$mail->Port       = PORT_EMAIL;                    // set the SMTP port for the GMAIL server
		$mail->Username   = USER_EMAIL; // SMTP account username
		$mail->Password   = PASS_EMAIL;        // SMTP account password
		
		$mail->SetFrom($setfrom_email, $setfrom_text);
		$mail->AddReplyTo($addreply_email, $addreply_text);
		
		$mail->Subject    = $subject;
		
		$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
		
		$mail->MsgHTML($body);
		
		$address = $email_tujuan;
		$mail->AddAddress($address, $email_tujuan_text);
		
		## ATTACHMENT ##
		if(count($attachment) > 0){
			foreach($attachment as $listdata){
				$mail->AddAttachment($listdata);
			}
		}
		
		if(!$mail->Send()) {
		  echo "Mailer Error: " . $mail->ErrorInfo;
		  return false;
		} else {
		  return true;
		}
	}
}

if ( ! function_exists('send_mail')) {
    function send_mail($params = array()) {
        include_once(dirname(__FILE__)."/../libraries/phpmailer/PHPMailerAutoload.php");
        // require 'PHPMailerAutoload.php';

        $email_tujuan = isset($params["email_tujuan"])?$params["email_tujuan"]:'';
        $email_tujuan_text = isset($params["email_tujuan_text"])?$params["email_tujuan_text"]:'';
        $body         = isset($params["body"])?$params["body"]:'';
        $subject      = isset($params["subject"])?$params["subject"]:'';
        
        $setfrom_email    = isset($params["setfrom_email"])?$params["setfrom_email"]:'';
        $setfrom_text     = isset($params["setfrom_text"])?$params["setfrom_text"]:'IPSPI Website';
        $addreply_email   = isset($params["addreply_email"])?$params["addreply_email"]:'';
        $addreply_text    = isset($params["addreply_text"])?$params["addreply_text"]:'IPSPI Admin';
        $email_cc     = isset($params["email_cc"])?$params["email_cc"]:'';
        
        $attachment   = isset($params["attachment"])?$params["attachment"]:array();

        $mail = new PHPMailer;

        $mail->SMTPDebug = 0;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = HOST_EMAIL;  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = USER_EMAIL;                 // SMTP username
        $mail->Password = PASS_EMAIL;                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = PORT_EMAIL;    
        $mail->SMTPOptions = array (
            'ssl' => array(
                'verify_peer'  => false,
                'verify_depth' => 3,
                'allow_self_signed' => true
            )
        );                                // TCP port to connect to

        $mail->SetFrom($setfrom_email, $setfrom_text);
        $mail->AddReplyTo($addreply_email, $addreply_text);

        // $mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
        
        // $mail->isHTML($body);
        // $mail->MsgHTML($body);
        
        $address = $email_tujuan;
        if($email_cc !=''){
            $mail->AddCC($address, $email_tujuan_text);
            $mail->AddCC($email_cc, $email_tujuan_text);
        }else{
            $mail->AddAddress($address, $email_tujuan_text);
        }

        ## ATTACHMENT ##
        if(count($attachment) > 0){
            foreach($attachment as $listdata){
                $mail->AddAttachment($listdata);
            }
        }

        // $mail->setFrom('from@example.com', 'Mailer');
        // $mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
        // $mail->addAddress('ellen@example.com');               // Name is optional
        // $mail->addReplyTo('info@example.com', 'Information');
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = $subject;
        $mail->Body    = $body;
        
        if(!$mail->send()) {
            return '0';
            // echo 'Message could not be sent.';
            // echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            // echo 'Message has been sent';
            return '1';
        }
    }
}