<?php

if ( ! function_exists('was_upload_file')) {
	function was_upload_file($fieldname, $newfbasename, $isuniquefname, $path, $allow_mime = array(), $forbid_mime = array(), $allow_ext = array(), $forbid_ext = array()) {
		$result["status"]  = 0;
		$result["message"] = "Uploaded succesfully";
		$result["name"]   = "";
		$result["ext"]    = "";
		$result["original"]["name"] = "";
		$result["original"]["type"] = "";
		$result["original"]["ext"]  = "";
		
		if (!isset($_FILES[$fieldname])) {
			$result["status"]  = 1;
			$result["message"] = "Input not found";
			return $result;
		}
		
		$ufile = $_FILES[$fieldname];
		
		if ($ufile["error"] != UPLOAD_ERR_OK) {
			$result["status"]  = $ufile["error"] + 1;
			if ($ufile["error"] == UPLOAD_ERR_INI_SIZE) $result["message"] = "File size exceeds maximum script can handle";
			else if ($ufile["error"] == UPLOAD_ERR_FORM_SIZE) $result["message"] = "File size exceeds maximum script can handle";
			else if ($ufile["error"] == UPLOAD_ERR_PARTIAL) $result["message"] = "File was only partially uploaded";
			else if ($ufile["error"] == UPLOAD_ERR_NO_FILE) $result["message"] = "No file was uploaded";
			return $result;
		}
		
		$pinfo    = pathinfo($ufile["name"]);
		$f_basename = "";
		$f_ext      = "";
		if (isset($pinfo["extension"])) {
			$f_ext = strtolower($pinfo["extension"] == ""?"":$pinfo["extension"]);
			$f_basename = basename($ufile["name"], ($pinfo["extension"] == ""?"":".".$pinfo["extension"]));
		} else {
			$f_basename = $ufile["name"];
		}
		
		$result["original"]["name"] = $ufile["name"];
		$result["original"]["type"] = $ufile["type"];
		$result["original"]["ext"]  = $f_ext;
		
		if (count($allow_mime) > 0) {
			if (!in_array($ufile["type"], $allow_mime)) {
				$result["status"]  = 6;
				$result["message"] = "Forbidden file type";
				return $result;
			}
		}
		
		if (count($forbid_mime) > 0) {
			if (in_array($ufile["type"], $allow_mime)) {
				$result["status"]  = 6;
				$result["message"] = "Forbidden file type";
				return $result;
			}
		}
		
		if (count($allow_ext) > 0) {
			if (!in_array($f_ext, $allow_ext)) {
				$result["status"]  = 7;
				$result["message"] = "Forbidden file extension ";
				return $result;
			}
		}
		
		if (count($forbid_ext) > 0) {
			if (in_array($f_ext, $forbid_ext)) {
				$result["status"]  = 7;
				$result["message"] = "Forbidden file extension";
				return $result;
			}
		}
		
		if ($isuniquefname) {
			$newfilename = NewTempName($path, $newfbasename, ".".$f_ext);
			$newfilename = basename($newfilename);
		} else {
			if ($newfbasename == "") {
				$newfilename = $f_basename.".".$f_ext;
			} else {
				$newfilename = str_replace("{FN}", $f_basename, $newfbasename).".".$f_ext;
			}
		}
		
		
		$result["name"] = $newfilename;
		$result["ext"]  = $f_ext;
		
		if (move_uploaded_file($ufile["tmp_name"], $path.$newfilename)) {
			chmod($path.$newfilename, 0666); //echo $path.$newfilename;
		} else {
			$result["status"]  = 8;
			$result["message"] = "Process failed";
			return $result;
		}
		
		return $result;
	}

}

if ( ! function_exists('was_upload_file_multi')) {
	function was_upload_file_multi($fieldname, $newfbasename, $isuniquefname, $path, $allow_mime = array(), $forbid_mime = array(), $allow_ext = array(), $forbid_ext = array()) {
		$result["status"]  = 0;
		$result["message"] = "Uploaded succesfully";
		$result["name"]   = "";
		$result["ext"]    = "";
		$result["original"]["name"] = "";
		$result["original"]["type"] = "";
		$result["original"]["ext"]  = "";
		
		if(count($_FILES['path']['size']) > 0){
			for ($si = 0; $si < count($_FILES['path']['size']); $si++) {


				if (!isset($_FILES[$fieldname])) {
					$result["status"]  = 1;
					$result["message"] = "Input not found";
					return $result;
				}
				
				$ufile = $_FILES[$fieldname];
				
				if ($ufile["error"][$si] != UPLOAD_ERR_OK) {
					$result["status"]  = $ufile["error"][$si] + 1;
					if ($ufile["error"][$si] == UPLOAD_ERR_INI_SIZE) $result["message"] = "File size exceeds maximum script can handle";
					else if ($ufile["error"][$si] == UPLOAD_ERR_FORM_SIZE) $result["message"] = "File size exceeds maximum script can handle";
					else if ($ufile["error"][$si] == UPLOAD_ERR_PARTIAL) $result["message"] = "File was only partially uploaded";
					else if ($ufile["error"][$si] == UPLOAD_ERR_NO_FILE) $result["message"] = "No file was uploaded";
					return $result;
				}
				
				$pinfo    = pathinfo($ufile["name"][$si]);
				$f_basename = "";
				$f_ext      = "";
				if (isset($pinfo["extension"])) {
					$f_ext = strtolower($pinfo["extension"] == ""?"":$pinfo["extension"]);
					$f_basename = basename($ufile["name"][$si], ($pinfo["extension"] == ""?"":".".$pinfo["extension"]));
				} else {
					$f_basename = $ufile["name"][$si];
				}
				
				$result["original"]["name"] = $ufile["name"][$si];
				$result["original"]["type"] = $ufile["type"][$si];
				$result["original"]["ext"]  = $f_ext;
				
				if (count($allow_mime) > 0) {
					if (!in_array($ufile["type"][$si], $allow_mime)) {
						$result["status"]  = 6;
						$result["message"] = "Forbidden file type";
						return $result;
					}
				}
				
				if (count($forbid_mime) > 0) {
					if (in_array($ufile["type"][$si], $allow_mime)) {
						$result["status"]  = 6;
						$result["message"] = "Forbidden file type";
						return $result;
					}
				}
				
				if (count($allow_ext) > 0) {
					if (!in_array($f_ext, $allow_ext)) {
						$result["status"]  = 7;
						$result["message"] = "Forbidden file extension ";
						return $result;
					}
				}
				
				if (count($forbid_ext) > 0) {
					if (in_array($f_ext, $forbid_ext)) {
						$result["status"]  = 7;
						$result["message"] = "Forbidden file extension";
						return $result;
					}
				}


				## FILENAME ##
				$extensionfile = explode('.', $ufile['name'][$si]);
        		$extension = strtoupper(end($extensionfile));

				$pos = strripos($ufile['name'][$si], '.');
				if($pos === false){
					$ordinary_name = $ufile['name'][$si];
				}else{
					$ordinary_name = substr($ufile['name'][$si], 0, $pos);
				}		
				$name_upload = date("Y_m_d-H_i_s")."_".md5($ordinary_name);
				$name_upload_save = $name_upload;
				
				if ($isuniquefname) {
					$newfilename = NewTempName($path, $name_upload_save, ".".$f_ext);
					$newfilename = basename($newfilename);
				} else {
					if ($name_upload_save == "") {
						$newfilename = $f_basename.".".$f_ext;
					} else {
						$newfilename = str_replace("{FN}", $f_basename, $name_upload_save).".".$f_ext;
					}
				}
				
				
				$result["name"] = $newfilename;
				$result["ext"]  = $f_ext;
				
				if (move_uploaded_file($ufile["tmp_name"][$si], $path.$newfilename)) {
					chmod($path.$newfilename, 0666); //echo $path.$newfilename;
				} else {
					$result["status"]  = 8;
					$result["message"] = "Process failed";
					return $result;
				}



			}
		}
		
		
		
		return $result;
	}

}
?>