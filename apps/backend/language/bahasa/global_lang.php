<?php
$lang['app_logo'] = "IPSPI App";

$lang['login_web_title'] = "Sistem Keamanan Login";
$lang['login_username'] = "username";
$lang['login_password'] = "password";
$lang['login_btn'] = "Masuk";

$lang['master'] = "Master Data";
$lang['no_content'] = "Tidak Ada Konten";
$lang['no_data'] = "Tidak Ada Data";
$lang['breadcrumb_home'] = "Beranda";

$lang['navigation_save'] = "Simpan";
$lang['navigation_add'] = "Tambah Data";
$lang['navigation_list'] = "Daftar Data";
$lang['navigation_search'] = "Cari";
$lang['navigation_modif'] = "Ubah";
$lang['navigation_modif_breadcrumb'] = "Ubah Data";
$lang['navigation_delete'] = "Hapus";
$lang['navigation_publish'] = "Aktifkan";
$lang['navigation_unpublish'] = "Nonaktifkan";
$lang['navigation_download'] = "Download";
$lang['navigation_crop'] = "Crop";
$lang['navigation_reset_search'] = "Reset";
$lang['navigation_data_aktif'] = "Data aktif";
$lang['navigation_data_nonaktif'] = "Data tidak aktif";
$lang['navigation_kirim'] = "Kirim";

$lang['tablelist_no'] = "No.";
$lang['tablelist_option'] = "Opsi";
$lang['tablelist_choose'] = "Pilih";
$lang['tanggal_combo'] = "--Tanggal--";
$lang['bulan_combo'] = "--Bulan--";
$lang['tahun_combo'] = "--Tahun--";
$lang['label_yes'] = "Ya";
$lang['label_no'] = "Tidak";

$lang['info_notif'] = "Informasi";
$lang['error_notif'] = "Pesan Error";
$lang['success_notif'] = "Pesan Sukses";

$lang['msg_empty_send'] = "Silahkan pilih salah satu data";
$lang['msg_success_send'] = "Newsletter telah berhasil di kirim ke subscriber";
$lang['msg_empty_delete'] = "Silahkan pilih salah satu data";
$lang['msg_empty_forward'] = "Surat masuk gagal di teruskan";
$lang['msg_success_delete'] = "Data telah dihapus dari database";
$lang['msg_success_send_revisi'] = "Revisi Sudah berhasil dikirimkan";
$lang['msg_failed_revision'] = "Revisi gagal dikirimkan";
$lang['msg_success_forward'] = "Surat telah berhasil diteruskan";
$lang['msg_error_database'] = "Maaf, telah terjadi kesalahan pada database. Silahkan mencoba beberapa saat lagi, atau menghubungi Administrator.";
$lang['msg_error_empty_field'] = "Silahkan memeriksa kembali form isian Anda.";
$lang['msg_error_empty_file_image'] = "Silahkan mengunggah file gambar";
$lang['msg_error_empty_article'] = "Silahkan masukkan minimal satu artikel";
$lang['msg_success_entry'] = "Data telah ditambahkan ke dalam database.";
$lang['msg_success_update'] = "Data telah disimpan ke dalam database.";
$lang['msg_success_publish'] = "Data telah diaktifkan.";
$lang['msg_empty_publish'] = "Data gagal diaktifkan.";
$lang['msg_success_unpublish'] = "Data telah dinonaktifkan.";
$lang['msg_empty_unpublish'] = "Data gagal dinonaktifkan.";
$lang['msg_success_entry_info'] = "Data telah disimpan ke dalam database.";
$lang['msg_error_interval1_empty'] = "Silahkan mengisi tanggal awal";
$lang['msg_error_interval2_empty'] = "Silahkan mengisi tanggal akhir";
$lang['msg_error_interval'] = "Tanggal mulai tidak boleh lebih besar dari tanggal akhir";
$lang['msg_error_minimum_dimensi_gambar'] = "Silahkan memilih gambar dengan dimensi yang lebih besar";

$lang['error_username_alfanumeric_users'] = "Silahkan mengisi username berupa huruf, angka, _ dan -";

$lang['alert_forward'] = "Apakah anda yakin akan meneruskan surat ini?";
$lang['alert_ditinjau'] = "Apakah anda yakin akan mengembalikan surat ini?";
$lang['alert_delete'] = "Apakah anda yakin akan menghapus data ini?";
$lang['alert_delete_approval'] = "Apakah anda yakin akan menghapus anggota approval ini?";
$lang['alert_approval'] = "Apakah anda yakin akan approval data ini?";
$lang['alert_publish'] = "Apakah yakin akan mengaktifkan data ini?";
$lang['alert_unpublish'] = "Apakah yakin akan me-non aktifkan data ini?";
$lang['alert_ok'] = "Benar";
$lang['alert_cancel'] = "Batal";
$lang['alert_date_greater'] = "Tanggal mulai lebih besar dari tanggal akhir, silahkan tentukan tanggal kembali.";
$lang['alert_date_empty'] = "Tanggal mulai atau tanggal akhir tidak boleh kosong.";

$lang['empty_userid'] = "Tamu";
$lang['empty_name'] = "Nama Kosong";

$lang['user_menu_profile'] = "Profil";
$lang['user_menu_message'] = "Pesan";
$lang['user_menu_logout'] = "Keluar";
$lang['user_menu_visit'] = "Lihat Website";
$lang['upload_btn'] = "Pilih File";
$lang['label_info_ukuran'] = "Silahkan upload gambar dengan ukuran file maksimum";
$lang['label_info_dimensi'] = "dan dimensi gambar";
$lang['navigation_add'] = "Tambah";
$lang['modif'] = "Ubah";
$lang['msg_empty_file_failed_ext'] = "Extensi file yang anda masukan salah";
$lang['msg_empty_file_failed_filesize'] = "Ukuran file yang anda masukan melebihi batas ukuran yang ditentukan";
$lang['msg_error_empty_file'] = "Silahkan unggah file";
$lang['look_web'] = "Lihat Website";

$lang['btn_slider_gambar'] = "Crop slider";
$lang['btn_news_gambar'] = "Crop berita";
$lang['btn_free_gambar'] = "Crop bebas";
$lang['btn_product_gambar'] = "Crop produk";
$lang['add_images'] = "Tambah Gambar";


$lang['label_interval_mulai'] = "Mulai Tanggal";
$lang['label_interval_akhir'] = "Sampai Dengan";
$lang['label_btn_tambahkan'] = "Tambahkan";
$lang['label_image_exist'] = "Gambar sudah dipilih";
$lang['label_pilih_lokasi'] = "-- Pilih Lokasi --";
$lang['label_pilih_kategori'] = "-- Pilih Kategori --";
$lang['label_ekstensi_gambar'] = "File harus berekstensi : .jpg / .jpeg / .png / .gif";
$lang['label_semua_kategori'] = "Semua Kategori";
$lang['label_aktif'] = "Aktif";
$lang['label_tidak_aktif'] = "Tidak Aktif";
$lang['label_min_password'] = "Minimal 5 karakter";
$lang['label_info_icon2'] = "File harus berekstensi : .png";
$lang['label_info_icon'] = "Silahkan upload ikon maksimum lebar/width 50px.";
$lang['label_info_icon_kategori_hotel'] = "Silahkan upload ikon maksimum lebar/width 100px.";
$lang['label_icon'] = "Ikon";
$lang['label_search_result'] = "Hasil Penelusuran";
$lang['label_undifined'] = "(Tidak terdefinisi)";
$lang['label_tgl_awal'] = "Tanggal awal";
$lang['label_tgl_akhir'] = "Tanggal akhir";
$lang['label_info_namakategori'] = "Maksimal 20 karakter";
$lang['label_image_notavailable'] = "Gambar tidak tersedia";
$lang['label_summary'] = "Ringkasan";
$lang['msg_empty_summary'] = "Silahkan mengisi ringkasan";

$lang['btn_recomended'] = "Merekomendasikan";
$lang['btn_unrecomended'] = "Hapus Recomendasi";
$lang['msg_exist_name'] = "Nama yang anda masukan sudah ada";
$lang['ok'] = "OK";
$lang['cancel'] = "Batal";
$lang['name'] = "Nama";
$lang['title'] = "Title/Judul";
$lang['seo_title'] = "Title seo";
$lang['datecreated'] = "Tgl. Pembuatan";
$lang['datemodified'] = "Tgl. Perubahan";
$lang['body'] = "Isi Konten";
$lang['detail'] = "Keterangan";
$lang['description'] = "Deskripsi";
$lang['seo_description'] = "Deskripsi seo";
$lang['slug'] = "Slug";
$lang['gambar'] = "Gambar";

## BAHASA MENU ##
$lang['side_menu_dashboard'] = "Beranda";
$lang['side_menu_logs'] = "Catatan Aplikasi";
$lang['side_menu_logs_teks'] = "Catatan aktivitas aplikasi";
$lang['side_menu_users'] = "Pengguna";
$lang['side_menu_users_teks'] = "Pengelolaan pengguna aplikasi";
$lang['side_menu_settings'] = "Konfigurasi";
$lang['side_menu_utility'] = "Utilitas";
$lang['side_menu_master_lain'] = "Master Lain";
$lang['side_menu_statistik'] = "Statistik";
$lang['side_menu_notifikasi'] = "Notifikasi";
$lang['importmember'] = "Import Member";

## MODUL LOGS ##
$lang['logs_delete_logs'] = "Menghapus catatan aplikasi";
$lang['label_date_logs'] = "Tanggal";
$lang['label_ip_logs'] = "Alamat IP";
$lang['label_username_logs'] = "username";
$lang['label_module_logs'] = "Modul aplikasi";
$lang['label_detail_logs'] = "Keterangan";


## MODUL USERS ##
$lang['logs_entry_users'] = "Menambahkan pengguna";
$lang['logs_modif_users'] = "Mengubah pengguna";
$lang['logs_delete_users'] = "Menghapus pengguna";
$lang['label_name_users'] = "Nama Pengguna";
$lang['label_username_users'] = "Username";
$lang['label_password_users'] = "Sandi Pengguna";
$lang['label_group_users'] = "Grup";
$lang['label_retype_password_users'] = "Ulangi Sandi Pengguna";
$lang['error_empty_name_users'] = "Silahkan mengisi nama pengguna";
$lang['error_empty_username_users'] = "Silahkan mengisi username";
$lang['error_empty_password_users'] = "Silahkan mengisi sandi pengguna";
$lang['error_empty_password_5_users'] = "Silahkan mengisi sandi minimal 5 karakter";
$lang['error_empty_retype_password_users'] = "Silahkan mengulang sandi pengguna";
$lang['error_empty_retype_password_true_users'] = "Silahkan mengulang sandi pengguna dengan benar";
$lang['error_duplicate_username'] = "Username telah digunakan, silahkan menggunakan username lain";
$lang['label_edit_users'] = "Ubah data pengguna";
$lang['label_add_users'] = "Tambah data pengguna";
$lang['error_empty_email_users'] = "Silahkan mengisi email";
$lang['email_users'] = "Email";
$lang['nip_users'] = "NIP";
$lang['error_empty_id_jabatan_users'] = "Silahkan memilih Jabatan";
$lang['error_empty_id_parent_users'] = "Silahkan memilih Atasan";
$lang['error_empty_nip_users'] = "Silahkan mengisi NIP";
$lang['atasan_user'] = "Atasan";

## LOGIN ##
$lang['error_login_failed_input'] = "Silahkan periksa kembali username dan password Anda.";
$lang['error_login_failed_login'] = "Login gagal, silahkan periksa username atau password Anda.";


## MODUL MENU ##
$lang['master'] = "Master Data";
$lang['menu_suratmasuk'] = "Surat Masuk";

## MODUL PROVINSI ##
$lang['provinsi'] = "Provinsi";
$lang['name_provinsi'] = "Nama Provinsi";
$lang['code_provinsi'] = "Kode Provinsi";
$lang['provinsi_teks'] = "Pengelolaan Provinsi";
$lang['logs_entry_provinsi'] = "Menambahkan Provinsi";
$lang['logs_modif_provinsi'] = "Mengubah Provinsi";
$lang['logs_delete_provinsi'] = "Menghapus Provinsi";
$lang['error_empty_code_provinsi'] = "Silahkan mengisi Kode Provinsi";
$lang['error_empty_name_provinsi'] = "Silahkan mengisi nama Provinsi";
$lang['add_provinsi'] = "Tambah provinsi";
$lang['modif_provinsi'] = "Ubah provinsi";
$lang['status'] = "Status";
$lang['pilih_propinsi'] = "Pilih propinsi";
$lang['pilih_kota'] = "Pilih kota";
$lang['logs_publish_provinsi'] = "Aktifkan propinsi";
$lang['logs_unpublish_provinsi'] = "Non Aktifkan propinsi";
$lang['aktif'] = "Aktif";
$lang['nonaktif'] = "Non Aktif";
$lang['kode_provinsi'] = "Kode Provinsi";
$lang['ketua_provinsi'] = "Ketua Provinsi";
$lang['alamat_provinsi'] = "Alamat Provinsi";
$lang['email_provinsi'] = "Email Provinsi";
$lang['telp_provinsi'] = "Telepon Provinsi";

## MODUL KOTA ##
$lang['kota'] = "Kota";
$lang['name_kota'] = "Nama Kota";
$lang['code_kota'] = "Kode Kota";
$lang['kota_teks'] = "Pengelolaan Kota";
$lang['logs_entry_kota'] = "Menambahkan Kota";
$lang['logs_modif_kota'] = "Mengubah Kota";
$lang['logs_delete_kota'] = "Menghapus Kota";
$lang['error_empty_code_kota'] = "Silahkan mengisi Kode Kota";
$lang['error_empty_name_kota'] = "Silahkan mengisi nama Kota";
$lang['add_kota'] = "Tambah kota";
$lang['modif_kota'] = "Ubah kota";
$lang['status'] = "Status";
$lang['pilih_propinsi'] = "Pilih propinsi";
$lang['pilih_kota'] = "Pilih kota";
$lang['logs_publish_kota'] = "Aktifkan propinsi";
$lang['logs_unpublish_kota'] = "Non Aktifkan propinsi";
$lang['aktif'] = "Aktif";
$lang['nonaktif'] = "Non Aktif";
$lang['kode_kota'] = "Kode Kota";

## MODUL MEMBERS ##
$lang['members'] = "Members";
$lang['name_members'] = "Nama Members";
$lang['code_members'] = "Kode Members";
$lang['members_teks'] = "Pengelolaan Members";
$lang['logs_entry_members'] = "Menambahkan Members";
$lang['logs_modif_members'] = "Mengubah Members";
$lang['logs_delete_members'] = "Menghapus Members";
$lang['error_empty_code_members'] = "Silahkan mengisi Kode Members";
$lang['error_empty_name_members'] = "Silahkan mengisi nama Members";
$lang['add_members'] = "Tambah members";
$lang['modif_members'] = "Ubah members";
$lang['status'] = "Status";
$lang['logs_publish_members'] = "Aktifkan propinsi";
$lang['logs_unpublish_members'] = "Non Aktifkan propinsi";
$lang['error_empty_id_propinsi'] = "Silahkan mengisi propinsi";
$lang['rekomendasi'] = "Surat Rekomendasi";
$lang['download_rekomendasi'] = "Download Surat Rekomendasi";
$lang['kartu'] = "Kartu Anggota";
$lang['download_kartu'] = "Download Kartu Anggota";

## MODUL CONFIGURATION ##
$lang['configuration'] = "Konfigurasi Website";
$lang['configuration_teks'] = "Pengelolaan konfigurasi Website";
$lang['tagline_text'] = "Tagline";
$lang['error_empty_meta_title_configuration'] = "Silahkan mengisi meta title";
$lang['error_empty_site_title_configuration'] = "Silahkan mengisi site title";
$lang['error_empty_tagline_text_configuration'] = "Silahkan mengisi tagline";
$lang['error_empty_meta_description_configuration'] = "Silahkan mengisi meta description";
$lang['error_empty_email_contact_configuration'] = "Silahkan mengisi kontak email";
$lang['error_empty_phone_configuration'] = "Silahkan mengisi no telepon";
$lang['error_empty_address_configuration'] = "Silahkan mengisi alamat";
$lang['error_empty_email_contact_valid_configuration'] = "Silahkan mengisi kontak email dengan benar";
$lang['meta_title'] = "Meta title";
$lang['site_title'] = "Site title";
$lang['meta_description'] = "Meta description";
$lang['youtube_url'] = "Youtube Url";
$lang['instagram_url'] = "Instagram Url";
$lang['namaprofil'] = "Nama Organisasi";
$lang['phone'] = "Telepon";
$lang['email_contact'] = "Kontak Email";
$lang['address'] = "Alamat";
$lang['fb_url'] = "Facebook Url";
$lang['twitter_url'] = "Twitter Url";
$lang['gplus_url'] = "Gplus Url";
$lang['fax'] = "Fax";

## MODUL STATIC PAGE ##
$lang['informasi'] = "Informasi";
$lang['informasi_teks'] = "Pengelolaan halaman Informasi";
$lang['add_informasi'] = "Tambah Informasi";
$lang['modif_informasi'] = "Ubah Informasi";
$lang['logs_entry_informasi'] = "Menambahkan halaman Informasi";
$lang['logs_modif_informasi'] = "Mengubah halaman Informasi";
$lang['logs_delete_informasi'] = "Menghapus halaman Informasi";
$lang['logs_publish_statispages'] = "Mengaktifkan halaman Informasi";
$lang['logs_unpublish_statispages'] = "Non aktifkan halaman Informasi";
$lang['error_empty_name_informasi'] = "Silahkan mengisi nama ";
$lang['error_empty_body_informasi'] = "Silahkan mengisi body ";
$lang['error_empty_description_informasi'] = "Silahkan mengisi deskripsi ";

# MODUL ABOUTUS ##
$lang['aboutus'] = "About us";
$lang['aboutus_teks'] = "Pengelolaan About us";
$lang['logs_entry_aboutus'] = "Menambahkan About us";
$lang['logs_modif_aboutus'] = "Mengubah About us";
$lang['logs_delete_aboutus'] = "Menghapus About us";
$lang['logs_publish_aboutus'] = "Mengaktifkan About us";
$lang['logs_unpublish_aboutus'] = "Non aktifkan About us";
$lang['error_empty_name_aboutus'] = "Silahkan mengisi nama ";
$lang['error_empty_seo_title_aboutus'] = "Silahkan mengisi title seo";
$lang['error_empty_body_aboutus'] = "Silahkan mengisi body Aboutus ";
$lang['error_empty_description_aboutus'] = "Silahkan mengisi deskripsi About us ";
$lang['error_empty_seo_description_aboutus'] = "Silahkan mengisi deskripsi seo About us ";
$lang['error_empty_id_category_aboutus'] = "Silahkan mengisi kategori Aboutus ";

# MODUL REGULATION ##
$lang['regulation'] = "Footer Informasi";
$lang['regulation_teks'] = "Pengelolaan Footer Informasi";
$lang['logs_entry_regulation'] = "Menambahkan Footer Informasi";
$lang['logs_modif_regulation'] = "Mengubah Footer Informasi";
$lang['logs_delete_regulation'] = "Menghapus Footer Informasi";
$lang['logs_publish_regulation'] = "Mengaktifkan Footer Informasi";
$lang['logs_unpublish_regulation'] = "Non aktifkan Footer Informasi";
$lang['error_empty_name_regulation'] = "Silahkan mengisi nama ";
$lang['error_empty_seo_title_regulation'] = "Silahkan mengisi title seo";
$lang['error_empty_body_regulation'] = "Silahkan mengisi body Regulation ";
$lang['error_empty_description_regulation'] = "Silahkan mengisi deskripsi Footer Informasi ";
$lang['error_empty_seo_description_regulation'] = "Silahkan mengisi deskripsi seo Footer Informasi ";
$lang['error_empty_id_category_regulation'] = "Silahkan mengisi kategori Regulation ";

# MODUL TINGKAT ##
$lang['tingkat'] = "Tingkat Pendidikan";
$lang['tingkat_teks'] = "Pengelolaan Tingkat Pendidikan";
$lang['add_tingkat'] = "Tambah Tingkat Pendidikan";
$lang['modif_tingkat'] = "Ubah Tingkat Pendidikan";
$lang['title_tingkat'] = "Nama ";
$lang['logs_entry_tingkat'] = "Menambahkan Tingkat Pendidikan";
$lang['logs_modif_tingkat'] = "Mengubah Tingkat Pendidikan";
$lang['logs_delete_tingkat'] = "Menghapus Tingkat Pendidikan";
$lang['logs_publish_tingkat'] = "Mengaktifkan Tingkat Pendidikan";
$lang['logs_unpublish_tingkat'] = "Non aktifkan Tingkat Pendidikan";
$lang['error_empty_name_tingkat'] = "Silahkan mengisi nama ";
$lang['error_empty_body_tingkat'] = "Silahkan mengisi body Tingkat ";
$lang['error_empty_description_tingkat'] = "Silahkan mengisi deskripsi Tingkat Pendidikan";
$lang['error_empty_id_category_tingkat'] = "Silahkan mengisi kategori Tingkat ";

# MODUL PEKERJAAN ##
$lang['pekerjaan'] = "Pekerjaan";
$lang['pekerjaan_teks'] = "Pengelolaan Pekerjaan";
$lang['add_pekerjaan'] = "Tambah Pekerjaan";
$lang['modif_pekerjaan'] = "Ubah Pekerjaan";
$lang['title_pekerjaan'] = "Nama ";
$lang['logs_entry_pekerjaan'] = "Menambahkan Pekerjaan";
$lang['logs_modif_pekerjaan'] = "Mengubah Pekerjaan";
$lang['logs_delete_pekerjaan'] = "Menghapus Pekerjaan";
$lang['logs_publish_pekerjaan'] = "Mengaktifkan Pekerjaan";
$lang['logs_unpublish_pekerjaan'] = "Non aktifkan Pekerjaan";
$lang['error_empty_name_pekerjaan'] = "Silahkan mengisi nama ";
$lang['error_empty_body_pekerjaan'] = "Silahkan mengisi body Pekerjaan ";
$lang['error_empty_description_pekerjaan'] = "Silahkan mengisi deskripsi Pekerjaan";
$lang['error_empty_id_category_pekerjaan'] = "Silahkan mengisi kategori Pekerjaan ";

# OTHER #
$lang['msg_error_empty_field'] = "Silahkan memeriksa kembali form isian Anda.";
$lang['msg_error_database'] = "Maaf, telah terjadi kesalahan pada database. Silahkan mencoba beberapa saat lagi, atau menghubungi Administrator.";
$lang['msg_error_empty_file_image'] = "Silahkan mengunggah file gambar";


## MODUL JURNAL ##
$lang['jurnal'] = "Jurnal";
$lang['jurnal_teks'] = "Pengelolaan halaman Jurnal";
$lang['add_jurnal'] = "Tambah Jurnal";
$lang['modif_jurnal'] = "Ubah Jurnal";
$lang['datepublish'] = "Tanggal Jurnal";
$lang['short_description'] = "Deskripsi singkat";
$lang['logs_entry_jurnal'] = "Menambahkan halaman Jurnal";
$lang['logs_modif_jurnal'] = "Mengubah halaman Jurnal";
$lang['logs_delete_jurnal'] = "Menghapus halaman Jurnal";
$lang['logs_publish_statispages'] = "Mengaktifkan halaman Jurnal";
$lang['logs_unpublish_statispages'] = "Non aktifkan halaman Jurnal";
$lang['error_empty_name_jurnal'] = "Silahkan mengisi nama ";
$lang['error_empty_body_jurnal'] = "Silahkan mengisi body ";
$lang['error_empty_description_jurnal'] = "Silahkan mengisi deskripsi ";
$lang['files'] = "Files ";
$lang['datepublish_jurnal'] = "Tanggal Jurnal ";

## MODUL MATERI ##
$lang['materi'] = "Files Materi";
$lang['materi_teks'] = "Pengelolaan halaman Materi";
$lang['add_materi'] = "Tambah Files Materi";
$lang['modif_materi'] = "Ubah Files Materi";
$lang['datepublish'] = "Tanggal Materi";
$lang['short_description'] = "Deskripsi singkat";
$lang['logs_entry_materi'] = "Menambahkan halaman Materi";
$lang['logs_modif_materi'] = "Mengubah halaman Materi";
$lang['logs_delete_materi'] = "Menghapus halaman Materi";
$lang['logs_publish_statispages'] = "Mengaktifkan halaman Materi";
$lang['logs_unpublish_statispages'] = "Non aktifkan halaman Materi";
$lang['error_empty_name_materi'] = "Silahkan mengisi nama ";
$lang['error_empty_body_materi'] = "Silahkan mengisi body ";
$lang['error_empty_description_materi'] = "Silahkan mengisi deskripsi ";

## MODUL KEBIJAKAN ##
$lang['kebijakan'] = "Files Kebijakan";
$lang['kebijakan_teks'] = "Pengelolaan halaman Kebijakan";
$lang['add_kebijakan'] = "Tambah FilesKebijakan";
$lang['modif_kebijakan'] = "Ubah Files Kebijakan";
$lang['datepublish'] = "Tanggal Kebijakan";
$lang['short_description'] = "Deskripsi singkat";
$lang['logs_entry_kebijakan'] = "Menambahkan halaman Kebijakan";
$lang['logs_modif_kebijakan'] = "Mengubah halaman Kebijakan";
$lang['logs_delete_kebijakan'] = "Menghapus halaman Kebijakan";
$lang['logs_publish_statispages'] = "Mengaktifkan halaman Kebijakan";
$lang['logs_unpublish_statispages'] = "Non aktifkan halaman Kebijakan";
$lang['error_empty_name_kebijakan'] = "Silahkan mengisi nama ";
$lang['error_empty_body_kebijakan'] = "Silahkan mengisi body ";
$lang['error_empty_description_kebijakan'] = "Silahkan mengisi deskripsi ";


## MODUL BERITA ##
$lang['berita'] = "Berita";
$lang['berita_teks'] = "Pengelolaan halaman Berita";
$lang['add_berita'] = "Tambah Berita";
$lang['modif_berita'] = "Ubah Berita";
$lang['datepublish'] = "Tanggal Berita";
$lang['short_description'] = "Deskripsi singkat";
$lang['logs_entry_berita'] = "Menambahkan halaman Berita";
$lang['logs_modif_berita'] = "Mengubah halaman Berita";
$lang['logs_delete_berita'] = "Menghapus halaman Berita";
$lang['logs_publish_statispages'] = "Mengaktifkan halaman Berita";
$lang['logs_unpublish_statispages'] = "Non aktifkan halaman Berita";
$lang['error_empty_name_berita'] = "Silahkan mengisi nama ";
$lang['error_empty_body_berita'] = "Silahkan mengisi body ";
$lang['error_empty_description_berita'] = "Silahkan mengisi deskripsi ";
$lang['featured'] = "Featured";
$lang['yes'] = "Ya";
$lang['no'] = "Tidak";

# MODUL CONTACT ##
$lang['contact'] = "Contact";
$lang['contact_teks'] = "Pengelolaan Contact";
$lang['add_contact'] = "Tambah Contact";
$lang['modif_contact'] = "Ubah Contact";
$lang['title_contact'] = "Nama ";
$lang['logs_entry_contact'] = "Menambahkan Contact";
$lang['logs_modif_contact'] = "Mengubah Contact";
$lang['logs_delete_contact'] = "Menghapus Contact";
$lang['logs_publish_contact'] = "Mengaktifkan Contact";
$lang['logs_unpublish_contact'] = "Non aktifkan Contact";
$lang['error_empty_name_contact'] = "Silahkan mengisi nama ";
$lang['error_empty_body_contact'] = "Silahkan mengisi body Contact ";
$lang['error_empty_description_contact'] = "Silahkan mengisi deskripsi Contact";
$lang['error_empty_id_category_contact'] = "Silahkan mengisi kategori Contact ";

# MODUL KELUHAN ##
$lang['keluhan'] = "Keluhan";
$lang['keluhan_teks'] = "Pengelolaan Keluhan";
$lang['add_keluhan'] = "Tambah Keluhan";
$lang['modif_keluhan'] = "Ubah Keluhan";
$lang['title_keluhan'] = "Nama ";
$lang['logs_entry_keluhan'] = "Menambahkan Keluhan";
$lang['logs_modif_keluhan'] = "Mengubah Keluhan";
$lang['logs_delete_keluhan'] = "Menghapus Keluhan";
$lang['logs_publish_keluhan'] = "Mengaktifkan Keluhan";
$lang['logs_unpublish_keluhan'] = "Non aktifkan Keluhan";
$lang['error_empty_name_keluhan'] = "Silahkan mengisi nama ";
$lang['error_empty_body_keluhan'] = "Silahkan mengisi body Keluhan ";
$lang['error_empty_description_keluhan'] = "Silahkan mengisi deskripsi Keluhan";
$lang['error_empty_id_category_keluhan'] = "Silahkan mengisi kategori Keluhan ";


# MODUL PEKERJAAN ##
$lang['instansi'] = "Instansi";
$lang['instansi_teks'] = "Pengelolaan Instansi";
$lang['add_instansi'] = "Tambah Instansi";
$lang['modif_instansi'] = "Ubah Instansi";
$lang['title_instansi'] = "Nama ";
$lang['logs_entry_instansi'] = "Menambahkan Instansi";
$lang['logs_modif_instansi'] = "Mengubah Instansi";
$lang['logs_delete_instansi'] = "Menghapus Instansi";
$lang['logs_publish_instansi'] = "Mengaktifkan Instansi";
$lang['logs_unpublish_instansi'] = "Non aktifkan Instansi";
$lang['error_empty_name_instansi'] = "Silahkan mengisi nama ";
$lang['error_empty_body_instansi'] = "Silahkan mengisi body Instansi ";
$lang['error_empty_description_instansi'] = "Silahkan mengisi deskripsi Instansi";
$lang['error_empty_id_category_instansi'] = "Silahkan mengisi kategori Instansi ";

$lang['nav_kembali'] = "Kembali";
$lang['harga'] = "Harga";
$lang['tanggal_expired'] = "Tanggal Expired";
$lang['konfirmasi_order'] = "Konfirmasi order";
$lang['bayar'] = "Bayar";
$lang['status_order'] = "Status Order";
$lang['payment'] = "Payment";
$lang['member_access'] = "Member Akses";
$lang['msg_empty_file'] = "Silahkan masukan file";

##LOKER
$lang['logs_entry_loker'] = "Menambahkan halaman Loker";
$lang['logs_modif_loker'] = "Mengubah halaman Loker";
$lang['logs_delete_loker'] = "Menghapus halaman Loker";
$lang['logs_publish_loker'] = "Mengaktifkan halaman Loker";
$lang['logs_unpublish_loker'] = "Non aktifkan halaman Loker";