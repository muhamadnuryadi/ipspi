<?php

class Wilayah extends CI_Controller {

    var $webconfig;

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->sharedModel('WilayahModel');
        $this->load->sharedModel('LoginModel');
        if (!$this->LoginModel->isLoggedIn()) {
            redirect(base_url() . 'login');
        }

        $this->webconfig = $this->config->item('webconfig');
        $this->module_name = $this->lang->line('wilayah');
    }

    function index() {
        redirect(base_url() . $this->router->class . '/listing');
    }

    function listing($page = 0) {
        $tdata = array();
        $ldata['content'] = $this->load->view($this->router->class . '/index', $tdata, true);
        $this->load->sharedView('template', $ldata);
    }

    function getAllData($page = 0) {

        $all_data = $this->WilayahModel->listDataCount();
        $cfg_pg ['base_url'] = base_url() . $this->uri->segment(1) . '/' . $this->uri->segment(2) . "/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah']) ? $all_data['jumlah'] : 0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 3;

        $cfg_pg['first_link'] = '&laquo;';
        $cfg_pg['first_tag_close'] = '</li>';
        $cfg_pg['first_tag_open'] = '<li>';

        $cfg_pg['last_link'] = '&raquo;';
        $cfg_pg['last_tag_open'] = '<li>';
        $cfg_pg['last_tag_close'] = '</li>';

        $cfg_pg['next_link'] = '&gt;';
        $cfg_pg['next_tag_open'] = '<li>';
        $cfg_pg['next_tag_close'] = '</li>';

        $cfg_pg['prev_link'] = '&lt;';
        $cfg_pg['prev_tag_open'] = '<li>';
        $cfg_pg['prev_tag_close'] = '</li>';

        $cfg_pg['num_tag_open'] = '<li>';
        $cfg_pg['num_tag_close'] = '</li>';

        $cfg_pg['cur_tag_open'] = '<li class="active"><span>';
        $cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
        $listdata["pagination"] = $this->pagination->create_links();

        $start_no = 0;
        if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
            $start_no = $this->uri->segment($cfg_pg ['uri_segment']);
        }

        $listdata["start_no"] = $start_no;

        $start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
        $start_idx = $start_idx < 0 ? 0 : $start_idx;

        $listdata['lists'] = $this->WilayahModel->listData(array(
            'start' => $start_idx
            , 'limit' => $cfg_pg['per_page']
        ));

        $this->load->view($this->router->class . '/list', $listdata);
    }

    public function getkota(){
        if($_POST){
            ## GET KOTA ##
            $prop = $this->input->post('prop');
            $outputkota = $this->WilayahModel->listKota(array('prop' => $prop));
            header('Content-Type: application/json');
            echo json_encode( $outputkota );
        }
    }

    public function getkecamatan(){
        if($_POST){
            ## GET KEC ##
            $city = $this->input->post('city');
            $outputkec = $this->WilayahModel->listKecamatan(array('city' => $city));
            header('Content-Type: application/json');
            echo json_encode( $outputkec );
        }
    }
}
