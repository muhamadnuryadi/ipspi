<script type="text/javascript">

function submitkie(){
	jQuery().ajaxStart(function() {
		jQuery('#loading').show();
		jQuery('#result').hide();
	}).ajaxStop(function() {
		jQuery('#loading').hide();
		jQuery('#result').fadeIn('slow');	
		jQuery("#loginform")[0].reset();
	});
	
	jQuery.ajax({
			type: 'POST',
			url: jQuery('#loginform').attr('action'),
			data: jQuery('#loginform').serialize(),
			success: function(data) {
				jQuery('.message').show();
				if(data == 'redirect'){
					top.location.href = '<?php echo base_url(); ?>dashboard';
				}else{
					jQuery('.message').html(data);
					jQuery('.message').slideDown(600,function () {$(this).fadeTo(350, 100)} );
				}		
			}
		})
		
		return false;
}
</script>
<div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <form name="loginform" id="loginform" action="<?php echo base_url(); ?>login/postProcess" method="post" onsubmit="submitkie();return false;">
    <div class="message" style="display:none;">&nbsp;</div>
      <div class="form-group has-feedback">
        <input name="username" type="username" class="form-control" placeholder="Username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input name="password" type="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

</div>
















