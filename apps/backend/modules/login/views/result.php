<?php if(isset($error_message)){ ?>
    <div role="alert" class="alert alert-danger alert-dismissible">
		<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
		<strong><?php echo $this->lang->line('error_notif'); ?></strong>
		<p><?php echo $error_message; ?></p>
	</div>
<?php } ?>
<?php if (isset($success)){echo "redirect";} ?>