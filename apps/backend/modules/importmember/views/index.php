<script type="text/javascript">
	jQuery(document).ready(function($) {
		// jQuery("#listData").load('<?php echo base_url().$this->router->class; ?>/getAllData/');
	});
	function downloadThis(){
		window.open('<?php echo $this->webconfig['base_template']; ?>img/template-member-ipspi.xls', '_blank');
	}
</script>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo strtoupper($this->module_name); ?>
			<small>Pengelolaan halaman Import Member</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('/'); ?>"><i class="fa fa-dashboard"></i><?php echo $this->lang->line('dashboard'); ?></a></li>
			<li class="active"><?php echo ucfirst($this->module_name); ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
	                <div class="box-header with-border">
		                 <div class="panel-body">
	                        <form class="form-horizontal" name="form" method="POST" action="<?php echo base_url().$this->router->class; ?>" enctype="multipart/form-data">
	                            <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
	                                <?php foreach($error_hash as $inp_err){ ?>
	                                    <script type="text/javascript">
	                                    jQuery(document).ready(function($) {
	                                        toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
	                                        });
	                                    </script>
	                                <?php } ?>
	                            <?php } ?>
	                            
	                            <?php if(isset($success)){ ?>
	                                <script type="text/javascript">
	                                jQuery(document).ready(function($) {
	                                    toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
	                                    });
	                                </script>
	                            <?php } ?>

	                            
	                            <div class="form-group">
	                                <label class="col-sm-2 control-label">Upload File <span>*</span></label>
	                                <div class="col-sm-10">
	                                    <input class="text gbr" type="file" id="uploadImage" name="path" value="<?php echo isset($path)?$path:''; ?>"><br class="clear">
	                                    <p><i>File Harus Berektensi .xls</i></p>
	                                    <p><i>Email tidak boleh ada yang sama apabila sama maka akan di abaikan oleh sistem</i>	</p>
	                                </div>
	                            </div>
	                            
	                            <div class="form-group footertable">
	                                <label class="col-sm-2 control-label">&nbsp;</label>
	                                <div class="col-sm-5 text-left">
	                                    <input class="btn btn-danger btn-sm mr5" type="submit" value="Upload">
	                                    <a href="javascript:void(0)" onclick="downloadThis()" title="<?php echo $this->lang->line('navigation_unpublish'); ?>">
										<button class="btn btn-warning btn-sm" type="button">
											<i class="fa fa-arrow-down"></i> Download Template
										</button>
										</a>
	                                </div>
	                            </div>
	                            
	                            <div class="form-group footertable">
	                                <label class="col-sm-2 control-label">Keterangan Pengisian dalam template</label>
	                                <div class="col-sm-2 text-left">
	                                	<b>Status Bayar</b>
	                                	<hr style="margin: 3px 3px;">
	                                    <option value="">Ya</option>
	                                    <option value="">Tidak</option>
	                                </div>
	                                <div class="col-sm-2 text-left">
	                                	<b>Bidang</b>
	                                	<hr style="margin: 3px 3px;">
	                                    <?php 
										foreach ($this->webconfig['bidang_setting'] as $key3 => $value3) {
										  ?>
										  <option value="<?php echo $key3 ?>" ><?php echo $value3 ?></option>
										  <?php
										}
				                      	?>
	                                </div>
	                                <div class="col-sm-2 text-left">
	                                	<b>Peran Utama</b>
	                                	<hr style="margin: 3px 3px;">
	                                    <?php 
										foreach ($this->webconfig['peran_utama'] as $key3 => $value3) {
										  ?>
										  <option value="<?php echo $key3 ?>" ><?php echo $value3 ?></option>
										  <?php
										}
				                      	?>
	                                </div>
	                                <div class="col-sm-2 text-left">
	                                	<b>Jenis Keahlian</b>
	                                	<hr style="margin: 3px 3px;">
	                                    <?php 
										foreach ($this->webconfig['jenis_keahlian'] as $key3 => $value3) {
										  ?>
										  <option value="<?php echo $key3 ?>" ><?php echo $value3 ?></option>
										  <?php
										}
				                      	?>
	                                </div>
	                                <div class="col-sm-2 text-left">
	                                	<b>Tipe Sertifikasi</b>
	                                	<hr style="margin: 3px 3px;">
	                                    <?php 
										foreach ($this->webconfig['sertifikasi_tipe'] as $key3 => $value3) {
										  ?>
										  <option value="<?php echo $key3 ?>" ><?php echo $value3 ?></option>
										  <?php
										}
				                      	?>
	                                </div>
	                                <div class="col-sm-2 text-left">
	                                	<b>Jenis Pendidikan</b>
	                                	<hr style="margin: 3px 3px;">
	                                    <?php 
										foreach ($this->webconfig['pendidikan_jenis'] as $key3 => $value3) {
										  ?>
										  <option value="<?php echo $key3 ?>" ><?php echo $value3 ?></option>
										  <?php
										}
				                      	?>
	                                </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	                
	            </div>
			</div>
		</div>
	</section>
</div>