<?php
class Importmember extends CI_Controller{
    var $webconfig;
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('image');
        $this->load->library('image_moo');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->library('excel_reader');
        $this->load->sharedModel('ImportmemberModel');
        $this->load->sharedModel('LoginModel');
        if(!$this->LoginModel->isLoggedIn()){
            redirect(base_url().'login');
        }
        
        $this->webconfig = $this->config->item('webconfig');
        $this->module_name = 'Import Member';
    }
    function index(){
        $tdata = array();

        if($_FILES){
             $doInsert = $this->ImportmemberModel->uploadData();
            
            if($doInsert == 'empty'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
                }else if($doInsert == 'failed'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
                }else if($doInsert == 'empty_file'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_empty_file'));
                }else if($doInsert == 'success'){
                    $tdata['success'] = $this->lang->line('msg_success_entry');             
                }

                $ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
                $this->load->sharedView('template', $ldata);
        }else{
            $ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }
    
}