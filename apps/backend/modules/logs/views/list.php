<script type="text/javascript">
$(document).ready(function($) {
	$('#checkall').click(function () {
		$('#tabellistdata .check-all').attr('checked', this.checked);
	});
	$(".pagination a").click(function(e){
		
		e.preventDefault(); 
		var linkUrl = $(this).attr("href");
		if(linkUrl){
			$('#listData').load(linkUrl.replace(/\s/g,'%20'));
		}
	 });
});
function submitkie(){
	jQuery().ajaxStart(function() {
		jQuery('#loading').show();
		jQuery('#result').hide();
	}).ajaxStop(function() {
		jQuery('#loading').hide();
		jQuery('#result').fadeIn('slow');	
		jQuery("#tabellistdata")[0].reset();
	});
	
	jQuery.ajax({
			type: 'POST',
			url: jQuery('#tabellistdata').attr('action'),
			data: jQuery('#tabellistdata').serialize(),
			success: function(response) {
				if(response == 'success'){
					toastr.success("<?php echo $this->lang->line('msg_success_delete'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
				}else{
					toastr.error("<?php echo $this->lang->line('msg_empty_delete'); ?>", "<?php echo $this->lang->line('error_notif'); ?>");
				}
				
				jQuery("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
		return false;
}
function deleteThis(code){
	var txt = "<?php echo $this->lang->line('alert_delete'); ?> <input type='hidden' id='alertName' name='alertName' value='"+code+"' />";
	jQuery.prompt(txt ,{  submit: doCondition, buttons: { <?php echo $this->lang->line('alert_ok'); ?>: true, <?php echo $this->lang->line('alert_cancel'); ?>: false },prefix:'jqismooth' });
}
function doCondition(v,m,f,e){
	  if(m){
	  	//alert(f.alertName);
		var posting = "dataid="+e.alertName;
		jQuery.ajax({
			type: 'post',
			url: "<?php echo base_url('/logs'); ?>/deleteThis",
			data: posting,
			success: function(response) {
				if(response == 'success'){
					toastr.success("<?php echo $this->lang->line('msg_success_delete'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
				}else{
					toastr.error("<?php echo $this->lang->line('msg_empty_delete'); ?>", "<?php echo $this->lang->line('error_notif'); ?>");
				}
				jQuery("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
	  }else{
	  	jQuery.prompt.close();
	  }
}

</script>

<form id="tabellistdata" method="post" action="<?php echo base_url().$this->router->class; ?>/postProcess" onsubmit="return false;">
<div class="box-body table-responsive no-padding">
	<?php if(isset($lists) && count($lists) > 0) {?>
	<table class="table table-bordered table-striped">
	<tr>
	    <th width="10" class="center_th_td">
	    	<input type="checkbox" id="checkall" />
	    </th>
	    <th width="10" class="center_th_td">
	    	<?php echo $this->lang->line('tablelist_no'); ?>
	    </th>
	    <th width="200">
	    	<?php echo $this->lang->line('label_date_logs'); ?>
	    </th>
	    <th width="200">
	    	<?php echo $this->lang->line('label_ip_logs'); ?>
	    </th>
	    <th width="200">
	    	<?php echo $this->lang->line('label_username_logs'); ?>
	    </th>
	    <th width="200">
	    	<?php echo $this->lang->line('label_module_logs'); ?>
	    </th>
	    <th width="200">
	    	<?php echo $this->lang->line('label_detail_logs'); ?>
	    </th>
	    <th class="center_th_td" width="30">
	    	<?php echo $this->lang->line('tablelist_option'); ?>
	    </th>
	</tr>
	<?php $i = $start_no; foreach ($lists as $list) { $i++; ?>
        <tr>
            <td class="center_th_td">
            	<input type="checkbox" name="datacek[]" class="check-all" value="<?php echo $list['id']; ?>" />
            </td>
            <td class="center_th_td">
            	<?php echo $i; ?>
            </td>
            <td>
            	<?php echo datetime_lang_reformat_long($list['date_created']); ?>
            </td>
            <td>
            	<?php echo clean_str($list['ip_address']); ?>
            </td>
            <td>
            	<?php echo clean_str($list['username']); ?>
            </td>
            <td>
            	<?php echo clean_str($list['module']); ?>
            </td>
            <td>
            	<?php echo clean_str($list['details']); ?>
            </td>
            <td width='80' class="center_th_td">
            	<a class='btn btn-block btn-danger btn-xs' href="javascript:void(0)" onclick="deleteThis(<?php echo $list['id']; ?>)">		        	
	        		<i class="fa fa-remove"></i> <?php echo $this->lang->line('navigation_delete'); ?>
	        	</a>  
            </td>
        </tr>
    <?php } ?>
	</table>
</div><!-- /.box-body -->
<div class="box-footer clearfix">
	<button onclick="submitkie()" type="button" class="btn btn-danger btn-xs pull-left">
		<i class="fa fa-trash"></i>Hapus
	</button>
  <ul class="pagination pagination-sm no-margin pull-right"><?php echo isset($pagination)?$pagination:""; ?></ul>
</div>
<?php } else { ?>
	<p><center><?php echo $this->lang->line('no_data'); ?></center></p>
<?php } ?>
</form>