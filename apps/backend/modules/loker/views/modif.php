<script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });
    tinymce.init({
        selector: "textarea#body",
        height: 250,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools jbimages'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true,
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ],
        relative_urls : false
     });
    
    
});
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo strtoupper($this->module_name); ?>
            <small>Ubah Loker</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('/'); ?>">
                    <i class="fa fa-dashboard"></i>
                    <?php echo $this->lang->line('dashboard'); ?>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url().$this->router->class; ?>"><?php echo ucfirst($this->module_name); ?></a>
            </li>
            <li class="active"><?php echo $this->lang->line('modif'); ?></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="col-xs-12 text-right">
                            <a class="btn btn-success btn-xs" href="<?php echo base_url().$this->router->class; ?>"  title="<?php echo $this->lang->line('navigation_list'); ?>">
                                <i class="fa fa-fw fa-plus"></i>
                                <?php echo $this->lang->line('navigation_list'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" name="form" method="POST" action="" enctype="multipart/form-data">
                            <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
                                <?php foreach($error_hash as $inp_err){ ?>
                                    <script type="text/javascript">
                                    jQuery(document).ready(function($) {
                                        toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                                        });
                                    </script>
                                <?php } ?>
                            <?php } ?>
                            
                            <?php if(isset($success)){ ?>
                                <script type="text/javascript">
                                jQuery(document).ready(function($) {
                                    toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
                                    });
                                </script>
                            <?php } ?>

                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo $this->lang->line('title'); ?> <span>*</span></label>
                                <div class="col-sm-5">
                                    <input type="text" id="title" name="title" value='<?php echo isset($lists[0]['title'])?$lists[0]['title']:''; ?>' size="50" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Tanggal Post <span>*</span></label>
                                <div class="col-sm-2">
                                    <input type="text" id="date_posted" name="date_posted" value='<?php echo isset($lists[0]['date_posted'])?date('d/m/Y',strtotime($lists[0]['date_posted'])):''; ?>' size="50" class="form-control datepicker">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo $this->lang->line('body'); ?> <span>*</span></label>
                                <div class="col-sm-10">
                                    <textarea type="text" id="body" name="body" class="form-control"><?php echo isset($lists[0]['body'])?stripcslashes($lists[0]['body']):''; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo $this->lang->line('status'); ?> <span>*</span></label>
                                <div class="col-sm-4 radio">
                                    <label>
                                        <input type="radio" name="status" value="1" <?php if(isset($lists[0]['status']) && $lists[0]['status'] == 1){ echo "checked=checked"; }?> />
                                        <?php echo $this->lang->line('aktif'); ?>
                                    </label>
                                    &nbsp;
                                    <label>
                                        <input type="radio" name="status" value="0" <?php if(!isset($lists[0]['status'])){ echo "checked=checked"; } ?> <?php if(isset($lists[0]['status']) && $lists[0]['status'] == 0){ echo "checked=checked"; }?> />
                                        <?php echo $this->lang->line('nonaktif'); ?> 
                                    </label>
                                </div>
                            </div>
                            
                            <div class="form-group footertable">
                                <label class="col-sm-2 control-label">&nbsp;</label>
                                <div class="col-sm-5 text-left">
                                    <input class="btn btn-danger btn-sm mr5" type="submit" value="<?php echo $this->lang->line('navigation_save'); ?>">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>