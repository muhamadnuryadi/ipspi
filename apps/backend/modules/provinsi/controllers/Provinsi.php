<?php

class Provinsi extends CI_Controller {

    var $webconfig;

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->helper('image');
        $this->load->library('image_moo');
        $this->load->library('pagination');
        $this->load->sharedModel('WilayahModel');
        $this->load->sharedModel('LoginModel');
        if (!$this->LoginModel->isLoggedIn()) {
            redirect(base_url() . 'login');
        }

        $this->webconfig = $this->config->item('webconfig');
        $this->module_name = $this->lang->line('provinsi');
    }

    function index() {
        redirect(base_url() . $this->router->class . '/listing');
    }

    function listing($page = 0) {
        $tdata = array();
        $ldata['content'] = $this->load->view($this->router->class . '/index', $tdata, true);
        $this->load->sharedView('template', $ldata);
    }

    function getAllData($page = 0) {
        if($this->session->userdata('role') == 2){
            $all_data = $this->WilayahModel->listDataProvinsiCount(array('id' => $this->session->userdata('userpropinsi')));
        }else{
            $all_data = $this->WilayahModel->listDataProvinsiCount();
        }
        $cfg_pg ['base_url'] = base_url() . $this->uri->segment(1) . '/' . $this->uri->segment(2) . "/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah']) ? $all_data['jumlah'] : 0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 3;

        $cfg_pg['first_link'] = '&laquo;';
        $cfg_pg['first_tag_close'] = '</li>';
        $cfg_pg['first_tag_open'] = '<li>';

        $cfg_pg['last_link'] = '&raquo;';
        $cfg_pg['last_tag_open'] = '<li>';
        $cfg_pg['last_tag_close'] = '</li>';

        $cfg_pg['next_link'] = '&gt;';
        $cfg_pg['next_tag_open'] = '<li>';
        $cfg_pg['next_tag_close'] = '</li>';

        $cfg_pg['prev_link'] = '&lt;';
        $cfg_pg['prev_tag_open'] = '<li>';
        $cfg_pg['prev_tag_close'] = '</li>';

        $cfg_pg['num_tag_open'] = '<li>';
        $cfg_pg['num_tag_close'] = '</li>';

        $cfg_pg['cur_tag_open'] = '<li class="active"><span>';
        $cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
        $listdata["pagination"] = $this->pagination->create_links();

        $start_no = 0;
        if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
            $start_no = $this->uri->segment($cfg_pg ['uri_segment']);
        }

        $listdata["start_no"] = $start_no;

        $start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
        $start_idx = $start_idx < 0 ? 0 : $start_idx;

        if($this->session->userdata('role') == 2){
            $listdata['lists'] = $this->WilayahModel->listDataProvinsi(array(
                'id' => $this->session->userdata('userpropinsi')
                ,'start' => $start_idx
                , 'limit' => $cfg_pg['per_page']
            ));
        }else{
            $listdata['lists'] = $this->WilayahModel->listDataProvinsi(array(
                'start' => $start_idx
                , 'limit' => $cfg_pg['per_page']
            ));
        }

        $this->load->view($this->router->class . '/list', $listdata);
    }

    function searchdata($name = 'name') {
        if ($name == '-') {
            $name = '';
        }

        # PAGINATION #
        if($this->session->userdata('role') == 2){
            $all_data = $this->WilayahModel->filterDataProvinsiCount(array(
                                                                            'name' => trim(urldecode($name))
                                                                            ,'id' => $this->session->userdata('userpropinsi')
                                                                        ));
        }else{
            $all_data = $this->WilayahModel->filterDataProvinsiCount(array('name' => trim(urldecode($name))));
        }

        $cfg_pg ['base_url'] = base_url() . $this->uri->segment(1) . '/' . $this->uri->segment(2) . "/" . $this->uri->segment(3) . "/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah']) ? $all_data['jumlah'] : 0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 4;

        $cfg_pg['first_link'] = '&laquo;';
        $cfg_pg['first_tag_close'] = '</li>';
        $cfg_pg['first_tag_open'] = '<li>';

        $cfg_pg['last_link'] = '&raquo;';
        $cfg_pg['last_tag_open'] = '<li>';
        $cfg_pg['last_tag_close'] = '</li>';

        $cfg_pg['next_link'] = '&gt;';
        $cfg_pg['next_tag_open'] = '<li>';
        $cfg_pg['next_tag_close'] = '</li>';

        $cfg_pg['prev_link'] = '&lt;';
        $cfg_pg['prev_tag_open'] = '<li>';
        $cfg_pg['prev_tag_close'] = '</li>';

        $cfg_pg['num_tag_open'] = '<li>';
        $cfg_pg['num_tag_close'] = '</li>';

        $cfg_pg['cur_tag_open'] = '<li class="active"><span>';
        $cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
        $listdata["pagination"] = $this->pagination->create_links();

        $start_no = 0;
        if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
            $start_no = $this->uri->segment($cfg_pg ['uri_segment']);
        }

        $listdata["start_no"] = $start_no;

        $start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
        $start_idx = $start_idx < 0 ? 0 : $start_idx;
        
        if($this->session->userdata('role') == 2){
            $listdata['lists'] = $this->WilayahModel->filterDataProvinsi(array(
                'id' => $this->session->userdata('userpropinsi')
                , 'name' => trim(urldecode($name))
                ,'start' => $start_idx
                , 'limit' => $cfg_pg['per_page']
            ));
        }else{
            $listdata['lists'] = $this->WilayahModel->filterDataProvinsi(array(
                'start' => $start_idx
                , 'limit' => $cfg_pg['per_page']
                , 'name' => trim(urldecode($name))
            ));
        }

        $this->load->view($this->router->class . '/list', $listdata);
    }

    function add() {
        $tdata = array();
        $tdata['wilayah'] = $this->WilayahModel->listpropinsi();
        require_once(dirname(__FILE__) . '/../../../libraries/formvalidator.php');

        if ($_POST) {

            $validator = new FormValidator();
            $validator->addValidation("name", "req", $this->lang->line('error_empty_name_provinsi'));
            $validator->addValidation("kode", "req", $this->lang->line('error_empty_code_provinsi'));

            if ($validator->ValidateForm()) {
                $doInsert = $this->WilayahModel->entriData(array(
                                                                    'name' => $this->input->post('name')
                                                                    ,'kode' => $this->input->post('kode')
                                                                    ,'status' => $this->input->post('status')
                                                                    ,'ketua' => $this->input->post('ketua')
                                                                    ,'alamat' => $this->input->post('alamat')
                                                                    ,'email' => $this->input->post('email')
                                                                    ,'telp' => $this->input->post('telp')
                                                                    ,'x1' => $this->input->post('x1')
                                                                    ,'y1' => $this->input->post('y1')
                                                                    ,'x2' => $this->input->post('x2')
                                                                    ,'y2' => $this->input->post('y2')
                                                                    ,'width_resize' => $this->webconfig["width_resize"]
                ));
                if ($doInsert == 'empty') {
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
                }else if($doInsert == 'failed') {
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
                }else if ($doInsert == 'exist') {
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_exist_name'));
                }else if($doInsert == 'success') {
                    $tdata['success'] = $this->lang->line('msg_success_entry');
                }
                $ldata['content'] = $this->load->view($this->router->class . '/add', $tdata, true);
                $this->load->sharedView('template', $ldata);
            } else {
                $tdata['name'] = $this->input->post('name');
                $tdata['kode'] = $this->input->post('kode');
                $tdata['error_hash'] = $validator->GetErrors();

                $ldata['content'] = $this->load->view($this->router->class . '/add', $tdata, true);
                $this->load->sharedView('template', $ldata);
            }
        } else {
            $ldata['content'] = $this->load->view($this->router->class . '/add', $tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }

    function modif($id = 0) {
        if ($id == 0) {
            redirect(base_url() . $this->router->class);
        }

        $tdata['lists'] = $this->WilayahModel->listData(array('id' => $id));

        require_once(dirname(__FILE__) . '/../../../libraries/formvalidator.php');

        if ($_POST) {
            $validator = new FormValidator();

            $validator->addValidation("name", "req", $this->lang->line('error_empty_name_provinsi'));
            $validator->addValidation("kode", "req", $this->lang->line('error_empty_code_provinsi'));

            if ($validator->ValidateForm()) {
                $doUpdate = $this->WilayahModel->updateData(array(
                                                                    'id' => $id
                                                                    ,'name' => $this->input->post('name')
                                                                    ,'kode' => $this->input->post('kode')
                                                                    ,'status' => $this->input->post('status')
                                                                    ,'ketua' => $this->input->post('ketua')
                                                                    ,'alamat' => $this->input->post('alamat')
                                                                    ,'email' => $this->input->post('email')
                                                                    ,'telp' => $this->input->post('telp')
                                                                    ,'x1' => $this->input->post('x1')
                                                                    ,'y1' => $this->input->post('y1')
                                                                    ,'x2' => $this->input->post('x2')
                                                                    ,'y2' => $this->input->post('y2')
                                                                    ,'width_resize' => $this->webconfig["width_resize"]
                ));

                if ($doUpdate == 'empty') {
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
                } else if ($doUpdate == 'failed') {
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
                } else if ($doUpdate == 'exist') {
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_exist_name'));
                } else if ($doUpdate == 'success') {
                    $tdata['success'] = $this->lang->line('msg_success_update');
                    $tdata['lists'] = $this->WilayahModel->listData(array('id' => $id));
                }
                $ldata['content'] = $this->load->view($this->router->class . '/modif', $tdata, true);
                $this->load->sharedView('template', $ldata);
            } else {

                $tdata['error_hash'] = $validator->GetErrors();

                $ldata['content'] = $this->load->view($this->router->class . '/modif', $tdata, true);
                $this->load->sharedView('template', $ldata);
            }
        } else {
            $ldata['content'] = $this->load->view($this->router->class . '/modif', $tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }

    public function deleteThis() {
        $doDelete = $this->__delete($this->input->post('dataid'));

        if ($doDelete == 'failed') {
            echo "nodata";
        } else if ($doDelete == 'success') {
            echo "success";
        }
    }

    public function postProcess() {
        $postvalue = $this->input->post('datacek');
        if ($postvalue != '') {
            foreach ($postvalue as $data) {
                $this->__delete($data);
            }
            echo "success";
        } else {
            echo "nodata";
        }
    }

    private function __delete($id) {
        if ($id == 0) {
            redirect(base_url() . $this->router->class);
        }
        $doDelete = $this->WilayahModel->deleteData($id);
        return $doDelete;
    }

    public function publish($id = 0){
        $doThis = $this->__doPublish($this->input->post('dataid'));

        if($doThis == 'failed'){
            echo "nodata";
        }else if($doThis == 'success'){
            echo "success";
        }
    }

    public function unpublish($id = 0){
        $doThis = $this->__doUnpublish($this->input->post('dataid'));

        if($doThis == 'failed'){
            echo "nodata";
        }else if($doThis == 'success'){
            echo "success";
        }
    }

    private function __doPublish($kodeid){
        if($kodeid == 0){
            redirect(base_url().$this->router->class."/");
        }
        $doPublish = $this->WilayahModel->doPublish($kodeid);
        return $doPublish;
    }

    private function __doUnpublish($kodeid){
        if($kodeid == 0){
            redirect(base_url().$this->router->class."/");
        }
        $doUnublish = $this->WilayahModel->doUnublish($kodeid);
        return $doUnublish;
    }

}
