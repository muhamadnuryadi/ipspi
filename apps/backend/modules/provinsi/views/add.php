<script type="text/javascript">
$(document).ready(function() {
    var oFReader = null;
    var image = null;
    var globalResizedWidth = '<?php if($this->webconfig["width_resize"] != ''){ echo $this->webconfig["width_resize"]; }else{ echo 0; } ?>';
    var jcrop_api, globalWidth, globalHeight, globalConfDimension;

    /* INIT */
    $('.image_block').remove();
    $("#uploadImage").val("");

    $("#uploadImage").change(function(){
        $(this).parent().find(".image_block").remove();
        $.when( createImageElement(this) ).done( cropImageElement(this) );
    });
    
    function createImageElement(obj) {
        var html = '<div class="image_block">';
        html += '<input type="hidden" class="x1" name="x1" value="0" />';
        html += '<input type="hidden" class="y1" name="y1" value="0" />';
        html += '<input type="hidden" class="x2" name="x2" value="0" />';
        html += '<input type="hidden" class="y2" name="y2" value="0" />';
        html += '<br><img class="image_preview" style="width:'+globalResizedWidth+'px" />';
        html += '</div>';
        $(obj).after(html);
    }

    function cropImageElement(obj) {
        var ext = getExtension($(obj).val());
        if(ext == "jpg" || ext == "jpeg" || ext == "png" || ext == "gif"){
            var dimensionconf = '<?php if(count($this->webconfig["image_dpd"]) > 0){ echo $this->webconfig["image_dpd"][0]; } ?>';
            var separator = dimensionconf.split("x");
            var dimheight = separator[1];
            var dimwidth = separator[0];
            if(dimwidth != 'undefined' || dimheight != 'undefined'){
                doLoadCropping(obj, dimwidth, dimheight);
            }
            return false;
        }else{
            alert('<?php echo $this->lang->line("msg_error_extension"); ?>');
            $('.image_block').remove();
            $("#uploadImage").val("");
            return false;
        }
    }

    function doLoadCropping(obj, dimwidth, dimheight) {
        if(oFReader !=null){
            oFReader = null;
        }
        
        var min_width = dimwidth;
        var min_height = dimheight;
        var objFile = obj.files[0];
        var max_foto_mb = '<?php echo $this->webconfig['max_foto_filesize']; ?>';
        var max_foto_byte = parseInt(max_foto_mb)*1048576; //convert MB to Byte
        
        if(objFile.size > max_foto_byte) {
            $(obj).parent().find(".image_block").remove();
            $(obj).val("");
            alert("<?php echo $this->lang->line('label_error_ukuran'); ?>");
            $('.image_block').remove();
            $("#uploadImage").val("");
        } else {
            // prepare HTML5 FileReader
            oFReader = new FileReader();
            image  = new Image();
            oFReader.readAsDataURL(objFile);
            
            oFReader.onload = function (_file) {
                image.src    = _file.target.result;
                image.onload = function() {
                        globalWidth = this.width;
                        globalHeight = this.height;
                        
                        $(obj).parent().find(".image_preview").attr("src", this.src);

                        if(globalWidth < min_width || globalHeight < min_height) {
                                $(obj).parent().find(".image_block").remove();
                                $(obj).val("");
                                alert("<?php echo $this->lang->line('label_error_dimensi'); ?>");
                        } else {
                            cropImage(globalWidth, globalHeight, min_width, min_height, $(obj).parent().find(".image_preview"));
                        }
                };

                image.onerror= function() {
                    alert('Invalid file type: '+ objFile.type);
                };     
                
            }
        }
    }

    function cropImage(width, height, minwidth, minheight, obj) {
        var resizedWidth = globalResizedWidth;
        var resizedHeight = (resizedWidth * height) / width;
        var resizedMinWidth = (minwidth * resizedWidth) / width;
        var resizedMinHeight = (minheight * resizedHeight) / height;
        
        if(minwidth != '' || minheight != ''){
            $(obj).Jcrop({
                setSelect: [ 0, 0, resizedMinWidth, resizedMinHeight ],
                minSize: [ resizedMinWidth, resizedMinHeight ],
                onSelect: updateCoords,
                allowSelect: false,
                bgFade: true,
                bgOpacity: 0.4,
                aspectRatio: minwidth / minheight
            },function(){
                jcrop_api = this;
            });
        }
    }
    function updateCoords(c){
        $('.x1').val(c.x);
        $('.y1').val(c.y);
        $('.x2').val(c.x2);
        $('.y2').val(c.y2);
        $('.w').val(c.w);
        $('.h').val(c.h);
    };
    function getExtension(filename) {
        return filename.split('.').pop().toLowerCase();
    }
});
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo strtoupper($this->module_name); ?>
            <small><?php echo $this->lang->line('add_provinsi'); ?></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('/'); ?>">
                    <i class="fa fa-dashboard"></i>
                    <?php echo $this->lang->line('dashboard'); ?>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url().$this->router->class; ?>"><?php echo ucfirst($this->module_name); ?></a>
            </li>
            <li class="active"><?php echo $this->lang->line('navigation_add'); ?></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="col-xs-12 text-right">
                            <a class="btn btn-success btn-xs" href="<?php echo base_url().$this->router->class; ?>"  title="<?php echo $this->lang->line('navigation_list'); ?>">
                                <i class="fa fa-fw fa-plus"></i>
                                <?php echo $this->lang->line('navigation_list'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" name="form" method="POST" action="<?php echo base_url().$this->router->class; ?>/add" enctype="multipart/form-data">
                            <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
                                <?php foreach($error_hash as $inp_err){ ?>
                                    <script type="text/javascript">
                                    jQuery(document).ready(function($) {
                                        toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                                        });
                                    </script>
                                <?php } ?>
                            <?php } ?>
                            
                            <?php if(isset($success)){ ?>
                                <script type="text/javascript">
                                jQuery(document).ready(function($) {
                                    toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
                                    });
                                </script>
                            <?php } ?>

                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo $this->lang->line('name_provinsi'); ?> <span>*</span></label>
                                <div class="col-sm-5">
                                    <input type="text" id="name" name="name" value='<?php echo isset($name)?$name:''; ?>' size="50" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo $this->lang->line('kode_provinsi'); ?> <span>*</span></label>
                                <div class="col-sm-5">
                                    <input type="text" id="kode" name="kode" value='<?php echo isset($kode)?$kode:''; ?>' size="50" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo $this->lang->line('status'); ?> <span>*</span></label>
                                <div class="col-sm-5 radio">
                                    <label>
                                        <input type="radio" name="status" value="1" <?php if(!isset($status)){ echo "checked=checked"; } ?> <?php if(isset($status) && $status == 1){ echo "checked=checked"; }?> />
                                        <?php echo $this->lang->line('aktif'); ?>
                                    </label>
                                    &nbsp
                                    <label>
                                        <input type="radio" name="status" value="0" <?php if(isset($status) && $status == 0){ echo "checked=checked"; }?> />
                                        <?php echo $this->lang->line('nonaktif'); ?> 
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo $this->lang->line('ketua_provinsi'); ?> </label>
                                <div class="col-sm-5">
                                    <input type="text" id="ketua" name="ketua" value='<?php echo isset($ketua)?$ketua:''; ?>'  class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo $this->lang->line('alamat_provinsi'); ?> </label>
                                <div class="col-sm-8">
                                    <input type="text" id="alamat" name="alamat" value='<?php echo isset($alamat)?$alamat:''; ?>'  class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo $this->lang->line('email_provinsi'); ?> </label>
                                <div class="col-sm-5">
                                    <input type="text" id="email" name="email" value='<?php echo isset($email)?$email:''; ?>' class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo $this->lang->line('telp_provinsi'); ?> </label>
                                <div class="col-sm-5">
                                    <input type="text" id="telp" name="telp" value='<?php echo isset($telp)?$telp:''; ?>' class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo $this->lang->line('gambar'); ?> <span>*</span></label>
                                <div class="col-sm-10">
                                    <input class="text gbr" type="file" id="uploadImage" name="files" value="<?php echo isset($files)?$files:''; ?>"><br class="clear">
                                    <p>
                                        <em>
                                            <?php echo $this->lang->line('label_info_ukuran'); ?> 
                                            <?php echo $this->webconfig['max_foto_filesize']; ?> MB
                                            <?php echo $this->lang->line('label_info_dimensi'); ?> 
                                            <?php if(count($this->webconfig["image_dpd"]) > 0){ echo $this->webconfig["image_dpd"][0]; }else{ echo 0; } ?> px
                                        </em>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group footertable">
                                <label class="col-sm-2 control-label">&nbsp;</label>
                                <div class="col-sm-5 text-left">
                                    <input class="btn btn-danger btn-sm mr5" type="submit" value="<?php echo $this->lang->line('navigation_save'); ?>">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>