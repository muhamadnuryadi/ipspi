<script type="text/javascript">
jQuery(document).ready(function($) {
	$('#checkall').click(function () {
		$('#tabellistdata .check-all').attr('checked', this.checked);
	});
	$(".pagination a").click(function(e){
		// stop normal link click
		e.preventDefault();
		var linkUrl = jQuery(this).attr("href");
		if(linkUrl){
			jQuery('#listData').load(linkUrl.replace(/\s/g,'%20'));
		}
	});
});

function submitkie(){
	jQuery().ajaxStart(function($) {
		$('#loading').show();
		$('#result').hide();
	}).ajaxStop(function($) {
		$('#loading').hide();
		$('#result').fadeIn('slow');
		$("#tabellistdata")[0].reset();
	});

	jQuery.ajax({
			type: 'POST',
			url: jQuery('#tabellistdata').attr('action'),
			data: jQuery('#tabellistdata').serialize(),
			success: function(response) {
				if(response == 'success'){
					toastr.success("<?php echo $this->lang->line('msg_success_delete'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
				}else{
					toastr.error("<?php echo $this->lang->line('msg_empty_delete'); ?>", "<?php echo $this->lang->line('error_notif'); ?>");
				}

				jQuery("#listData").load('<?php echo $_SERVER["REQUEST_URI"]; ?>');
			}
		})
		return false;
}
function deleteThis(code){
	console.log(code)
	var txt = "<?php echo $this->lang->line('alert_delete'); ?> <input type='hidden' id='alertName' name='alertName' value='"+code+"' />";
	jQuery.prompt(txt ,{  submit: doCondition, buttons: { <?php echo $this->lang->line('ok'); ?>: true, <?php echo $this->lang->line('cancel'); ?>: false },prefix:'jqismooth' });
}
function doCondition(v,m,f,e){
	console.log(v)
		console.log(m)
		console.log(f)
		console.log(e)
	  if(m){
		var posting = "dataid="+e.alertName;
		jQuery.ajax({
			type: 'POST',
			url: "<?php echo base_url().$this->router->class; ?>/deleteThis",
			data: posting,
			success: function(response) {
				if(response == 'success'){
					toastr.success("<?php echo $this->lang->line('msg_success_delete'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
				}else{
					toastr.error("<?php echo $this->lang->line('msg_empty_delete'); ?>", "<?php echo $this->lang->line('error_notif'); ?>");
				}
				jQuery("#listData").load('<?php echo $_SERVER["REQUEST_URI"]; ?>');
			}
		})
	  }else{
	  	jQuery.prompt.close();
	  }
}
function publishThis(code){
	var txt = "<?php echo $this->lang->line('alert_publish'); ?>  <input type='hidden' id='alertName' name='alertName' value='"+code+"' />";
	$.prompt(txt ,{  submit: doPublish, buttons: { Ok: true, Cancel: false },prefix:'jqismooth' });
}
function doPublish(v,m,f,e){
	  if(m){
		var posting = "dataid="+e.alertName;
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url().$this->router->class; ?>/publish/",
			data: posting,
			success: function(response) {
				if(response == 'success'){
					toastr.success('<?php echo $this->lang->line('msg_success_publish'); ?>','<?php echo $this->lang->line('success_notif'); ?>', 3000);
				}else{
					toastr.error('<?php echo $this->lang->line('msg_empty_publish'); ?>', '<?php echo $this->lang->line('error_notif'); ?>',3000);
				}
				$("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
	  }else{
	  	$.prompt.close();
	  }
}
function unpublishThis(code,page){
	var txt = "<?php echo $this->lang->line('alert_unpublish'); ?>  <input type='hidden' id='alertName' name='alertName' value='"+code+"' />";
	$.prompt(txt ,{  submit: doUnpublish, buttons: { Ok: true, Cancel: false },prefix:'jqismooth' });
}
function doUnpublish(v,m,f,e){
	  if(m){
	  	var posting = "dataid="+e.alertName;

		$.ajax({
			type: 'POST',
			url: "<?php echo base_url().$this->router->class; ?>/unpublish/",
			data: posting,
			success: function(response) {
				if(response == 'success'){
					toastr.success('<?php echo $this->lang->line('msg_success_unpublish'); ?>','<?php echo $this->lang->line('success_notif'); ?>', 3000);
				}else{
					toastr.error('<?php echo $this->lang->line('msg_empty_unpublish'); ?>','<?php echo $this->lang->line('error_notif'); ?>', 3000);
				}
				$("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
	  }else{
	  	$.prompt.close();
	  }
}
</script>
<form id="tabellistdata" method="post" action="<?php echo base_url().$this->router->class; ?>/postProcess" onsubmit="return false;">
<div class="box-body table-responsive no-padding">
	<?php if(isset($lists) && count($lists) > 0) {?>
	<table class="table table-bordered table-striped">
	<tr>
        <th width="10" class="text-center">
        	<input type="checkbox" id="checkall" />
        </th>
        <th width="10" class="text-center">
        	<?php echo $this->lang->line('tablelist_no'); ?>
        </th>
        <th>
        	<?php echo $this->lang->line('name_provinsi'); ?>
        </th>
        <th>
        	<?php echo $this->lang->line('kode_provinsi'); ?>
        </th>
        <th>
        	<?php echo $this->lang->line('status'); ?>
        </th>
        <th colspan="3" class="text-center" width="30">
        	<?php echo $this->lang->line('tablelist_option'); ?>
        </th>
    </tr>
	<?php $i = $start_no; foreach ($lists as $list) { $i++; ?>
        <tr>
           	<td class="text-center">
				<?php if($this->session->userdata('role') == '0' || $this->session->userdata('role') == '1'){?>
            	<input type="checkbox" name="datacek[]" class="check-all" value="<?php echo $list['id']; ?>" />
				<?php } ?>
            </td>
            <td class="text-center">
            	<?php echo $i; ?>
            </td>
            <td>
            	<?php echo $list['nama']; ?>
            </td>
            <td>
            	<?php echo $list['kode']; ?>
            </td>
            <td>
            	<?php echo statustext($list['status']); ?>
            </td>
			<?php if($this->session->userdata('role') == '0' || $this->session->userdata('role') == '1'){?>
            <td width='30' class="text-center">
            	<?php
					switch($list['status']){
						case 0:
						?>
							<a href="javascript:void(0)" onclick="publishThis(<?php echo $list['id']; ?>)"  title="<?php echo $this->lang->line('navigation_publish'); ?>">
							<button class="btn btn-primary btn-xs" type="button">
								<i class="fa fa-arrow-up"></i> <?php echo $this->lang->line('navigation_publish'); ?>
							</button>
							</a>
                        <?php
							break;
						case 1:
						?>
							<a href="javascript:void(0)" onclick="unpublishThis(<?php echo $list['id']; ?>)" title="<?php echo $this->lang->line('navigation_unpublish'); ?>">
							<button class="btn btn-warning btn-xs" type="button">
								<i class="fa fa-arrow-down"></i> <?php echo $this->lang->line('navigation_unpublish'); ?>
							</button>
							</a>
                        <?php
							break;
					}
				?>
            </td>
			<?php } ?>
            <td width='10' class="text-center">
            	<a href="<?php echo base_url('/provinsi')."/modif/".$list['id']; ?>" title='<?php echo $this->lang->line('navigation_modif'); ?>'>
            	<button class="btn btn-info btn-xs" type="button">
        			<i class="fa fa-fw fa-edit"></i>
        		</button>
	        	</a>
            </td>
            <td width='10' class="text-center">
				<?php if($this->session->userdata('role') == '0' || $this->session->userdata('role') == '1'){?>
            	<button class="btn btn-danger btn-xs" type="button" onclick="deleteThis(<?php echo $list['id']; ?>)" title="<?php echo $this->lang->line('navigation_delete'); ?>">
        			<i class="fa fa-fw fa-trash"></i>
        		</button>
				<?php } ?>
            </td>
        </tr>
    <?php } ?>
	</table>
</div><!-- /.box-body -->
<div class="box-footer clearfix">
	<?php if($this->session->userdata('role') == '0' || $this->session->userdata('role') == '1'){?>
	<button onclick="submitkie()" type="button" class="btn btn-danger btn-xs pull-left">
		<i class="fa fa-trash"></i>Hapus
	</button>
	<?php } ?>
  	<ul class="pagination pagination-sm no-margin pull-right"><?php echo isset($pagination)?$pagination:""; ?></ul>
</div>
<?php } else { ?>
	<p><center><?php echo $this->lang->line('no_data'); ?></center></p>
<?php } ?>
</form>
