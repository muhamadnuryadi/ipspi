<script type="text/javascript">
jQuery(document).ready(function($) {
	$('#checkall').click(function () {
		$('#tabellistdata .check-all').attr('checked', this.checked);
	});
	$(".pagination a").click(function(e){
		// stop normal link click
		e.preventDefault(); 
		var linkUrl = jQuery(this).attr("href");
		if(linkUrl){
			jQuery('#listData').load(linkUrl.replace(/\s/g,'%20'));
		}
	});

	$('.fancybox').fancybox();
});

function submitkie(){
	jQuery().ajaxStart(function($) {
		$('#loading').show();
		$('#result').hide();
	}).ajaxStop(function($) {
		$('#loading').hide();
		$('#result').fadeIn('slow');	
		$("#tabellistdata")[0].reset();
	});
	
	jQuery.ajax({
			type: 'POST',
			url: jQuery('#tabellistdata').attr('action'),
			data: jQuery('#tabellistdata').serialize(),
			success: function(response) {
				if(response == 'success'){
					toastr.success("<?php echo $this->lang->line('msg_success_delete'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
				}else{
					toastr.error("<?php echo $this->lang->line('msg_empty_delete'); ?>", "<?php echo $this->lang->line('error_notif'); ?>");
				}
				
				jQuery("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
		return false;
}
function deleteThis(code){
	var txt = "<?php echo $this->lang->line('alert_delete'); ?> <input type='hidden' id='alertName' name='alertName' value='"+code+"' />";
	jQuery.prompt(txt ,{  submit: doCondition, buttons: { <?php echo $this->lang->line('alert_ok'); ?>: true, <?php echo $this->lang->line('alert_cancel'); ?>: false },prefix:'jqismooth' });
}
function doCondition(v,m,f,e){
	  if(m){
	  	//alert(f.alertName);
		var posting = "dataid="+e.alertName;
		jQuery.ajax({
			type: 'POST',
			url: "<?php echo base_url().$this->router->class; ?>/deleteThis",
			data: posting,
			success: function(response) {
				if(response == 'success'){
					toastr.success("<?php echo $this->lang->line('msg_success_delete'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
				}else{
					toastr.error("<?php echo $this->lang->line('msg_empty_delete'); ?>", "<?php echo $this->lang->line('error_notif'); ?>");
				}
				jQuery("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
	  }else{
	  	jQuery.prompt.close();
	  }
}
</script>
<form id="tabellistdata" method="post" action="<?php echo base_url().$this->router->class; ?>/postProcess" onsubmit="return false;">
<div class="box-body table-responsive no-padding">
	<?php if(isset($lists) && count($lists) > 0) {?>
	<table class="table table-bordered table-striped">
	<tr>
		<?php 
		/*
		<th style="width: 10px"><input type="checkbox" id="checkall" /></th>
		*/
		?>
		
		<th style="width: 10px"><?php echo $this->lang->line('tablelist_no'); ?></th>
		<th><?php echo $this->lang->line('name'); ?></th>
		<th><?php echo $this->lang->line('email_users'); ?></th>
		<th><?php echo $this->lang->line('status'); ?></th>
		<th>Tanggal Dibuat</th>
		<th>Tanggal Dibayar</th>
		<th>Tipe Pembayaran</th>
		<th <th colspan="2">ID Pembayaran</th>
		<?php /*<th colspan="2" class="text-center"><?php echo $this->lang->line('tablelist_option'); ?></th>*/ ?>
	</tr>
	<?php $i = $start_no; foreach ($lists as $list) { $i++; ?>
        <tr>
        	<?php 
        	/*
			<td class="text-center">	
            	<input type="checkbox" name="datacek[]" class="check-all" value="<?php echo $list['id']; ?>" />
            </td>
        	*/
        	?>
            <td class="text-center">
            	<?php echo $i; ?>
            </td>
            <td>
            	<?php echo $list['nama']; ?>
            </td>
            <td>
            	<?php echo $list['email']; ?>
            </td>

            <td>
            	<?php echo $list['status']; ?>
            </td>

            <td>
            	<?php echo $list['date_created']; ?>
            </td>

            <td>
            	<?php echo $list['date_paid']; ?>
            </td>

            <td>
            	<?php echo $list['payment_type']; ?>
            </td>
            <td width='10' class="text-center">
		        <?php echo $list['charge_id']; ?>
			</td>
            <td width='10' class="text-center">
            	<?php 
            	/*
            	<a class="btn btn-danger btn-xs fancybox fancybox.ajax" href="<?php echo base_url().$this->router->class; ?>/view/<?php echo $list['id']; ?>">
					Detail
				</a>

				<a href="<?php echo base_url('/member_access')."/modif/".$list['id']; ?>" title='<?php echo $this->lang->line('navigation_modif'); ?>'>
            	<button class="btn btn-info btn-xs" type="button">
        			<i class="fa fa-fw fa-edit"></i>
        		</button>
	        	</a>  
            	*/
            	?>
            </td>
            <td width='10' class="text-center">
            	<?php 
            	/*
				<button class="btn btn-danger btn-xs" type="button" onclick="deleteThis(<?php echo $list['id']; ?>)" title="<?php echo $this->lang->line('navigation_delete'); ?>">
        			<i class="fa fa-fw fa-trash"></i>
        		</button>
            	*/
            	?>
            </td>
        </tr>
    <?php } ?>
	</table>
</div><!-- /.box-body -->
<div class="box-footer clearfix">
<?php if($this->session->userdata('role') < 2){?>
  	<ul class="pagination pagination-sm no-margin pull-right"><?php echo isset($pagination)?$pagination:""; ?></ul>
<?php } ?>
</div>
<?php } else { ?>
	<p><center><?php echo $this->lang->line('no_data'); ?></center></p>
<?php } ?>
</form>