<script type="text/javascript">
	jQuery(document).ready(function($) {
		jQuery("#listData").load('<?php echo base_url().$this->router->class; ?>/getAllData/');
	});
	function searchThis(){
		var frm = document.searchform;
		var email = frm.email.value;
		var name = frm.name.value;
		if(name == ''){ name = '-'; }
		jQuery("#listData").load("<?php echo base_url().$this->router->class; ?>/searchdata/"+encode_js(name)+"/"+encode_js(email));
	}
</script>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo strtoupper($this->module_name); ?>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('/'); ?>"><i class="fa fa-dashboard"></i><?php echo $this->lang->line('dashboard'); ?></a></li>
			<li class="active"><?php echo ucfirst($this->module_name); ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
	                <div class="box-header with-border">
		                 <div class="row">
	                 		<div class="col-md-10">
	                 			<form id="searchform" name="searchform" method="POST" action="<?php echo base_url().$this->router->class; ?>searchdata/" onsubmit="searchThis(); return false;">
									<div class="input-group">
										<div class="row">
											<div class="col-xs-5">
												<input type="text" name="name" class="form-control input-sm pull-left"  placeholder="<?php echo $this->lang->line('name'); ?>"/>
											</div>
											<div class="col-xs-5">
												<input type="text" name="email" class="form-control input-sm pull-left"  placeholder="<?php echo $this->lang->line('email_users'); ?>"/>
											</div>
											<div class="col-xs-2">
												<div class="input-group-btn">
													<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
												</div> 
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="col-md-2 col-md-12 text-right">
								<?php 
								/*
								<a class="btn btn-success btn-xs" href="<?php echo base_url().$this->router->class; ?>/add">
									<i class="fa fa-fw fa-plus"></i>
									<?php echo $this->lang->line('navigation_add'); ?>
								</a>
								*/
								?>
							</div>
	                 	</div>
	                </div>
	                <div id="listData">
	                	<center>
	                		<img src='<?php echo $this->webconfig['back_images']; ?>ajax_loading.gif' align='middle' style="margin:5px;" />
	                	</center>
	                </div>
	            </div>
			</div>
		</div>
	</section>
</div>