<script type="text/javascript">
jQuery(document).ready(function($) {
	$('#checkall').click(function () {
		$('#tabellistdata .check-all').attr('checked', this.checked);
	});
	$(".pagination a").click(function(e){
		// stop normal link click
		e.preventDefault(); 
		var linkUrl = jQuery(this).attr("href");
		if(linkUrl){
			jQuery('#listData').load(linkUrl.replace(/\s/g,'%20'));
		}
	});
	$('.fancybox').fancybox();
});

function submitkie(){
	jQuery().ajaxStart(function($) {
		$('#loading').show();
		$('#result').hide();
	}).ajaxStop(function($) {
		$('#loading').hide();
		$('#result').fadeIn('slow');	
		$("#tabellistdata")[0].reset();
	});
	
	jQuery.ajax({
			type: 'POST',
			url: jQuery('#tabellistdata').attr('action'),
			data: jQuery('#tabellistdata').serialize(),
			success: function(response) {
				if(response == 'success'){
					toastr.success("<?php echo $this->lang->line('msg_success_delete'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
				}else{
					toastr.error("<?php echo $this->lang->line('msg_empty_delete'); ?>", "<?php echo $this->lang->line('error_notif'); ?>");
				}
				
				jQuery("#listData").load('<?php echo $_SERVER["REQUEST_URI"]; ?>');
			}
		})
		return false;
}
function deleteThis(code){
	console.log(code)
	var txt = "<?php echo $this->lang->line('alert_delete'); ?> <input type='hidden' id='alertName' name='alertName' value='"+code+"' />";
	jQuery.prompt(txt ,{  submit: doCondition, buttons: { <?php echo $this->lang->line('ok'); ?>: true, <?php echo $this->lang->line('cancel'); ?>: false },prefix:'jqismooth' });
}
function doCondition(v,m,f,e){
	console.log(v)
		console.log(m)
		console.log(f)
		console.log(e)
	  if(m){
		var posting = "dataid="+e.alertName;
		jQuery.ajax({
			type: 'POST',
			url: "<?php echo base_url().$this->router->class; ?>/deleteThis",
			data: posting,
			success: function(response) {
				if(response == 'success'){
					toastr.success("<?php echo $this->lang->line('msg_success_delete'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
				}else{
					toastr.error("<?php echo $this->lang->line('msg_empty_delete'); ?>", "<?php echo $this->lang->line('error_notif'); ?>");
				}
				jQuery("#listData").load('<?php echo $_SERVER["REQUEST_URI"]; ?>');
			}
		})
	  }else{
	  	jQuery.prompt.close();
	  }
}
function publishThis(code){
	var txt = "<?php echo $this->lang->line('alert_publish'); ?>  <input type='hidden' id='alertName' name='alertName' value='"+code+"' />";
	$.prompt(txt ,{  submit: doPublish, buttons: { Ok: true, Cancel: false },prefix:'jqismooth' });
}
function doPublish(v,m,f,e){
	  if(m){
		var posting = "dataid="+e.alertName;
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url().$this->router->class; ?>/publish/",
			data: posting,
			success: function(response) {
				if(response == 'success'){
					toastr.success('<?php echo $this->lang->line('msg_success_publish'); ?>','<?php echo $this->lang->line('success_notif'); ?>', 3000);
				}else{
					toastr.error('<?php echo $this->lang->line('msg_empty_publish'); ?>', '<?php echo $this->lang->line('error_notif'); ?>',3000);
				}
				$("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
	  }else{
	  	$.prompt.close();
	  }
}
function unpublishThis(code,page){
	var txt = "<?php echo $this->lang->line('alert_unpublish'); ?>  <input type='hidden' id='alertName' name='alertName' value='"+code+"' />";
	$.prompt(txt ,{  submit: doUnpublish, buttons: { Ok: true, Cancel: false },prefix:'jqismooth' });
}
function doUnpublish(v,m,f,e){
	  if(m){
	  	var posting = "dataid="+e.alertName;
		
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url().$this->router->class; ?>/unpublish/",
			data: posting,
			success: function(response) {
				if(response == 'success'){
					toastr.success('<?php echo $this->lang->line('msg_success_unpublish'); ?>','<?php echo $this->lang->line('success_notif'); ?>', 3000);
				}else{
					toastr.error('<?php echo $this->lang->line('msg_empty_unpublish'); ?>','<?php echo $this->lang->line('error_notif'); ?>', 3000);
				}
				$("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
	  }else{
	  	$.prompt.close();
	  }
}
</script>
<form id="tabellistdata" method="post" action="<?php echo base_url().$this->router->class; ?>/postProcess" onsubmit="return false;">
<div class="box-body table-responsive no-padding">
	<?php if(isset($lists) && count($lists) > 0) {?>
	<table class="table table-bordered table-striped">
		<tr>
	        <th width="10" class="text-center">
	        	<input type="checkbox" id="checkall" />
	        </th>
	        <th width="10" class="text-center">
	        	<?php echo $this->lang->line('tablelist_no'); ?>
	        </th>
	        <th>
	        	<?php echo $this->lang->line('title'); ?>
	        </th>
	        <th>
	        	Penulis
	        </th>
	        <th colspan="4" class="text-center" width="30">
	        	<?php echo $this->lang->line('tablelist_option'); ?>
	        </th>
	    </tr>
	<?php $i = $start_no; foreach ($lists as $key => $list) { $i++; ?>
        <tr id="listItem_<?php echo $list['id']; ?>">
            <td class="text-center">
            	<input type="checkbox" name="datacek[]" class="check-all" value="<?php echo $list['id']; ?>" />
            	<input type="hidden" name="id_pos[]" value="<?php echo $list['id']; ?>" />
                <input class="position" type="hidden" name="position[]" value="<?php echo ($key+1); ?>" />
            </td>
            <td class="center_th_td nomor">
            	<?php echo $i; ?>
            </td>
            <td>
                <strong><?php echo $list['title']; ?></strong>
            </td>
            <td>
                <strong><?php echo getPenulis($list['id_user'],$list['role']); ?></strong>
            </td>
            <td  width='40' class="text-center">
            	<a class="btn btn-danger btn-xs fancybox fancybox.ajax" href="<?php echo base_url().$this->router->class; ?>/view/<?php echo $list['id']; ?>">
        			Detail
        		</a>
            </td>
            <td width='40' class="text-center">
            	<?php 
					switch($list['status']){
						case 0:
						?>
							<a href="javascript:void(0)" onclick="publishThis(<?php echo $list['id']; ?>)"  title="<?php echo $this->lang->line('navigation_publish'); ?>">
							<button class="btn btn-primary btn-xs" type="button">
								<i class="fa fa-arrow-up"></i> <?php echo $this->lang->line('navigation_publish'); ?>
							</button>
							</a>
                        <?php
							break;
						case 1:
						?>
							<a href="javascript:void(0)" onclick="unpublishThis(<?php echo $list['id']; ?>)" title="<?php echo $this->lang->line('navigation_unpublish'); ?>">
							<button class="btn btn-warning btn-xs" type="button">
								<i class="fa fa-arrow-down"></i> <?php echo $this->lang->line('navigation_unpublish'); ?>
							</button>
							</a>
                        <?php
							break;
					}
				?>
            </td>
            <td width='10' class="text-center">
            	<a href="<?php echo base_url().$this->router->class."/modif/".$list['id']; ?>">
            	<button class="btn btn-info btn-xs" type="button">
        			<i class="fa fa-pencil"></i>
        		</button>
	        	</a>  
            </td>
            <td width='10' class="text-center">
            	<button class="btn btn-danger btn-xs" type="button" onclick="deleteThis(<?php echo $list['id']; ?>)">
        			<i class="fa fa-trash"></i>
        		</button> 
            </td>
        </tr>
    <?php } ?>
	</table>
</div><!-- /.box-body -->
<div class="box-footer clearfix">
	<button onclick="submitkie()" type="button" class="btn btn-danger btn-xs pull-left">
		<i class="fa fa-trash"></i>Hapus
	</button>
  	<ul class="pagination pagination-sm no-margin pull-right"><?php echo isset($pagination)?$pagination:""; ?></ul>
</div>
<?php } else { ?>
	<p><center><?php echo $this->lang->line('no_data'); ?></center></p>
<?php } ?>
</form>