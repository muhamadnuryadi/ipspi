<?php
class Export extends CI_Controller {
	var $webconfig;
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
        $this->load->helper('csv');
        $this->load->library('image_moo');
        $this->load->helper('mpdf');
        $this->load->library('pagination');
		$this->lang->load('global', 'bahasa');
        $this->load->sharedModel('WilayahModel');
		$this->load->sharedModel('ConfigurationModel');
        $this->load->sharedModel('PekerjaanModel');
        $this->load->sharedModel('InstansiModel');
		$this->load->sharedModel('ExportModel');
        $this->load->sharedModel('TingkatModel');
		$this->load->sharedModel('LoginModel');
		
		$this->load->library('session');
		$this->webconfig = $this->config->item('webconfig');
		$this->module_name = 'Export';		

		if(!$this->LoginModel->isLoggedIn()){
			redirect(base_url().'login');
		}

        if ($this->session->userdata('role') == 3) {
            if ($this->session->userdata('status_access') != 1) {
                redirect(base_url().'pembayaran');
            }
        }
	}

	function index() {
        redirect(base_url() . $this->router->class . '/listing');
    }

    function listing($page = 0) {
        $tdata = array();
        $tdata['tingkatpendidikan'] = $this->TingkatModel->listData();
        $tdata['wilayah'] = $this->WilayahModel->listpropinsi();
        $tdata['pekerjaan'] = $this->PekerjaanModel->listData();
        $tdata['instansi'] = $this->InstansiModel->listData();
        
        $ldata['content'] = $this->load->view($this->router->class . '/index', $tdata, true);
        $this->load->sharedView('template', $ldata);
    }

    function getAllData($page = 0) {

        // $all_data = $this->ExportModel->listDataCount();
        $all_data = 0;
        $cfg_pg ['base_url'] = base_url() . $this->uri->segment(1) . '/' . $this->uri->segment(2) . "/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah']) ? $all_data['jumlah'] : 0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 3;

        $cfg_pg['first_link'] = '&laquo;';
        $cfg_pg['first_tag_close'] = '</li>';
        $cfg_pg['first_tag_open'] = '<li>';

        $cfg_pg['last_link'] = '&raquo;';
        $cfg_pg['last_tag_open'] = '<li>';
        $cfg_pg['last_tag_close'] = '</li>';

        $cfg_pg['next_link'] = '&gt;';
        $cfg_pg['next_tag_open'] = '<li>';
        $cfg_pg['next_tag_close'] = '</li>';

        $cfg_pg['prev_link'] = '&lt;';
        $cfg_pg['prev_tag_open'] = '<li>';
        $cfg_pg['prev_tag_close'] = '</li>';

        $cfg_pg['num_tag_open'] = '<li>';
        $cfg_pg['num_tag_close'] = '</li>';

        $cfg_pg['cur_tag_open'] = '<li class="active"><span>';
        $cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
        $listdata["pagination"] = $this->pagination->create_links();

        $start_no = 0;
        if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
            $start_no = $this->uri->segment($cfg_pg ['uri_segment']);
        }

        $listdata["start_no"] = $start_no;

        $start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
        $start_idx = $start_idx < 0 ? 0 : $start_idx;

        $listdata['lists'] = $this->ExportModel->listData(array(
            'start' => $start_idx
            , 'limit' => $cfg_pg['per_page']
        ));

        $this->load->view($this->router->class . '/list', $listdata);
    }

    function searchdata($name = '',$id_propinsi = '',$id_kota = '',$tingkat = '',$id_pekerjaan = '',$id_instansi = '',$id_sertifikasi = '',$no_anggota = '',$email = '',$id_jenis_anggota = '',$jurusan = '') {
        if ($name == '-') {$name = '';}
        if ($id_propinsi == '-') {$id_propinsi = '';}
        if ($id_kota == '-') {$id_kota = '';}
        if ($tingkat == '-') {$tingkat = '';}
        if ($id_pekerjaan == '-') {$id_pekerjaan = '';}
        if ($id_instansi == '-') {$id_instansi = '';}
        if ($id_sertifikasi == '-') {$id_sertifikasi = '';}
        if ($no_anggota == '-') {$no_anggota = '';}
        if ($email == '-') {$email = '';}
        if ($id_jenis_anggota == '-') {$id_jenis_anggota = '';}
        if ($jurusan == '-') {$jurusan = '';}
        
        # PAGINATION #
        // $all_data = $this->ExportModel->filterDataCount(array(
        //                                                         'name' => trim(urldecode($name))
        //                                                         ,'id_propinsi' => trim(urldecode($id_propinsi))
        //                                                         ,'id_kota' => trim(urldecode($id_kota))
        //                                                         ,'tingkat' => trim(urldecode($tingkat))
        //                                                         ,'id_pekerjaan' => trim(urldecode($id_pekerjaan))
        //                                                         ,'id_instansi' => trim(urldecode($id_instansi))
        //                                                         ,'id_sertifikasi' => trim(urldecode($id_sertifikasi))
        //                                                         ,'no_anggota' => trim(urldecode($no_anggota))
        //                                                         ,'email' => trim(urldecode($email))
        //                                                         ,'id_jenis_anggota' => trim(urldecode($id_jenis_anggota))
        //                                                         ,'jurusan' => trim(urldecode($jurusan))
        //                                                     ));
        $all_data = 0;
        $cfg_pg ['base_url'] = base_url() . $this->uri->segment(1).'/'.$this->uri->segment(2)."/".$this->uri->segment(3)."/".$this->uri->segment(4)."/".$this->uri->segment(5)."/".$this->uri->segment(6)."/".$this->uri->segment(7)."/".$this->uri->segment(8)."/".$this->uri->segment(9)."/".$this->uri->segment(10)."/".$this->uri->segment(11)."/".$this->uri->segment(12)."/"."/".$this->uri->segment(13)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah']) ? $all_data['jumlah'] : 0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 14;

        $cfg_pg['first_link'] = '&laquo;';
        $cfg_pg['first_tag_close'] = '</li>';
        $cfg_pg['first_tag_open'] = '<li>';

        $cfg_pg['last_link'] = '&raquo;';
        $cfg_pg['last_tag_open'] = '<li>';
        $cfg_pg['last_tag_close'] = '</li>';

        $cfg_pg['next_link'] = '&gt;';
        $cfg_pg['next_tag_open'] = '<li>';
        $cfg_pg['next_tag_close'] = '</li>';

        $cfg_pg['prev_link'] = '&lt;';
        $cfg_pg['prev_tag_open'] = '<li>';
        $cfg_pg['prev_tag_close'] = '</li>';

        $cfg_pg['num_tag_open'] = '<li>';
        $cfg_pg['num_tag_close'] = '</li>';

        $cfg_pg['cur_tag_open'] = '<li class="active"><span>';
        $cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
        $listdata["pagination"] = $this->pagination->create_links();

        $start_no = 0;
        if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
            $start_no = $this->uri->segment($cfg_pg ['uri_segment']);
        }

        $listdata["start_no"] = $start_no;

        $start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
        $start_idx = $start_idx < 0 ? 0 : $start_idx;

        $listdata['lists'] = $this->ExportModel->filterData(array(
                                                                    'start' => $start_idx
                                                                    ,'limit' => $cfg_pg['per_page']
                                                                    ,'name' => trim(urldecode($name))
                                                                    ,'id_propinsi' => trim(urldecode($id_propinsi))
                                                                    ,'id_kota' => trim(urldecode($id_kota))
                                                                    ,'tingkat' => trim(urldecode($tingkat))
                                                                    ,'id_pekerjaan' => trim(urldecode($id_pekerjaan))
                                                                    ,'id_instansi' => trim(urldecode($id_instansi))
                                                                    ,'id_sertifikasi' => trim(urldecode($id_sertifikasi))
                                                                    ,'no_anggota' => trim(urldecode($no_anggota))
                                                                    ,'email' => trim(urldecode($email))
                                                                    ,'id_jenis_anggota' => trim(urldecode($id_jenis_anggota))
                                                                    ,'jurusan' => trim(urldecode($jurusan))
        ));
        $this->load->view($this->router->class . '/list', $listdata);
    }

    public function exportCSV(){
        // if ($_POST) {

            $data = $this->ExportModel->listData();
            $field = array('0'=>array(
                'Nama','Email','Nama Provinsi','Nama Kota','No Anggota','Status Kawin','Jumlah Anak','Alamat Rumah','No Handphone','NIK'
                ,'Nama Perguruan Tinggi','Tahun Lulus'
                ,'Nama Pekerjaan'
                ,'Praktek Peran','Praktek Jabatan','Praktek Keahlian'
                ,'Penghargaan Nama','Penghargaan Instansi','Penghargaan Tahun'
                ,'Penghargaan Nama','Penghargaan Instansi','Penghargaan Tahun'
                ,'Sertifikasi Nama','Sertifikasi Lembaga','Sertifikasi Tahun'
                ,'Profesi Nama','Profesi Tahun'
                ));
            
            // $arr_nama = $this->input->post('nama');
            // $arr_email = $this->input->post('email');
            // $arr_provinsi = $this->input->post('nama_provinsi');
            // $arr_kota = $this->input->post('nama_kota');
            // $arr_no_anggota = $this->input->post('no_anggota');
            // $arr_status = $this->input->post('status');
            // $arr_tgllahir = $this->input->post('tgllahir');
            // $arr_statuskawin = $this->input->post('statuskawin');
            // $arr_jumanak = $this->input->post('jumanak');
            // $arr_alamatrumah = $this->input->post('alamatrumah');
            // $arr_hp = $this->input->post('hp');
            // $arr_ktp = $this->input->post('ktp');

            foreach ($data as $key => $value) {
                $pushdata[$key]['nama'] = $value['nama'];
                $pushdata[$key]['email'] = $value['email'];
                $pushdata[$key]['provinsi'] = $value['nama_provinsi']['nama'];
                $pushdata[$key]['kota'] = $value['nama_kota']['nama'];
                $pushdata[$key]['no_anggota'] = $value['no_anggota'];
                $pushdata[$key]['statuskawin'] = $value['statuskawin'];
                $pushdata[$key]['jumanak'] = $value['jumanak'];
                $pushdata[$key]['alamatrumah'] = $value['alamatrumah'];
                $pushdata[$key]['hp'] = $value['hp'];
                $pushdata[$key]['ktp'] = $value['ktp'];

                $pushdata[$key]['pendidikan_pt'] = isset($value['pendidikan']['pt'])?$value['pendidikan']['pt']:'';
                $pushdata[$key]['pendidikan_thn_lulus'] = isset($value['pendidikan']['thn_lulus'])?$value['pendidikan']['thn_lulus']:'';

                $pushdata[$key]['pekerjaan_nama'] = isset($value['pekerjaan']['nama'])?$value['pekerjaan']['nama']:'';

                $pushdata[$key]['praktek_peran'] = isset($value['praktek']['peran'])?$value['praktek']['peran']:'';
                $pushdata[$key]['praktek_jabatan'] = isset($value['praktek']['jabatan'])?$value['praktek']['jabatan']:'';
                $pushdata[$key]['praktek_keahlian'] = isset($value['praktek']['keahlian'])?$value['praktek']['keahlian']:'';

                $pushdata[$key]['penghargaan_nama'] = isset($value['penghargaan']['nama'])?$value['penghargaan']['nama']:'';
                $pushdata[$key]['penghargaan_instansi'] = isset($value['penghargaan']['instansi'])?$value['penghargaan']['instansi']:'';
                $pushdata[$key]['penghargaan_tahun'] = isset($value['penghargaan']['tahun'])?$value['penghargaan']['tahun']:'';

                $pushdata[$key]['sertifikasi_nama'] = isset($value['sertifikasi']['nama'])?$value['sertifikasi']['nama']:'';
                $pushdata[$key]['sertifikasi_lembaga'] = isset($value['sertifikasi']['lembaga'])?$value['sertifikasi']['lembaga']:'';
                $pushdata[$key]['sertifikasi_tahun'] = isset($value['sertifikasi']['tahun'])?$value['sertifikasi']['tahun']:'';

                $pushdata[$key]['profesi_nama'] = isset($value['profesi']['nama'])?$value['profesi']['nama']:'';
                $pushdata[$key]['profesi_tahun'] = isset($value['profesi']['tahun'])?$value['profesi']['tahun']:'';
            }

            $result =array_merge($field,$pushdata);
            array_to_csv($result, "Member ". strtotime(date('Y-m-d H:i:s')).".csv", ',');
        // }
    }

    public function searchdataExport($name = '',$id_propinsi = '',$id_kota = '',$tingkat = '',$id_pekerjaan = '',$id_instansi = '',$id_sertifikasi = '',$no_anggota = '',$email = '',$id_jenis_anggota = '',$jurusan = ''){
        if ($name == '-') {$name = '';}
        if ($id_propinsi == '-') {$id_propinsi = '';}
        if ($id_kota == '-') {$id_kota = '';}
        if ($tingkat == '-') {$tingkat = '';}
        if ($id_pekerjaan == '-') {$id_pekerjaan = '';}
        if ($id_instansi == '-') {$id_instansi = '';}
        if ($id_sertifikasi == '-') {$id_sertifikasi = '';}
        if ($no_anggota == '-') {$no_anggota = '';}
        if ($email == '-') {$email = '';}
        if ($id_jenis_anggota == '-') {$id_jenis_anggota = '';}
        if ($jurusan == '-') {$jurusan = '';}

        $start_idx = 0;

        $data = $this->ExportModel->filterData(array(
                        'start' => $start_idx
                        ,'name' => trim(urldecode($name))
                        ,'id_propinsi' => trim(urldecode($id_propinsi))
                        ,'id_kota' => trim(urldecode($id_kota))
                        ,'tingkat' => trim(urldecode($tingkat))
                        ,'id_pekerjaan' => trim(urldecode($id_pekerjaan))
                        ,'id_instansi' => trim(urldecode($id_instansi))
                        ,'id_sertifikasi' => trim(urldecode($id_sertifikasi))
                        ,'no_anggota' => trim(urldecode($no_anggota))
                        ,'email' => trim(urldecode($email))
                        ,'id_jenis_anggota' => trim(urldecode($id_jenis_anggota))
                        ,'jurusan' => trim(urldecode($jurusan))
                    ));

        $field = array('0'=>array(
                'Nama','Email','Nama Provinsi','Nama Kota','No Anggota','Status Kawin','Jumlah Anak','Alamat Rumah','No Handphone','NIK'
                ,'Nama Perguruan Tinggi','Tahun Lulus'
                ,'Nama Pekerjaan'
                ,'Praktek Peran','Praktek Jabatan','Praktek Keahlian'
                ,'Penghargaan Nama','Penghargaan Instansi','Penghargaan Tahun'
                ,'Penghargaan Nama','Penghargaan Instansi','Penghargaan Tahun'
                ,'Sertifikasi Nama','Sertifikasi Lembaga','Sertifikasi Tahun'
                ,'Profesi Nama','Profesi Tahun'
                ));

        foreach ($data as $key => $value) {
            $pushdata[$key]['nama'] = $value['nama'];
            $pushdata[$key]['email'] = $value['email'];
            $pushdata[$key]['provinsi'] = $value['nama_provinsi']['nama'];
            $pushdata[$key]['kota'] = $value['nama_kota']['nama'];
            $pushdata[$key]['no_anggota'] = $value['no_anggota'];
            $pushdata[$key]['statuskawin'] = $value['statuskawin'];
            $pushdata[$key]['jumanak'] = $value['jumanak'];
            $pushdata[$key]['alamatrumah'] = $value['alamatrumah'];
            $pushdata[$key]['hp'] = $value['hp'];
            $pushdata[$key]['ktp'] = $value['ktp'];

            $pushdata[$key]['pendidikan_pt'] = isset($value['pendidikan']['pt'])?$value['pendidikan']['pt']:'';
            $pushdata[$key]['pendidikan_thn_lulus'] = isset($value['pendidikan']['thn_lulus'])?$value['pendidikan']['thn_lulus']:'';

            $pushdata[$key]['pekerjaan_nama'] = isset($value['pekerjaan']['nama'])?$value['pekerjaan']['nama']:'';

            $pushdata[$key]['praktek_peran'] = isset($value['praktek']['peran'])?$value['praktek']['peran']:'';
            $pushdata[$key]['praktek_jabatan'] = isset($value['praktek']['jabatan'])?$value['praktek']['jabatan']:'';
            $pushdata[$key]['praktek_keahlian'] = isset($value['praktek']['keahlian'])?$value['praktek']['keahlian']:'';

            $pushdata[$key]['penghargaan_nama'] = isset($value['penghargaan']['nama'])?$value['penghargaan']['nama']:'';
            $pushdata[$key]['penghargaan_instansi'] = isset($value['penghargaan']['instansi'])?$value['penghargaan']['instansi']:'';
            $pushdata[$key]['penghargaan_tahun'] = isset($value['penghargaan']['tahun'])?$value['penghargaan']['tahun']:'';

            $pushdata[$key]['sertifikasi_nama'] = isset($value['sertifikasi']['nama'])?$value['sertifikasi']['nama']:'';
            $pushdata[$key]['sertifikasi_lembaga'] = isset($value['sertifikasi']['lembaga'])?$value['sertifikasi']['lembaga']:'';
            $pushdata[$key]['sertifikasi_tahun'] = isset($value['sertifikasi']['tahun'])?$value['sertifikasi']['tahun']:'';

            $pushdata[$key]['profesi_nama'] = isset($value['profesi']['nama'])?$value['profesi']['nama']:'';
            $pushdata[$key]['profesi_tahun'] = isset($value['profesi']['tahun'])?$value['profesi']['tahun']:'';
        }

        $result =array_merge($field,$pushdata);
        array_to_csv($result, "Member ". strtotime(date('Y-m-d H:i:s')).".csv", ',');
    }

}