<script type="text/javascript">
jQuery(document).ready(function($) {
	
	$(".pagination a").click(function(e){
		// stop normal link click
        jQuery(".table-responsive").html("<center><img src='<?php echo $this->webconfig['back_images']; ?>ajax_loading.gif' align='middle' style='margin:5px;' /></center>");
		e.preventDefault(); 
		var linkUrl = jQuery(this).attr("href");
		if(linkUrl){
			jQuery('#listData').load(linkUrl.replace(/\s/g,'%20'));
		}
	});
	$('.fancybox').fancybox();
});
</script>
<form id="tabellistdata" method="post" action="<?php echo base_url().$this->router->class; ?>/exportCSV">

<div class="box-body table-responsive no-padding">
	<?php if(isset($lists) && count($lists) > 0) {?>
	<table class="table table-bordered table-striped">
	<tr>
        <th width="10" class="text-center">
        	<?php echo $this->lang->line('tablelist_no'); ?>
        </th>
        <th>
        	<?php echo $this->lang->line('name_members'); ?> / Email
        </th>
        <th>
        	Provinsi / DPD
        </th>

        <th>
        	Kota
        </th>

        <th>
        	No. Anggota
        </th>
        
        <th>
        	<?php echo $this->lang->line('status'); ?>
        </th>
       
    </tr>
	<?php $i = $start_no; foreach ($lists as $list) { $i++; ?>
        <tr>
            <td class="text-center">
            	<?php echo $i; ?>
            </td>
            <td>
            	<?php echo $list['nama']; ?>
            	<br/>
            	<small>
            		<?php echo isset($list['email'])?$list['email']:''; ?>		
            	</small>
            </td>
            <td>
            	<?php 
            	echo isset($list['nama_provinsi']['nama'])?$list['nama_provinsi']['nama']:'';
            	?>	
            </td>
            <td>
            	<?php 
            	echo isset($list['nama_kota']['nama'])?$list['nama_kota']['nama']:'';
            	?>	
            </td>
            <td>
            	<?php echo isset($list['no_anggota'])?$list['no_anggota']:''; ?>
            </td>
            <td>
            	<?php if($this->session->userdata('role') != '3'){?>
            	<?php 
					switch($list['status']){
						case 0:
							echo "Nonaktif";
							break;
						case 1:
							echo "Aktif";
							break;
					}
				?>
				<?php } ?>
				
            </td>
            <?php 
            /*
            <input type="hidden" name="nama[]" value="<?php echo isset($list['nama'])?$list['nama']:''; ?>">
            <input type="hidden" name="email[]" value="<?php echo isset($list['email'])?$list['email']:''; ?>">
            <input type="hidden" name="nama_provinsi[]" value="<?php echo isset($list['nama_provinsi']['nama'])?$list['nama_provinsi']['nama']:''; ?>">
            <input type="hidden" name="nama_kota[]" value="<?php echo isset($list['nama_kota']['nama'])?$list['nama_kota']['nama']:''; ?>">
            <input type="hidden" name="no_anggota[]" value="<?php echo isset($list['no_anggota'])?$list['no_anggota']:''; ?>">
            <input type="hidden" name="status[]" value="<?php echo isset($list['status'])?$list['status']:''; ?>">

            <input type="hidden" name="tgllahir[]" value="<?php echo isset($list['tgllahir'])?$list['tgllahir']:''; ?>">
            <input type="hidden" name="statuskawin[]" value="<?php echo isset($list['statuskawin'])?$list['statuskawin']:''; ?>">
            <input type="hidden" name="jumanak[]" value="<?php echo isset($list['jumanak'])?$list['jumanak']:''; ?>">
            <input type="hidden" name="alamatrumah[]" value="<?php echo isset($list['alamatrumah'])?$list['alamatrumah']:''; ?>">
            <input type="hidden" name="hp[]" value="<?php echo isset($list['hp'])?$list['hp']:''; ?>">
            <input type="hidden" name="ktp[]" value="<?php echo isset($list['ktp'])?$list['ktp']:''; ?>">
            <input type="hidden" name="jk[]" value="<?php echo isset($list['jk'])?$list['jk']:''; ?>">
            */
            ?>
            <input type="hidden" name="is_post" value="1">
        </tr>
    <?php } ?>
	</table>
</div><!-- /.box-body -->
<div class="box-footer clearfix">
	
  	<ul class="pagination pagination-sm no-margin pull-right"><?php echo isset($pagination)?$pagination:""; ?></ul>
</div>
<?php } else { ?>
	<p><center><?php echo $this->lang->line('no_data'); ?></center></p>
<?php } ?>
</form>
