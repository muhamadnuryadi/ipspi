<?php
class Member_access extends CI_Controller{
	var $webconfig;
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->sharedModel('Member_accessModel');
		$this->load->sharedModel('WilayahModel');
		$this->load->sharedModel('LoginModel');
		if(!$this->LoginModel->isLoggedIn()){
			redirect(base_url().'login');
		}

		if($this->session->userdata('role') != 0 && $this->session->userdata('role') != 1){
			redirect(base_url().'dashboard');
		}
		$this->webconfig = $this->config->item('webconfig');
		$this->module_name = $this->lang->line('member_access');
	}
	function index(){
		redirect(base_url().$this->router->class.'/listing');
	}
	function listing($page = 0){
		$tdata = array();
		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
		$this->load->sharedView('template', $ldata);
	}
	function getAllData($page = 0){
		$all_data = $this->Member_accessModel->listDataCount();
		
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 3;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '&gt;';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '&lt;';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><a>';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['lists'] = $this->Member_accessModel->listData(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
															));

		$this->load->view($this->router->class.'/list', $listdata);
	}
	function searchdata($name = 'name'){
		if($name == '-'){$name = '';}

		# PAGINATION #
		$all_data = $this->Member_accessModel->filterDataCount(array('fullname'=>trim(urldecode($name))));
		$cfg_pg ['base_url'] = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2)."/".$this->uri->segment(3)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah'])?$all_data['jumlah']:0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 4;

		$cfg_pg['first_link'] = '&laquo;';
		$cfg_pg['first_tag_close'] = '</li>';
		$cfg_pg['first_tag_open'] = '<li>';

		$cfg_pg['last_link'] = '&raquo;';
		$cfg_pg['last_tag_open'] = '<li>';
		$cfg_pg['last_tag_close'] = '</li>';

		$cfg_pg['next_link'] = '&gt;';
		$cfg_pg['next_tag_open'] = '<li>';
		$cfg_pg['next_tag_close'] = '</li>';

		$cfg_pg['prev_link'] = '&lt;';
		$cfg_pg['prev_tag_open'] = '<li>';
		$cfg_pg['prev_tag_close'] = '</li>';

		$cfg_pg['num_tag_open'] = '<li>';
		$cfg_pg['num_tag_close'] = '</li>';

		$cfg_pg['cur_tag_open'] = '<li class="active"><a>';
		$cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
		$listdata["pagination"] = $this->pagination->create_links();

		$start_no = 0;
		if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
			$start_no = $this->uri->segment($cfg_pg ['uri_segment']);
		}

		$listdata["start_no"] = $start_no;

		$start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
		$start_idx = $start_idx < 0?0:$start_idx;

		$listdata['lists'] = $this->Member_accessModel->filterData(array(
																'start'  => $start_idx
																,'limit' => $cfg_pg['per_page']
																,'fullname' => trim(urldecode($name))
															));
		$this->load->view($this->router->class.'/list', $listdata);
	}
	function add(){
		$tdata = array();

		$tdata['provinsi'] = $this->WilayahModel->listpropinsi(array('status' => 1));
		require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
		if($_POST)
		{

			$validator = new FormValidator();
		    $validator->addValidation("fullname","req",$this->lang->line('error_empty_name_users'));
		    $validator->addValidation("email","req",$this->lang->line('error_empty_email_users'));
		    $validator->addValidation("password","req",$this->lang->line('error_empty_password_users'));
			$validator->addValidation("password","minlen=5",$this->lang->line('error_empty_password_5_users'));
		    $validator->addValidation("password_1","req",$this->lang->line('error_empty_retype_password_users'));
		    $validator->addValidation("password_1","eqelmnt=password",$this->lang->line('error_empty_retype_password_true_users'));
		    if($this->input->post('role') > 3){
		    	$validator->addValidation("id_parent","req",$this->lang->line('error_empty_id_parent_users'));
		    }

		    if($validator->ValidateForm())
		    {
		        $doInsert = $this->Member_accessModel->entriData(array(
														        'fullname'=>$this->input->post('fullname')
														        ,'username'=>$this->input->post('username')
														        ,'email'=>$this->input->post('email')
														        ,'password'=>$this->input->post('password')
														        ,'role'=>$this->input->post('role')
														        ,'id_provinsi'=>$this->input->post('id_provinsi')
														  ));
				if($doInsert == 'empty'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
				}else if($doInsert == 'exist'){
					$tdata['error_hash'] = array('error' => $this->lang->line('error_duplicate_username'));
				}else if($doInsert == 'failed'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
				}else if($doInsert == 'success'){
					$tdata['success'] = $this->lang->line('msg_success_entry');
				}
				$ldata['content'] = $this->load->view($this->router->class.'/add',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		    else
		    {
		        $tdata['fullname']     = $this->input->post('fullname');
			    $tdata['username'] = $this->input->post('username');
			    $tdata['email'] = $this->input->post('email');
			    $tdata['role'] = $this->input->post('role');
			    $tdata['id_provinsi'] = $this->input->post('id_provinsi');
		    	$tdata['error_hash'] = $validator->GetErrors();

				$ldata['content'] = $this->load->view($this->router->class.'/add',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		}else{
			$ldata['content'] = $this->load->view($this->router->class.'/add',$tdata, true);
			$this->load->sharedView('template', $ldata);
		}
	}
	function modif($id = 0){
		if($id == 0){
			redirect(base_url().$this->router->class);
		}

		if($this->session->userdata('role') != '0' && $this->session->userdata('role') != '1'){
			$tdata['provinsi'] = $this->WilayahModel->listpropinsi(array('status' => 1, 'id' => $this->session->userdata('userpropinsi')));
		}else{
			$tdata['provinsi'] = $this->WilayahModel->listpropinsi(array('status' => 1));
		}
		$tdata['lists'] = $this->Member_accessModel->listData(array('id'=>$id));

		require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
		if($_POST)
		{
			$validator = new FormValidator();

		    $validator->addValidation("fullname","req",$this->lang->line('error_empty_name_users'));
		    // $validator->addValidation("email","req",$this->lang->line('error_empty_email_users'));
		    $validator->addValidation("username","req",$this->lang->line('error_empty_username_users'));
		    $validator->addValidation("password_1","eqelmnt=password",$this->lang->line('error_empty_retype_password_true_users'));
		    if($this->input->post('role') > 3){
		    	$validator->addValidation("id_provinsi","req",$this->lang->line('error_empty_id_parent_users'));
		    }
		    if($validator->ValidateForm())
		    {
		        $doUpdate = $this->Member_accessModel->updateData(array(
														        'id' => $id
		        												,'fullname'=>$this->input->post('fullname')
														        ,'username'=>$this->input->post('username')
														        ,'email'=>$this->input->post('email')
														        ,'password'=>$this->input->post('password')
														        ,'role'=>$this->input->post('role')
														        ,'id_provinsi'=>$this->input->post('id_provinsi')
														  ));

				if($doUpdate == 'empty'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
				}else if($doUpdate == 'exist'){
					$tdata['error_hash'] = array('error' => $this->lang->line('error_duplicate_username'));
				}else if($doUpdate == 'failed'){
					$tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
				}else if($doUpdate == 'min_5'){
					$tdata['error_hash'] = array('error' => $this->lang->line('error_empty_password_5_users'));
				}else if($doUpdate == 'success'){
					$tdata['success'] = $this->lang->line('msg_success_update');
					$tdata['lists'] = $this->Member_accessModel->listData(array('id'=>$id));
				}
				$ldata['content'] = $this->load->view($this->router->class.'/modif',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		    else
		    {
		        $tdata['fullname']     = $this->input->post('fullname');
			    $tdata['username'] = $this->input->post('username');
			    $tdata['password'] = $this->input->post('password');
			    $tdata['password_1'] = $this->input->post('password_1');
			    $tdata['role'] = $this->input->post('role');
			    $tdata['id_provinsi'] = $this->input->post('id_provinsi');
		    	$tdata['error_hash'] = $validator->GetErrors();

				$ldata['content'] = $this->load->view($this->router->class.'/modif',$tdata, true);
				$this->load->sharedView('template', $ldata);
		    }
		}else{
			$ldata['content'] = $this->load->view($this->router->class.'/modif',$tdata, true);
			$this->load->sharedView('template', $ldata);
		}
	}
	public function deleteThis(){
		$doDelete = $this->__delete($this->input->post('dataid'));

		if($doDelete == 'failed'){
			echo "nodata";
		}else if($doDelete == 'success'){
			echo "success";
		}
	}
	public function postProcess(){
		$postvalue = $this->input->post('datacek');
		if($postvalue != ''){
			foreach($postvalue as $data){
				$this->__delete($data);
			}
			echo "success";
		}else{
			echo "nodata";
		}
	}
	private function __delete($id){
		if($id == 0){
			redirect(base_url().$this->router->class);
		}
		$doDelete = $this->Member_accessModel->deleteData($id);
    	return $doDelete;
	}

	public function generate(){
		$generate = $this->Member_accessModel->generate();
	}
}
