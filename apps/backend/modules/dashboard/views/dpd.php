<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.js" type="text/javascript"></script>
<script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/chartjs/utils.js" type="text/javascript"></script>
<style type="text/css">

</style>
<script type="text/javascript">
jQuery(document).ready(function($) {
    var totalpemakaian = '<?php echo $members;?>';
    var json_totalpemakaian = $.parseJSON(totalpemakaian);//parse JSON
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: <?php echo $prop;?>,
            datasets: [{
                label: 'Jumlah Member',
                data: json_totalpemakaian,
                backgroundColor: window.chartColors.blue,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    window.onload = function() {
      
    };
});
function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
function format_hasil(jumlahtot){
  jumlahtot = jumlahtot.toString();
  var pattern = /(-?\d+)(\d{3})/;
  while (pattern.test(jumlahtot))
    jumlahtot = jumlahtot.replace(pattern, "$1.$2");
  return jumlahtot;
}
</script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 406px;">
    <section class="content-header">
        <h1>
            Dashboard System<small> List Dashboard System </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>
    <section class="content" style="min-height: auto;">
      
        <div class="col-md-12 col-xs-12">
          <div class="box box-info">
            <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-tag"></i> Grafik</h3>
              <canvas id="myChart" ></canvas>
                <a href="<?php echo base_url('/dashboard'); ?>" title=''>
                <button class="btn btn-info btn-xs" type="button">
                    <i class="fa fa-fw fa-edit"></i>Kembali
                </button>
                </a>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
    </section>


 
</div>