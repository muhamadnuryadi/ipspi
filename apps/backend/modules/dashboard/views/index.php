<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.js" type="text/javascript"></script>
<script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/chartjs/utils.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.3/FileSaver.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/emn178/chartjs-plugin-labels/src/chartjs-plugin-labels.js"></script>
<style type="text/css">

</style>
<script type="text/javascript">
jQuery(document).ready(function($) {

    $("#save-btn-dpd").click(function() {
        $("#pieDpd").get(0).toBlob(function(blob) {
          saveAs(blob, "chart_DPD.png");
        });

        // var myLine = document.getElementById("pieDpd").toDataURL("image/jpg");
        // var url = myLine.replace(/^data:image\/[^;]+/, 'data:application/octet-stream');
        // window.open(url, "test.png");
    });

    $("#save-btn-status").click(function() {
        $("#pieStats").get(0).toBlob(function(blob) {
          saveAs(blob, "chart_STATUS.png");
        });
    });

    $("#save-btn-kota").click(function() {
        $("#pieKota").get(0).toBlob(function(blob) {
          saveAs(blob, "chart_KOTA.png");
        });
    });

    $("#save-btn-pendidikan").click(function() {
        $("#piePend").get(0).toBlob(function(blob) {
          saveAs(blob, "chart_PENDIDIKAN.png");
        });
    });

    $("#save-btn-work").click(function() {
        $("#pieWork").get(0).toBlob(function(blob) {
          saveAs(blob, "chart_PEKERJAAN.png");
        });
    });

    $("#save-btn-sertifikasi").click(function() {
        $("#pieSertifikasi").get(0).toBlob(function(blob) {
          saveAs(blob, "chart_SERTIFIKASI.png");
        });
    });

    $("#save-btn-instansi").click(function() {
        $("#pieInstansi").get(0).toBlob(function(blob) {
          saveAs(blob, "chart_INSTANSI.png");
        });
    });

    // draw background
    var backgroundColor = 'white';
    Chart.plugins.register({
        beforeDraw: function(c) {
            var ctx = c.chart.ctx;
            ctx.fillStyle = backgroundColor;
            ctx.fillRect(0, 0, c.chart.width, c.chart.height);
        }
    });


    /*DPD*/
    var dpdkey = '<?php echo $nama;?>';
    var json_dpdkey = $.parseJSON(dpdkey);//parse JSON
    var dpd = '<?php echo $jum;?>';
    var sumdpd = '<?php echo $sumdat;?>';
    var json_dpd = $.parseJSON(dpd);//parse JSON

    var dynamicColors = function() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    };
    var randcolor = [];
    for (var i = 0; i < sumdpd; i++ ) {
        randcolor.push(getRandomColor());
    }
    
    var configdpd = {
        type: 'pie',
        data: {
            datasets: [{
                data: json_dpd,
                backgroundColor: randcolor
            }],
            labels: json_dpdkey
        },
        options: {
            responsive: true,
            tooltips: {
              mode: 'label',
              callbacks: {
                   label: function(tooltipItem, data) {

                    var dataset = data.datasets[tooltipItem.datasetIndex];
                    var datasetLabel = data.labels[tooltipItem.index] || '';

                    return datasetLabel + ": " + format_hasil($.parseJSON(dataset.data[tooltipItem.index]));
                    
                }
              }
            },
            plugins: {
              labels: {
                render: function (args) {
                  return args.percentage + '% (' + args.value + ')';
                },
                position: 'outside'
              }
            }
        }
    };

    /*DPD*/
    var statskey = '<?php echo $arrnamastats;?>';
    var json_statskey = $.parseJSON(statskey);//parse JSON
    var stats = '<?php echo $arrstats;?>';
    var json_stats = $.parseJSON(stats);//parse JSON
    
    var configstats = {
        type: 'pie',
        data: {
            datasets: [{
                data: json_stats,
                backgroundColor: ['red','green']
            }],
            labels: json_statskey
        },
        options: {
            responsive: true,
            tooltips: {
              mode: 'label',
              callbacks: {
                   label: function(tooltipItem, data) {

                    var dataset = data.datasets[tooltipItem.datasetIndex];
                    var datasetLabel = data.labels[tooltipItem.index] || '';

                    return datasetLabel + ": " + format_hasil($.parseJSON(dataset.data[tooltipItem.index]));
                    
                }
              }
            },
            plugins: {
              labels: {
                render: function (args) {
                  return args.percentage + '% (' + args.value + ')';
                },
                position: 'outside'
              }
            }
        }
    };

    /*KOTA*/
    var kotakey = '<?php echo $namakota;?>';
    var json_kotakey = $.parseJSON(kotakey);//parse JSON
    var kota = '<?php echo $jumkota;?>';
    var sumkota = '<?php echo $sumkota;?>';
    var json_kota = $.parseJSON(kota);//parse JSON

    var dynamicColors = function() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    };
    var randcolor = [];
    for (var i = 0; i < sumkota; i++ ) {
        randcolor.push(getRandomColor());
    }
    
    var configkota = {
        type: 'pie',
        data: {
            datasets: [{
                data: json_kota,
                backgroundColor: randcolor
            }],
            labels: json_kotakey
        },
        options: {
            responsive: true,
            tooltips: {
              mode: 'label',
              callbacks: {
                   label: function(tooltipItem, data) {

                    var dataset = data.datasets[tooltipItem.datasetIndex];
                    var datasetLabel = data.labels[tooltipItem.index] || '';

                    return datasetLabel + ": " + format_hasil($.parseJSON(dataset.data[tooltipItem.index]));
                    
                }
              }
            },
            plugins: {
              labels: {
                render: function (args) {
                  return args.percentage + '% (' + args.value + ')';
                },
                position: 'outside'
              }
            }
        }
    };

    /*PENDIDIKAN*/
    var pendkey = '<?php echo $namapend;?>';
    var json_pendkey = $.parseJSON(pendkey);//parse JSON
    var pend = '<?php echo $jumpend;?>';
    var sumpend = '<?php echo $sumpend;?>';
    var json_pend = $.parseJSON(pend);//parse JSON

    var dynamicColors = function() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    };
    var randcolor = [];
    for (var i = 0; i < sumpend; i++ ) {
        randcolor.push(getRandomColor());
    }
    
    var configpend = {
        type: 'pie',
        data: {
            datasets: [{
                data: json_pend,
                backgroundColor: randcolor
            }],
            labels: json_pendkey
        },
        options: {
            responsive: true,
            tooltips: {
              mode: 'label',
              callbacks: {
                   label: function(tooltipItem, data) {

                    var dataset = data.datasets[tooltipItem.datasetIndex];
                    var datasetLabel = data.labels[tooltipItem.index] || '';

                    return datasetLabel + ": " + format_hasil($.parseJSON(dataset.data[tooltipItem.index]));
                    
                }
              }
            },
            plugins: {
              labels: {
                render: function (args) {
                  return args.percentage + '% (' + args.value + ')';
                },
                position: 'outside'
              }
            }
        }
    };

    /*PEKERJAAN*/
    var workkey = '<?php echo $namawork;?>';
    var json_workkey = $.parseJSON(workkey);//parse JSON
    var work = '<?php echo $jumwork;?>';
    var sumwork = '<?php echo $sumwork;?>';
    var json_work = $.parseJSON(work);//parse JSON

    var dynamicColors = function() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    };
    var randcolor = [];
    for (var i = 0; i < sumwork; i++ ) {
        randcolor.push(getRandomColor());
    }
    
    var configwork = {
        type: 'pie',
        data: {
            datasets: [{
                data: json_work,
                backgroundColor: randcolor
            }],
            labels: json_workkey
        },
        options: {
            responsive: true,
            tooltips: {
              mode: 'label',
              callbacks: {
                   label: function(tooltipItem, data) {

                    var dataset = data.datasets[tooltipItem.datasetIndex];
                    var datasetLabel = data.labels[tooltipItem.index] || '';

                    return datasetLabel + ": " + format_hasil($.parseJSON(dataset.data[tooltipItem.index]));
                    
                }
              }
            },
            plugins: {
              labels: {
                render: function (args) {
                  return args.percentage + '% (' + args.value + ')';
                },
                position: 'outside'
              }
            }
        }
    };

    //SERTIFIKASI
    var sertifikasikey = '<?php echo $nama_sertifikasi;?>';
    var json_sertifikasikey = $.parseJSON(sertifikasikey);//parse JSON
    var sertifikasi = '<?php echo $jum_sertifikasi;?>';
    var sertifikasi_tipe = '<?php echo $sertifikasi_tipe;?>';
    var json_sertifikasi = $.parseJSON(sertifikasi);//parse JSON

    var dynamicColors = function() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    };
    var randcolor = [];
    for (var i = 0; i < sertifikasi_tipe; i++ ) {
        randcolor.push(getRandomColor());
    }
    
    var configsertifikasi = {
        type: 'pie',
        data: {
            datasets: [{
                data: json_sertifikasi,
                backgroundColor: randcolor
            }],
            labels: json_sertifikasikey
        },
        options: {
            responsive: true,
            tooltips: {
              mode: 'label',
              callbacks: {
                   label: function(tooltipItem, data) {

                    var dataset = data.datasets[tooltipItem.datasetIndex];
                    var datasetLabel = data.labels[tooltipItem.index] || '';

                    return datasetLabel + ": " + format_hasil($.parseJSON(dataset.data[tooltipItem.index]));
                    
                }
              }
            },
            plugins: {
              labels: {
                render: function (args) {
                  return args.percentage + '% (' + args.value + ')';
                },
                position: 'outside'
              }
            }
        }
    };

    //INSTANSI
    var instansikey = '<?php echo $nama_instansi;?>';
    var json_instansikey = $.parseJSON(instansikey);//parse JSON
    var instansi = '<?php echo $jum_instansi;?>';
    var instansi_tipe = '<?php echo $instansi_tipe;?>';
    var json_instansi = $.parseJSON(instansi);//parse JSON

    var dynamicColors = function() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    };
    var randcolor = [];
    for (var i = 0; i < instansi_tipe; i++ ) {
        randcolor.push(getRandomColor());
    }
    
    var configinstansi = {
        type: 'pie',
        data: {
            datasets: [{
                data: json_instansi,
                backgroundColor: randcolor
            }],
            labels: json_instansikey
        },
        options: {
            responsive: true,
            tooltips: {
              mode: 'label',
              callbacks: {
                   label: function(tooltipItem, data) {

                    var dataset = data.datasets[tooltipItem.datasetIndex];
                    var datasetLabel = data.labels[tooltipItem.index] || '';

                    return datasetLabel + ": " + format_hasil($.parseJSON(dataset.data[tooltipItem.index]));
                    
                }
              }
            },
            plugins: {
              labels: {
                render: function (args) {
                  return args.percentage + '% (' + args.value + ')';
                },
                position: 'outside'
              }
            }
        }
    };

    window.onload = function() {
      var ctxdpd = document.getElementById("pieDpd").getContext("2d");
      window.myPiedpd = new Chart(ctxdpd, configdpd);

      var ctxstats = document.getElementById("pieStats").getContext("2d");
      window.myPiestats = new Chart(ctxstats, configstats);

      var ctxkota = document.getElementById("pieKota").getContext("2d");
      window.myPiekota = new Chart(ctxkota, configkota);

      var ctxpend = document.getElementById("piePend").getContext("2d");
      window.myPiepend = new Chart(ctxpend, configpend);

      var ctxwork = document.getElementById("pieWork").getContext("2d");
      window.myPiework = new Chart(ctxwork, configwork);

      var ctxsertifikasi = document.getElementById("pieSertifikasi").getContext("2d");
      window.myPiesertifikasi = new Chart(ctxsertifikasi, configsertifikasi);

      var ctxinstansi = document.getElementById("pieInstansi").getContext("2d");
      window.myPieinstansi = new Chart(ctxinstansi, configinstansi);
    };
});
function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
function format_hasil(jumlahtot){
  jumlahtot = jumlahtot.toString();
  var pattern = /(-?\d+)(\d{3})/;
  while (pattern.test(jumlahtot))
    jumlahtot = jumlahtot.replace(pattern, "$1.$2");
  return jumlahtot;
}
</script>
<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper" style="min-height: 406px;">
    <section class="content-header">
        <h1>
            Dashboard System<small> List Dashboard System </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>
    <?php if($this->session->userdata('role') == 0 || $this->session->userdata('role') == 1 || $this->session->userdata('role') == 2){?>
    <section class="content" style="min-height: auto;">
        <div class="clearfix"></div>
        <div class="col-md-12 col-xs-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-tag"></i> DPD</h3>
              <canvas id="pieDpd"></canvas>
              <div class="clearfix"></div>
                <a href="<?php echo base_url('/dashboard/dpd'); ?>" title=''>
                    <button class="btn btn-info btn-xs" type="button">
                        <i class="fa fa-fw fa-edit"></i>Detail
                    </button>
                </a>
                <button class="btn btn-info btn-xs" type="button" id="save-btn-dpd">Save Chart Image</button>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-xs-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-tag"></i> STATUS</h3>
              <canvas id="pieStats"></canvas>
              <div class="clearfix"></div>
              <button class="btn btn-info btn-xs" type="button" id="save-btn-status">Save Chart Image</button>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-12 col-xs-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-tag"></i> KOTA</h3>
              <canvas id="pieKota"></canvas>
              <div class="clearfix"></div>
              <button class="btn btn-info btn-xs" type="button" id="save-btn-kota">Save Chart Image</button>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-xs-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-tag"></i> PENDIDIKAN</h3>
              <canvas id="piePend"></canvas>
              <div class="clearfix"></div>
              <button class="btn btn-info btn-xs" type="button" id="save-btn-pendidikan">Save Chart Image</button>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-12 col-xs-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-tag"></i> PEKERJAAN</h3>
              <canvas id="pieWork"></canvas>
              <div class="clearfix"></div>
              <button class="btn btn-info btn-xs" type="button" id="save-btn-work">Save Chart Image</button>
            </div>
          </div>
        </div>
        
        <div class="col-xs-12 col-xs-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-tag"></i> SERTIFIKASI</h3>
              <canvas id="pieSertifikasi"></canvas>
              <div class="clearfix"></div>
              <button class="btn btn-info btn-xs" type="button" id="save-btn-sertifikasi">Save Chart Image</button>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-xs-12 col-xs-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-tag"></i> INSTANSI</h3>
              <canvas id="pieInstansi"></canvas>
              <div class="clearfix"></div>
              <button class="btn btn-info btn-xs" type="button" id="save-btn-instansi">Save Chart Image</button>
            </div>
          </div>
        </div>

        <div class="clearfix"></div>        
    </section>
    <?php } ?>
</div>