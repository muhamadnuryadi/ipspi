<?php
class Dashboard extends CI_Controller {
	var $webconfig;
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
        $this->load->library('pagination');
		$this->lang->load('global', 'bahasa');
		$this->load->sharedModel('DashboardModel');
		$this->load->sharedModel('ConfigurationModel');
		$this->load->sharedModel('LoginModel');
		
		$this->load->library('session');
		$this->webconfig = $this->config->item('webconfig');
		$this->module_name = $this->lang->line('side_menu_dashboard');		

		if(!$this->LoginModel->isLoggedIn()){
			redirect(base_url().'login');
		}
	}

	public function index()
	{
		$tdata = array();

		/*DPD/PROV==================================================*/
		$qdata = $this->DashboardModel->listDataCount();
		$adata = array();
		foreach ($qdata as $key => $value) {
			if($value['jumlah'] == ''){
				$adata[$key]['jum'] = 0;
				$adata[$key]['nama'] = $value['nama'];
			}else{
				$adata[$key]['jum'] = $value['jumlah']['total'];
				$adata[$key]['nama'] = $value['nama'];
			}
		}
		$numbers = $adata; 
		rsort($numbers);
		$ndata = array();
		foreach ($numbers as $row => $list) {
			if($row < 10){
				$ndata[$row]['jum'] = $list['jum'];
				$ndata[$row]['nama'] = $list['nama'];
			}
		}
		$sdata = array();
		foreach ($ndata as $key => $value) {
			$sdata[$key] = $value['jum'];
		}
		$sdata2 = array();
		foreach ($ndata as $key => $value) {
			$sdata2[$key] = $value['nama'];
		}
		$tdata['sumdat'] = count($sdata);
		$tdata['jum'] = json_encode($sdata);
		$tdata['nama'] = json_encode($sdata2);

		/*STATUS=======================================================*/
		$mdata = $this->DashboardModel->listDataMember();
		$mstats = array();
		foreach ($mdata as $key => $value) {
			$mstats[$key] = $value['total'];
		}
		$namastats = array('Tidak Aktif','Aktif');
		$tdata['arrstats'] = json_encode($mstats);
		$tdata['arrnamastats'] = json_encode($namastats);
		/*KOTA=====================================================*/
		$kdata = $this->DashboardModel->listMemberKota();
		$kmdata = array();
		foreach ($kdata as $key => $value) {
			if($value['jumlah'] == ''){
				$kmdata[$key]['jum'] = 0;
				$kmdata[$key]['nama'] = $value['nama'];
			}else{
				$kmdata[$key]['jum'] = $value['jumlah']['total'];
				$kmdata[$key]['nama'] = $value['nama'];
			}
		}
		$numbkota = $kmdata; 
		rsort($numbkota);
		$kmbdata = array();
		foreach ($numbkota as $row => $list) {
			if($row < 10){
				$kmbdata[$row]['jum'] = $list['jum'];
				$kmbdata[$row]['nama'] = $list['nama'];
			}
		}
		$kmadata = array();
		foreach ($kmbdata as $key => $value) {
			$kmadata[$key] = $value['jum'];
		}
		$kmadata2 = array();
		foreach ($kmbdata as $key => $value) {
			$kmadata2[$key] = $value['nama'];
		}
		$tdata['sumkota'] = count($kmadata);
		$tdata['jumkota'] = json_encode($kmadata);
		$tdata['namakota'] = json_encode($kmadata2);
		/*PENDIDIKAN========================================================*/
		$pdata = $this->DashboardModel->listMemberPendidikan();
		$pmdata = array();
		foreach ($pdata as $key => $value) {
			if($value['jumlah'] == ''){
				$pmdata[$key]['jum'] = 0;
				$pmdata[$key]['nama'] = $value['name'];
			}else{
				$pmdata[$key]['jum'] = $value['jumlah']['total'];
				$pmdata[$key]['nama'] = $value['name'];
			}
		}
		$numbpend = $pmdata; 
		rsort($numbpend);
		$pmbdata = array();
		foreach ($numbpend as $row => $list) {
			if($row < 10){
				$pmbdata[$row]['jum'] = $list['jum'];
				$pmbdata[$row]['nama'] = $list['nama'];
			}
		}
		$pmadata = array();
		foreach ($pmbdata as $key => $value) {
			$pmadata[$key] = $value['jum'];
		}
		$pmadata2 = array();
		foreach ($pmbdata as $key => $value) {
			$pmadata2[$key] = $value['nama'];
		}
		$tdata['sumpend'] = count($pmadata);
		$tdata['jumpend'] = json_encode($pmadata);
		$tdata['namapend'] = json_encode($pmadata2);

		/*PEKERJAAN========================================================*/
		$wdata = $this->DashboardModel->listMemberPekerjaan();
		$wmdata = array();
		foreach ($wdata as $key => $value) {
			if($value['jumlah'] == ''){
				$wmdata[$key]['jum'] = 0;
				$wmdata[$key]['nama'] = $value['name'];
			}else{
				$wmdata[$key]['jum'] = $value['jumlah']['total'];
				$wmdata[$key]['nama'] = $value['name'];
			}
		}
		$numbwork = $wmdata; 
		rsort($numbwork);
		$wmbdata = array();
		foreach ($numbwork as $row => $list) {
			if($row < 10){
				$wmbdata[$row]['jum'] = $list['jum'];
				$wmbdata[$row]['nama'] = $list['nama'];
			}
		}
		$wmadata = array();
		foreach ($wmbdata as $key => $value) {
			$wmadata[$key] = $value['jum'];
		}
		$wmadata2 = array();
		foreach ($wmbdata as $key => $value) {
			$wmadata2[$key] = $value['nama'];
		}
		$tdata['sumwork'] = count($wmadata);
		$tdata['jumwork'] = json_encode($wmadata);
		$tdata['namawork'] = json_encode($wmadata2);
		

		//SERTIFIKASI
		$sertifikasidata = $this->DashboardModel->listMemberSertifikasi();
		$sertifikasi_tipe = count($this->webconfig['sertifikasi_tipe']);
		$jum_sertifikasi = array();
		$nama_sertifikasi = array();

		if (count($sertifikasidata) > 0) {
			foreach ($sertifikasidata as $key => $value) {
				$jum_sertifikasi[] = $value;
				$nama_sertifikasi[] = $key;
			}
		}

		$tdata['sertifikasi_tipe'] = $sertifikasi_tipe;
		$tdata['jum_sertifikasi'] = json_encode($jum_sertifikasi);
		$tdata['nama_sertifikasi'] = json_encode($nama_sertifikasi);

		//============================================================
		//JENIS INSTANSI
		$instansidata = $this->DashboardModel->listMemberInstansi();
		
		$instansi_tipe = count($instansidata);
		$jum_instansi = array();
		$nama_instansi = array();

		if (count($instansidata) > 0) {
			foreach ($instansidata as $key => $value) {
				$jum_instansi[] = $value;
				$nama_instansi[] = $key;
			}
		}

		$tdata['instansi_tipe'] = $instansi_tipe;
		$tdata['jum_instansi'] = json_encode($jum_instansi);
		$tdata['nama_instansi'] = json_encode($nama_instansi);
		
		## LOAD LAYOUT ##
		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
		$this->load->sharedView('template', $ldata);
	}

	public function dpd()
	{
		$tdata = array();
		$all_data = $this->DashboardModel->listDataCount();
		
		$dataprovinsi = array();
		foreach ($all_data as $key => $value) {
			$dataprovinsi[$key] = $value['nama'];
		}
		
		$jummember = array();
		foreach ($all_data as $key => $value) {
			if($value['jumlah'] == ''){
				$jummember[$key] = 0;
			}else{
				$jummember[$key] = $value['jumlah']['total'];
			}
		}
		
		$tdata['prop'] = json_encode($dataprovinsi);
		$tdata['members'] = json_encode($jummember);

		## LOAD LAYOUT ##
		$ldata['content'] = $this->load->view($this->router->class.'/dpd',$tdata, true);
		$this->load->sharedView('template', $ldata);
	}
}