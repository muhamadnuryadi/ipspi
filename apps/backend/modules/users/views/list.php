<script type="text/javascript">
jQuery(document).ready(function($) {
	$('#checkall').click(function () {
		$('#tabellistdata .check-all').attr('checked', this.checked);
	});
	$(".pagination a").click(function(e){
		// stop normal link click
		e.preventDefault(); 
		var linkUrl = jQuery(this).attr("href");
		if(linkUrl){
			jQuery('#listData').load(linkUrl.replace(/\s/g,'%20'));
		}
	});
});

function submitkie(){
	jQuery().ajaxStart(function($) {
		$('#loading').show();
		$('#result').hide();
	}).ajaxStop(function($) {
		$('#loading').hide();
		$('#result').fadeIn('slow');	
		$("#tabellistdata")[0].reset();
	});
	
	jQuery.ajax({
			type: 'POST',
			url: jQuery('#tabellistdata').attr('action'),
			data: jQuery('#tabellistdata').serialize(),
			success: function(response) {
				if(response == 'success'){
					toastr.success("<?php echo $this->lang->line('msg_success_delete'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
				}else{
					toastr.error("<?php echo $this->lang->line('msg_empty_delete'); ?>", "<?php echo $this->lang->line('error_notif'); ?>");
				}
				
				jQuery("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
		return false;
}
function deleteThis(code){
	var txt = "<?php echo $this->lang->line('alert_delete'); ?> <input type='hidden' id='alertName' name='alertName' value='"+code+"' />";
	jQuery.prompt(txt ,{  submit: doCondition, buttons: { <?php echo $this->lang->line('alert_ok'); ?>: true, <?php echo $this->lang->line('alert_cancel'); ?>: false },prefix:'jqismooth' });
}
function doCondition(v,m,f,e){
	  if(m){
	  	//alert(f.alertName);
		var posting = "dataid="+e.alertName;
		jQuery.ajax({
			type: 'POST',
			url: "<?php echo base_url().$this->router->class; ?>/deleteThis",
			data: posting,
			success: function(response) {
				if(response == 'success'){
					toastr.success("<?php echo $this->lang->line('msg_success_delete'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
				}else{
					toastr.error("<?php echo $this->lang->line('msg_empty_delete'); ?>", "<?php echo $this->lang->line('error_notif'); ?>");
				}
				jQuery("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
	  }else{
	  	jQuery.prompt.close();
	  }
}
</script>
<form id="tabellistdata" method="post" action="<?php echo base_url().$this->router->class; ?>/postProcess" onsubmit="return false;">
<div class="box-body table-responsive no-padding">
	<?php if(isset($lists) && count($lists) > 0) {?>
	<table class="table table-bordered table-striped">
	<tr>
		<th style="width: 10px"><input type="checkbox" id="checkall" /></th>
		<th style="width: 10px"><?php echo $this->lang->line('tablelist_no'); ?></th>
		<th><?php echo $this->lang->line('label_name_users'); ?> / <?php echo $this->lang->line('label_username_users'); ?></th>
		<th><?php echo $this->lang->line('label_group_users'); ?></th>
		<th><?php echo $this->lang->line('provinsi'); ?></th>
		<th colspan="2" class="text-center"><?php echo $this->lang->line('tablelist_option'); ?></th>
	</tr>
	<?php $i = $start_no; foreach ($lists as $list) { $i++; ?>
        <tr>
           	<td class="text-center">
           		<?php if($this->session->userdata('role') < 2){?>
            	<input type="checkbox" name="datacek[]" class="check-all" value="<?php echo $list['id']; ?>" />
            	<?php } ?>
            </td>
            <td class="text-center">
            	<?php echo $i; ?>
            </td>
            <td>
            	<?php echo $list['fullname']; ?> / <b><?php echo $list['username']; ?></b>
            </td>
            <td>
            	<?php 
					switch($list['role']){
						case 0:
							echo "Programmer";
						break;
						case 1:
							echo "Super Admin";
						break;
						case 2:
							echo "Admin";
						break;
						case 3:
							echo "User/Member";
						break;
					}
				?>
            </td>
            <td>
            	<?php echo userwilayah($list['id_provinsi']); ?>
            </td>
            <td width='10' class="text-center">
            	<a href="<?php echo base_url('/users')."/modif/".$list['id']; ?>" title='<?php echo $this->lang->line('navigation_modif'); ?>'>
            	<button class="btn btn-info btn-xs" type="button">
        			<i class="fa fa-fw fa-edit"></i>
        		</button>
	        	</a>  
            </td>
            <td width='10' class="text-center">
            <?php if($this->session->userdata('role') < 2){?>
            	<button class="btn btn-danger btn-xs" type="button" onclick="deleteThis(<?php echo $list['id']; ?>)" title="<?php echo $this->lang->line('navigation_delete'); ?>">
        			<i class="fa fa-fw fa-trash"></i>
        		</button> 
        	<?php }else{ ?>
        	-
        	<?php } ?>
            </td>
        </tr>
    <?php } ?>
	</table>
</div><!-- /.box-body -->
<div class="box-footer clearfix">
<?php if($this->session->userdata('role') < 2){?>
  	<ul class="pagination pagination-sm no-margin pull-right"><?php echo isset($pagination)?$pagination:""; ?></ul>
<?php } ?>
</div>
<?php } else { ?>
	<p><center><?php echo $this->lang->line('no_data'); ?></center></p>
<?php } ?>
</form>