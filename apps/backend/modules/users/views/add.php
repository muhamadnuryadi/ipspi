<script type="text/javascript">
jQuery(document).ready(function($) {
    
    $(".username").change(function () {
        $.ajax({
            type: 'post',
            url: "<?php echo base_url() ?>users/checkUsername/",
            dataType: "json",
            data: $(this).serialize(),
            success: function(data) {
                    if(data != 0){
                       alert("<?php echo $this->lang->line('error_duplicate_username'); ?>");
                       $('.username').val('');
                    }
                }
        });
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo strtoupper($this->module_name); ?>
            <small><?php echo $this->lang->line('label_add_users'); ?></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('/'); ?>">
                    <i class="fa fa-dashboard"></i>
                    <?php echo $this->lang->line('dashboard'); ?>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url().$this->router->class; ?>"><?php echo ucfirst($this->module_name); ?></a>
            </li>
            <li class="active"><?php echo $this->lang->line('navigation_add'); ?></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="col-xs-12 text-right">
                            <a class="btn btn-success btn-xs" href="<?php echo base_url().$this->router->class; ?>"  title="<?php echo $this->lang->line('navigation_list'); ?>">
                                <i class="fa fa-fw fa-plus"></i>
                                <?php echo $this->lang->line('navigation_list'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" name="form" method="POST" action="<?php echo base_url().$this->router->class; ?>/add" enctype="multipart/form-data">
                            <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
                                <?php foreach($error_hash as $inp_err){ ?>
                                    <script type="text/javascript">
                                    jQuery(document).ready(function($) {
                                        toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                                        });
                                    </script>
                                <?php } ?>
                            <?php } ?>
                            
                            <?php if(isset($success)){ ?>
                                <script type="text/javascript">
                                jQuery(document).ready(function($) {
                                    toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
                                    });
                                </script>
                            <?php } ?>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('label_name_users'); ?> <span>*</span></label>
                                <div class="col-sm-5">
                                    <input type="text" name="fullname" value='<?php echo isset($fullname)?$fullname:''; ?>' size="50" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('label_username_users'); ?> <span>*</span></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control username"  name="username" value='<?php echo isset($username)?$username:''; ?>'>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('email_users'); ?> <span>*</span></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control email"  name="email" value='<?php echo isset($email)?$email:''; ?>'>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('label_password_users'); ?> <span>*</span></label>
                                <div class="col-sm-4">
                                    <input type="password" class="form-control" name="password" value='<?php echo isset($password)?$password:''; ?>'>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('label_retype_password_users'); ?> <span>*</span></label>
                                <div class="col-sm-4">
                                    <input type="password" class="form-control" name="password_1" value='<?php echo isset($password_1)?$password_1:''; ?>'>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('pilih_propinsi'); ?> <span>*</span></label>
                                <div class="col-sm-4">
                                    <select name="id_provinsi" class="form-control" id="id_provinsi" style="padding: 3px;">
                                        <option value="">-- <?php echo $this->lang->line('pilih_propinsi'); ?> --</option>
                                        <?php foreach ($provinsi as $key => $value){ ?>
                                            <option <?php echo (isset($id_provinsi) && $id_provinsi == $value['id'])?"selected='selected'":""; ?> value="<?php echo $value['id']; ?>"><?php echo $value['nama']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('label_group_users'); ?> <span>*</span></label>
                                <div class="col-sm-3">
                                    <select id="role" name="role" class="form-control">
                                        <?php if($this->session->userdata('role') == 0){ ?>
                                            <option value="0" <?php if(isset($role) && $role == 0) echo "selected='selected'"; ?>>Programmer</option>
                                        <?php } ?>

                                        <?php if($this->session->userdata('role') == 0 || $this->session->userdata('role') == 1 ){ ?>
                                            <option value="1" <?php if(isset($role) && $role == 1) echo "selected='selected'"; ?>>Super Admin</option>
                                        <?php } ?>

                                        <?php if($this->session->userdata('role') == 0 || $this->session->userdata('role') == 1 ){ ?>
                                            <option value="2" <?php if(isset($role) && $role == 2) echo "selected='selected'"; ?>>Admin</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group footertable">
                                <label class="col-sm-3 control-label">&nbsp;</label>
                                <div class="col-sm-5 text-left">
                                    <input class="btn btn-danger btn-sm mr5" type="submit" value="<?php echo $this->lang->line('navigation_save'); ?>">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>