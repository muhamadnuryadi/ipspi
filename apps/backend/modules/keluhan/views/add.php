<script type="text/javascript">
jQuery(document).ready(function($) {

});
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo strtoupper($this->module_name); ?>
            <small><?php echo $this->lang->line('add_keluhan'); ?></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('/'); ?>">
                    <i class="fa fa-dashboard"></i>
                    <?php echo $this->lang->line('dashboard'); ?>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url().$this->router->class; ?>"><?php echo ucfirst($this->module_name); ?></a>
            </li>
            <li class="active"><?php echo $this->lang->line('navigation_add'); ?></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="col-xs-12 text-right">
                            <a class="btn btn-success btn-xs" href="<?php echo base_url().$this->router->class; ?>"  title="<?php echo $this->lang->line('navigation_list'); ?>">
                                <i class="fa fa-fw fa-plus"></i>
                                <?php echo $this->lang->line('navigation_list'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" name="form" method="POST" action="<?php echo base_url().$this->router->class; ?>/add" enctype="multipart/form-data">
                            <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
                                <?php foreach($error_hash as $inp_err){ ?>
                                    <script type="text/javascript">
                                    jQuery(document).ready(function($) {
                                        toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                                        });
                                    </script>
                                <?php } ?>
                            <?php } ?>
                            
                            <?php if(isset($success)){ ?>
                                <script type="text/javascript">
                                jQuery(document).ready(function($) {
                                    toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
                                    });
                                </script>
                            <?php } ?>

                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo $this->lang->line('title_keluhan'); ?> <span>*</span></label>
                                <div class="col-sm-5">
                                    <input type="text" id="name" name="name" value='<?php echo isset($name)?$name:''; ?>' size="50" class="form-control">
                                </div>
                            </div>
                            
                            <div class="form-group footertable">
                                <label class="col-sm-2 control-label">&nbsp;</label>
                                <div class="col-sm-5 text-left">
                                    <input class="btn btn-danger btn-sm mr5" type="submit" value="<?php echo $this->lang->line('navigation_save'); ?>">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>