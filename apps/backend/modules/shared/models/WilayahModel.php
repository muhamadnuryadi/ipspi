<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class WilayahModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "wilayah";
	}
	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "-- ORDER BY id DESC";

		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listpropinsi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$status = isset($params["status"])?$params["status"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "-- ORDER BY id DESC";

		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($status != '') {
			$conditional .= "AND status = '".$status."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE id_parent = '0'
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listDataCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listKota($params=array()){
		$prop = isset($params["prop"])?$params["prop"]:'';

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE id_parent = '".$prop."'

		");

		$result = $q->result_array();
		return $result;
	}

	public function listKecamatan($params=array()){
		$city = isset($params["city"])?$params["city"]:'';

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE id_parent = '".$city."'

		");

		$result = $q->result_array();
		return $result;
	}

	public function listDataProvinsiCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional = "AND id = '".$id."'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE id_parent = '0'
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listDataProvinsi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional = "AND id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE id_parent = '0'
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function filterDataProvinsi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$name = isset($params["name"])?$params["name"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($name != '') {
			$conditional .= "AND nama LIKE '%".$this->db->escape_str($name)."%'";
		}
		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE id_parent = '0'
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function filterDataProvinsiCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$name = isset($params["name"])?$params["name"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($name != '') {
			$conditional .= "AND nama LIKE '%".$this->db->escape_str($name)."%'";
		}
		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE id_parent = '0'
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function doPublish($id){
		if($id == 0) return 'failed';
		$doPublish = $this->db->query("
			UPDATE ".$this->maintablename."
			SET
				status = '1'
			WHERE
				id = ".$id."
		");
		if($doPublish){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_publish_provinsi')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function doUnublish($id){
		if($id == 0) return 'failed';

		$doUnublish = $this->db->query("
			UPDATE ".$this->maintablename."
			SET status = '0'
			WHERE
				id = ".$id."
		");
		if($doUnublish){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_unpublish_provinsi')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function entriData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$kode = isset($params["kode"])?$params["kode"]:'';
		$status = isset($params["status"])?$params["status"]:'';
		$ketua = isset($params['ketua'])?$params['ketua']:'';
        $alamat = isset($params['alamat'])?$params['alamat']:'';
        $email = isset($params['email'])?$params['email']:'';
        $telp = isset($params['telp'])?$params['telp']:'';

        $x1     = isset($params["x1"])?$params["x1"]:'';
        $y1     = isset($params["y1"])?$params["y1"]:'';
        $x2      = isset($params["x2"])?$params["x2"]:'';
        $y2      = isset($params["y2"])?$params["y2"]:'';
        $width_resize      = isset($params["width_resize"])?$params["width_resize"]:'';

        $upload_image = $this->uploadImage($width_resize, $x1, $y1, $x2, $y2);

        if(isset($upload_image["error"])) {
            if($upload_image["error"] == "failed_ext") {
                return "failed_ext";
            }
            if($upload_image["error"] == "empty_file") {
                return "empty_file";
            }
        }

		$data ['nama'] = $this->db->escape_str($name);
		$data ['kode'] = $this->db->escape_str($kode);
		$data ['status'] = $this->db->escape_str($status);
		$data ['ketua'] = $this->db->escape_str($ketua);
		$data ['alamat'] = $this->db->escape_str($alamat);
		$data ['email'] = $this->db->escape_str($email);
		$data ['telp'] = $this->db->escape_str($telp);
		$data ['id_parent'] = 0;
		$data ['files'] = isset($upload_image["files"])?$upload_image["files"]:"";

		$doInsert = $this->db->insert($this->maintablename, $data);
		if($doInsert){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_entry_provinsi')." dengan judul = ".htmlentities($name).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function updateData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$name = isset($params["name"])?$params["name"]:'';
		$kode = isset($params["kode"])?$params["kode"]:'';
		$status = isset($params["status"])?$params["status"]:'';
		$ketua = isset($params['ketua'])?$params['ketua']:'';
        $alamat = isset($params['alamat'])?$params['alamat']:'';
        $email = isset($params['email'])?$params['email']:'';
        $telp = isset($params['telp'])?$params['telp']:'';

        $x1     = isset($params["x1"])?$params["x1"]:'';
        $y1     = isset($params["y1"])?$params["y1"]:'';
        $x2      = isset($params["x2"])?$params["x2"]:'';
        $y2      = isset($params["y2"])?$params["y2"]:'';
        $width_resize      = isset($params["width_resize"])?$params["width_resize"]:'';

        if ($_FILES['files']['size']<>0){
            $getData = $this->listData(array('id' => $id));
            if(count($getData) > 0) {
                $pfile = $this->webconfig['media-path-dpd'].$getData[0]['files'];
                if (file_exists($pfile)){
                    @unlink($pfile);
                }
                $delete_cropresize = $this->del_cropresize($getData[0]['files']);
            }
        }

        $upload_image = $this->uploadImage($width_resize, $x1, $y1, $x2, $y2);

        if(isset($upload_image["error"])) {
            if($upload_image["error"] == "failed_ext") {
                return "failed_ext";
            }
        }

        $files = isset($upload_image["files"])?$upload_image["files"]:"";

		$sql_user = "
					nama = '".$this->db->escape_str($name)."'
					,kode = '".$this->db->escape_str($kode)."'
					,status = '".$this->db->escape_str($status)."'
					,ketua = '".$this->db->escape_str($ketua)."'
					,alamat = '".$this->db->escape_str($alamat)."'
					,email = '".$this->db->escape_str($email)."'
					,telp = '".$this->db->escape_str($telp)."'
					";

		/* jika update gambar baru */
        if ($_FILES['files']['size']<>0) $sql_user .= ", files = '".$this->db->escape_str($files)."'";

		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_provinsi')." id = ".$id.", name = ".htmlentities($name).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteData($id){
		if($id == 0) return 'failed';

		$doDelete = $this->db->query("
		DELETE FROM ".$this->maintablename."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_provinsi')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function listDataKotaDpd($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$status = isset($params["status"])?$params["status"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "-- ORDER BY b.id DESC";

		if($id != '') {
			$conditional .= "AND b.id = '".$id."'";
		}

		if($status != '') {
			$conditional .= "AND b.status = '".$status."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				a.nama as namaprovinsi,
			    b.id AS id,
			    b.nama AS kota,
			    b.kode AS kodekota,
			    b.status AS statuskota
			FROM
			    ".$this->maintablename." a
			        LEFT JOIN
			    ".$this->maintablename." b ON (b.id_parent = a.id)
			WHERE
			    a.id_parent = '0'
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listDataKotaDpdCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY b.id DESC";
		if($id != '') {
			$conditional .= "AND b.id = '".$id."'";
		}

		$q = $this->db->query("
			SELECT
			    count(b.id) as jumlah
			FROM
			    ".$this->maintablename." a
			        LEFT JOIN
			    ".$this->maintablename." b ON (b.id_parent = a.id)
			WHERE
			    a.id_parent = '0'
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function filterDataKotaDpd($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$name = isset($params["name"])?$params["name"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY b.id DESC";

		if($name != '') {
			$conditional .= "AND b.nama LIKE '%".$this->db->escape_str($name)."%'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				a.nama as namaprovinsi,
			    b.id AS id,
			    b.nama AS kota,
			    b.kode AS kodekota,
			    b.status AS statuskota
			FROM
			    ".$this->maintablename." a
			        LEFT JOIN
			    ".$this->maintablename." b ON (b.id_parent = a.id)
			WHERE
			    a.id_parent = '0'
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function filterDataKotaDpdCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$name = isset($params["name"])?$params["name"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY b.id DESC";
		if($name != '') {
			$conditional .= "AND b.nama LIKE '%".$this->db->escape_str($name)."%'";
		}

		$q = $this->db->query("
			SELECT
			    count(b.id) as jumlah
			FROM
			    ".$this->maintablename." a
			        LEFT JOIN
			    ".$this->maintablename." b ON (b.id_parent = a.id)
			WHERE
			    a.id_parent = '0'
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

	private function uploadImage($width_resize, $x1, $y1, $x2, $y2) {
        $this->ci->load->helper('upload');
        if ($_FILES['files']['size']<>0){
            $data = array();

            $tmpName = $_FILES['files']['tmp_name'];
            list($width, $height, $type, $attr) = getimagesize($tmpName);
            # Rasio Ukuran Berita #
            $rasio = $width/$width_resize;
            $expfile = explode('.', $_FILES['files']['name']);
            $extension_thumb = strtoupper(end($expfile));
            if (!(($extension_thumb == 'JPEG') || ($extension_thumb == 'JPG') || ($extension_thumb == 'PNG') || ($extension_thumb == 'GIF'))){
                $data['error'] = "failed_ext";
            }

            # UPLOAD #
            $pathdir = date("Y/m/d");
            $thepath = $this->webconfig['media-path-dpd'].$pathdir."/";
            $thepath_create = $this->webconfig['media-path-dpd'].$pathdir."/temp/";
            $exp = explode("/",$thepath_create);
            $way = '';
            foreach($exp as $n){
                $way .= $n.'/';
                @mkdir($way);
            }

            $pos = strripos($_FILES['files']['name'], '.');
            if($pos === false){
                $ordinary_name = $_FILES['files']['name'];
            }else{
                $ordinary_name = substr($_FILES['files']['name'], 0, $pos);
            }

            $name_upload = date("Y_m_d-H_i_s")."_".md5($ordinary_name);
            $name_upload_c = $name_upload.'.'.strtolower($extension_thumb);

            $result = was_upload_file('files', $name_upload, false, $thepath_create."/", array(), array(), array());

            if ($result['status'] > 0){
                $data['files'] = "";
            }else{
                $path1 = $pathdir."/".$name_upload_c;
                $data['files'] = $path1;
                if(isset($rasio) && $rasio !=''){
                    $finalcrop_resize = $this->croping_resize($result,$x1,$y1,$x2,$y2,$rasio,$thepath);
                }
            }
        }else{
            $data['error'] = "empty_file";
        }

        return $data;
    }
    private function croping_resize($result,$x1,$y1,$x2,$y2,$rasio,$image_path) {

        # RESIZE and CROP #
        $thepath = $this->webconfig['media-path-dpd-thumb'].date("Y/m/d")."/";
        $exp = explode("/",$thepath);
        $way ='';
        foreach($exp as $n){
            $way.=$n.'/';
            @mkdir($way);
        }

        $file_name = $result['name'];
        $src_path = $image_path . 'temp/' . $file_name;

        $exp_file = explode('.', $file_name, 2);

        $des_path =  $image_path .$file_name;
        $xx1 = $x1 * $rasio;
        $yy1 = $y1 * $rasio;
        $xx2 = $x2 * $rasio;
        $yy2 = $y2 * $rasio;
        if($des_path !=''){
            $this->image_moo
                ->load($src_path)
                ->crop($xx1,$yy1,$xx2,$yy2)
                ->save($des_path);
        }

        ## RESIZE IMAGE ##
        foreach($this->webconfig['image_dpd'] as $key => $value){
            $size_file = explode('x',$value);
            $file_final_name_resize = $exp_file[0].'_'.$value.'_thumb.'.$exp_file[1];
            $des_path_resize =  $thepath . $file_final_name_resize;
            $this->image_moo
                ->load($des_path)
                ->stretch($size_file[0],$size_file[1])
                ->save($des_path_resize);
        }

        /* Delete Temp File */
        if (file_exists($src_path)){
            @unlink($src_path);
        }
    }
    private function del_cropresize($cfile) {
        if($cfile == "") return false;

        $explod_file = explode("/",$cfile);
        $filename = end($explod_file);
        $exp_file = explode(".",$filename);
        $exp_thumb_path = explode("/", $this->webconfig['media-path-dpd-thumb'].$cfile, -1);
        $thumb_path = implode("/", $exp_thumb_path);

        foreach($this->webconfig['image_dpd'] as $key => $value){
            $delete_resize = $thumb_path.'/'.$exp_file[0]."_".$value."_thumb.".$exp_file[1];
            if (file_exists($delete_resize)){
                @unlink($delete_resize);
            }
        }
    }
}
