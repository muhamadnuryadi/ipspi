<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Member_accessModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "members";
		$this->table_member_access = "members_access";
	}

	public function entriData($params=array()){
		$nama = isset($params['nama'])?$params['nama']:'';
        $username = isset($params['username'])?$params['username']:'';
        $email = isset($params['email'])?$params['email']:'';
        $password = isset($params['password'])?$params['password']:'';
        $password_1 = isset($params['password_1'])?$params['password_1']:'';
        $tgllahir = isset($params['tgllahir'])?$params['tgllahir']:'';
        $jumanak = isset($params['jumanak'])?$params['jumanak']:'';
        $alamatkantor = isset($params['alamatkantor'])?$params['alamatkantor']:'';
        $telpkantor = isset($params['telpkantor'])?$params['telpkantor']:'';
        $faxkantor = isset($params['faxkantor'])?$params['faxkantor']:'';
        $alamatrumah = isset($params['alamatrumah'])?$params['alamatrumah']:'';
        $id_propinsi = isset($params['id_propinsi'])?$params['id_propinsi']:'';
        $id_kota = isset($params['id_kota'])?$params['id_kota']:'';
        $telp = isset($params['telp'])?$params['telp']:'';
        $hp = isset($params['hp'])?$params['hp']:'';
        $status = isset($params['status'])?$params['status']:'';
        $created_at = isset($params['created_at'])?$params['created_at']:'';

		$upload_image_ijazah = $this->uploadImageIjazah();
		$upload_image_ktp = $this->uploadImageKtp();
		$upload_image_foto = $this->uploadImageFoto();

		if(isset($upload_image_ijazah["error"])) {
            if($upload_image_ijazah["error"] == "failed_ext") {
                return "failed_ext";
            }
            if($upload_image_ijazah["error"] == "empty_file") {
                return "empty_file";
            }
        }

        if(isset($upload_image_ktp["error"])) {
            if($upload_image_ktp["error"] == "failed_ext") {
                return "failed_ext";
            }
            if($upload_image_ktp["error"] == "empty_file") {
                return "empty_file";
            }
        }

		$data['nama'] = $this->db->escape_str($nama);
		$data['username'] = $this->db->escape_str($username);
        $data['email'] = $this->db->escape_str($email);
        $data['password'] = md5($password);
        $data['tgllahir'] = date("Y-m-d", strtotime($tgllahir));
        $data['jumanak'] = $this->db->escape_str($jumanak);
        $data['alamatkantor'] = $this->db->escape_str($alamatkantor);
        $data['telpkantor'] = $this->db->escape_str($telpkantor);
        $data['faxkantor'] = $this->db->escape_str($faxkantor);
        $data['alamatrumah'] = $this->db->escape_str($alamatrumah);
        $data['id_propinsi'] = $this->db->escape_str($id_propinsi);
        $data['id_kota'] = $this->db->escape_str($id_kota);
        $data['telp'] = $this->db->escape_str($telp);
        $data['hp'] = $this->db->escape_str($hp);
        $data['status'] = $this->db->escape_str($status);
        $data['created_at'] = date("Y-m-d", strtotime($created_at));
        $data['path_ijazah'] = isset($upload_image_ijazah["path_ijazah"])?$upload_image_ijazah["path_ijazah"]:"";
        $data['path_ktp'] = isset($upload_image_ktp["path_ktp"])?$upload_image_ktp["path_ktp"]:"";
        $data['foto'] = isset($upload_image_ktp["foto"])?$upload_image_ktp["foto"]:"";
		
		$doInsert = $this->db->insert($this->maintablename, $data);
		if($doInsert){
			$this->session->set_flashdata('id_insert', $this->db->insert_id());
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_entry_members')." dengan nama = ".htmlentities($nama).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function updateData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$fullname = isset($params["fullname"])?$params["fullname"]:'';
		$birthday = isset($params["birthday"])?$params["birthday"]:'';
		$address = isset($params["address"])?$params["address"]:'';
		$postcode = isset($params["postcode"])?$params["postcode"]:'';
		$gender = isset($params["gender"])?$params["gender"]:'';
		$phone = isset($params["phone"])?$params["phone"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$password = isset($params["password"])?$params["password"]:'';
		$status = isset($params["status"])?$params["status"]:'';
		$username = isset($params["username"])?$params["username"]:'';
		$instagram = isset($params["instagram"])?$params["instagram"]:'';
		$tipe_user = isset($params["tipe_user"])?$params["tipe_user"]:'';
		$discount = isset($params["discount"])?$params["discount"]:'';
		$created_at = isset($params["created_at"])?$params["created_at"]:'';

		$exist_username = $this->__CheckUserExistEdit($id,$username);
		
		if($exist_username > 0){return 'exist_username';}

		$a1     = isset($params["a1"])?$params["a1"]:'';
        $b1     = isset($params["b1"])?$params["b1"]:'';
        $a2      = isset($params["a2"])?$params["a2"]:'';
        $b2      = isset($params["b2"])?$params["b2"]:'';
        $width_resize      = isset($params["width_resize"])?$params["width_resize"]:'';

        if ($_FILES['pathprofile']['size']<>0){
            $getData = $this->listData(array('id' => $id));     
            if(count($getData) > 0) {
                $pfile = $this->webconfig['media-path-profile'].$getData[0]['pathprofile'];
                if (file_exists($pfile)){
                    @unlink($pfile);
                }
                $delete_cropresize = $this->del_cropresize_profile($getData[0]['pathprofile']);
            }
        }

        $upload_image_profile = $this->uploadImageProfile($width_resize, $a1, $b1, $a2, $b2);

        if(isset($upload_image_profile["error"])) {
            if($upload_image_profile["error"] == "failed_ext") {
                return "failed_ext";
            }
        }

        $pathktp = isset($upload_image_profile["pathprofile"])?$upload_image_profile["pathprofile"]:"";

		if($password != ''){
			if(strlen($password) < 5){
				return 'min_5';
			}else{
				$sql_user = "
							fullname = '".$this->db->escape_str($fullname)."'
							,birthday = '".date("Y-m-d", strtotime($birthday))."'
							,address = '".$this->db->escape_str($address)."'
							,postcode = '".$this->db->escape_str($postcode)."'
							,gender = '".$this->db->escape_str($gender)."'
							,phone = '".$this->db->escape_str($phone)."'
							,email = '".$this->db->escape_str($email)."'
							,status = '".$this->db->escape_str($status)."'
							,password = '".$this->db->escape_str(md5($password))."'
							,username = '".$this->db->escape_str($username)."'
							,instagram = '".$this->db->escape_str($instagram)."'
							,tipe_user = '".$this->db->escape_str($tipe_user)."'
							,discount = '".$this->db->escape_str($discount)."'
							,created_at = '".date("Y-m-d", strtotime($created_at))."' ";
			}

		}else{
			$sql_user = "
						fullname = '".$this->db->escape_str($fullname)."'
						,birthday = '".date("Y-m-d", strtotime($birthday))."'
						,address = '".$this->db->escape_str($address)."'
						,postcode = '".$this->db->escape_str($postcode)."'
						,gender = '".$this->db->escape_str($gender)."'
						,phone = '".$this->db->escape_str($phone)."'
						,email = '".$this->db->escape_str($email)."'
						,status = '".$this->db->escape_str($status)."'
						,username = '".$this->db->escape_str($username)."'
						,instagram = '".$this->db->escape_str($instagram)."'
						,tipe_user = '".$this->db->escape_str($tipe_user)."'
						,discount = '".$this->db->escape_str($discount)."'
						,created_at = '".$this->db->escape_str($created_at)."'";
		}
		
		if ($_FILES['pathprofile']['size']<>0) $sql_user .= ", pathprofile = '".$this->db->escape_str($pathktp)."'";

		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.", name = ".htmlentities($fullname).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteData($id){
		if($id == 0) return 'failed';

		# DELETE MEMBER PROJECT # 
		$this->db->where('id',$id);
		$queproject = $this->db->get($this->maintablename);
		$resultproject = $queproject->result_array();
		if(isset($resultproject) && count($resultproject) > 0){
			foreach ($resultproject as $key => $value) {
				
				// @unlink($this->webconfig['media-path-profile'].$value['logo_img']);
				@unlink($this->webconfig['media-path-files'].$value['path_ktp']);
				@unlink($this->webconfig['media-path-files'].$value['path_ijazah']);
				@unlink($this->webconfig['media-path-files'].$value['foto']);
				// $exp_img = explode(".", $value['logo_img']);
				// foreach ($this->webconfig['image_profile'] as $key => $image_data) {
				// 	@unlink($this->webconfig['media-path-profile-thumb'].$exp_img[0]."_".$image_data."_thumb.".$exp_img[1]);
				// }
			}
		}

		$doDelete = $this->db->query("
		DELETE FROM ".$this->maintablename."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function filterData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$id_propinsi = isset($params["id_propinsi"])?$params["id_propinsi"]:'';
		$id_kota = isset($params["id_kota"])?$params["id_kota"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$tingkat = isset($params["tingkat"])?$params["tingkat"]:'';
		$id_pekerjaan = isset($params["id_pekerjaan"])?$params["id_pekerjaan"]:'';
		$id_instansi = isset($params["id_instansi"])?$params["id_instansi"]:'';
		$id_sertifikasi = isset($params["id_sertifikasi"])?$params["id_sertifikasi"]:'';
		$no_anggota = isset($params["no_anggota"])?$params["no_anggota"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$rest  = " ORDER BY m.id DESC";
		$conditional = '';
		if($name != ''){
			$conditional .= "AND m.nama LIKE '%".$this->db->escape_str($name)."%'";
		}
		if($id_propinsi != ''){
			$conditional .= "AND m.id_propinsi = '".$this->db->escape_str($id_propinsi)."'";
		}
		if($id_kota != ''){
			$conditional .= "AND m.id_kota = '".$this->db->escape_str($id_kota)."'";
		}
		
		if($no_anggota != ''){
			$conditional .= " AND m.no_anggota LIKE '%".$this->db->escape_str($no_anggota)."%'";
		}

		if($tingkat != ''){
			$conditional .= " AND (SELECT tingkat FROM members_pendidikan a WHERE a.id_members = m.id ORDER BY a.id DESC LIMIT 1  ) = '".$this->db->escape_str($tingkat)."'";
		}

		if ($id_pekerjaan != '') {
			$conditional .= " AND (SELECT id_pekerjaan FROM members_pekerjaan b WHERE b.id_members = m.id ORDER BY b.id DESC LIMIT 1  ) = '".$this->db->escape_str($id_pekerjaan)."' ";
		}

		if ($id_instansi != '') {
			$conditional .= " AND  (SELECT id_instansi FROM members_pekerjaan b WHERE b.id_members = m.id ORDER BY b.id DESC LIMIT 1  ) = '".$this->db->escape_str($id_instansi)."' ";
		}

		if ($id_sertifikasi != '') {
			$conditional .= " AND  (SELECT tipe FROM members_sertifikasi c WHERE c.id_members = m.id ORDER BY c.id DESC LIMIT 1  ) = '".$this->db->escape_str($id_sertifikasi)."' ";
		}

		if($this->session->userdata('role') == 2){
			$conditional .= "AND m.id_propinsi = '".$this->session->userdata('userpropinsi')."' ";
		}else if($this->session->userdata('role') == 3){
			$conditional .= "AND m.email = '".$this->session->userdata('email')."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$select = "
		SELECT 
			m.id 
			,m.nama
			,m.id_propinsi
			,m.id_kota
			,m.no_anggota
			,m.status
			,(SELECT tingkat FROM members_pendidikan a WHERE a.id_members = m.id ORDER BY a.id DESC LIMIT 1  ) as id_tingkat
			,(SELECT id_pekerjaan FROM members_pekerjaan b WHERE b.id_members = m.id ORDER BY b.id DESC LIMIT 1  ) as id_pekerjaan
			,(SELECT id_instansi FROM members_pekerjaan b WHERE b.id_members = m.id ORDER BY b.id DESC LIMIT 1  ) as id_instansi
			,(SELECT tipe FROM members_sertifikasi c WHERE c.id_members = m.id ORDER BY c.id DESC LIMIT 1  ) as tipe_sertifikasi
		FROM members m 
		WHERE (1=1)
		".$conditional."
		".$rest."
		".$offsetData."
		";

		// $q = $this->db->query("
		// 	SELECT
		// 		*
		// 	FROM
		// 		".$this->maintablename."
		// 	WHERE (1=1)
		// 	".$conditional."
		// 	".$rest."
		// 	".$offsetData."
		// ");

		$q = $this->db->query($select);

		$result = $q->result_array();
		$result = $this->getProvinsi($result);
		$result = $this->getKota($result);
		return $result;
	}

	public function filterDataCount($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$id_propinsi = isset($params["id_propinsi"])?$params["id_propinsi"]:'';
		$id_kota = isset($params["id_kota"])?$params["id_kota"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$tingkat = isset($params["tingkat"])?$params["tingkat"]:'';
		$id_pekerjaan = isset($params["id_pekerjaan"])?$params["id_pekerjaan"]:'';
		$id_instansi = isset($params["id_instansi"])?$params["id_instansi"]:'';
		$id_sertifikasi = isset($params["id_sertifikasi"])?$params["id_sertifikasi"]:'';
		$no_anggota = isset($params["no_anggota"])?$params["no_anggota"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';

		$rest  = "ORDER BY m.id DESC";
		$conditional = '';
		if($name != ''){
			$conditional .= "AND m.nama LIKE '%".$this->db->escape_str($name)."%'";
		}
		if($id_propinsi != ''){
			$conditional .= "AND m.id_propinsi = '".$this->db->escape_str($id_propinsi)."'";
		}
		if($id_kota != ''){
			$conditional .= "AND m.id_kota = '".$this->db->escape_str($id_kota)."'";
		}

		if($no_anggota != ''){
			$conditional .= " AND m.no_anggota LIKE '%".$this->db->escape_str($no_anggota)."%' ";
		}
		
		if($tingkat != ''){
			$conditional .= " AND (SELECT tingkat FROM members_pendidikan a WHERE a.id_members = m.id ORDER BY a.id DESC LIMIT 1  ) = '".$this->db->escape_str($tingkat)."'";
		}

		if ($id_pekerjaan != '') {
			$conditional .= " AND (SELECT id_pekerjaan FROM members_pekerjaan b WHERE b.id_members = m.id ORDER BY b.id DESC LIMIT 1  ) = '".$this->db->escape_str($id_pekerjaan)."' ";
		}

		if ($id_instansi != '') {
			$conditional .= " AND  (SELECT id_instansi FROM members_pekerjaan b WHERE b.id_members = m.id ORDER BY b.id DESC LIMIT 1  ) = '".$this->db->escape_str($id_instansi)."' ";
		}

		if ($id_sertifikasi != '') {
			$conditional .= " AND  (SELECT tipe FROM members_sertifikasi c WHERE c.id_members = m.id ORDER BY c.id DESC LIMIT 1  ) = '".$this->db->escape_str($id_sertifikasi)."' ";
		}

		if($this->session->userdata('role') == 2){
			$conditional .= "AND m.id_propinsi = '".$this->session->userdata('userpropinsi')."' ";
		}else if($this->session->userdata('role') == 3){
			$conditional .= "AND m.email = '".$this->session->userdata('email')."'";
		}



		$select = "
			SELECT 
				count(id) as jumlah
			FROM members m 
			WHERE (1=1)
			".$conditional."
			".$rest."
			
			";

		// $q = $this->db->query("
		// 	SELECT
		// 		count(id) as jumlah
		// 	FROM
		// 		".$this->maintablename."
		// 	WHERE (1=1)
		// 	".$conditional."
		// 	".$rest."
		// ");

		$q = $this->db->query($select);

		$result = $q->first_row('array');
		
		// die();
		return $result;
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT 
				m.id
				,m.email
				,m.nama
				,ma.date_start
				,ma.date_expired
				,ma.`status`
			FROM ".$this->maintablename." m
			INNER JOIN (SELECT status,date_expired,date_start,id_member FROM ".$this->table_member_access." ORDER BY id DESC} ) ma
			WHERE m.id = ma.id_member AND ma.status = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();

		return $result;
	}

	public function listDataCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		
		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		$q = $this->db->query("
			SELECT 
				count(m.id) as jumlah
			FROM ".$this->maintablename." m
			INNER JOIN (SELECT status,date_expired,date_start,id_member FROM ".$this->table_member_access." ORDER BY id DESC} ) ma
			WHERE m.id = ma.id_member AND ma.status = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");

		$result = $q->first_row('array');
		return $result;
	}

    public function insertPayment($params = array()){
    	$id_member = isset($params['id_member'])?$params['id_member']:'';
    	$price = isset($params['price'])?$params['price']:'';
    	$charge_id = isset($params['charge_id'])?$params['charge_id']:'';
        
        $data['id_member'] = $this->db->escape_str($id_member);
		$data['price'] = $this->db->escape_str($price);
		$data['total_price'] = $this->db->escape_str($price);
        $data['charge_id'] = $this->db->escape_str($charge_id);
        $data['date_created'] = date("Y-m-d H:i:s");
        $data['status'] = 0;
        
		$doInsert = $this->db->insert($this->table_payments, $data);
		if ($doInsert) {
			return true;
		}else{
			return false;
		}
    }

    public function generate(){
    	$query = $this->db->query(
    		"
    		SELECT id,no_anggota,created_at
    		FROM members
    		wHERE no_anggota != ''
    		"
    		);
    	$result = $query->result_array();
    	if ($result > 0) {
    		foreach ($result as $key => $value) {
    			if ($value['no_anggota'] != '' && $value['created_at'] != '') {
    				$data['id_member'] = $value['id'];
    				$data['status'] = 1;
    				$data['date_start'] = $value['created_at'];
    				// $data['date_expired'] = date('Y-m-d', strtotime(date('Y-m-d',$value['created_at']) '+'.$this->webconfig['masa_berlaku'].' years'));
    				$data['date_expired'] = date('Y-m-d',strtotime("+".$this->webconfig['masa_berlaku']." years",strtotime($value['created_at'])));
    				
    				// $expired = date('Y-m-d', strtotime('+'.$this->webconfig['masa_berlaku'].' years'));
    				$this->db->insert('members_access',$data);
    			}
    		}
    	}
    }
}