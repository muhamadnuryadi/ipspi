<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class ImportmemberModel extends CI_Model{
	var $ci;
	function __construct() {	 
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->load->sharedModel('WilayahModel');
		$this->maintablename = "members";
		$this->table_members_pendidikan = "members_pendidikan";
		$this->table_members_pekerjaan = "members_pekerjaan";
		$this->table_members_praktek = "members_praktek";
		$this->table_members_penghargaan = "members_penghargaan";
		$this->table_members_sertifikasi = "members_sertifikasi";
		$this->table_members_referensi = "members_referensi";
		$this->table_members_profesi = "members_profesi";
		$this->table_members_komunitas = "members_komunitas";
		$this->table_payments = "payments";
		$this->table_wilayah = "wilayah";
		$this->table_tingkat = "tingkat";
		$this->table_instansi = "instansi";
		$this->table_pekerjaan = "pekerjaan";
		$this->table_member_access = "members_access";
	}
	public function uploadData($params=array()){
		$webconfig = $this->config->item('webconfig');

		if ($_FILES['path']['size']<>0){
				$expfile = explode('.', $_FILES['path']['name']);
            	$extension_thumb = strtoupper(end($expfile));
				if (!( ($extension_thumb == 'XLS') ))
				{
					return "failed_ext";
				}
				
				# UPLOAD #
				$thepath = $webconfig['media-path-upload']."/";

				$files = glob($webconfig['media-path-upload'].'{,.}*', GLOB_BRACE);
				
				foreach($files as $file){ // iterate files
				  if(is_file($file))
				    @unlink($file); // delete file
				}		
					
				$exp=explode("/",$thepath);
				$way='';
				foreach($exp as $n){
					$way.=$n.'/';
					@mkdir($way);		
				}
				
				## FILENAME ##
				$expfiles = explode('.', $_FILES['path']['name']);
            	$extension = strtoupper(end($expfiles));
					$pos = strripos($_FILES['path']['name'], '.');
					if($pos === false){
						$ordinary_name = $_FILES['path']['name'];
					}else{
						$ordinary_name = substr($_FILES['path']['name'], 0, $pos);
					}		
				$name_upload = $ordinary_name;
				$replace =  array( ",", "-", "'", ":", ";", "{", "}", "[", "]", "|"," ");
				$new_name = str_replace($replace, "_", $name_upload);
				$name_upload_save = date("Y-m-d-H-i-s")."_".$new_name;
				$name_upload_c = $name_upload_save.'.xls';
				
				$result = was_upload_file('path', $name_upload_save, false, $thepath."/", array(), array(), array());
				
				if ($result['status'] > 0){
					return "failed_upload";
				}else{
					ini_set('memory_limit', '1024M');
					ini_set('max_execution_time', 3000);
					$this->excel_reader->setOutputEncoding('CP1251');
					$this->excel_reader->read($webconfig['media-path-upload'].$name_upload_c);
					error_reporting(E_ALL ^ E_NOTICE);
					$data = $this->excel_reader->sheets[0];
					
					$dataexcel = Array();
					for ($i = 1; $i <= count($data['cells']); $i++) {
						if ($i > 1) {
							$memberexist = $this->listMemberExist(array('email'=> $data['cells'][$i][13]));

							if($memberexist == 0){
								$xdata['ktp'] = $data['cells'][$i][1];
		                    
			                    $tgllahir = explode('/', $data['cells'][$i][2]);
			                    $xdata['tgllahir'] = $tgllahir[2].'-'.$tgllahir[1].'-'.($tgllahir[0] - 1);
			                    
			                    if(stripos($data['cells'][$i][3], 'Sudah Menikah') !== FALSE){
			                    	$xdata['statuskawin'] = '1';
			                    }else if(stripos($data['cells'][$i][3], 'Duda') !== FALSE){
			                    	$xdata['statuskawin'] = '2';
			                    }else if(stripos($data['cells'][$i][3], 'Janda') !== FALSE){
			                    	$xdata['statuskawin'] = '2';
			                    }else{
			                    	$xdata['statuskawin'] = '0';
			                    }

			                    $xdata['jumanak'] = isset($data['cells'][$i][4])?$data['cells'][$i][4]:'0';
			                    $xdata['alamatkantor'] = $data['cells'][$i][5];
			                    $xdata['telpkantor'] = $data['cells'][$i][6];
			                    $xdata['faxkantor'] = $data['cells'][$i][7];
			                    $xdata['alamatrumah'] = $data['cells'][$i][8];

			                    
			                    $idpropinsi = $this->listPropinsi(array('propinsi' => trim($data['cells'][$i][10])));
			                    $xdata['id_propinsi'] = $idpropinsi[0]['id'];
			                    
			                    $idkota = $this->listKota(array(
			                    								'id_parent' => $idpropinsi[0]['id']
			                    								,'kota' => trim($data['cells'][$i][9])
			                									));

			                    $xdata['id_kota'] = $idkota[0]['id'];



			                    $xdata['telp'] = $data['cells'][$i][11];
			                    $xdata['hp'] = $data['cells'][$i][12];
			                    $xdata['email'] = $data['cells'][$i][13];

			                    $created = explode('/', $data['cells'][$i][14]);
			                    $xdata['created_at'] = $created[2].'-'.$created[1].'-'.($created[0] - 1);

			                    $xdata['username'] = $data['cells'][$i][15];
			                    $xdata['password'] = md5('12345678');
			                    $xdata['nama'] = $data['cells'][$i][16];
			                    

			                    if(stripos($data['cells'][$i][17], 'laki-laki') !== FALSE){
			                    	$xdata['jk'] = '1';
			                    }else if(stripos($data['cells'][$i][17], 'Perempuan') !== FALSE){
			                    	$xdata['jk'] = '2';
			                    }

			                    if(stripos($data['cells'][$i][18], 'ya') !== FALSE){
			                    	$xdata['status'] = '1';
			                    }
			                    // $xdata['status'] = $data['cells'][$i][18];

			                    $doInsert = $this->db->insert($this->maintablename, $xdata);
								if($doInsert){
									$id_members = $this->db->insert_id();
									if(stripos($data['cells'][$i][18], 'ya') !== FALSE){
				                    	$this->insertMemberAccess(array(
																		'id_members' => $id_members
																		,'data' => $data['cells'][$i]
																	));
				                    	$this->updateMember(array(
																	'id_members' => $id_members
																	,'data' => $data['cells'][$i]
																));
				                    }

									$this->insertPendidikan(array(
																	'id_members' => $id_members
																	,'data' => $data['cells'][$i]
																));
									$this->insertPekerjaan(array(
																	'id_members' => $id_members
																	,'data' => $data['cells'][$i]
																));
									// $this->insertPraktek(array(
									// 								'id_members' => $id_members
									// 								,'data' => $data['cells'][$i]
									// 							));
									// $this->insertSertifikasi(array(
									// 								'id_members' => $id_members
									// 								,'data' => $data['cells'][$i]
									// 							));
									// $this->insertProfesi(array(
									// 								'id_members' => $id_members
									// 								,'data' => $data['cells'][$i]
									// 							));
									// $this->insertPenghargaan(array(
									// 								'id_members' => $id_members
									// 								,'data' => $data['cells'][$i]
									// 							));
									// $this->insertReferensi(array(
									// 								'id_members' => $id_members
									// 								,'data' => $data['cells'][$i]
									// 							));
									// $this->insertKomunitas(array(
									// 								'id_members' => $id_members
									// 								,'data' => $data['cells'][$i]
									// 							));
								}
							} 

		                    // if($data['cells'][$i][1] == '') break;

		                    
		                    
		                }
					}
					
				}	
		}else{
			return "empty_file";
		}
		return 'success';
	}

	public function generateAnggota($params=array()){
        $id_members = isset($params["id_members"])?$params["id_members"]:'';
        $member = $this->listData(array('id'=>$id_members));

        $tdata = array();
        $id_member = $member[0]['id'];
        $id_provinsi = isset($member[0]['id_propinsi'])?$member[0]['id_propinsi']:'';
        $id_kota = isset($member[0]['id_kota'])?$member[0]['id_kota']:'';
        $no_anggota_from_db = $member[0]['no_anggota'];
        $created_at = isset($member[0]['created_at'])?$member[0]['created_at']:'';
        
        // //get date join
        if($created_at != ''){
            $month = date('m',strtotime($created_at));
            $year = substr(date('Y',strtotime($created_at)), 2) ;
        }else{
            if($fotomember != ''){
                $expfoto = explode('/', $fotomember);
                $month = $expfoto[1];
                $year = substr($expfoto[0], 2);
            } else if ($pathijazahmember != '') {
                $expfoto = explode('/', $pathijazahmember);
                $month = $expfoto[1];
                $year = substr($expfoto[0], 2);
            } else if ($pathktpmember != ''){
                $expfoto = explode('/', $pathktpmember);
                $month = $expfoto[1];
                $year = substr($expfoto[0], 2);
            } else {
                $month = date('m');
                $year = date('Y');
            }
        }

        //auto no anggota
        $allmember = $this->getLastNoAnggota();
        $newnumb = max(array_column($allmember, 'lastnumb'));
        $newno_anggota = $newnumb + 1;

        //untuk get kode wilayah
        $getKodeProvinsi = $this->WilayahModel->listData(array('id'=>$id_provinsi));
        $getKodeKota = $this->WilayahModel->listData(array('id'=>$id_kota));

        //untuk get pendidikan sosial dan non sosial/jenis anggota
        $pendidikan = $this->listDataPendidikan(array('id'=>$id_member));
        $pendidikanterakhir = end($pendidikan);
        $jenis_pendidikan = $pendidikanterakhir['jenis'];
        
        if ($jenis_pendidikan == 2) {//jika sosial
            $jenis_anggota = 1;
        }else{
            $jenis_anggota = 2;
        }

        $kodeprovinsi = isset($getKodeProvinsi[0]['kode'])?$getKodeProvinsi[0]['kode']:'';
        $kodekota = isset($getKodeKota[0]['kode'])?$getKodeKota[0]['kode']:'';

        $no_anggota_generate = $kodeprovinsi.'.'.$kodekota.'.'.$month.$year.'.'.$jenis_anggota.'.'.$newno_anggota;
        
        if($no_anggota_from_db == ''){
            return $no_anggota_generate;
        }else{
            return $no_anggota_from_db;
        }
    }

	public function updateMember($params=array()){
		$id_members = isset($params["id_members"])?$params["id_members"]:'';
		$data = isset($params["data"])?$params["data"]:'';
		
		$no_anggota = $this->generateAnggota(array('id_members' => $id_members));

		$udata = array(
		        'no_anggota' => $no_anggota
		);

		$this->db->where('id', $id_members);
		$this->db->update($this->maintablename, $udata);
	}

	public function insertMemberAccess($params=array()){
		$id_members = isset($params["id_members"])?$params["id_members"]:'';
		$data = isset($params["data"])?$params["data"]:'';

		$created = explode('/', $data[14]);
		$memberaccess['date_start'] = $created[2].'-'.$created[1].'-'.($created[0] - 1);

		$memberaccess['date_expired'] = date('Y-m-d',strtotime("+".$this->webconfig['masa_berlaku']." years",strtotime($memberaccess['date_start'])));

		$memberaccess['status'] = '1';
		$memberaccess['id_member'] = $id_members;
		$this->db->insert($this->table_member_access, $memberaccess);
	}

	public function insertPendidikan($params=array()){
		$id_members = isset($params["id_members"])?$params["id_members"]:'';
		$data = isset($params["data"])?$params["data"]:'';

		$tingkatdata = $this->listTingkat(array('tingkat' => $data[19]));

    	$pendidikan['tingkat'] = isset($tingkatdata[0]['id'])?$tingkatdata[0]['id']:'0';
		$pendidikan['pt'] = isset($data[20])?$data[20]:'-';
		$pendidikan['jurusan'] = isset($data[21])?$data[21]:'-';
		$pendidikan['gelar'] = isset($data[22])?$data[22]:'-';
		$pendidikan['thn_lulus'] = isset($data[23])?$data[23]:'-';
		$pendidikan['id_members'] = $id_members;
		if(stripos($data[24], 'rekan') !== FALSE){
			$pendidikan['jenis'] = '2';
		}else{
			$pendidikan['jenis'] = '1';
		}
    	$this->db->insert($this->table_members_pendidikan, $pendidikan);
	}

	public function insertPekerjaan($params=array()){
		$id_members = isset($params["id_members"])?$params["id_members"]:'';
		$data = isset($params["data"])?$params["data"]:'';

    	$pekerjaan['nama'] = isset($data[25])?$data[25]:'-';
		$pekerjaan['jabatan'] = isset($data[26])?$data[26]:'-';
		$pekerjaan['mulai'] = isset($data[27])?$data[20]:'-';
		$pekerjaan['akhir'] = isset($data[28])?$data[20]:'-';

		$pekerjaandata = $this->listPekerjaan(array('pekerjaan' => $data[29]));
		$pekerjaan['id_pekerjaan'] = isset($pekerjaandata[0]['id'])?$pekerjaandata[0]['id']:'0';

		$instansidata = $this->listInstansi(array('instansi' => $data[30]));
		$pekerjaan['id_instansi'] = isset($instansidata[0]['id'])?$instansidata[0]['id']:'0';

		$pekerjaan['id_members'] = $id_members;
		$this->db->insert($this->table_members_pekerjaan, $pekerjaan);
	}

	public function insertPraktek($params=array()){
		$id_members = isset($params["id_members"])?$params["id_members"]:'';
		$data = isset($params["data"])?$params["data"]:'';

		$settingdata = $this->listBidangSetting(array('setting' => $data[31]));
		$praktek['setting'] = $settingdata;

		$praktek['tahun'] = $data[32];
		$praktek['jabatan'] = $data[33];

		$peran_utamadata = $this->listPeranUtama(array('peran_utama' => $data[34]));
		$praktek['peran'] = $peran_utamadata;

		$jenis_keahliandata = $this->listJenisKeahlian(array('jenis_keahlian' => $data[35]));
		$praktek['keahlian'] = $jenis_keahliandata;

		$praktek['id_members'] = $id_members;
		$this->db->insert($this->table_members_praktek, $praktek);
	}

	public function insertSertifikasi($params=array()){
		$id_members = isset($params["id_members"])?$params["id_members"]:'';
		$data = isset($params["data"])?$params["data"]:'';

		$sertifikasi['nama'] = $data[36];
		$sertifikasi['lembaga'] = $data[37];
		$sertifikasi['tahun'] = $data[38];

		$sertifikasi_tipedata = $this->listTipeSertifikasi(array('sertifikasi_tipe' => $data[39]));
		$sertifikasi['tipe'] = $sertifikasi_tipedata;

		$sertifikasi['id_members'] = $id_members;
		$this->db->insert($this->table_members_sertifikasi, $sertifikasi);
	}

	public function insertProfesi($params=array()){
		$id_members = isset($params["id_members"])?$params["id_members"]:'';
		$data = isset($params["data"])?$params["data"]:'';

		$profesi['nama'] = $data[40];
		$profesi['tahun'] = $data[41];
		$profesi['keterangan'] = $data[42];

		$profesi['id_members'] = $id_members;
		$this->db->insert($this->table_members_profesi, $profesi);
	}

	public function insertPenghargaan($params=array()){
		$id_members = isset($params["id_members"])?$params["id_members"]:'';
		$data = isset($params["data"])?$params["data"]:'';

		$penghargaan['nama'] = $data[43];
		$penghargaan['instansi'] = $data[44];
		$penghargaan['tahun'] = $data[45];

		$penghargaan['id_members'] = $id_members;
		$this->db->insert($this->table_members_penghargaan, $penghargaan);
	}

	public function insertReferensi($params=array()){
		$id_members = isset($params["id_members"])?$params["id_members"]:'';
		$data = isset($params["data"])?$params["data"]:'';

		$referensi['nama'] = $data[46];
		$referensi['jabatan'] = $data[47];
		$referensi['telp'] = $data[48];

		$referensi['id_members'] = $id_members;
		$this->db->insert($this->table_members_referensi, $referensi);
	}

	public function insertKomunitas($params=array()){
		$id_members = isset($params["id_members"])?$params["id_members"]:'';
		$data = isset($params["data"])?$params["data"]:'';

		$komunitas['nama'] = $data[49];
		$komunitas['alamat'] = $data[50];

		$komunitas['id_members'] = $id_members;
		$this->db->insert($this->table_members_komunitas, $komunitas);
	}

	public function listBidangSetting($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$setting = isset($params["setting"])?$params["setting"]:'';

		$result = '0';
		foreach ($this->webconfig['bidang_setting'] as $key => $value) {
			if(stripos($setting, $value) !== FALSE){
	        	$result = $key;
	        }
		}
		return $result;
	}

	public function listPeranUtama($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$peran_utama = isset($params["peran_utama"])?$params["peran_utama"]:'';

		$result = '0';
		foreach ($this->webconfig['peran_utama'] as $key => $value) {
			if(stripos($peran_utama, $value) !== FALSE){
	        	$result = $key;
	        }
		}
		return $result;
	}

	public function listJenisKeahlian($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$jenis_keahlian = isset($params["jenis_keahlian"])?$params["jenis_keahlian"]:'';

		$result = '0';
		foreach ($this->webconfig['jenis_keahlian'] as $key => $value) {
			if(stripos($jenis_keahlian, $value) !== FALSE){
	        	$result = $key;
	        }
		}
		return $result;
	}

	public function listTipeSertifikasi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$sertifikasi_tipe = isset($params["sertifikasi_tipe"])?$params["sertifikasi_tipe"]:'';

		$result = '0';
		foreach ($this->webconfig['sertifikasi_tipe'] as $key => $value) {
			if(stripos($sertifikasi_tipe, $value) !== FALSE){
	        	$result = $key;
	        }
		}
		return $result;
	}

	public function listMemberExist($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$conditional = "";
		
		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($email != '') {
			$conditional .= "AND email = '".$email."'";
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE 1 = 1
			".$conditional."
		");
		$result = $q->result_array();
		if($email == ''){
			return '0';
		}else{
			return count($result);
		}
	}

	public function listTingkat($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$tingkat = isset($params["tingkat"])?$params["tingkat"]:'';
		$conditional = "";
		
		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($tingkat != '') {
			$conditional .= "AND name LIKE '".$tingkat."'";
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_tingkat."
			WHERE 1 = 1
			".$conditional."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listInstansi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$instansi = isset($params["instansi"])?$params["instansi"]:'';
		$conditional = "";
		
		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($instansi != '') {
			$conditional .= "AND name LIKE '".$instansi."'";
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_instansi."
			WHERE 1 = 1
			".$conditional."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listPekerjaan($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$pekerjaan = isset($params["pekerjaan"])?$params["pekerjaan"]:'';
		$conditional = "";
		
		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($pekerjaan != '') {
			$conditional .= "AND name LIKE '".$pekerjaan."'";
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_pekerjaan."
			WHERE 1 = 1
			".$conditional."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listPropinsi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$propinsi = isset($params["propinsi"])?$params["propinsi"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$conditional = "";

		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($propinsi != '') {
			$conditional .= "AND nama = '".$propinsi."'";
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_wilayah."
			WHERE 1 = 1 AND id_parent = '0'
			".$conditional."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listKota($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$id_parent = isset($params["id_parent"])?$params["id_parent"]:'';
		$kota = isset($params["kota"])?$params["kota"]:'';
		$conditional = "";
		
		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($id_parent != '') {
			$conditional .= "AND id_parent = '".$id_parent."'";
		}

		if($kota != '') {
			$conditional .= "AND nama = '".$kota."'";
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_wilayah."
			WHERE 1 = 1
			".$conditional."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($email != '') {
			$conditional .= " AND email = '".$email."'";
		}

		if($id != '') {
			$conditional .= " AND id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE (1=1)
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function getLastNoAnggota($params=array()){
		$conditional = '';
		$q = $this->db->query("
			SELECT
				id,no_anggota,
				SUBSTRING_INDEX(no_anggota, '.', -1) AS lastnumb
			FROM
				".$this->maintablename."
			WHERE no_anggota != ''
			".$conditional."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listDataPendidikan($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id ASC";

		if($id != '') {
			$conditional = "WHERE id_members = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_members_pendidikan."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}
}