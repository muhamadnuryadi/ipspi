<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ExportModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "members";
		$this->table_members_pendidikan = "members_pendidikan";
		$this->table_members_pekerjaan = "members_pekerjaan";
		$this->table_members_praktek = "members_praktek";
		$this->table_members_penghargaan = "members_penghargaan";
		$this->table_members_sertifikasi = "members_sertifikasi";
		$this->table_members_referensi = "members_referensi";
		$this->table_members_profesi = "members_profesi";
		$this->table_members_komunitas = "members_komunitas";
		$this->table_payments = "payments";
	}

	public function filterData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$id_propinsi = isset($params["id_propinsi"])?$params["id_propinsi"]:'';
		$id_kota = isset($params["id_kota"])?$params["id_kota"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$tingkat = isset($params["tingkat"])?$params["tingkat"]:'';
		$id_pekerjaan = isset($params["id_pekerjaan"])?$params["id_pekerjaan"]:'';
		$id_instansi = isset($params["id_instansi"])?$params["id_instansi"]:'';
		$id_sertifikasi = isset($params["id_sertifikasi"])?$params["id_sertifikasi"]:'';
		$no_anggota = isset($params["no_anggota"])?$params["no_anggota"]:'';
		$id_jenis_anggota = isset($params["id_jenis_anggota"])?$params["id_jenis_anggota"]:'';
		$jurusan = isset($params["jurusan"])?$params["jurusan"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$rest  = " ORDER BY m.created_at DESC";
		$conditional = '';
		if($name != ''){
			$conditional .= "AND m.nama LIKE '%".$this->db->escape_str($name)."%'";
		}
		if($id_propinsi != ''){
			$conditional .= "AND m.id_propinsi = '".$this->db->escape_str($id_propinsi)."'";
		}
		if($id_kota != ''){
			$conditional .= "AND m.id_kota = '".$this->db->escape_str($id_kota)."'";
		}
		
		if($no_anggota != ''){
			$conditional .= " AND m.no_anggota LIKE '%".$this->db->escape_str($no_anggota)."%'";
		}

		if($email != ''){
			$conditional .= " AND m.email LIKE '%".$this->db->escape_str($email)."%'";
		}

		if($tingkat != ''){
			$conditional .= " AND mp.tingkat = '".$this->db->escape_str($tingkat)."'";
		}

		if ($id_pekerjaan != '') {
			$conditional .= " AND mp2.id_pekerjaan = '".$this->db->escape_str($id_pekerjaan)."' ";
		}

		if ($id_instansi != '') {
			$conditional .= " AND mp2.id_instansi = '".$this->db->escape_str($id_instansi)."' ";
		}

		if ($id_sertifikasi != '') {
			$conditional .= " AND ms.tipe = '".$this->db->escape_str($id_sertifikasi)."' ";
		}

		if ($id_jenis_anggota != '') {
			$conditional .= " AND mp.jenis = '".$this->db->escape_str($id_jenis_anggota)."' ";
		}

		if ($jurusan != '') {
			$conditional .= " AND mp.jurusan_2 = '".$this->db->escape_str($jurusan)."' ";
		}

		if($this->session->userdata('role') == 2){
			$conditional .= "AND m.id_propinsi = '".$this->session->userdata('userpropinsi')."' ";
		}else if($this->session->userdata('role') == 3){
			$conditional .= "AND m.email = '".$this->session->userdata('email')."'";
		}

		// if($limit > 0){
		// 	if($start > 0){
		// 		$offsetData = "LIMIT ".$start.", ".$limit."";
		// 	}else{
		// 		$offsetData = "LIMIT 0, ".$limit."";
		// 	}
		// }

		if ($tingkat == '' && $id_pekerjaan == '' && $id_instansi == '' && $id_sertifikasi == '' && $id_jenis_anggota == '' && $jurusan == '') {
			$select = "
				SELECT
					m.id,
					m.tgllahir,
					m.statuskawin,
					m.jumanak,
					m.alamatkantor,
					m.telpkantor,
					m.alamatrumah,
					m.id_kota,
					m.id_propinsi,
					m.telp,
					m.hp,
					m.email,
					m.status,
					m.nama,
					m.jk,
					m.ktp,
					m.no_anggota
				FROM
					members m
				WHERE 1=1
				".$conditional."
				".$rest."
				".$offsetData."
			";	
		}else{
			$select = "
				SELECT
					m.id,
					m.tgllahir,
					m.statuskawin,
					m.jumanak,
					m.alamatkantor,
					m.telpkantor,
					m.alamatrumah,
					m.id_kota,
					m.id_propinsi,
					m.telp,
					m.hp,
					m.email,
					m.status,
					m.nama,
					m.jk,
					m.ktp,
					m.no_anggota
				FROM
					members m
				LEFT JOIN members_pendidikan mp ON m.id = mp.id_members
				LEFT JOIN members_pekerjaan mp2 ON m.id = mp2.id_members
				LEFT JOIN members_sertifikasi ms ON m.id = ms.id_members
				WHERE 1=1
				".$conditional."
				GROUP BY m.id
				".$rest."
				".$offsetData."
			";	
		}

		$q = $this->db->query($select);

		$result = $q->result_array();
		$result = $this->getProvinsi($result);
		$result = $this->getKota($result);
		$result = $this->getPendidikan($result);
		$result = $this->getPekerjaan($result);
		$result = $this->getPraktek($result);
		$result = $this->getPenghargaan($result);
		$result = $this->getSertifikasi($result);
		$result = $this->getProfesi($result);
		return $result;
	}

	public function filterDataCount($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$id_propinsi = isset($params["id_propinsi"])?$params["id_propinsi"]:'';
		$id_kota = isset($params["id_kota"])?$params["id_kota"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$tingkat = isset($params["tingkat"])?$params["tingkat"]:'';
		$id_pekerjaan = isset($params["id_pekerjaan"])?$params["id_pekerjaan"]:'';
		$id_instansi = isset($params["id_instansi"])?$params["id_instansi"]:'';
		$id_sertifikasi = isset($params["id_sertifikasi"])?$params["id_sertifikasi"]:'';
		$no_anggota = isset($params["no_anggota"])?$params["no_anggota"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$id_jenis_anggota = isset($params["id_jenis_anggota"])?$params["id_jenis_anggota"]:'';
		$jurusan = isset($params["jurusan"])?$params["jurusan"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';

		$rest  = "ORDER BY m.id DESC";
		$conditional = '';
		if($name != ''){
			$conditional .= "AND m.nama LIKE '%".$this->db->escape_str($name)."%'";
		}
		if($id_propinsi != ''){
			$conditional .= "AND m.id_propinsi = '".$this->db->escape_str($id_propinsi)."'";
		}
		if($id_kota != ''){
			$conditional .= "AND m.id_kota = '".$this->db->escape_str($id_kota)."'";
		}
		
		if($no_anggota != ''){
			$conditional .= " AND m.no_anggota LIKE '%".$this->db->escape_str($no_anggota)."%'";
		}

		if($email != ''){
			$conditional .= " AND m.email LIKE '%".$this->db->escape_str($email)."%'";
		}

		if($tingkat != ''){
			$conditional .= " AND mp.tingkat = '".$this->db->escape_str($tingkat)."'";
		}

		if ($id_pekerjaan != '') {
			$conditional .= " AND mp2.id_pekerjaan = '".$this->db->escape_str($id_pekerjaan)."' ";
		}

		if ($id_instansi != '') {
			$conditional .= " AND mp2.id_instansi = '".$this->db->escape_str($id_instansi)."' ";
		}

		if ($id_sertifikasi != '') {
			$conditional .= " AND ms.tipe = '".$this->db->escape_str($id_sertifikasi)."' ";
		}

		if ($id_jenis_anggota != '') {
			$conditional .= " AND mp.jenis = '".$this->db->escape_str($id_jenis_anggota)."' ";
		}

		if ($jurusan != '') {
			$conditional .= " AND mp.jurusan_2 = '".$this->db->escape_str($jurusan)."' ";
		}

		if($this->session->userdata('role') == 2){
			$conditional .= "AND m.id_propinsi = '".$this->session->userdata('userpropinsi')."' ";
		}else if($this->session->userdata('role') == 3){
			$conditional .= "AND m.email = '".$this->session->userdata('email')."'";
		}

		if ($tingkat == '' && $id_pekerjaan == '' && $id_instansi == '' && $id_sertifikasi == '' && $id_jenis_anggota == '' && $jurusan == '') {
			$select = "
				SELECT
					count(m.id) as jumlah
				FROM
					members m
				WHERE 1=1
				".$conditional."
				".$rest."	
			";

			$q = $this->db->query($select);
			$result = $q->first_row('array');

		}else{
			$select = "
				SELECT
					m.id
				FROM
					members m
				LEFT JOIN members_pendidikan mp ON m.id = mp.id_members
				LEFT JOIN members_pekerjaan mp2 ON m.id = mp2.id_members
				LEFT JOIN members_sertifikasi ms ON m.id = ms.id_members
				WHERE 1=1
				".$conditional."
				GROUP BY m.id
				".$rest."
				
			";

			$q = $this->db->query($select);
			$result = $q->result_array();
			$result['jumlah'] = count($result);	
		}
		
		return $result;
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY created_at DESC";

		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}
		
		if($this->session->userdata('role') == 2){
			$conditional .= "AND id_propinsi = '".$this->session->userdata('userpropinsi')."' ";
		}else if($this->session->userdata('role') == 3){
			$conditional .= "AND email = '".$this->session->userdata('email')."'";
		}

		$q = $this->db->query("
			SELECT
				id,
				tgllahir,
				statuskawin,
				jumanak,
				alamatkantor,
				telpkantor,
				alamatrumah,
				id_kota,
				id_propinsi,
				telp,
				hp,
				email,
				status,
				nama,
				jk,
				ktp,
				no_anggota
			FROM
				".$this->maintablename."
			WHERE (1=1)
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		$result = $this->getProvinsi($result);
		$result = $this->getKota($result);
		$result = $this->getPendidikan($result);
		$result = $this->getPekerjaan($result);
		$result = $this->getPraktek($result);
		$result = $this->getPenghargaan($result);
		$result = $this->getSertifikasi($result);
		$result = $this->getProfesi($result);
			
		return $result;
	}

	public function listDataCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($this->session->userdata('role') == 2){
			$conditional .= "AND id_propinsi = '".$this->session->userdata('userpropinsi')."' ";
		}else if($this->session->userdata('role') == 3){
			$conditional .= "AND email = '".$this->session->userdata('email')."'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE (1=1)
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

	private function getProvinsi($result){
		if (count($result) > 0) {
			foreach ($result as $key => $value) {
				if ($value['id_propinsi'] != '') {
					$result[$key]['nama_provinsi'] = $this->db->query("
						SELECT nama FROM wilayah WHERE id = ".$value['id_propinsi']."
						")->first_row('array');
				}
			}
		}

		return $result;
	}

	private function getKota($result){
		if (count($result) > 0) {
			foreach ($result as $key => $value) {
				if ($value['id_kota'] != '') {
					$result[$key]['nama_kota'] = $this->db->query("
						SELECT nama FROM wilayah WHERE id = ".$value['id_kota']."
						")->first_row('array');
				}
			}
		}

		return $result;
	}

	private function getPendidikan($result){
		if (count($result) > 0) {
			foreach ($result as $key => $value) {
				if ($value['id'] != '') {
					$result[$key]['pendidikan'] = $this->db->query("
						SELECT pt,jurusan,thn_lulus FROM members_pendidikan WHERE id_members = ".$value['id']." ORDER BY id DESC LIMIT 1
						")->first_row('array');
				}
			}
		}

		return $result;
	}

	private function getPekerjaan($result){
		if (count($result) > 0) {
			foreach ($result as $key => $value) {
				if ($value['id'] != '') {
					$result[$key]['pekerjaan'] = $this->db->query("
						SELECT nama,jabatan FROM members_pekerjaan WHERE id_members = ".$value['id']." ORDER BY id DESC LIMIT 1
						")->first_row('array');
				}
			}
		}

		return $result;
	}

	private function getPraktek($result){
		if (count($result) > 0) {
			foreach ($result as $key => $value) {
				if ($value['id'] != '') {
					$result[$key]['praktek'] = $this->db->query("
						SELECT peran,jabatan,keahlian FROM members_praktek WHERE id_members = ".$value['id']." ORDER BY id DESC LIMIT 1
						")->first_row('array');
				}
			}
		}

		return $result;
	}

	private function getPenghargaan($result){
		if (count($result) > 0) {
			foreach ($result as $key => $value) {
				if ($value['id'] != '') {
					$result[$key]['penghargaan'] = $this->db->query("
						SELECT nama,instansi,tahun FROM members_penghargaan WHERE id_members = ".$value['id']." ORDER BY id DESC LIMIT 1
						")->first_row('array');
				}
			}
		}

		return $result;
	}

	private function getSertifikasi($result){
		if (count($result) > 0) {
			foreach ($result as $key => $value) {
				if ($value['id'] != '') {
					$result[$key]['sertifikasi'] = $this->db->query("
						SELECT nama,lembaga,tahun FROM members_sertifikasi WHERE id_members = ".$value['id']." ORDER BY id DESC LIMIT 1
						")->first_row('array');
				}
			}
		}

		return $result;
	}

	private function getProfesi($result){
		if (count($result) > 0) {
			foreach ($result as $key => $value) {
				if ($value['id'] != '') {
					$result[$key]['profesi'] = $this->db->query("
						SELECT nama,tahun FROM members_profesi WHERE id_members = ".$value['id']." ORDER BY id DESC LIMIT 1
						")->first_row('array');
				}
			}
		}

		return $result;
	}

    public function getInstansi($result){
    	if (count($result) > 0) {
    		foreach ($result as $key => $value) {
    			if ($value['id_instansi'] != 0) {
    				$result[$key]['instansi'] = $this->db->query("
    					SELECT name FROM instansi WHERE id = ".$value['id_instansi']."
    					")->first_row('array');
    			}
    		}
    	}

    	return $result;
    }
}
