<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class RegulationModel extends CI_Model{
	var $ci;
	function __construct() {	 
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "regulation";		
	}
	public function entriData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$body = isset($params["body"])?$params["body"]:'';

        if($name == ''){
            return "empty_name";
        }
        if($body == ''){
            return "empty_body";
        }
        
		$data ['name'] = $this->db->escape_str($name);
		$data ['slug'] = url_title($name,'-',TRUE);
		$data ['body'] = $this->db->escape_str($body);
		$data ['status'] = '1';
		$data ['datecreated'] = date("Y-m-d H:i");
		
		$doInsert = $this->db->insert($this->maintablename, $data);
		if($doInsert){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_entry_regulation')." dengan judul = ".htmlentities($name).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function updateData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$name = isset($params["name"])?$params["name"]:'';
        $body = isset($params["body"])?$params["body"]:'';

        if($name == ''){
            return "empty_name";
        }
        if($body == ''){
            return "empty_body";
        }
        
		$sql_user = "
					name = '".$this->db->escape_str($name)."'
					,body = '".$this->db->escape_str($body)."'
					,slug = '".$this->db->escape_str(url_title($name,'-',TRUE))."'
					,datemodified = '".$this->db->escape_str(date("Y-m-d H:i"))."'
					";
		
		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_regulation')." id = ".$id.", title = ".htmlentities($name).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteData($id){
		if($id == 0) return 'failed';

		$doDelete = $this->db->query("
		DELETE FROM ".$this->maintablename."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_regulation')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional = "AND id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE (1=1)
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listDataCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional = "AND id = '".$id."'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE (1=1)
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}
}