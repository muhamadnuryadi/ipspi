<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class LokerModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "loker";
	}
	public function entriData($params=array()){
		$title = isset($params["title"])?$params["title"]:'';
		$date_posted = isset($params["date_posted"])?$params["date_posted"]:'';
		$body = isset($params["body"])?$params["body"]:'';
		$tags = isset($params["tags"])?$params["tags"]:'';
		$status = isset($params["status"])?$params["status"]:'';
		
        $expdate = explode('/', $date_posted);
        $newdate_posted = $expdate[2].'-'.$expdate[1].'-'.$expdate[0];

		$data ['title'] = $this->db->escape_str($title);
		$data ['date_posted'] = $this->db->escape_str($newdate_posted);
		$data ['slug'] = url_title($title,'-',TRUE);
		$data ['body'] = $this->db->escape_str($body);
		$data ['status'] = $this->db->escape_str($status);
		$data ['date_created'] = date("Y-m-d H:i");
		
		$doInsert = $this->db->insert($this->maintablename, $data);
		if($doInsert){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_entry_loker')." dengan judul = ".htmlentities($title).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function updateData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$title = isset($params["title"])?$params["title"]:'';
		$date_posted = isset($params["date_posted"])?$params["date_posted"]:'';
		$body = isset($params["body"])?$params["body"]:'';
		$status = isset($params["status"])?$params["status"]:'';

        $expdate = explode('/', $date_posted);
        $newdate_posted = $expdate[2].'-'.$expdate[1].'-'.$expdate[0];

        $path = isset($upload_image["path"])?$upload_image["path"]:"";
		
		$sql_user = "
					title = '".$this->db->escape_str($title)."'
					,date_posted = '".$this->db->escape_str($newdate_posted)."'
					,body = '".$this->db->escape_str($body)."'
					,slug = '".$this->db->escape_str(url_title($title,'-',TRUE))."'
					,status = '".$this->db->escape_str($status)."'
					,date_modified = '".date("Y-m-d H:i")."'
					";

		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_loker')." id = ".$id.", title = ".htmlentities($title).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteData($id){
		if($id == 0) return 'failed';
		// $getData = $this->listData(array('id' => $id));
		
		$doDelete = $this->db->query("
		DELETE FROM ".$this->maintablename."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_loker')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}


    public function doPublish($id){
		if($id == 0) return 'failed';
		
		$doPublish = $this->db->query("
			UPDATE ".$this->maintablename."
			SET status = '1'
			WHERE 
				id = ".$id."
		");
		if($doPublish){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_publish_loker')." id = ".$id.""));		
			return 'success';
		}else{
			return 'failed';
		}
	}
	public function doUnublish($id){
		if($id == 0) return 'failed';
		
		$doUnublish = $this->db->query("
			UPDATE ".$this->maintablename."
			SET status = '0'
			WHERE 
				id = ".$id."
		");
		if($doUnublish){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_unpublish_loker')." id = ".$id.""));		
			return 'success';
		}else{
			return 'failed';
		}
	}
	public function filterData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional  = "";
		$rest  = "ORDER BY id DESC";
		if($name !=''){
			$conditional .= "AND title LIKE '%".$this->db->escape_str($name)."%'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE (1=1)
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function filterDataCount($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$conditional = '';
		$rest  = "ORDER BY id DESC";
		if($name !=''){
			$conditional .= "AND title LIKE '%".$this->db->escape_str($name)."%'";
		}
		
		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE (1=1)
			".$conditional."
			".$rest."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($this->session->userdata('role') != 0){
			if($this->session->userdata('role') == 1){
				$conditional .= "";
			}else if($this->session->userdata('role') == 2){
				$conditional .= "AND id_provinsi = '".$this->session->userdata('userprovinsi')."'";
			}else if($this->session->userdata('role') == 3){
				$conditional .= "AND id_user = '".$this->session->userdata('userid')."'";
			}
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE (1=1)
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listDataCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($this->session->userdata('role') != 0){
			if($this->session->userdata('role') == 1){
				$conditional .= "";
			}else if($this->session->userdata('role') == 2){
				$conditional .= "AND id_provinsi != '".$this->session->userdata('userprovinsi')."'";
			}else if($this->session->userdata('role') == 3){
				$conditional .= "AND id_user != '".$this->session->userdata('userid')."'";
			}
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE (1=1)
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}
    
}