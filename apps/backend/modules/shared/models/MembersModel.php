<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MembersModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "members";
		$this->table_members_pendidikan = "members_pendidikan";
		$this->table_members_pekerjaan = "members_pekerjaan";
		$this->table_members_praktek = "members_praktek";
		$this->table_members_penghargaan = "members_penghargaan";
		$this->table_members_sertifikasi = "members_sertifikasi";
		$this->table_members_referensi = "members_referensi";
		$this->table_members_profesi = "members_profesi";
		$this->table_members_komunitas = "members_komunitas";
		$this->table_payments = "payments";
	}
	public function entriData($params=array()){
		$nama = isset($params['nama'])?$params['nama']:'';
        $username = isset($params['username'])?$params['username']:'';
        $email = isset($params['email'])?$params['email']:'';
        $password = isset($params['password'])?$params['password']:'';
        $password_1 = isset($params['password_1'])?$params['password_1']:'';
        $tgllahir = isset($params['tgllahir'])?$params['tgllahir']:'';
        $jumanak = isset($params['jumanak'])?$params['jumanak']:'';
        $ktp = isset($params['ktp'])?$params['ktp']:'';
        $alamatkantor = isset($params['alamatkantor'])?$params['alamatkantor']:'';
        $telpkantor = isset($params['telpkantor'])?$params['telpkantor']:'';
        $faxkantor = isset($params['faxkantor'])?$params['faxkantor']:'';
        $alamatrumah = isset($params['alamatrumah'])?$params['alamatrumah']:'';
        $id_propinsi = isset($params['id_propinsi'])?$params['id_propinsi']:'';
        $id_kota = isset($params['id_kota'])?$params['id_kota']:'';
        $telp = isset($params['telp'])?$params['telp']:'';
        $hp = isset($params['hp'])?$params['hp']:'';
        $status = isset($params['status'])?$params['status']:'';
        $created_at = isset($params['created_at'])?$params['created_at']:'';

		$upload_image_ijazah = $this->uploadImageIjazah();
		$upload_image_ktp = $this->uploadImageKtp();
		$upload_image_foto = $this->uploadImageFoto();

		if(isset($upload_image_ijazah["error"])) {
            if($upload_image_ijazah["error"] == "failed_ext") {
                return "failed_ext";
            }
            if($upload_image_ijazah["error"] == "empty_file") {
                return "empty_file";
            }
        }

        if(isset($upload_image_ktp["error"])) {
            if($upload_image_ktp["error"] == "failed_ext") {
                return "failed_ext";
            }
            if($upload_image_ktp["error"] == "empty_file") {
                return "empty_file";
            }
        }

		$data['nama'] = $this->db->escape_str($nama);
		$data['username'] = $this->db->escape_str($username);
        $data['email'] = $this->db->escape_str($email);
        $data['password'] = md5($password);
        $data['tgllahir'] = date("Y-m-d", strtotime($tgllahir));
        $data['jumanak'] = $this->db->escape_str($jumanak);
        $data['ktp'] = $this->db->escape_str($ktp);
        $data['alamatkantor'] = $this->db->escape_str($alamatkantor);
        $data['telpkantor'] = $this->db->escape_str($telpkantor);
        $data['faxkantor'] = $this->db->escape_str($faxkantor);
        $data['alamatrumah'] = $this->db->escape_str($alamatrumah);
        $data['id_propinsi'] = $this->db->escape_str($id_propinsi);
        $data['id_kota'] = $this->db->escape_str($id_kota);
        $data['telp'] = $this->db->escape_str($telp);
        $data['hp'] = $this->db->escape_str($hp);
        $data['status'] = $this->db->escape_str($status);
        $data['created_at'] = date("Y-m-d", strtotime($created_at));
        $data['path_ijazah'] = isset($upload_image_ijazah["path_ijazah"])?$upload_image_ijazah["path_ijazah"]:"";
        $data['path_ktp'] = isset($upload_image_ktp["path_ktp"])?$upload_image_ktp["path_ktp"]:"";
        $data['foto'] = isset($upload_image_ktp["foto"])?$upload_image_ktp["foto"]:"";
		
		$doInsert = $this->db->insert($this->maintablename, $data);
		if($doInsert){
			$this->session->set_flashdata('id_insert', $this->db->insert_id());
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_entry_members')." dengan nama = ".htmlentities($nama).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	private function uploadImageProfile($width_resize, $a1, $b1, $a2, $b2) {
        $this->ci->load->helper('upload');
        if ($_FILES['path_ktp']['size']<>0){
            $data = array();

            $tmpName = $_FILES['path_ktp']['tmp_name']; 
            list($width, $height, $type, $attr) = getimagesize($tmpName);
            # Rasio Ukuran Sliders #
            $rasio = $width/$width_resize;
            $extension_thumb = strtoupper(end(explode('.', $_FILES['path_ktp']['name'])));
            if (!(($extension_thumb == 'JPEG') || ($extension_thumb == 'JPG') || ($extension_thumb == 'PNG') || ($extension_thumb == 'GIF'))){
                $data['error'] = "failed_ext";
            }
            
            # UPLOAD #
            $pathdir = date("Y/m/d");
            $thepath = $this->webconfig['media-path-profile'].$pathdir."/";                       
            $thepath_create = $this->webconfig['media-path-profile'].$pathdir."/temp/";                       
            $exp = explode("/",$thepath_create);
            $way = '';
            foreach($exp as $n){
                $way .= $n.'/';
                @mkdir($way);       
            }
            
            $pos = strripos($_FILES['path_ktp']['name'], '.');
            if($pos === false){
                $ordinary_name = $_FILES['path_ktp']['name'];
            }else{
                $ordinary_name = substr($_FILES['path_ktp']['name'], 0, $pos);
            }

            $name_upload = date("Y_m_d-H_i_s")."_".md5($ordinary_name);
            $name_upload_c = $name_upload.'.'.strtolower($extension_thumb);
            
            $result = was_upload_file('pathprofile', $name_upload, false, $thepath_create."/", array(), array(), array());

            if ($result['status'] > 0){
                $data['path_ktp'] = "";
            }else{
                $path1 = $pathdir."/".$name_upload_c;
                $data['path_ktp'] = $path1;
                if(isset($rasio) && $rasio !=''){
                    $finalcrop_resize = $this->croping_resize_profile($result,$a1, $b1, $a2, $b2,$rasio,$thepath);
                }
            }
        }else{
            $data['error'] = "empty_file";
        }

        return $data;
    }

    private function croping_resize_profile($result,$a1, $b1, $a2, $b2,$rasio,$image_path) {
        
        # RESIZE and CROP #
        $thepath = $this->webconfig['media-path-profile-thumb'].date("Y/m/d")."/";                       
        $exp = explode("/",$thepath);
        $way ='';
        foreach($exp as $n){
            $way.=$n.'/';
            @mkdir($way);       
        }
        
        $file_name = $result['name'];
        $src_path = $image_path . 'temp/' . $file_name;
        
        $exp_file = explode('.', $file_name, 2);
        
        $des_path =  $image_path .$file_name;
        $aa1 = $a1 * $rasio;
        $bb1 = $b1 * $rasio;
        $aa2 = $a2 * $rasio;
        $bb2 = $b2 * $rasio;
        if($des_path !=''){
            $this->image_moo
                ->load($src_path)
                ->crop($aa1,$bb1,$aa2,$bb2)
                ->save($des_path);
        }

        ## RESIZE IMAGE ##
        foreach($this->webconfig['image_profile'] as $key => $value){
            $size_file = explode('x',$value);
            $file_final_name_resize = $exp_file[0].'_'.$value.'_thumb.'.$exp_file[1];
            $des_path_resize =  $thepath . $file_final_name_resize;
            $this->image_moo
                ->load($des_path)
                ->stretch($size_file[0],$size_file[1])
                ->save($des_path_resize);
        }

        /* Delete Temp File */
        if (file_exists($src_path)){
            @unlink($src_path);
        }
    }

    private function del_cropresize_profile($cfile) {
        if($cfile == "") return false;
        
        $explod_file = explode("/",$cfile);
        $filename = end($explod_file);
        $exp_file = explode(".",$filename);
        $exp_thumb_path = explode("/", $this->webconfig['media-path-profile-thumb'].$cfile, -1);
        $thumb_path = implode("/", $exp_thumb_path);
        
        foreach($this->webconfig['image_profile'] as $key => $value){
            $delete_resize = $thumb_path.'/'.$exp_file[0]."_".$value."_thumb.".$exp_file[1];
            if (file_exists($delete_resize)){
                @unlink($delete_resize);
            }
        }
    }

	public function updateTglSurat($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$count_surat = isset($params["count_surat"])?$params["count_surat"]:'';
		$tgl_awal_surat = isset($params["tgl_awal_surat"])?$params["tgl_awal_surat"]:'';
		$tgl_akhir_surat = isset($params["tgl_akhir_surat"])?$params["tgl_akhir_surat"]:'';

		$sql_user = "
						tgl_awal_surat = '".$this->db->escape_str($tgl_awal_surat)."'
						,tgl_akhir_surat = '".$this->db->escape_str($tgl_akhir_surat)."'
						,count_surat = '".$this->db->escape_str($count_surat)."'
					";

		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." Tgl surat id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function updateData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$fullname = isset($params["fullname"])?$params["fullname"]:'';
		$birthday = isset($params["birthday"])?$params["birthday"]:'';
		$address = isset($params["address"])?$params["address"]:'';
		$postcode = isset($params["postcode"])?$params["postcode"]:'';
		$gender = isset($params["gender"])?$params["gender"]:'';
		$phone = isset($params["phone"])?$params["phone"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$password = isset($params["password"])?$params["password"]:'';
		$status = isset($params["status"])?$params["status"]:'';
		$username = isset($params["username"])?$params["username"]:'';
		$instagram = isset($params["instagram"])?$params["instagram"]:'';
		$tipe_user = isset($params["tipe_user"])?$params["tipe_user"]:'';
		$discount = isset($params["discount"])?$params["discount"]:'';
		$created_at = isset($params["created_at"])?$params["created_at"]:'';

		$exist_username = $this->__CheckUserExistEdit($id,$username);
		
		if($exist_username > 0){return 'exist_username';}

		$a1     = isset($params["a1"])?$params["a1"]:'';
        $b1     = isset($params["b1"])?$params["b1"]:'';
        $a2      = isset($params["a2"])?$params["a2"]:'';
        $b2      = isset($params["b2"])?$params["b2"]:'';
        $width_resize      = isset($params["width_resize"])?$params["width_resize"]:'';

        if ($_FILES['pathprofile']['size']<>0){
            $getData = $this->listData(array('id' => $id));     
            if(count($getData) > 0) {
                $pfile = $this->webconfig['media-path-profile'].$getData[0]['pathprofile'];
                if (file_exists($pfile)){
                    @unlink($pfile);
                }
                $delete_cropresize = $this->del_cropresize_profile($getData[0]['pathprofile']);
            }
        }

        $upload_image_profile = $this->uploadImageProfile($width_resize, $a1, $b1, $a2, $b2);

        if(isset($upload_image_profile["error"])) {
            if($upload_image_profile["error"] == "failed_ext") {
                return "failed_ext";
            }
        }

        $pathktp = isset($upload_image_profile["pathprofile"])?$upload_image_profile["pathprofile"]:"";

		if($password != ''){
			if(strlen($password) < 5){
				return 'min_5';
			}else{
				$sql_user = "
							fullname = '".$this->db->escape_str($fullname)."'
							,birthday = '".date("Y-m-d", strtotime($birthday))."'
							,address = '".$this->db->escape_str($address)."'
							,postcode = '".$this->db->escape_str($postcode)."'
							,gender = '".$this->db->escape_str($gender)."'
							,phone = '".$this->db->escape_str($phone)."'
							,email = '".$this->db->escape_str($email)."'
							,status = '".$this->db->escape_str($status)."'
							,password = '".$this->db->escape_str(md5($password))."'
							,username = '".$this->db->escape_str($username)."'
							,instagram = '".$this->db->escape_str($instagram)."'
							,tipe_user = '".$this->db->escape_str($tipe_user)."'
							,discount = '".$this->db->escape_str($discount)."'
							,created_at = '".date("Y-m-d", strtotime($created_at))."' ";
			}

		}else{
			$sql_user = "
						fullname = '".$this->db->escape_str($fullname)."'
						,birthday = '".date("Y-m-d", strtotime($birthday))."'
						,address = '".$this->db->escape_str($address)."'
						,postcode = '".$this->db->escape_str($postcode)."'
						,gender = '".$this->db->escape_str($gender)."'
						,phone = '".$this->db->escape_str($phone)."'
						,email = '".$this->db->escape_str($email)."'
						,status = '".$this->db->escape_str($status)."'
						,username = '".$this->db->escape_str($username)."'
						,instagram = '".$this->db->escape_str($instagram)."'
						,tipe_user = '".$this->db->escape_str($tipe_user)."'
						,discount = '".$this->db->escape_str($discount)."'
						,created_at = '".$this->db->escape_str($created_at)."'";
		}
		
		if ($_FILES['pathprofile']['size']<>0) $sql_user .= ", pathprofile = '".$this->db->escape_str($pathktp)."'";

		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.", name = ".htmlentities($fullname).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteData($id){
		if($id == 0) return 'failed';

		# DELETE MEMBER PROJECT # 
		$this->db->where('id',$id);
		$queproject = $this->db->get($this->maintablename);
		$resultproject = $queproject->result_array();
		if(isset($resultproject) && count($resultproject) > 0){
			foreach ($resultproject as $key => $value) {
				
				// @unlink($this->webconfig['media-path-profile'].$value['logo_img']);
				@unlink($this->webconfig['media-path-files'].$value['path_ktp']);
				@unlink($this->webconfig['media-path-files'].$value['path_ijazah']);
				@unlink($this->webconfig['media-path-files'].$value['foto']);
				// $exp_img = explode(".", $value['logo_img']);
				// foreach ($this->webconfig['image_profile'] as $key => $image_data) {
				// 	@unlink($this->webconfig['media-path-profile-thumb'].$exp_img[0]."_".$image_data."_thumb.".$exp_img[1]);
				// }
			}
		}

		$doDelete = $this->db->query("
		DELETE FROM ".$this->maintablename."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function filterData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$id_propinsi = isset($params["id_propinsi"])?$params["id_propinsi"]:'';
		$id_kota = isset($params["id_kota"])?$params["id_kota"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$tingkat = isset($params["tingkat"])?$params["tingkat"]:'';
		$id_pekerjaan = isset($params["id_pekerjaan"])?$params["id_pekerjaan"]:'';
		$id_instansi = isset($params["id_instansi"])?$params["id_instansi"]:'';
		$id_sertifikasi = isset($params["id_sertifikasi"])?$params["id_sertifikasi"]:'';
		$no_anggota = isset($params["no_anggota"])?$params["no_anggota"]:'';
		$id_jenis_anggota = isset($params["id_jenis_anggota"])?$params["id_jenis_anggota"]:'';
		$jurusan = isset($params["jurusan"])?$params["jurusan"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$rest  = " ORDER BY m.id DESC";
		$conditional = '';
		if($name != ''){
			$conditional .= "AND m.nama LIKE '%".$this->db->escape_str($name)."%'";
		}
		if($id_propinsi != ''){
			$conditional .= "AND m.id_propinsi = '".$this->db->escape_str($id_propinsi)."'";
		}
		if($id_kota != ''){
			$conditional .= "AND m.id_kota = '".$this->db->escape_str($id_kota)."'";
		}
		
		if($no_anggota != ''){
			$conditional .= " AND m.no_anggota LIKE '%".$this->db->escape_str($no_anggota)."%'";
		}

		if($email != ''){
			$conditional .= " AND m.email LIKE '%".$this->db->escape_str($email)."%'";
		}

		if($tingkat != ''){
			$conditional .= " AND mp.tingkat = '".$this->db->escape_str($tingkat)."'";
		}

		if ($id_pekerjaan != '') {
			$conditional .= " AND mp2.id_pekerjaan = '".$this->db->escape_str($id_pekerjaan)."' ";
		}

		if ($id_instansi != '') {
			$conditional .= " AND mp2.id_instansi = '".$this->db->escape_str($id_instansi)."' ";
		}

		if ($id_sertifikasi != '') {
			$conditional .= " AND ms.tipe = '".$this->db->escape_str($id_sertifikasi)."' ";
		}

		if ($id_jenis_anggota != '') {
			$conditional .= " AND mp.jenis = '".$this->db->escape_str($id_jenis_anggota)."' ";
		}

		if ($jurusan != '') {
			$conditional .= " AND mp.jurusan_2 = '".$this->db->escape_str($jurusan)."' ";
		}

		if($this->session->userdata('role') == 2){
			$conditional .= "AND m.id_propinsi = '".$this->session->userdata('userpropinsi')."' ";
		}else if($this->session->userdata('role') == 3){
			$conditional .= "AND m.email = '".$this->session->userdata('email')."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		if ($tingkat == '' && $id_pekerjaan == '' && $id_instansi == '' && $id_sertifikasi == '' && $id_jenis_anggota == '' && $jurusan == '') {
			$select = "
				SELECT
					m.id,
					m.nama,
					m.email,
					m.no_anggota,
					m.id_propinsi,
					m.id_kota,
					m.status
				FROM
					members m
				WHERE 1=1
				".$conditional."
				".$rest."
				".$offsetData."
			";	
		}else{
			$select = "
				SELECT
					m.id,
					m.nama,
					m.email,
					m.no_anggota,
					m.id_propinsi,
					m.id_kota,
					m.status
				FROM
					members m
				LEFT JOIN members_pendidikan mp ON m.id = mp.id_members
				LEFT JOIN members_pekerjaan mp2 ON m.id = mp2.id_members
				LEFT JOIN members_sertifikasi ms ON m.id = ms.id_members
				WHERE 1=1
				".$conditional."
				GROUP BY m.id
				".$rest."
				".$offsetData."
			";	
		}

		$q = $this->db->query($select);

		$result = $q->result_array();
		$result = $this->getProvinsi($result);
		$result = $this->getKota($result);
		return $result;
	}

	public function filterDataCount($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$id_propinsi = isset($params["id_propinsi"])?$params["id_propinsi"]:'';
		$id_kota = isset($params["id_kota"])?$params["id_kota"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$tingkat = isset($params["tingkat"])?$params["tingkat"]:'';
		$id_pekerjaan = isset($params["id_pekerjaan"])?$params["id_pekerjaan"]:'';
		$id_instansi = isset($params["id_instansi"])?$params["id_instansi"]:'';
		$id_sertifikasi = isset($params["id_sertifikasi"])?$params["id_sertifikasi"]:'';
		$no_anggota = isset($params["no_anggota"])?$params["no_anggota"]:'';
		$email = isset($params["email"])?$params["email"]:'';
		$id_jenis_anggota = isset($params["id_jenis_anggota"])?$params["id_jenis_anggota"]:'';
		$jurusan = isset($params["jurusan"])?$params["jurusan"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';

		$rest  = "ORDER BY m.id DESC";
		$conditional = '';
		if($name != ''){
			$conditional .= "AND m.nama LIKE '%".$this->db->escape_str($name)."%'";
		}
		if($id_propinsi != ''){
			$conditional .= "AND m.id_propinsi = '".$this->db->escape_str($id_propinsi)."'";
		}
		if($id_kota != ''){
			$conditional .= "AND m.id_kota = '".$this->db->escape_str($id_kota)."'";
		}
		
		if($no_anggota != ''){
			$conditional .= " AND m.no_anggota LIKE '%".$this->db->escape_str($no_anggota)."%'";
		}

		if($email != ''){
			$conditional .= " AND m.email LIKE '%".$this->db->escape_str($email)."%'";
		}

		if($tingkat != ''){
			$conditional .= " AND mp.tingkat = '".$this->db->escape_str($tingkat)."'";
		}

		if ($id_pekerjaan != '') {
			$conditional .= " AND mp2.id_pekerjaan = '".$this->db->escape_str($id_pekerjaan)."' ";
		}

		if ($id_instansi != '') {
			$conditional .= " AND mp2.id_instansi = '".$this->db->escape_str($id_instansi)."' ";
		}

		if ($id_sertifikasi != '') {
			$conditional .= " AND ms.tipe = '".$this->db->escape_str($id_sertifikasi)."' ";
		}

		if ($id_jenis_anggota != '') {
			$conditional .= " AND mp.jenis = '".$this->db->escape_str($id_jenis_anggota)."' ";
		}

		if ($jurusan != '') {
			$conditional .= " AND mp.jurusan_2 = '".$this->db->escape_str($jurusan)."' ";
		}

		if($this->session->userdata('role') == 2){
			$conditional .= "AND m.id_propinsi = '".$this->session->userdata('userpropinsi')."' ";
		}else if($this->session->userdata('role') == 3){
			$conditional .= "AND m.email = '".$this->session->userdata('email')."'";
		}

		if ($tingkat == '' && $id_pekerjaan == '' && $id_instansi == '' && $id_sertifikasi == '' && $id_jenis_anggota == '' && $jurusan == '') {
			$select = "
				SELECT
					count(m.id) as jumlah
				FROM
					members m
				WHERE 1=1
				".$conditional."
				".$rest."	
			";

			$q = $this->db->query($select);
			$result = $q->first_row('array');

		}else{
			$select = "
				SELECT
					m.id
				FROM
					members m
				LEFT JOIN members_pendidikan mp ON m.id = mp.id_members
				LEFT JOIN members_pekerjaan mp2 ON m.id = mp2.id_members
				LEFT JOIN members_sertifikasi ms ON m.id = ms.id_members
				WHERE 1=1
				".$conditional."
				GROUP BY m.id
				".$rest."
				
			";

			$q = $this->db->query($select);
			$result = $q->result_array();
			$result['jumlah'] = count($result);	
		}
		
		return $result;
	}

	public function allListData($params=array()){
		$q = $this->db->query("
			SELECT
				MAX(DISTINCT count_surat) as maxsurat
			FROM
				".$this->maintablename."
			WHERE (1=1) AND status = 1 AND YEAR(tgl_awal_surat) = YEAR(CURDATE()) 
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}
		
		if($this->session->userdata('role') == 2){
			$conditional .= "AND id_propinsi = '".$this->session->userdata('userpropinsi')."' ";
		}else if($this->session->userdata('role') == 3){
			$conditional .= "AND email = '".$this->session->userdata('email')."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE (1=1)
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		$result = $this->getProvinsi($result);
		$result = $this->getKota($result);
		
		return $result;
	}

	public function getLastNoAnggota($params=array()){
		$conditional = '';
		$q = $this->db->query("
			SELECT
				id,no_anggota,
				SUBSTRING_INDEX(no_anggota, '.', -1) AS lastnumb
			FROM
				".$this->maintablename."
			WHERE no_anggota != ''
			".$conditional."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listDataCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($this->session->userdata('role') == 2){
			$conditional .= "AND id_propinsi = '".$this->session->userdata('userpropinsi')."' ";
		}else if($this->session->userdata('role') == 3){
			$conditional .= "AND email = '".$this->session->userdata('email')."'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE (1=1)
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

	private function getProvinsi($result){
		if (count($result) > 0) {
			foreach ($result as $key => $value) {
				if ($value['id_propinsi'] != '') {
					$result[$key]['nama_provinsi'] = $this->db->query("
						SELECT nama FROM wilayah WHERE id = ".$value['id_propinsi']."
						")->first_row('array');
				}
			}
		}

		return $result;
	}

	private function getKota($result){
		if (count($result) > 0) {
			foreach ($result as $key => $value) {
				if ($value['id_kota'] != '') {
					$result[$key]['nama_kota'] = $this->db->query("
						SELECT nama FROM wilayah WHERE id = ".$value['id_kota']."
						")->first_row('array');
				}
			}
		}

		return $result;
	}

	function checkname($nama){
		$q = $this->db->query("
			SELECT
				id
				,nama
			FROM
				".$this->maintablename."
			WHERE
				nama = '".$this->db->escape_str($nama)."'
		");
		$result = $q->first_row('array');
		return $result;
	}

	private function __CheckUserExist($username){
		if($username !=''){
			$db = $this->db->query("
				SELECT
					count(id) as jumlah
				FROM
					".$this->maintablename."
				WHERE username LIKE '".$this->db->escape_str($username)."'
			")->result_array();

			$jumlah = $db[0]['jumlah'];
			return $jumlah;
		}else{
			return 'failed';
		}
	}

	private function __CheckUserExistEdit($id,$username){
		$conditional = "";
		if($id !='' AND $username !=''){
			$db = $this->db->query("
				SELECT
					count(id) as jumlah
				FROM
					".$this->maintablename."
				WHERE id NOT IN (".$id.")
				AND username LIKE '".$this->db->escape_str($username)."'
			")->result_array();

			$jumlah = $db[0]['jumlah'];
			return $jumlah;
		}
	}

	public function filterSelect2($query = ""){
		$q = $this->db->query("
			SELECT
				id
				,fullname as text
			FROM
				".$this->maintablename."
			WHERE fullname LIKE '%".$query."%'
			limit 0, 10
		");
		$result = $q->result_array();
		return $result;
	}

	public function updateDataProfile($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$nama = isset($params['nama'])?$params['nama']:'';
        $username = isset($params['username'])?$params['username']:'';
        $email = isset($params['email'])?$params['email']:'';
        $password = isset($params['password'])?$params['password']:'';
        $password_1 = isset($params['password_1'])?$params['password_1']:'';
        $tgllahir = isset($params['tgllahir'])?$params['tgllahir']:'';
        $jumanak = isset($params['jumanak'])?$params['jumanak']:'';
        $ktp = isset($params['ktp'])?$params['ktp']:'';
        $alamatkantor = isset($params['alamatkantor'])?$params['alamatkantor']:'';
        $telpkantor = isset($params['telpkantor'])?$params['telpkantor']:'';
        $faxkantor = isset($params['faxkantor'])?$params['faxkantor']:'';
        $alamatrumah = isset($params['alamatrumah'])?$params['alamatrumah']:'';
        $id_propinsi = isset($params['id_propinsi'])?$params['id_propinsi']:'';
        $id_kota = isset($params['id_kota'])?$params['id_kota']:'';
        $telp = isset($params['telp'])?$params['telp']:'';
        $hp = isset($params['hp'])?$params['hp']:'';
        $status = isset($params['status'])?$params['status']:'';
        $created_at = isset($params['created_at'])?$params['created_at']:'';
        $statuskawin = isset($params['statuskawin'])?$params['statuskawin']:'';
        $path_ijazah_old = isset($params['path_ijazah_old'])?$params['path_ijazah_old']:'';
        $path_ktp_old = isset($params['path_ktp_old'])?$params['path_ktp_old']:'';
        $foto_old = isset($params['foto_old'])?$params['foto_old']:'';

        if ($_FILES['path_ijazah']['size']<>0) {
        	$upload_image_ijazah = $this->uploadImageIjazah();
        	if (file_exists($this->webconfig['media-path-images'].$path_ijazah_old)){
	            @unlink($this->webconfig['media-path-images'].$path_ijazah_old);
	        }
        }

        if ($_FILES['path_ktp']['size']<>0) {
        	$upload_image_ktp = $this->uploadImageKtp();
        	if (file_exists($this->webconfig['media-path-images'].$path_ktp_old)){
	            @unlink($this->webconfig['media-path-images'].$path_ktp_old);
	        }
        }

        if ($_FILES['foto']['size']<>0) {
        	$upload_image_foto = $this->uploadImageFoto();
        	if (file_exists($this->webconfig['media-path-images'].$foto_old)){
	            @unlink($this->webconfig['media-path-images'].$foto_old);
	        }
        }
		

		if(isset($upload_image_ijazah["error"])) {
            if($upload_image_ijazah["error"] == "failed_ext") {
                return "failed_ext";
            }
            if($upload_image_ijazah["error"] == "empty_file") {
                return "empty_file";
            }
        }

        if(isset($upload_image_ktp["error"])) {
            if($upload_image_ktp["error"] == "failed_ext") {
                return "failed_ext";
            }
            if($upload_image_ktp["error"] == "empty_file") {
                return "empty_file";
            }
        }

        if(isset($upload_image_foto["error"])) {
            if($upload_image_foto["error"] == "failed_ext") {
                return "failed_ext";
            }
            if($upload_image_foto["error"] == "empty_file") {
                return "empty_file";
            }
        }

        $path_ijazah = isset($upload_image_ijazah["path_ijazah"])?$upload_image_ijazah["path_ijazah"]:"";
        $path_ktp = isset($upload_image_ktp["path_ktp"])?$upload_image_ktp["path_ktp"]:"";
        $fotopath = isset($upload_image_foto["foto"])?$upload_image_foto["foto"]:"";

        if($password != ''){
        	$sql_user = "
					nama = '".$this->db->escape_str($nama)."'
					,username = '".$this->db->escape_str($username)."'
					,email = '".$this->db->escape_str($email)."'
					,password = '".$this->db->escape_str(md5($password))."'
					,tgllahir = '".date("Y-m-d", strtotime($tgllahir))."'
					,jumanak = '".$this->db->escape_str($jumanak)."'
					,ktp = '".$this->db->escape_str($ktp)."'
					,alamatkantor = '".$this->db->escape_str($alamatkantor)."'
					,telpkantor = '".$this->db->escape_str($telpkantor)."'
					,faxkantor = '".$this->db->escape_str($faxkantor)."'
					,alamatrumah = '".$this->db->escape_str($alamatrumah)."'
					,id_propinsi = '".$this->db->escape_str($id_propinsi)."'
					,id_kota = '".$this->db->escape_str($id_kota)."'
					,telp = '".$this->db->escape_str($telp)."'
					,hp = '".$this->db->escape_str($hp)."'
					,status = '".$this->db->escape_str($status)."'			
					,statuskawin = '".$this->db->escape_str($statuskawin)."'			
					,created_at = '".date("Y-m-d", strtotime($created_at))."'
				";
        }else{
        	$sql_user = "
					nama = '".$this->db->escape_str($nama)."'
					,username = '".$this->db->escape_str($username)."'
					,email = '".$this->db->escape_str($email)."'
					,tgllahir = '".date("Y-m-d", strtotime($tgllahir))."'
					,jumanak = '".$this->db->escape_str($jumanak)."'
					,ktp = '".$this->db->escape_str($ktp)."'
					,alamatkantor = '".$this->db->escape_str($alamatkantor)."'
					,telpkantor = '".$this->db->escape_str($telpkantor)."'
					,faxkantor = '".$this->db->escape_str($faxkantor)."'
					,alamatrumah = '".$this->db->escape_str($alamatrumah)."'
					,id_propinsi = '".$this->db->escape_str($id_propinsi)."'
					,id_kota = '".$this->db->escape_str($id_kota)."'
					,telp = '".$this->db->escape_str($telp)."'
					,hp = '".$this->db->escape_str($hp)."'
					,status = '".$this->db->escape_str($status)."'			
					,statuskawin = '".$this->db->escape_str($statuskawin)."'			
					,created_at = '".date("Y-m-d", strtotime($created_at))."'
				";
        }
		
		if ($path_ijazah != '') {
			$sql_user .= ",path_ijazah = '".$this->db->escape_str($path_ijazah)."'";
		}

		if ($path_ktp != '') {
			$sql_user .= ",path_ktp = '".$this->db->escape_str($path_ktp)."'";
		}

		if ($fotopath != '') {
			$sql_user .= ",foto = '".$this->db->escape_str($fotopath)."'";
		}
		
		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");

		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function listDataPendidikan($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id ASC";

		if($id != '') {
			$conditional = "WHERE id_members = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_members_pendidikan."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function updateDataPendidikan($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$idmember = isset($params["idmember"])?$params["idmember"]:'';
		$tingkat = isset($params["tingkat"])?$params["tingkat"]:'';
		$pt = isset($params["pt"])?$params["pt"]:'';
		$jurusan = isset($params["jurusan"])?$params["jurusan"]:'';
		$gelar = isset($params["gelar"])?$params["gelar"]:'';
		$thn_lulus = isset($params["thn_lulus"])?$params["thn_lulus"]:'';
		$jenis_pendidikan = isset($params["jenis_pendidikan"])?$params["jenis_pendidikan"]:'';

			
		if (count($idmember) > 0) {
        	for ($si = 0; $si < count($idmember); $si++) {
        		$sql_detail = "
							tingkat = '".$this->db->escape_str($tingkat[$si])."'
							,pt = '".$this->db->escape_str($pt[$si])."'
							,jurusan_2 = '".$this->db->escape_str($jurusan[$si])."'
							,gelar = '".$this->db->escape_str($gelar[$si])."'
							,thn_lulus = '".$this->db->escape_str($thn_lulus[$si])."'
						";
        		$doUpdate = $this->db->query("
		    	REPLACE INTO ".$this->table_members_pendidikan."
				    (id, tingkat, pt, jurusan_2, gelar, thn_lulus, id_members, jenis)
				VALUES
				    (
				    	'".(isset($idmember[$si])?$this->db->escape_str($idmember[$si]):'')."',
				    	'".(isset($tingkat[$si])?$this->db->escape_str($tingkat[$si]):'')."',
				    	'".(isset($pt[$si])?$this->db->escape_str($pt[$si]):'')."',
				    	'".(isset($jurusan[$si])?$this->db->escape_str($jurusan[$si]):'')."',
				    	'".(isset($gelar[$si])?$this->db->escape_str($gelar[$si]):'')."',
				    	'".(isset($thn_lulus[$si])?$this->db->escape_str($thn_lulus[$si]):'')."',
				    	'".(isset($id)?$id:'')."',
				    	'".(isset($jenis_pendidikan[$si])?$this->db->escape_str($jenis_pendidikan[$si]):'')."'
				    )
				
				");
        	}
        }

		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteDataPendidikan($id){
		if($id == 0) return 'failed';

		$doDelete = $this->db->query("
		DELETE FROM ".$this->table_members_pendidikan."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function listDataPekerjaan($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id ASC";

		if($id != '') {
			$conditional = "WHERE id_members = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_members_pekerjaan."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		$result = $this->getInstansi($result);
		return $result;
	}

	public function updateDataPekerjaan($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$id_pekerjaan = isset($params["id_pekerjaan"])?$params["id_pekerjaan"]:'';
		$idmember = isset($params["idmember"])?$params["idmember"]:'';
		$nama = isset($params["nama"])?$params["nama"]:'';
		$jabatan = isset($params["jabatan"])?$params["jabatan"]:'';
		$mulai = isset($params["mulai"])?$params["mulai"]:'';
		$akhir = isset($params["akhir"])?$params["akhir"]:'';
		$id_instansi = isset($params["id_instansi"])?$params["id_instansi"]:'';

		if (count($idmember) > 0) {
        	for ($si = 0; $si < count($idmember); $si++) {
      //   		$sql_detail = "
						// 	jabatan = '".$this->db->escape_str($jabatan[$si])."'
						// 	,mulai = '".$this->db->escape_str($mulai[$si])."'
						// 	,akhir = '".$this->db->escape_str($akhir[$si])."'
						// 	,id_pekerjaan = '".$this->db->escape_str($id_pekerjaan[$si])."'
						// 	,id_instansi = '".$this->db->escape_str($id_instansi[$si])."'
						// ";
        		$doUpdate = $this->db->query("
		    	REPLACE INTO ".$this->table_members_pekerjaan."
				    (id ,nama, jabatan, mulai, akhir, id_members, id_pekerjaan, id_instansi)
				VALUES
				    (
				    	'".(isset($id[$si])?$this->db->escape_str($id[$si]):'')."',
				    	'".(isset($nama[$si])?$this->db->escape_str($nama[$si]):'')."',
				    	'".(isset($jabatan[$si])?$this->db->escape_str($jabatan[$si]):'')."',
				    	'".(isset($mulai[$si])?$this->db->escape_str($mulai[$si]):'')."',
				    	'".(isset($akhir[$si])?$this->db->escape_str($akhir[$si]):'')."',
				    	'".(isset($idmember[$si])?$idmember[$si]:'')."',
				    	'".(isset($id_pekerjaan[$si])?$this->db->escape_str($id_pekerjaan[$si]):'')."',
				    	'".(isset($id_instansi[$si])?$this->db->escape_str($id_instansi[$si]):'')."'
				    )
				
				");
        	}
        }

		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteDataPekerjaan($id){
		if($id == 0) return 'failed';

		$doDelete = $this->db->query("
		DELETE FROM ".$this->table_members_pekerjaan."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function listDataPraktek($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id ASC";

		if($id != '') {
			$conditional = "WHERE id_members = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_members_praktek."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function updateDataPraktek($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$idmember = isset($params["idmember"])?$params["idmember"]:'';
		$setting = isset($params["setting"])?$params["setting"]:'';
		$tahun = isset($params["tahun"])?$params["tahun"]:'';
		$jabatan = isset($params["jabatan"])?$params["jabatan"]:'';
		$peran = isset($params["peran"])?$params["peran"]:'';
		$keahlian = isset($params["keahlian"])?$params["keahlian"]:'';
		
		if (count($idmember) > 0) {
        	for ($si = 0; $si < count($idmember); $si++) {
        		$sql_detail = "
							setting = '".$this->db->escape_str($setting[$si])."'
							,tahun = '".$this->db->escape_str($tahun[$si])."'
							,jabatan = '".$this->db->escape_str($jabatan[$si])."'
							,peran = '".$this->db->escape_str($peran[$si])."'
							,keahlian = '".$this->db->escape_str($keahlian[$si])."'
						";
        		$doUpdate = $this->db->query("
		    	INSERT INTO ".$this->table_members_praktek."
				    (id, setting, tahun, jabatan, peran, keahlian, id_members)
				VALUES
				    (
				    	'".(isset($idmember)?$this->db->escape_str($idmember[$si]):'')."',
				    	'".(isset($setting)?$this->db->escape_str($setting[$si]):'')."',
				    	'".(isset($tahun)?$this->db->escape_str($tahun[$si]):'')."',
				    	'".(isset($jabatan)?$this->db->escape_str($jabatan[$si]):'')."',
				    	'".(isset($peran)?$this->db->escape_str($peran[$si]):'')."',
				    	'".(isset($keahlian)?$this->db->escape_str($keahlian[$si]):'')."',
				    	'".(isset($id)?$id:'')."'
				    )
				ON DUPLICATE KEY UPDATE
				    ".$sql_detail."
				");
        	}
        }

		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteDataPraktek($id){
		if($id == 0) return 'failed';

		$doDelete = $this->db->query("
		DELETE FROM ".$this->table_members_praktek."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function listDataPenghargaan($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id ASC";

		if($id != '') {
			$conditional = "WHERE id_members = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_members_penghargaan."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function updateDataPenghargaan($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$idmember = isset($params["idmember"])?$params["idmember"]:'';
		$nama = isset($params["nama"])?$params["nama"]:'';
		$instansi = isset($params["instansi"])?$params["instansi"]:'';
		$tahun = isset($params["tahun"])?$params["tahun"]:'';
		
		if (count($idmember) > 0) {
        	for ($si = 0; $si < count($idmember); $si++) {
        		$sql_detail = "
							nama = '".$this->db->escape_str($nama[$si])."'
							,instansi = '".$this->db->escape_str($instansi[$si])."'
							,tahun = '".$this->db->escape_str($tahun[$si])."'
						";
        		$doUpdate = $this->db->query("
		    	INSERT INTO ".$this->table_members_penghargaan."
				    (id, nama, instansi, tahun, id_members)
				VALUES
				    (
				    	'".(isset($idmember)?$this->db->escape_str($idmember[$si]):'')."',
				    	'".(isset($nama)?$this->db->escape_str($nama[$si]):'')."',
				    	'".(isset($instansi)?$this->db->escape_str($instansi[$si]):'')."',
				    	'".(isset($tahun)?$this->db->escape_str($tahun[$si]):'')."',
				    	'".(isset($id)?$id:'')."'
				    )
				ON DUPLICATE KEY UPDATE
				    ".$sql_detail."
				");
        	}
        }

		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteDataPenghargaan($id){
		if($id == 0) return 'failed';

		$doDelete = $this->db->query("
		DELETE FROM ".$this->table_members_penghargaan."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function listDataSertifikasi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id ASC";

		if($id != '') {
			$conditional = "WHERE id_members = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_members_sertifikasi."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function updateDataSertifikasi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$idmember = isset($params["idmember"])?$params["idmember"]:'';
		$nama = isset($params["nama"])?$params["nama"]:'';
		$lembaga = isset($params["lembaga"])?$params["lembaga"]:'';
		$tahun = isset($params["tahun"])?$params["tahun"]:'';
		$tipe = isset($params["tipe"])?$params["tipe"]:'';
		
		if (count($idmember) > 0) {
        	for ($si = 0; $si < count($idmember); $si++) {
        		$sql_detail = "
							nama = '".$this->db->escape_str($nama[$si])."'
							,lembaga = '".$this->db->escape_str($lembaga[$si])."'
							,tahun = '".$this->db->escape_str($tahun[$si])."'
							,tipe = '".$this->db->escape_str($tipe[$si])."'
							,id_members = '".$this->db->escape_str($idmember[$si])."'
						";

        		$doUpdate = $this->db->query("
		    	REPLACE INTO ".$this->table_members_sertifikasi."
				    (id,nama, lembaga, tahun, tipe, id_members)
				VALUES
				    (
				    	'".(isset($id[$si])?$this->db->escape_str($id[$si]):'')."',
				    	'".(isset($nama[$si])?$this->db->escape_str($nama[$si]):'')."',
				    	'".(isset($lembaga[$si])?$this->db->escape_str($lembaga[$si]):'')."',
				    	'".(isset($tahun[$si])?$this->db->escape_str($tahun[$si]):'')."',
				    	'".(isset($tipe[$si])?$this->db->escape_str($tipe[$si]):'')."',
				    	'".(isset($idmember[$si])?$idmember[$si]:'')."'
				    )
				
				");
        	}
        }

		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteDataSertifikasi($id){
		if($id == 0) return 'failed';

		$doDelete = $this->db->query("
		DELETE FROM ".$this->table_members_sertifikasi."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function listDataReferensi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id ASC";

		if($id != '') {
			$conditional = "WHERE id_members = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_members_referensi."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function updateDataReferensi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$idmember = isset($params["idmember"])?$params["idmember"]:'';
		$nama = isset($params["nama"])?$params["nama"]:'';
		$jabatan = isset($params["jabatan"])?$params["jabatan"]:'';
		$telp = isset($params["telp"])?$params["telp"]:'';
		
		if (count($idmember) > 0) {
        	for ($si = 0; $si < count($idmember); $si++) {
        		$sql_detail = "
							nama = '".$this->db->escape_str($nama[$si])."'
							,jabatan = '".$this->db->escape_str($jabatan[$si])."'
							,telp = '".$this->db->escape_str($telp[$si])."'
						";
        		$doUpdate = $this->db->query("
		    	INSERT INTO ".$this->table_members_referensi."
				    (id, nama, jabatan, telp, id_members)
				VALUES
				    (
				    	'".(isset($idmember)?$this->db->escape_str($idmember[$si]):'')."',
				    	'".(isset($nama)?$this->db->escape_str($nama[$si]):'')."',
				    	'".(isset($jabatan)?$this->db->escape_str($jabatan[$si]):'')."',
				    	'".(isset($telp)?$this->db->escape_str($telp[$si]):'')."',
				    	'".(isset($id)?$id:'')."'
				    )
				ON DUPLICATE KEY UPDATE
				    ".$sql_detail."
				");
        	}
        }

		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteDataReferensi($id){
		if($id == 0) return 'failed';

		$doDelete = $this->db->query("
		DELETE FROM ".$this->table_members_referensi."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function listDataProfesi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id ASC";

		if($id != '') {
			$conditional = "WHERE id_members = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_members_profesi."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function updateDataProfesi($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$idmember = isset($params["idmember"])?$params["idmember"]:'';
		$nama = isset($params["nama"])?$params["nama"]:'';
		$tahun = isset($params["tahun"])?$params["tahun"]:'';
		$keterangan = isset($params["keterangan"])?$params["keterangan"]:'';
		
		if (count($idmember) > 0) {
        	for ($si = 0; $si < count($idmember); $si++) {
        		$sql_detail = "
							nama = '".$this->db->escape_str($nama[$si])."'
							,tahun = '".$this->db->escape_str($tahun[$si])."'
							,keterangan = '".$this->db->escape_str($keterangan[$si])."'
						";
        		$doUpdate = $this->db->query("
		    	INSERT INTO ".$this->table_members_profesi."
				    (id, nama, tahun, keterangan, id_members)
				VALUES
				    (
				    	'".(isset($idmember)?$this->db->escape_str($idmember[$si]):'')."',
				    	'".(isset($nama)?$this->db->escape_str($nama[$si]):'')."',
				    	'".(isset($tahun)?$this->db->escape_str($tahun[$si]):'')."',
				    	'".(isset($keterangan)?$this->db->escape_str($keterangan[$si]):'')."',
				    	'".(isset($id)?$id:'')."'
				    )
				ON DUPLICATE KEY UPDATE
				    ".$sql_detail."
				");
        	}
        }

		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteDataProfesi($id){
		if($id == 0) return 'failed';

		$doDelete = $this->db->query("
		DELETE FROM ".$this->table_members_profesi."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function listDataKomunitas($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id ASC";

		if($id != '') {
			$conditional = "WHERE id_members = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->table_members_komunitas."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function updateDataKomunitas($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$idmember = isset($params["idmember"])?$params["idmember"]:'';
		$nama = isset($params["nama"])?$params["nama"]:'';
		$alamat = isset($params["alamat"])?$params["alamat"]:'';
		
		if (count($idmember) > 0) {
        	for ($si = 0; $si < count($idmember); $si++) {
        		$sql_detail = "
							nama = '".$this->db->escape_str($nama[$si])."'
							,alamat = '".$this->db->escape_str($alamat[$si])."'
						";
        		$doUpdate = $this->db->query("
		    	INSERT INTO ".$this->table_members_komunitas."
				    (id, nama, alamat, id_members)
				VALUES
				    (
				    	'".(isset($idmember)?$this->db->escape_str($idmember[$si]):'')."',
				    	'".(isset($nama)?$this->db->escape_str($nama[$si]):'')."',
				    	'".(isset($alamat)?$this->db->escape_str($alamat[$si]):'')."',
				    	'".(isset($id)?$id:'')."'
				    )
				ON DUPLICATE KEY UPDATE
				    ".$sql_detail."
				");
        	}
        }

		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteDataKomunitas($id){
		if($id == 0) return 'failed';

		$doDelete = $this->db->query("
		DELETE FROM ".$this->table_members_komunitas."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_members')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function doPublish($id){
		if($id == 0) return 'failed';
		
		$doPublish = $this->db->query("
			UPDATE ".$this->maintablename."
			SET status = '1'
			WHERE 
				id = ".$id."
		");
		if($doPublish){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_publish_members')." id = ".$id.""));		
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function doUnublish($id){
		if($id == 0) return 'failed';
		
		$doUnublish = $this->db->query("
			UPDATE ".$this->maintablename."
			SET status = '0'
			WHERE 
				id = ".$id."
		");
		if($doUnublish){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_unpublish_members')." id = ".$id.""));		
			return 'success';
		}else{
			return 'failed';
		}
	}

	private function uploadImageIjazah() {
        $this->ci->load->helper('upload');
        if ($_FILES['path_ijazah']['size']<>0){
            $data = array();

            $tmpName = $_FILES['path_ijazah']['tmp_name']; 
            list($width, $height, $type, $attr) = getimagesize($tmpName);
            
            $ext_thumb = explode('.', $_FILES['path_ijazah']['name']);
            $extension_thumb = strtoupper(end($ext_thumb));
            if (!(($extension_thumb == 'JPEG') || ($extension_thumb == 'JPG') || ($extension_thumb == 'PNG') || ($extension_thumb == 'GIF'))){
                $data['error'] = "failed_ext";
            }else{
            	# UPLOAD #
	            $pathdir = date("Y/m/d");
	                               
	            $thepath_create = $this->webconfig['media-path-files'].$pathdir."/";
	            $exp = explode("/",$thepath_create);
	            $way = '';
	            foreach($exp as $n){
	                $way .= $n.'/';
	                @mkdir($way);       
	            }
	            
	            $pos = strripos($_FILES['path_ijazah']['name'], '.');
	            if($pos === false){
	                $ordinary_name = $_FILES['path_ijazah']['name'];
	            }else{
	                $ordinary_name = substr($_FILES['path_ijazah']['name'], 0, $pos);
	            }

	            $name_upload = date("Y_m_d-H_i_s")."_".md5($ordinary_name);
	            $name_upload_c = $name_upload.'.'.strtolower($extension_thumb);
	            
	            $result = was_upload_file('path_ijazah', $name_upload, false, $thepath_create."/", array(), array(), array());

	            if ($result['status'] > 0){
	                $data['path_ijazah'] = "";
	            }else{
	                $path1 = $pathdir."/".$name_upload_c;
	                $data['path_ijazah'] = $path1;
	            }
            }
        }else{
            $data['error'] = "empty_file";
        }

        return $data;
    }

    private function uploadImageFoto() {
        $this->ci->load->helper('upload');
        if ($_FILES['foto']['size']<>0){
            $data = array();

            $tmpName = $_FILES['foto']['tmp_name']; 
            list($width, $height, $type, $attr) = getimagesize($tmpName);
            
            $ext_thumb = explode('.', $_FILES['foto']['name']);
            $extension_thumb = strtoupper(end($ext_thumb));
            if (!(($extension_thumb == 'JPEG') || ($extension_thumb == 'JPG') || ($extension_thumb == 'PNG') || ($extension_thumb == 'GIF'))){
                $data['error'] = "failed_ext";
            }else{
            	# UPLOAD #
	            $pathdir = date("Y/m/d");
	                               
	            $thepath_create = $this->webconfig['media-path-files'].$pathdir."/";
	            $exp = explode("/",$thepath_create);
	            $way = '';
	            foreach($exp as $n){
	                $way .= $n.'/';
	                @mkdir($way);       
	            }
	            
	            $pos = strripos($_FILES['foto']['name'], '.');
	            if($pos === false){
	                $ordinary_name = $_FILES['foto']['name'];
	            }else{
	                $ordinary_name = substr($_FILES['foto']['name'], 0, $pos);
	            }

	            $name_upload = date("Y_m_d-H_i_s")."_".md5($ordinary_name);
	            $name_upload_c = $name_upload.'.'.strtolower($extension_thumb);
	            
	            $result = was_upload_file('foto', $name_upload, false, $thepath_create."/", array(), array(), array());

	            if ($result['status'] > 0){
	                $data['foto'] = "";
	            }else{
	                $path1 = $pathdir."/".$name_upload_c;
	                $data['foto'] = $path1;
	            }
            }
        }else{
            $data['error'] = "empty_file";
        }

        return $data;
    }

    private function uploadImageKtp() {
        $this->ci->load->helper('upload');
        if ($_FILES['path_ktp']['size']<>0){
            $data = array();

            $tmpName = $_FILES['path_ktp']['tmp_name']; 
            list($width, $height, $type, $attr) = getimagesize($tmpName);
            
            $ext_thumb = explode('.', $_FILES['path_ktp']['name']);
            $extension_thumb = strtoupper(end($ext_thumb));
            if (!(($extension_thumb == 'JPEG') || ($extension_thumb == 'JPG') || ($extension_thumb == 'PNG') || ($extension_thumb == 'GIF'))){
                $data['error'] = "failed_ext";
            }else{
            	# UPLOAD #
	            $pathdir = date("Y/m/d");
	                               
	            $thepath_create = $this->webconfig['media-path-files'].$pathdir."/";
	            $exp = explode("/",$thepath_create);
	            $way = '';
	            foreach($exp as $n){
	                $way .= $n.'/';
	                @mkdir($way);       
	            }
	            
	            $pos = strripos($_FILES['path_ktp']['name'], '.');
	            if($pos === false){
	                $ordinary_name = $_FILES['path_ktp']['name'];
	            }else{
	                $ordinary_name = substr($_FILES['path_ktp']['name'], 0, $pos);
	            }

	            $name_upload = date("Y_m_d-H_i_s")."_".md5($ordinary_name);
	            $name_upload_c = $name_upload.'.'.strtolower($extension_thumb);
	            
	            $result = was_upload_file('path_ktp', $name_upload, false, $thepath_create."/", array(), array(), array());

	            if ($result['status'] > 0){
	                $data['path_ktp'] = "";
	            }else{
	                $path1 = $pathdir."/".$name_upload_c;
	                $data['path_ktp'] = $path1;
	            }
            }
        }else{
            $data['error'] = "empty_file";
        }

        return $data;
    }

    public function InsertNoAnggota($params = array()){
    	$id = isset($params['id'])?$params['id']:'';
    	$noanggota = isset($params['noanggota'])?$params['noanggota']:'';
    	$noanggota_input = isset($params['noanggota_input'])?$params['noanggota_input']:'';
    	$cekdata = $this->checkNoAnggota($noanggota.$noanggota_input);

    	if($cekdata['jumnoanggota'] == 0){
    		$doUpdatenoAnggota = $this->db->query("
    		UPDATE members set no_anggota = '".$noanggota.$noanggota_input."' WHERE id = ".$id."
    		");
	    	if ($doUpdatenoAnggota) {
	    		return 'success';
	    	}else{
	    		return 'false';
	    	}
    	}else{
    		return 'exist';
    	}
    	
    }

    public function checkNoAnggota($no_anggota){
    	$q = $this->db->query("
			SELECT
				count(id) AS jumnoanggota
			FROM
				".$this->maintablename."
			WHERE no_anggota = '".$no_anggota."'
		");
		$result = $q->first_row('array');
		return $result;
    }

    public function getInstansi($result){
    	if (count($result) > 0) {
    		foreach ($result as $key => $value) {
    			if ($value['id_instansi'] != 0) {
    				$result[$key]['instansi'] = $this->db->query("
    					SELECT name FROM instansi WHERE id = ".$value['id_instansi']."
    					")->first_row('array');
    			}
    		}
    	}

    	return $result;
    }

    public function updateAvatar($params=array()){
		$id = isset($params["id"])?$params["id"]:'';

		$x1     = isset($params["x1"])?$params["x1"]:'';
        $y1     = isset($params["y1"])?$params["y1"]:'';
        $x2      = isset($params["x2"])?$params["x2"]:'';
        $y2      = isset($params["y2"])?$params["y2"]:'';
        $width_resize      = isset($params["width_resize"])?$params["width_resize"]:'';

        if ($_FILES['path']['size']<>0){
            $getData = $this->listData(array('id' => $id));     
            if(count($getData) > 0) {
                $pfile = $this->webconfig['media-path-avatar'].$getData[0]['path'];
                if (file_exists($pfile)){
                    @unlink($pfile);
                }
                $delete_cropresize = $this->del_cropresizeAvatar($getData[0]['path']);
            }
        }

        $upload_image = $this->uploadImageAvatar($width_resize, $x1, $y1, $x2, $y2);

        if(isset($upload_image["error"])) {
            if($upload_image["error"] == "failed_ext") {
                return "failed_ext";
            }
        }
       
        $path = isset($upload_image["path"])?$upload_image["path"]:"";
		
		$sql_user = "";
		
		/* jika update gambar baru */
        if ($_FILES['path']['size']<>0) $sql_user .= " path_avatar = '".$this->db->escape_str($path)."'";

		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			$this->session->set_userdata('avatar', $path);
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_berita')." id = ".$id.", title = ".htmlentities($title).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	private function uploadImageAvatar($width_resize, $x1, $y1, $x2, $y2) {
        $this->ci->load->helper('upload');
        if ($_FILES['path']['size']<>0){
            $data = array();

            $tmpName = $_FILES['path']['tmp_name']; 
            list($width, $height, $type, $attr) = getimagesize($tmpName);
            # Rasio Ukuran avatar #
            $rasio = $width/$width_resize;
            $expfile = explode('.', $_FILES['path']['name']);
            $extension_thumb = strtoupper(end($expfile));
            if (!(($extension_thumb == 'JPEG') || ($extension_thumb == 'JPG') || ($extension_thumb == 'PNG') || ($extension_thumb == 'GIF'))){
                $data['error'] = "failed_ext";
            }
            
            # UPLOAD #
            $pathdir = date("Y/m/d");
            $thepath = $this->webconfig['media-path-avatar'].$pathdir."/";                       
            $thepath_create = $this->webconfig['media-path-avatar'].$pathdir."/temp/";                       
            $exp = explode("/",$thepath_create);
            $way = '';
            foreach($exp as $n){
                $way .= $n.'/';
                @mkdir($way);       
            }
            
            $pos = strripos($_FILES['path']['name'], '.');
            if($pos === false){
                $ordinary_name = $_FILES['path']['name'];
            }else{
                $ordinary_name = substr($_FILES['path']['name'], 0, $pos);
            }

            $name_upload = date("Y_m_d-H_i_s")."_".md5($ordinary_name);
            $name_upload_c = $name_upload.'.'.strtolower($extension_thumb);
            
            $result = was_upload_file('path', $name_upload, false, $thepath_create."/", array(), array(), array());

            if ($result['status'] > 0){
                $data['path'] = "";
            }else{
                $path1 = $pathdir."/".$name_upload_c;
                $data['path'] = $path1;
                if(isset($rasio) && $rasio !=''){
                    $finalcrop_resize = $this->croping_resizeAvatar($result,$x1,$y1,$x2,$y2,$rasio,$thepath);
                }
            }
        }else{
            $data['error'] = "empty_file";
        }

        return $data;
    }
    private function croping_resizeAvatar($result,$x1,$y1,$x2,$y2,$rasio,$image_path) {
        
        # RESIZE and CROP #
        $thepath = $this->webconfig['media-path-avatar-thumb'].date("Y/m/d")."/";                       
        $exp = explode("/",$thepath);
        $way ='';
        foreach($exp as $n){
            $way.=$n.'/';
            @mkdir($way);       
        }
        
        $file_name = $result['name'];
        $src_path = $image_path . 'temp/' . $file_name;
        
        $exp_file = explode('.', $file_name, 2);
        
        $des_path =  $image_path .$file_name;
        $xx1 = $x1 * $rasio;
        $yy1 = $y1 * $rasio;
        $xx2 = $x2 * $rasio;
        $yy2 = $y2 * $rasio;
        if($des_path !=''){
            $this->image_moo
                ->load($src_path)
                ->crop($xx1,$yy1,$xx2,$yy2)
                ->save($des_path);
        }

        ## RESIZE IMAGE ##
        foreach($this->webconfig['image_avatar'] as $key => $value){
            $size_file = explode('x',$value);
            $file_final_name_resize = $exp_file[0].'_'.$value.'_thumb.'.$exp_file[1];
            $des_path_resize =  $thepath . $file_final_name_resize;
            $this->image_moo
                ->load($des_path)
                ->stretch($size_file[0],$size_file[1])
                ->save($des_path_resize);
        }

        /* Delete Temp File */
        if (file_exists($src_path)){
            @unlink($src_path);
        }
    }
    private function del_cropresizeAvatar($cfile) {
        if($cfile == "") return false;
        
        $explod_file = explode("/",$cfile);
        $filename = end($explod_file);
        $exp_file = explode(".",$filename);
        $exp_thumb_path = explode("/", $this->webconfig['media-path-avatar-thumb'].$cfile, -1);
        $thumb_path = implode("/", $exp_thumb_path);
        
        foreach($this->webconfig['image_avatar'] as $key => $value){
            $delete_resize = $thumb_path.'/'.$exp_file[0]."_".$value."_thumb.".$exp_file[1];
            if (file_exists($delete_resize)){
                @unlink($delete_resize);
            }
        }
    }

    public function insertPayment($params = array()){
    	$id_member = isset($params['id_member'])?$params['id_member']:'';
    	$price = isset($params['price'])?$params['price']:'';
    	$charge_id = isset($params['charge_id'])?$params['charge_id']:'';
        
        $data['id_member'] = $this->db->escape_str($id_member);
		$data['price'] = $this->db->escape_str($price);
		$data['total_price'] = $this->db->escape_str($price);
        $data['charge_id'] = $this->db->escape_str($charge_id);
        $data['date_created'] = date("Y-m-d H:i:s");
        $data['status'] = 0;
        
		$doInsert = $this->db->insert($this->table_payments, $data);
		if ($doInsert) {
			return true;
		}else{
			return false;
		}
    }
}
