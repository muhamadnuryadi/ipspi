<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class KebijakanModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "materi";
	}
	public function entriData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$status = isset($params["status"])?$params["status"]:'';
        
        if ($_FILES['files']['size']<>0){
			$arr_path = explode('.', $_FILES['files']['name']);
			$extension_thumb = strtoupper(end($arr_path));

			if (!(($extension_thumb == 'PDF')))
			{
				return 'failed_ext';
			}else if($_FILES['files']['size'] > $this->webconfig['max_file_size'] * 1000000){
				return 'failed_filesize';
			}
		}

        if ($_FILES['files']['size']<>0){
			$arr_path = explode('.', $_FILES['files']['name']);
			$extension_thumb = strtoupper(end($arr_path));

			if (!(($extension_thumb == 'JPG') || ($extension_thumb == 'JPEG') || ($extension_thumb == 'PNG') || ($extension_thumb == 'PDF')))
			{
				return 'failed_ext';
			}else if($_FILES['files']['size'] > $this->webconfig['max_file_size'] * 1000000){
				return 'failed_filesize';
			}else{
				

				# UPLOAD #
				$pathdir = date("Y/m/d");
	            $thepath = $this->webconfig['media-path-files'].$pathdir."/";                    
	            $exp = explode("/",$thepath);
	            $way = '';
	            foreach($exp as $n){
	                $way .= $n.'/';
	                @mkdir($way);       
	            }

				## FILENAME ##
	            $pos = strripos($_FILES['files']['name'], '.');
	            if($pos === false){
	                $ordinary_name = $_FILES['files']['name'];
	            }else{
	                $ordinary_name = substr($_FILES['files']['name'], 0, $pos);
	            }

	            $name_upload = date("Y_m_d-H_i_s")."_".md5($ordinary_name);
	            $name_upload_c = $name_upload.'.'.strtolower($extension_thumb);

				$result = was_upload_file('files', $name_upload, false, $thepath."/", array(), array(), array());

				$path1 = date("Y/m/d")."/".$name_upload_c;
				$data['files'] = $path1;
			}
		} else {
			return 'empty_file';
		}

		$data ['name'] = $this->db->escape_str($name);
		$data ['status'] = $this->db->escape_str($status);
		$data ['tipe'] = 1;
		$data ['datecreated'] = date("Y-m-d H:i");
		
		$doInsert = $this->db->insert($this->maintablename, $data);
		if($doInsert){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_entry_kebijakan')." dengan judul = ".htmlentities($name).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function updateData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$name = isset($params["name"])?$params["name"]:'';
		$status = isset($params["status"])?$params["status"]:'';

        if ($_FILES['files']['size']<>0){
			$arr_path = explode('.', $_FILES['files']['name']);
			$extension_thumb = strtoupper(end($arr_path));

			if (!(($extension_thumb == 'JPG') || ($extension_thumb == 'JPEG') || ($extension_thumb == 'PNG') || ($extension_thumb == 'PDF')))
			{
				return 'failed_ext';
			}else if($_FILES['files']['size'] > $this->webconfig['max_file_size'] * 1000000){
				return 'failed_filesize';
			}else{
				

				# UPLOAD #
				$pathdir = date("Y/m/d");
	            $thepath = $this->webconfig['media-path-files'].$pathdir."/";                    
	            $exp = explode("/",$thepath);
	            $way = '';
	            foreach($exp as $n){
	                $way .= $n.'/';
	                @mkdir($way);       
	            }

				## FILENAME ##
	            $pos = strripos($_FILES['files']['name'], '.');
	            if($pos === false){
	                $ordinary_name = $_FILES['files']['name'];
	            }else{
	                $ordinary_name = substr($_FILES['files']['name'], 0, $pos);
	            }

	            $name_upload = date("Y_m_d-H_i_s")."_".md5($ordinary_name);
	            $name_upload_c = $name_upload.'.'.strtolower($extension_thumb);

				$result = was_upload_file('files', $name_upload, false, $thepath."/", array(), array(), array());

				$path1 = date("Y/m/d")."/".$name_upload_c;
			}
		}
		
		$sql_user = "
					name = '".$this->db->escape_str($name)."'
					,status = '".$this->db->escape_str($status)."'
					,tipe = '1'
					,datemodified = '".date("Y-m-d H:i")."'
					";
		
		/* jika update file baru */
        if ($_FILES['files']['size']<>0) $sql_user .= ", files = '".$this->db->escape_str($path1)."'";

		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_kebijakan')." id = ".$id.", name = ".htmlentities($name).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteData($id){
		if($id == 0) return 'failed';
		$getData = $this->listData(array('id' => $id));
		if(isset($getData) && count($getData) > 0){
			foreach ($getData as $key => $value) {
				$pfile = $this->webconfig['media-path-kebijakan'].$value['files'];
				if (file_exists($pfile)){
					@unlink($pfile);
				}
			}
		}
		$doDelete = $this->db->query("
		DELETE FROM ".$this->maintablename."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_kebijakan')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}


    public function doPublish($id){
		if($id == 0) return 'failed';
		
		$doPublish = $this->db->query("
			UPDATE ".$this->maintablename."
			SET status = '1'
			WHERE 
				id = ".$id."
		");
		if($doPublish){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_publish_kebijakan')." id = ".$id.""));		
			return 'success';
		}else{
			return 'failed';
		}
	}
	public function doUnublish($id){
		if($id == 0) return 'failed';
		
		$doUnublish = $this->db->query("
			UPDATE ".$this->maintablename."
			SET status = '0'
			WHERE 
				id = ".$id."
		");
		if($doUnublish){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_unpublish_kebijakan')." id = ".$id.""));		
			return 'success';
		}else{
			return 'failed';
		}
	}
	public function filterData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional  = "";
		$rest  = "ORDER BY id DESC";
		if($name !=''){
			$conditional .= "AND name LIKE '%".$this->db->escape_str($name)."%'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE tipe = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function filterDataCount($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$conditional = '';
		$rest  = "ORDER BY id DESC";
		if($name !=''){
			$conditional .= "AND name LIKE '%".$this->db->escape_str($name)."%'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE tipe = 1
			".$conditional."
			".$rest."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE tipe = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listDataCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE tipe = 1
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}
    
}