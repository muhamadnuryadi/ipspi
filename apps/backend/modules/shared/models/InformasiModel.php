<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class InformasiModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "informasi";
	}
	public function entriData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$description = isset($params["description"])?$params["description"]:'';
		$slug = isset($params["slug"])?$params["slug"]:'';
		$body = isset($params["body"])?$params["body"]:'';
		$status = isset($params["status"])?$params["status"]:'';

		$x1     = isset($params["x1"])?$params["x1"]:'';
        $y1     = isset($params["y1"])?$params["y1"]:'';
        $x2      = isset($params["x2"])?$params["x2"]:'';
        $y2      = isset($params["y2"])?$params["y2"]:'';
        $width_resize      = isset($params["width_resize"])?$params["width_resize"]:'';

        /*$upload_image = $this->uploadImage($width_resize, $x1, $y1, $x2, $y2);

        if(isset($upload_image["error"])) {
            if($upload_image["error"] == "failed_ext") {
                return "failed_ext";
            }
            if($upload_image["error"] == "empty_file") {
                return "empty_file";
            }
        }*/

		$data ['id'] = "Null";
		$data ['title'] = $this->db->escape_str($name);
		$data ['description'] = $this->db->escape_str($description);
		$data ['slug'] = url_title($name,'-',TRUE);
		$data ['body'] = $this->db->escape_str($body);
		$data ['status'] = $this->db->escape_str($status);
		$data ['path'] = isset($upload_image["path"])?$upload_image["path"]:"";
		
		$doInsert = $this->db->insert($this->maintablename, $data);
		if($doInsert){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_entry_informasi')." dengan nama = ".htmlentities($name).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function updateData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$name = isset($params["name"])?$params["name"]:'';
		$description = isset($params["description"])?$params["description"]:'';
		$slug = isset($params["slug"])?$params["slug"]:'';
		$body = isset($params["body"])?$params["body"]:'';
		$status = isset($params["status"])?$params["status"]:'';

		$x1     = isset($params["x1"])?$params["x1"]:'';
        $y1     = isset($params["y1"])?$params["y1"]:'';
        $x2      = isset($params["x2"])?$params["x2"]:'';
        $y2      = isset($params["y2"])?$params["y2"]:'';
        $width_resize      = isset($params["width_resize"])?$params["width_resize"]:'';

        /*if ($_FILES['path']['size']<>0){
            $getData = $this->listData(array('id' => $id));     
            if(count($getData) > 0) {
                $pfile = $this->webconfig['media-path-informasi'].$getData[0]['path'];
                if (file_exists($pfile)){
                    @unlink($pfile);
                }
                $delete_cropresize = $this->del_cropresize($getData[0]['path']);
            }
        }

        $upload_image = $this->uploadImage($width_resize, $x1, $y1, $x2, $y2);

        if(isset($upload_image["error"])) {
            if($upload_image["error"] == "failed_ext") {
                return "failed_ext";
            }
        }*/
       
        $path = isset($upload_image["path"])?$upload_image["path"]:"";
		
		$sql_user = "
					title = '".$this->db->escape_str($name)."'
					,description = '".$this->db->escape_str($description)."'
					,body = '".$this->db->escape_str($body)."'
					,slug = '".$this->db->escape_str(url_title($name,'-',TRUE))."'
					,status = '".$this->db->escape_str($status)."'
					";
		
		/* jika update gambar baru */
        /*if ($_FILES['path']['size']<>0) $sql_user .= ", path = '".$this->db->escape_str($path)."'";*/

		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_informasi')." id = ".$id.", name = ".htmlentities($name).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteData($id){
		if($id == 0) return 'failed';
		$getData = $this->listData(array('id' => $id));
		if(isset($getData) && count($getData) > 0){
			foreach ($getData as $key => $value) {
				$pfile = $this->webconfig['media-path-informasi'].$value['path'];
				if (file_exists($pfile)){
					@unlink($pfile);
				}
				$exp_img = explode(".", $value['path']);
				foreach ($this->webconfig['image_informasi'] as $key => $image_informasi_data) {
					@unlink($this->webconfig['media-path-informasi-thumb'].$exp_img[0]."_".$image_informasi_data."_thumb.".$exp_img[1]);
				}
			}
		}
		$doDelete = $this->db->query("
		DELETE FROM ".$this->maintablename."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_informasi')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}


    public function doPublish($id){
		if($id == 0) return 'failed';
		
		$doPublish = $this->db->query("
			UPDATE ".$this->maintablename."
			SET status = '1'
			WHERE 
				id = ".$id."
		");
		if($doPublish){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_publish_informasi')." id = ".$id.""));		
			return 'success';
		}else{
			return 'failed';
		}
	}
	public function doUnublish($id){
		if($id == 0) return 'failed';
		
		$doUnublish = $this->db->query("
			UPDATE ".$this->maintablename."
			SET status = '0'
			WHERE 
				id = ".$id."
		");
		if($doUnublish){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_unpublish_informasi')." id = ".$id.""));		
			return 'success';
		}else{
			return 'failed';
		}
	}
	public function filterData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$rest  = "ORDER BY position ASC";
		$conditional = "WHERE title LIKE '%".$this->db->escape_str($name)."%'";

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function filterDataCount($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';

		$rest  = "ORDER BY position ASC";
		$conditional = "WHERE title LIKE '%".$this->db->escape_str($name)."%'";

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY position ASC";

		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listDataCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY position ASC";
		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

	private function uploadImage($width_resize, $x1, $y1, $x2, $y2) {
        $this->ci->load->helper('upload');
        if ($_FILES['path']['size']<>0){
            $data = array();

            $tmpName = $_FILES['path']['tmp_name']; 
            list($width, $height, $type, $attr) = getimagesize($tmpName);
            # Rasio Ukuran Informasi #
            $rasio = $width/$width_resize;
            $expfile = explode('.', $_FILES['path']['name']);
            $extension_thumb = strtoupper(end($expfile));
            if (!(($extension_thumb == 'JPEG') || ($extension_thumb == 'JPG') || ($extension_thumb == 'PNG') || ($extension_thumb == 'GIF'))){
                $data['error'] = "failed_ext";
            }
            
            # UPLOAD #
            $pathdir = date("Y/m/d");
            $thepath = $this->webconfig['media-path-informasi'].$pathdir."/";                       
            $thepath_create = $this->webconfig['media-path-informasi'].$pathdir."/temp/";                       
            $exp = explode("/",$thepath_create);
            $way = '';
            foreach($exp as $n){
                $way .= $n.'/';
                @mkdir($way);       
            }
            
            $pos = strripos($_FILES['path']['name'], '.');
            if($pos === false){
                $ordinary_name = $_FILES['path']['name'];
            }else{
                $ordinary_name = substr($_FILES['path']['name'], 0, $pos);
            }

            $name_upload = date("Y_m_d-H_i_s")."_".md5($ordinary_name);
            $name_upload_c = $name_upload.'.'.strtolower($extension_thumb);
            
            $result = was_upload_file('path', $name_upload, false, $thepath_create."/", array(), array(), array());

            if ($result['status'] > 0){
                $data['path'] = "";
            }else{
                $path1 = $pathdir."/".$name_upload_c;
                $data['path'] = $path1;
                if(isset($rasio) && $rasio !=''){
                    $finalcrop_resize = $this->croping_resize($result,$x1,$y1,$x2,$y2,$rasio,$thepath);
                }
            }
        }else{
            $data['error'] = "empty_file";
        }

        return $data;
    }
    private function croping_resize($result,$x1,$y1,$x2,$y2,$rasio,$image_path) {
        
        # RESIZE and CROP #
        $thepath = $this->webconfig['media-path-informasi-thumb'].date("Y/m/d")."/";                       
        $exp = explode("/",$thepath);
        $way ='';
        foreach($exp as $n){
            $way.=$n.'/';
            @mkdir($way);       
        }
        
        $file_name = $result['name'];
        $src_path = $image_path . 'temp/' . $file_name;
        
        $exp_file = explode('.', $file_name, 2);
        
        $des_path =  $image_path .$file_name;
        $xx1 = $x1 * $rasio;
        $yy1 = $y1 * $rasio;
        $xx2 = $x2 * $rasio;
        $yy2 = $y2 * $rasio;
        if($des_path !=''){
            $this->image_moo
                ->load($src_path)
                ->crop($xx1,$yy1,$xx2,$yy2)
                ->save($des_path);
        }

        ## RESIZE IMAGE ##
        foreach($this->webconfig['image_informasi'] as $key => $value){
            $size_file = explode('x',$value);
            $file_final_name_resize = $exp_file[0].'_'.$value.'_thumb.'.$exp_file[1];
            $des_path_resize =  $thepath . $file_final_name_resize;
            $this->image_moo
                ->load($des_path)
                ->stretch($size_file[0],$size_file[1])
                ->save($des_path_resize);
        }

        /* Delete Temp File */
        if (file_exists($src_path)){
            @unlink($src_path);
        }
    }
    private function del_cropresize($cfile) {
        if($cfile == "") return false;
        
        $explod_file = explode("/",$cfile);
        $filename = end($explod_file);
        $exp_file = explode(".",$filename);
        $exp_thumb_path = explode("/", $this->webconfig['media-path-informasi-thumb'].$cfile, -1);
        $thumb_path = implode("/", $exp_thumb_path);
        
        foreach($this->webconfig['image_informasi'] as $key => $value){
            $delete_resize = $thumb_path.'/'.$exp_file[0]."_".$value."_thumb.".$exp_file[1];
            if (file_exists($delete_resize)){
                @unlink($delete_resize);
            }
        }
    }
    public function orderpositiondata($params=array()){
		$id_pos      = isset($params["id_pos"])?$params["id_pos"]:'';
		$position      = isset($params["position"])?$params["position"]:'';
		foreach ($id_pos as $key => $item)
		{
			// $position = $position + 1;
			$doUnublish = $this->db->query("
				UPDATE ".$this->maintablename."
				SET position = '".$position[$key]."'
				WHERE 
					id = ".$item."
			");
		}
	}
}