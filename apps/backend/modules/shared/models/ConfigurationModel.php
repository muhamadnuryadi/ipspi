<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class ConfigurationModel extends CI_Model{
	var $ci;
	function __construct() {	 
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "configuration";		
	}
	public function entriData($params=array()){
		$meta_title      = isset($params["meta_title"])?$params["meta_title"]:'';
		$site_title      = isset($params["site_title"])?$params["site_title"]:'';
		$meta_description      = isset($params["meta_description"])?$params["meta_description"]:'';
		$fb_url      = isset($params["fb_url"])?$params["fb_url"]:'';
		$twitter_url      = isset($params["twitter_url"])?$params["twitter_url"]:'';
		$gplus_url      = isset($params["gplus_url"])?$params["gplus_url"]:'';			
		$youtube_url      = isset($params["youtube_url"])?$params["youtube_url"]:'';			
		$instagram_url      = isset($params["instagram_url"])?$params["instagram_url"]:'';			
		$email_contact      = isset($params["email_contact"])?$params["email_contact"]:'';	
		$address      = isset($params["address"])?$params["address"]:'';			
		$phone      = isset($params["phone"])?$params["phone"]:'';
		$phone_2      = isset($params["phone_2"])?$params["phone_2"]:'';
		$namaprofil      = isset($params["namaprofil"])?$params["namaprofil"]:'';
		$fax      = isset($params["fax"])?$params["fax"]:'';
		$bank      = isset($params["bank"])?$params["bank"]:'';
		$norek      = isset($params["norek"])?$params["norek"]:'';
		
		$data ['meta_title'] = $this->db->escape_str($meta_title);
		$data ['site_title'] = $this->db->escape_str($site_title);
		$data ['meta_description'] = $this->db->escape_str($meta_description);
		$data ['fb_url'] = $this->db->escape_str($fb_url);
		$data ['twitter_url'] = $this->db->escape_str($twitter_url);
		$data ['gplus_url'] = $this->db->escape_str($gplus_url);
		$data ['youtube_url'] = $this->db->escape_str($youtube_url);
		$data ['instagram_url'] = $this->db->escape_str($instagram_url);
		$data ['email_contact'] = $this->db->escape_str($email_contact);
		$data ['address'] = $this->db->escape_str($address);
		$data ['phone'] = $this->db->escape_str($phone);
		$data ['phone_2'] = $this->db->escape_str($phone_2);
		$data ['namaprofil'] = $this->db->escape_str($namaprofil);
		$data ['fax'] = $this->db->escape_str($fax);
		$data ['bank'] = $this->db->escape_str($bank);
		$data ['norek'] = $this->db->escape_str($norek);
		
		$doInsert = $this->db->insert($this->maintablename, $data);
		if($doInsert){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_entry_configuration')." text = ".$meta_title.""));	
			return 'success';
		}else{
			return 'failed';
		}
	}
	public function updateData($params=array()){
		$id   = isset($params["id"])?$params["id"]:'';
		$meta_title      = isset($params["meta_title"])?$params["meta_title"]:'';
		$site_title      = isset($params["site_title"])?$params["site_title"]:'';
		$tagline_text      = isset($params["tagline_text"])?$params["tagline_text"]:'';
		$meta_description      = isset($params["meta_description"])?$params["meta_description"]:'';
		$fb_url      = isset($params["fb_url"])?$params["fb_url"]:'';
		$twitter_url      = isset($params["twitter_url"])?$params["twitter_url"]:'';
		$gplus_url      = isset($params["gplus_url"])?$params["gplus_url"]:'';
		$youtube_url      = isset($params["youtube_url"])?$params["youtube_url"]:'';			
		$instagram_url      = isset($params["instagram_url"])?$params["instagram_url"]:'';
		$email_contact      = isset($params["email_contact"])?$params["email_contact"]:'';
		$address      = isset($params["address"])?$params["address"]:'';
		$phone      = isset($params["phone"])?$params["phone"]:'';
		$phone_2      = isset($params["phone_2"])?$params["phone_2"]:'';
		$namaprofil      = isset($params["namaprofil"])?$params["namaprofil"]:'';
		$fax      = isset($params["fax"])?$params["fax"]:'';
		$bank      = isset($params["bank"])?$params["bank"]:'';
		$norek      = isset($params["norek"])?$params["norek"]:'';
		$lat      = isset($params["lat"])?$params["lat"]:'';
		$lng      = isset($params["lng"])?$params["lng"]:'';
			
		$sql_user = "
						meta_title = '".$this->db->escape_str($meta_title)."'
						,site_title = '".$this->db->escape_str($site_title)."'
						,meta_description = '".$this->db->escape_str($meta_description)."'
						,fb_url = '".$this->db->escape_str($fb_url)."'
						,twitter_url = '".$this->db->escape_str($twitter_url)."'
						,gplus_url = '".$this->db->escape_str($gplus_url)."'
						,youtube_url = '".$this->db->escape_str($youtube_url)."'
						,instagram_url = '".$this->db->escape_str($instagram_url)."'
						,phone = '".$this->db->escape_str($phone)."'
						,phone_2 = '".$this->db->escape_str($phone_2)."'
						,email_contact = '".$this->db->escape_str($email_contact)."'
						,address = '".$this->db->escape_str($address)."'
						,namaprofil = '".$this->db->escape_str($namaprofil)."'
						,fax = '".$this->db->escape_str($fax)."'
						,bank = '".$this->db->escape_str($bank)."'
						,norek = '".$this->db->escape_str($norek)."'
						,lat = '".$this->db->escape_str($lat)."'
						,lng = '".$this->db->escape_str($lng)."'
					";
		
		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET 
			".$sql_user."
		WHERE 
			id = ".$id."
		");
		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_configuration')." text = ".$meta_title." dan id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}
	
	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		
		$offsetData  = "";
		$conditional = "";
		$rest = "";
		if($id != '') {
			$conditional = "WHERE id = '".$id."'";
		}
		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}
		
		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}
}