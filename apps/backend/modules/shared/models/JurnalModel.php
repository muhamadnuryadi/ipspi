<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class JurnalModel extends CI_Model{
	var $ci;
	function __construct() {
		parent::__construct();
		$this->ci = & get_instance();
		$this->ci->load->library('session');
		$this->maintablename = "jurnal";
	}
	public function entriData($params=array()){
		$title = isset($params["title"])?$params["title"]:'';
		$datepublish = isset($params["datepublish"])?$params["datepublish"]:'';
		$body = isset($params["body"])?$params["body"]:'';
		$description = isset($params["description"])?$params["description"]:'';
		$tags = isset($params["tags"])?$params["tags"]:'';
		$status = isset($params["status"])?$params["status"]:'';

		/*$x1     = isset($params["x1"])?$params["x1"]:'';
        $y1     = isset($params["y1"])?$params["y1"]:'';
        $x2      = isset($params["x2"])?$params["x2"]:'';
        $y2      = isset($params["y2"])?$params["y2"]:'';
        $width_resize      = isset($params["width_resize"])?$params["width_resize"]:'';

        $expdate = explode('/', $datepublish);
        $newdatepublish = $expdate[2].'-'.$expdate[1].'-'.$expdate[0];

        $upload_image = $this->uploadImage($width_resize, $x1, $y1, $x2, $y2);

        if(isset($upload_image["error"])) {
            if($upload_image["error"] == "failed_ext") {
                return "failed_ext";
            }
            if($upload_image["error"] == "empty_file") {
                return "empty_file";
            }
        }*/
        $expdate = explode('/', $datepublish);
        $newdatepublish = $expdate[2].'-'.$expdate[1].'-'.$expdate[0];
        
        if ($_FILES['files']['size']<>0){
			$arr_path = explode('.', $_FILES['files']['name']);
			$extension_thumb = strtoupper(end($arr_path));

			if (!(($extension_thumb == 'PDF')))
			{
				return 'failed_ext';
			}else if($_FILES['files']['size'] > $this->webconfig['max_file_size'] * 1000000){
				return 'failed_filesize';
			}
		}

        if ($_FILES['files']['size']<>0){
			$arr_path = explode('.', $_FILES['files']['name']);
			$extension_thumb = strtoupper(end($arr_path));

			if (!(($extension_thumb == 'JPG') || ($extension_thumb == 'JPEG') || ($extension_thumb == 'PNG') || ($extension_thumb == 'PDF')))
			{
				return 'failed_ext';
			}else if($_FILES['files']['size'] > $this->webconfig['max_file_size'] * 1000000){
				return 'failed_filesize';
			}else{
				

				# UPLOAD #
				$pathdir = date("Y/m/d");
	            $thepath = $this->webconfig['media-path-files'].$pathdir."/";                    
	            $exp = explode("/",$thepath);
	            $way = '';
	            foreach($exp as $n){
	                $way .= $n.'/';
	                @mkdir($way);       
	            }

				## FILENAME ##
	            $pos = strripos($_FILES['files']['name'], '.');
	            if($pos === false){
	                $ordinary_name = $_FILES['files']['name'];
	            }else{
	                $ordinary_name = substr($_FILES['files']['name'], 0, $pos);
	            }

	            $name_upload = date("Y_m_d-H_i_s")."_".md5($ordinary_name);
	            $name_upload_c = $name_upload.'.'.strtolower($extension_thumb);

				$result = was_upload_file('files', $name_upload, false, $thepath."/", array(), array(), array());

				$path1 = date("Y/m/d")."/".$name_upload_c;
				$data['files'] = $path1;
			}
		} else {
			return 'empty_file';
		}

		$data ['id'] = "Null";
		$data ['title'] = $this->db->escape_str($title);
		$data ['datepublish'] = $this->db->escape_str($newdatepublish);
		$data ['slug'] = url_title($title,'-',TRUE);
		$data ['body'] = $this->db->escape_str($body);
		$data ['description'] = $this->db->escape_str($description);
		$data ['status'] = $this->db->escape_str($status);
		$data ['datecreated'] = date("Y-m-d H:i");
		$data ['id_user'] = $this->session->userdata('iduser');
		$data ['id_provinsi'] = $this->session->userdata('userpropinsi');
		$data ['role'] = $this->session->userdata('role');
		
		$doInsert = $this->db->insert($this->maintablename, $data);
		if($doInsert){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_entry_jurnal')." dengan judul = ".htmlentities($title).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function updateData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$title = isset($params["title"])?$params["title"]:'';
		$datepublish = isset($params["datepublish"])?$params["datepublish"]:'';
		$body = isset($params["body"])?$params["body"]:'';
		$description = isset($params["description"])?$params["description"]:'';
		$tags = isset($params["tags"])?$params["tags"]:'';
		$status = isset($params["status"])?$params["status"]:'';

        $expdate = explode('/', $datepublish);
        $newdatepublish = $expdate[2].'-'.$expdate[1].'-'.$expdate[0];

        if ($_FILES['files']['size']<>0){
			$arr_path = explode('.', $_FILES['files']['name']);
			$extension_thumb = strtoupper(end($arr_path));

			if (!(($extension_thumb == 'JPG') || ($extension_thumb == 'JPEG') || ($extension_thumb == 'PNG') || ($extension_thumb == 'PDF')))
			{
				return 'failed_ext';
			}else if($_FILES['files']['size'] > $this->webconfig['max_file_size'] * 1000000){
				return 'failed_filesize';
			}else{
				

				# UPLOAD #
				$pathdir = date("Y/m/d");
	            $thepath = $this->webconfig['media-path-files'].$pathdir."/";                    
	            $exp = explode("/",$thepath);
	            $way = '';
	            foreach($exp as $n){
	                $way .= $n.'/';
	                @mkdir($way);       
	            }

				## FILENAME ##
	            $pos = strripos($_FILES['files']['name'], '.');
	            if($pos === false){
	                $ordinary_name = $_FILES['files']['name'];
	            }else{
	                $ordinary_name = substr($_FILES['files']['name'], 0, $pos);
	            }

	            $name_upload = date("Y_m_d-H_i_s")."_".md5($ordinary_name);
	            $name_upload_c = $name_upload.'.'.strtolower($extension_thumb);

				$result = was_upload_file('files', $name_upload, false, $thepath."/", array(), array(), array());

				$path1 = date("Y/m/d")."/".$name_upload_c;
			}
		}
		
		$sql_user = "
					title = '".$this->db->escape_str($title)."'
					,datepublish = '".$this->db->escape_str($newdatepublish)."'
					,body = '".$this->db->escape_str($body)."'
					,slug = '".$this->db->escape_str(url_title($title,'-',TRUE))."'
					,description = '".$this->db->escape_str($description)."'
					,status = '".$this->db->escape_str($status)."'
					,datemodified = '".date("Y-m-d H:i")."'
					,id_user = '".$this->session->userdata('iduser')."'
					,id_provinsi = '".$this->session->userdata('userpropinsi')."'
					,role = '".$this->session->userdata('role')."'
					";
		
		/* jika update file baru */
        if ($_FILES['files']['size']<>0) $sql_user .= ", files = '".$this->db->escape_str($path1)."'";

		$doUpdate = $this->db->query("
		UPDATE ".$this->maintablename."
		SET
			".$sql_user."
		WHERE
			id = ".$id."
		");
		if($doUpdate){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_modif_jurnal')." id = ".$id.", title = ".htmlentities($title).""));
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function deleteData($id){
		if($id == 0) return 'failed';
		$getData = $this->listData(array('id' => $id));
		if(isset($getData) && count($getData) > 0){
			foreach ($getData as $key => $value) {
				$pfile = $this->webconfig['media-path-jurnal'].$value['path'];
				if (file_exists($pfile)){
					@unlink($pfile);
				}
				$exp_img = explode(".", $value['path']);
				foreach ($this->webconfig['image_jurnal'] as $key => $image_jurnal_data) {
					@unlink($this->webconfig['media-path-jurnal-thumb'].$exp_img[0]."_".$image_jurnal_data."_thumb.".$exp_img[1]);
				}
			}
		}
		$doDelete = $this->db->query("
		DELETE FROM ".$this->maintablename."
		WHERE
			id = ".$id."
		");
		if($doDelete){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_delete_jurnal')." id = ".$id.""));
			return 'success';
		}else{
			return 'failed';
		}
	}


    public function doPublish($id){
		if($id == 0) return 'failed';
		
		$doPublish = $this->db->query("
			UPDATE ".$this->maintablename."
			SET status = '1'
			WHERE 
				id = ".$id."
		");
		if($doPublish){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_publish_jurnal')." id = ".$id.""));		
			return 'success';
		}else{
			return 'failed';
		}
	}
	public function doUnublish($id){
		if($id == 0) return 'failed';
		
		$doUnublish = $this->db->query("
			UPDATE ".$this->maintablename."
			SET status = '0'
			WHERE 
				id = ".$id."
		");
		if($doUnublish){
			writeLog(array('module' => $this->module_name, 'details' => $this->lang->line('logs_unpublish_jurnal')." id = ".$id.""));		
			return 'success';
		}else{
			return 'failed';
		}
	}
	public function filterData($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional  = "";
		$rest  = "ORDER BY id DESC";
		if($name !=''){
			$conditional .= "AND title LIKE '%".$this->db->escape_str($name)."%'";
		}
		if($this->session->userdata('role') != 0){
			if($this->session->userdata('role') == 1){
				$conditional .= "";
			}else if($this->session->userdata('role') == 2){
				$conditional .= "AND id_provinsi = '".$this->session->userdata('userprovinsi')."'";
			}else if($this->session->userdata('role') == 3){
				$conditional .= "AND id_user = '".$this->session->userdata('iduser')."'";
			}
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE (1=1)
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function filterDataCount($params=array()){
		$name = isset($params["name"])?$params["name"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$conditional = '';
		$rest  = "ORDER BY id DESC";
		if($name !=''){
			$conditional .= "AND title LIKE '%".$this->db->escape_str($name)."%'";
		}
		if($this->session->userdata('role') != 0){
			if($this->session->userdata('role') == 1){
				$conditional .= "";
			}else if($this->session->userdata('role') == 2){
				$conditional .= "AND id_provinsi = '".$this->session->userdata('userprovinsi')."'";
			}else if($this->session->userdata('role') == 3){
				$conditional .= "AND id_user = '".$this->session->userdata('iduser')."'";
			}
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE (1=1)
			".$conditional."
			".$rest."
		");
		$result = $q->first_row('array');
		return $result;
	}

	public function listData($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";

		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($this->session->userdata('role') != 0){
			if($this->session->userdata('role') == 1){
				$conditional .= "";
			}else if($this->session->userdata('role') == 2){
				$conditional .= "AND id_provinsi = '".$this->session->userdata('userprovinsi')."'";
			}else if($this->session->userdata('role') == 3){
				$conditional .= "AND id_user = '".$this->session->userdata('iduser')."'";
			}
		}

		if($limit > 0){
			if($start > 0){
				$offsetData = "LIMIT ".$start.", ".$limit."";
			}else{
				$offsetData = "LIMIT 0, ".$limit."";
			}
		}

		$q = $this->db->query("
			SELECT
				*
			FROM
				".$this->maintablename."
			WHERE (1=1)
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->result_array();
		return $result;
	}

	public function listDataCount($params=array()){
		$id = isset($params["id"])?$params["id"]:'';
		$start = isset($params["start"])?$params["start"]:'';
		$limit = isset($params["limit"])?$params["limit"]:'';
		$offsetData  = "";
		$conditional = "";
		$rest = "ORDER BY id DESC";
		if($id != '') {
			$conditional .= "AND id = '".$id."'";
		}

		if($this->session->userdata('role') != 0){
			if($this->session->userdata('role') == 1){
				$conditional .= "";
			}else if($this->session->userdata('role') == 2){
				$conditional .= "AND id_provinsi != '".$this->session->userdata('userprovinsi')."'";
			}else if($this->session->userdata('role') == 3){
				$conditional .= "AND id_user != '".$this->session->userdata('iduser')."'";
			}
		}

		$q = $this->db->query("
			SELECT
				count(id) as jumlah
			FROM
				".$this->maintablename."
			WHERE (1=1)
			".$conditional."
			".$rest."
			".$offsetData."
		");
		$result = $q->first_row('array');
		return $result;
	}

	private function uploadImage($width_resize, $x1, $y1, $x2, $y2) {
        $this->ci->load->helper('upload');
        if ($_FILES['path']['size']<>0){
            $data = array();

            $tmpName = $_FILES['path']['tmp_name']; 
            list($width, $height, $type, $attr) = getimagesize($tmpName);
            # Rasio Ukuran Jurnal #
            $rasio = $width/$width_resize;
            $expfile = explode('.', $_FILES['path']['name']);
            $extension_thumb = strtoupper(end($expfile));
            if (!(($extension_thumb == 'JPEG') || ($extension_thumb == 'JPG') || ($extension_thumb == 'PNG') || ($extension_thumb == 'GIF'))){
                $data['error'] = "failed_ext";
            }
            
            # UPLOAD #
            $pathdir = date("Y/m/d");
            $thepath = $this->webconfig['media-path-jurnal'].$pathdir."/";                       
            $thepath_create = $this->webconfig['media-path-jurnal'].$pathdir."/temp/";                       
            $exp = explode("/",$thepath_create);
            $way = '';
            foreach($exp as $n){
                $way .= $n.'/';
                @mkdir($way);       
            }
            
            $pos = strripos($_FILES['path']['name'], '.');
            if($pos === false){
                $ordinary_name = $_FILES['path']['name'];
            }else{
                $ordinary_name = substr($_FILES['path']['name'], 0, $pos);
            }

            $name_upload = date("Y_m_d-H_i_s")."_".md5($ordinary_name);
            $name_upload_c = $name_upload.'.'.strtolower($extension_thumb);
            
            $result = was_upload_file('path', $name_upload, false, $thepath_create."/", array(), array(), array());

            if ($result['status'] > 0){
                $data['path'] = "";
            }else{
                $path1 = $pathdir."/".$name_upload_c;
                $data['path'] = $path1;
                if(isset($rasio) && $rasio !=''){
                    $finalcrop_resize = $this->croping_resize($result,$x1,$y1,$x2,$y2,$rasio,$thepath);
                }
            }
        }else{
            $data['error'] = "empty_file";
        }

        return $data;
    }
    private function croping_resize($result,$x1,$y1,$x2,$y2,$rasio,$image_path) {
        
        # RESIZE and CROP #
        $thepath = $this->webconfig['media-path-jurnal-thumb'].date("Y/m/d")."/";                       
        $exp = explode("/",$thepath);
        $way ='';
        foreach($exp as $n){
            $way.=$n.'/';
            @mkdir($way);       
        }
        
        $file_name = $result['name'];
        $src_path = $image_path . 'temp/' . $file_name;
        
        $exp_file = explode('.', $file_name, 2);
        
        $des_path =  $image_path .$file_name;
        $xx1 = $x1 * $rasio;
        $yy1 = $y1 * $rasio;
        $xx2 = $x2 * $rasio;
        $yy2 = $y2 * $rasio;
        if($des_path !=''){
            $this->image_moo
                ->load($src_path)
                ->crop($xx1,$yy1,$xx2,$yy2)
                ->save($des_path);
        }

        ## RESIZE IMAGE ##
        foreach($this->webconfig['image_jurnal'] as $key => $value){
            $size_file = explode('x',$value);
            $file_final_name_resize = $exp_file[0].'_'.$value.'_thumb.'.$exp_file[1];
            $des_path_resize =  $thepath . $file_final_name_resize;
            $this->image_moo
                ->load($des_path)
                ->stretch($size_file[0],$size_file[1])
                ->save($des_path_resize);
        }

        /* Delete Temp File */
        if (file_exists($src_path)){
            @unlink($src_path);
        }
    }
    private function del_cropresize($cfile) {
        if($cfile == "") return false;
        
        $explod_file = explode("/",$cfile);
        $filename = end($explod_file);
        $exp_file = explode(".",$filename);
        $exp_thumb_path = explode("/", $this->webconfig['media-path-jurnal-thumb'].$cfile, -1);
        $thumb_path = implode("/", $exp_thumb_path);
        
        foreach($this->webconfig['image_jurnal'] as $key => $value){
            $delete_resize = $thumb_path.'/'.$exp_file[0]."_".$value."_thumb.".$exp_file[1];
            if (file_exists($delete_resize)){
                @unlink($delete_resize);
            }
        }
    }
    
}