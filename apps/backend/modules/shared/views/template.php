<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?php echo $this->module_name; ?> | <?php echo $this->lang->line('app_logo'); ?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo $this->webconfig['back_base_template']; ?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $this->webconfig['back_base_template']; ?>dist/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo $this->webconfig['back_base_template']; ?>dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo $this->webconfig['back_base_template']; ?>dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo $this->webconfig['back_base_template']; ?>dist/css/alert.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="<?php echo $this->webconfig['back_base_template']; ?>plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="<?php echo $this->webconfig['back_base_template']; ?>plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="<?php echo $this->webconfig['back_base_template']; ?>plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="<?php echo $this->webconfig['back_base_template']; ?>plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="<?php echo $this->webconfig['back_base_template']; ?>plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="<?php echo $this->webconfig['back_base_template']; ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $this->webconfig['back_base_template']; ?>plugins/iCheck/all.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $this->webconfig['back_base_template']; ?>js/toastr/toastr.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $this->webconfig['back_base_template']; ?>dist/css/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <!-- <link href="<?php echo $this->webconfig['back_base_template']; ?>dist/css/select2.css" rel="stylesheet" type="text/css" /> -->
    <link href="<?php echo $this->webconfig['back_base_template']; ?>plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $this->webconfig['back_base_template']; ?>plugins/timepicker/jquery.timepicker.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo $this->webconfig['back_base_template']; ?>dist/css/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo $this->webconfig['back_base_template']; ?>dist/css/jquery.Jcrop.css">
    <link href="<?php echo $this->webconfig['back_base_template']; ?>dist/css/custom.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<?php echo $this->webconfig['back_base_template']; ?>img/favicon.png" type="image/x-icon" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery 2.1.4 -->

    <script src="<?php echo $this->webconfig['back_base_template']; ?>js/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>js/jquery-impromptu.js" type="text/javascript"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>js/toastr/toastr.js" type="text/javascript"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>js/jquery.fancybox.js" type="text/javascript"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/chartjs/Chart.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/chartist/chartist.min.js" type="text/javascript"></script>
    <!-- <script src="<?php echo $this->webconfig['back_base_template']; ?>js/select2.js" type="text/javascript"></script> -->
    <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>dist/js/jquery.browser.js" type="text/javascript"></script>


    <script type="text/javascript">
    jQuery(document).ready(function($) {
      jQuery('.select2').select2();
    });

    function encode_js(str){
        str = (str + '').toString();
        return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
        replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '%20').replace(/\s/g,'%20').replace(/%2F/g,'-').replace(/%/g,'%25');
    }
    </script>
    <style type="text/css">
      .select2-container--default .select2-selection--single, .select2-selection .select2-selection--single {
          border: 1px solid #d2d6de;
          border-radius: 0;
          font-size: 12px;
          line-height: 1;
      }
      .select2-container {
        width:100%;
      }

      .select2-container a.select2-choice {
        font-size: 11px;
        line-height: 1;
       /* height: 38px;
        padding: 8px 12px;
        line-height: 1.42857;*/
      }
      .select2-container-multi .select2-choices .select2-search-field input {
        font-size: 11px !important;
        line-height: 1;
      }
      .select2-results__option {
          -moz-user-select: none;
          line-height: 1;
          padding: 6px;
      }
    </style>
    <style type="text/css">
      .main-header {
         border-bottom: 3px solid #91e000;
       }
    </style>
  </head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a class="logo" href="#">
          <span class="logo-lg">
                <img src="<?php echo $this->webconfig['back_base_template']; ?>img/ipspi.png" class="img" alt="User Image" style="width: 50%;float: left;margin-top: 5px;">
          </span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav role="navigation" class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a role="button" data-toggle="offcanvas" class="sidebar-toggle" href="#">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>

          <div class="navbar-custom-menu">

            <!-- <ul class="nav navbar-nav">
              <li class="dropdown">
                <a aria-expanded="false" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-exchange"> </i> &nbsp;&nbsp; Add <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">

                </ul>
              </li>
            </ul> -->

            <ul class="nav navbar-nav">

              <!-- User Account: style can be found in dropdown.less -->

              <li class="dropdown user user-menu">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                  <?php if($this->session->userdata('avatar') != ''){?>
                    <img alt="User Image" class="user-image" src="<?php echo thumb_image($this->session->userdata('avatar'),'', 'avatar'); ?>">
                  <?php }else{ ?>
                    <img alt="User Image" class="user-image" src="<?php echo $this->webconfig['back_base_template']; ?>dist/img/avatar5.png">
                  <?php } ?>
                  <span class="hidden-xs">
                    <?php echo $this->session->userdata('name') != '' ? $this->session->userdata('name') : $this->lang->line('empty_name'); ?>
                        (<?php echo $this->session->userdata('userid') != '' ? $this->session->userdata('userid') : $this->lang->line('empty_userid'); ?>)
                        <?php /*| <?php
                          if($this->session->userdata('role') != ''):
                            switch($this->session->userdata('role')){
                              case 0:
                                echo "Programmer";
                              break;
                              case 1:
                                echo "Super Admin";
                              break;
                              case 2:
                                echo "Admin";
                              break;
                              case 3:
                                echo "User";
                              break;
                            }
                          endif;
                      ?> */ ?>
                  </span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <?php if($this->session->userdata('avatar') != ''){?>
                      <img alt="User Image" class="img-circle" src="<?php echo thumb_image($this->session->userdata('avatar'),'', 'avatar'); ?>">
                    <?php }else{ ?>
                      <img alt="User Image" class="img-circle" src="<?php echo $this->webconfig['back_base_template']; ?>dist/img/avatar5.png">
                    <?php } ?>
                    <p>
                      <?php echo $this->session->userdata('name') != '' ? $this->session->userdata('name') : $this->lang->line('empty_name'); ?>
                      <small><?php echo $this->session->userdata('userid') != '' ? $this->session->userdata('userid') : $this->lang->line('empty_userid'); ?></small>
                    </p>
                  </li>
                  <!-- Menu Footer--> 
                  <li class="user-footer">
                    <?php if($this->session->userdata('role') == 3){?>
                      <div class="pull-left">
                        <a href="<?php echo base_url(); ?>members/uploadavatar" class="btn btn-default btn-flat">Upload Avatar</a>
                      </div>
                    <?php }else{ ?>
                      <div class="pull-left">
                        <a href="<?php echo base_url(); ?>users" class="btn btn-default btn-flat">Pengguna</a>
                      </div>
                    <?php } ?>
                    <div class="pull-right">
                      <a href="<?php echo base_url(); ?>login/logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>

                </ul>
              </li>
              <!-- Notifications: style can be found in dropdown.less -->
          </ul></div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 407px;">
<section class="sidebar" style="height: 407px; overflow: hidden; width: auto;">
  <!-- <div class="user-panel">
    <div class="pull-left image">
      <img src="<?php echo $this->webconfig['back_base_template']; ?>img/ipspi.png" class="img" alt="User Image">
    </div>
    <div class="pull-left info">
      <p>IPSPI</p>
      <p>KOTA BANDUNG</p>
    </div>
  </div> -->
    <ul class="sidebar-menu">
        <li class="header">MENU</li>

        <?php if($this->session->userdata('role') == 0 || $this->session->userdata('role') == 1 || $this->session->userdata('role') == 2){?>
        <li <?php if($this->module_name == $this->lang->line('dashboard')){ echo 'class="active"';} ?>>
            <a href="<?php echo base_url(); ?>dashboard">
                <i class="fa fa-home fa-fw"></i><span>Dashboard</span>
            </a>
        </li>
        <?php } ?>

        <?php if($this->session->userdata('role') == 0 || $this->session->userdata('role') == 1 || $this->session->userdata('role') == 2 || $this->session->userdata('role') == 3){?>
        <li <?php if($this->module_name == $this->lang->line('jurnal')){ echo 'class="active"';} ?>>
            <a href="<?php echo base_url(); ?>jurnal">
                <i class="fa fa-book"></i><span>Jurnal</span>
            </a>
        </li>
        <?php } ?>

        <?php if($this->session->userdata('role') == 0 || $this->session->userdata('role') == 1 || $this->session->userdata('role') == 2){?>
        <li <?php if($this->module_name == $this->lang->line('berita')){ echo 'class="active"';} ?>>
            <a href="<?php echo base_url(); ?>berita">
                <i class="fa fa-arrow-circle-right"></i><span>Berita</span>
            </a>
        </li>
        <?php } ?>

        <?php if($this->session->userdata('role') == 0 || $this->session->userdata('role') == 1 || $this->session->userdata('role') == 2){?>
        <li <?php if($this->module_name == 'Loker'){ echo 'class="active"';} ?>>
            <a href="<?php echo base_url(); ?>loker">
                <i class="fa fa-arrow-circle-right"></i><span>Loker</span>
            </a>
        </li>
        <?php } ?>

        <?php if($this->session->userdata('role') == 0 || $this->session->userdata('role') == 1 || $this->session->userdata('role') == 2 || $this->session->userdata('role') == 3){?>
        <!-- <li <?php if($this->module_name == $this->lang->line('dashboard')){ echo 'class="active"';} ?>>
            <a href="<?php echo base_url(); ?>members">
                <i class="fa fa-users"></i><span>Member</span>
            </a>
        </li> -->

        <li <?php if($this->module_name == $this->lang->line('members')){ echo 'class="active"';} ?>>
            <a href="<?php echo base_url(); ?>members">
                <i class="fa fa-user"></i><span>Member</span>
            </a>
        </li>
        <?php 
        if ($this->session->userdata('role') == 3) {
          ?>
        <li <?php if($this->module_name == $this->lang->line('rekomendasi')){ echo 'class="active"';} ?>>
            <a href="<?php echo base_url(); ?>members/rekomendasi">
                <i class="fa fa-file"></i><span>Surat Rekomendasi</span>
            </a>
        </li>
        <li <?php if($this->module_name == $this->lang->line('kartu')){ echo 'class="active"';} ?>>
            <a href="<?php echo base_url(); ?>members/kartu">
                <i class="fa fa-file"></i><span>Kartu Anggota</span>
            </a>
        </li>
        <?php
        }
        ?>

        <?php 
        if ($this->session->userdata('role') == 3) {
          if ($this->session->status_access != 1) {
            ?>
            <li <?php if($this->module_name == 'Pembayaran'){ echo 'class="active"';} ?>>
                <a href="<?php echo base_url(); ?>pembayaran">
                    <i class="fa fa-money"></i><span>Pembayaran</span>
                </a>
            </li>
            <?php
          }
          
        }
        ?>

        <?php } ?>

        <?php if($this->session->userdata('role') == 0 || $this->session->userdata('role') == 1 || $this->session->userdata('role') == 2){?>
        <li <?php if($this->module_name == $this->lang->line('side_menu_users')){ echo 'class="active"';} ?>>
            <a href="<?php echo base_url(); ?>users">
                <i class="fa fa-user"></i>
                <span><?php echo $this->lang->line('side_menu_users'); ?></span>
            </a>
        </li>
        <?php } ?>

        <?php if($this->session->userdata('role') == 0 || $this->session->userdata('role') == 1 ){?>
        <li <?php if(
                $this->module_name == $this->lang->line('informasi')
                || $this->module_name == $this->lang->line('aboutus')
                || $this->module_name == $this->lang->line('regulation')
                ){ echo 'class="active"';} ?>>
            <a href="javascript:void(0)">
                <i class="fa fa-book"></i>
                <span>CMS</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li <?php if($this->module_name == $this->lang->line('informasi')){ echo 'class="active"';} ?>>
                    <a href="<?php echo base_url(); ?>informasi">
                        <i class="fa fa-arrow-circle-right"></i><span>Informasi/Keanggotaan</span>
                    </a>
                </li>
                <li <?php if($this->module_name == $this->lang->line('aboutus')){ echo 'class="active"';} ?>>
                    <a href="<?php echo base_url(); ?>aboutus">
                        <i class="fa fa-arrow-circle-right"></i><span>Tentang Kami</span>
                    </a>
                </li>
                <li <?php if($this->module_name == $this->lang->line('regulation')){ echo 'class="active"';} ?>>
                    <a href="<?php echo base_url(); ?>regulation">
                        <i class="fa fa-arrow-circle-right"></i><span>Footer Informasi</span>
                    </a>
                </li>
            </ul>
        </li>
        <?php } ?>

        <?php if($this->session->userdata('role') == 2){?>
            <li <?php if($this->module_name == $this->lang->line('provinsi')){ echo 'class="active"';} ?>>
                <a href="<?php echo base_url(); ?>provinsi">
                    <i class="fa fa-arrow-circle-right"></i><span><?php echo $this->lang->line('provinsi'); ?> / DPD</span>
                </a>
            </li>
        <?php } ?>

        <?php if($this->session->userdata('role') == 0 || $this->session->userdata('role') == 1 ){?>
        <li <?php if(
                $this->module_name == $this->lang->line('provinsi')
                || $this->module_name == $this->lang->line('kota')
                || $this->module_name == $this->lang->line('tingkat')
                || $this->module_name == $this->lang->line('pekerjaan')
                || $this->module_name == $this->lang->line('instansi')
                || $this->module_name == $this->lang->line('materi')
                || $this->module_name == $this->lang->line('kebijakan')
                ){ echo 'class="active"';} ?>>
            <a href="javascript:void(0)">
                <i class="fa fa-book"></i>
                <span><?php echo $this->lang->line('master'); ?></span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu"
              <?php if(
                $this->module_name == $this->lang->line('provinsi')
                || $this->module_name == $this->lang->line('kota')
                || $this->module_name == $this->lang->line('tingkat')
                || $this->module_name == $this->lang->line('pekerjaan')
                || $this->module_name == $this->lang->line('instansi')
                || $this->module_name == $this->lang->line('materi')
                || $this->module_name == $this->lang->line('kebijakan')
              ){ echo 'style="display: block;"';} ?>
            >
                <li <?php if($this->module_name == $this->lang->line('provinsi')){ echo 'class="active"';} ?>>
                    <a href="<?php echo base_url(); ?>provinsi">
                        <i class="fa fa-circle-o"></i><span><?php echo $this->lang->line('provinsi'); ?> / DPD</span>
                    </a>
                </li>
                <li <?php if($this->module_name == $this->lang->line('kota')){ echo 'class="active"';} ?>>
                    <a href="<?php echo base_url(); ?>kota">
                        <i class="fa fa-circle-o"></i><span><?php echo $this->lang->line('kota'); ?></span>
                    </a>
                </li>
                <li <?php if($this->module_name == $this->lang->line('tingkat')){ echo 'class="active"';} ?>>
                    <a href="<?php echo base_url(); ?>tingkat">
                        <i class="fa fa-circle-o"></i><span><?php echo $this->lang->line('tingkat'); ?></span>
                    </a>
                </li>
                <li <?php if($this->module_name == $this->lang->line('pekerjaan')){ echo 'class="active"';} ?>>
                    <a href="<?php echo base_url(); ?>pekerjaan">
                        <i class="fa fa-circle-o"></i><span><?php echo $this->lang->line('pekerjaan'); ?></span>
                    </a>
                </li>
                <li <?php if($this->module_name == $this->lang->line('instansi')){ echo 'class="active"';} ?>>
                    <a href="<?php echo base_url(); ?>instansi">
                        <i class="fa fa-circle-o"></i><span><?php echo $this->lang->line('instansi'); ?></span>
                    </a>
                </li>
                <li <?php if($this->module_name == $this->lang->line('materi')){ echo 'class="active"';} ?>>
                    <a href="<?php echo base_url(); ?>materi">
                        <i class="fa fa-circle-o"></i><span><?php echo $this->lang->line('materi'); ?></span>
                    </a>
                </li>
                <li <?php if($this->module_name == $this->lang->line('kebijakan')){ echo 'class="active"';} ?>>
                    <a href="<?php echo base_url(); ?>kebijakan">
                        <i class="fa fa-circle-o"></i><span><?php echo $this->lang->line('kebijakan'); ?></span>
                    </a>
                </li>

            </ul>
        </li>
        <?php } ?>

        <?php if($this->session->userdata('role') == 0 || $this->session->userdata('role') == 1 ){?>
        <li <?php if(
                $this->module_name == $this->lang->line('side_menu_logs')
                || $this->module_name == $this->lang->line('configuration')
                ){ echo 'class="active"';} ?>>
            <a href="javascript:void(0)">
                <i class="fa fa-gear"></i>
                <span><?php echo $this->lang->line('side_menu_utility'); ?></span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li <?php if($this->module_name == $this->lang->line('side_menu_logs')){ echo 'class="active"';} ?>>
                    <a href="<?php echo base_url(); ?>logs">
                        <i class="fa fa-circle-o"></i> <span><?php echo $this->lang->line('side_menu_logs'); ?></span>
                    </a>
                </li>
                <li <?php if($this->module_name == $this->lang->line('configuration')){ echo 'class="active"';} ?>>
                    <a href="<?php echo base_url(); ?>configuration">
                        <i class="fa fa-circle-o"></i> <span><?php echo $this->lang->line('configuration'); ?></span>
                    </a>
                </li>
                <?php if($this->session->userdata('role') == 0){?>
                <li <?php if($this->module_name == $this->lang->line('importmember')){ echo 'class="active"';} ?>>
                    <a href="<?php echo base_url(); ?>importmember">
                        <i class="fa fa-circle-o"></i> <span><?php echo $this->lang->line('importmember'); ?></span>
                    </a>
                </li>
                <?php } ?>
            </ul>
        </li>

        <li <?php if($this->module_name == $this->lang->line('payment')){ echo 'class="active"';} ?>>
            <a href="<?php echo base_url(); ?>payment">
                <i class="fa fa-money"></i><span>Payment</span>
            </a>
        </li>

        <li <?php if($this->module_name == $this->lang->line('export')){ echo 'class="active"';} ?>>
            <a href="<?php echo base_url(); ?>export">
                <i class="fa fa-list"></i><span>Export</span>
            </a>
        </li>

        <?php 
        /*
        <li <?php if($this->module_name == $this->lang->line('member_access')){ echo 'class="active"';} ?>>
            <a href="<?php echo base_url(); ?>member_access">
                <i class="fa fa-users"></i><span><?php echo $this->lang->line('member_access') ?></span>
            </a>
        </li>
        */
        ?>
        

        <li <?php if($this->module_name == $this->lang->line('contact')){ echo 'class="active"';} ?>>
            <a href="<?php echo base_url(); ?>contact">
                <i class="fa fa-arrow-circle-right"></i><span>Contact</span>
            </a>
        </li>
        <?php 
        /*
        <li <?php if($this->module_name == $this->lang->line('keluhan')){ echo 'class="active"';} ?>>
            <a href="<?php echo base_url(); ?>keluhan">
                <i class="fa fa-arrow-circle-right"></i><span>Keluhan</span>
            </a>
        </li>
        */
        ?>
        
        <?php } ?>

        <li <?php if($this->module_name == $this->lang->line('look_web')){ echo 'class="active"';} ?>>
             <a href="<?php echo 'https://'.APP_DOMAIN; ?>" target="_blank">
                <i class="fa fa-home"></i>
                <span><?php echo $this->lang->line('look_web'); ?></span>
            </a>
        </li>
    </ul>
</section>
<div class="slimScrollBar" style="background: rgba(0, 0, 0, 0.2) none repeat scroll 0% 0%; width: 3px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 407px;"></div>
<div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
<!-- /.sidebar -->
</aside>



    <?php echo isset($content)?$content:''; ?>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">

        </div>
        <strong>Copyright &copy; <?php echo date('Y');?> IPSPI.</strong> All rights reserved.
    </footer>


      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
    <div class='control-sidebar-bg'></div>
    </div><!-- ./wrapper -->


    <!-- jQuery UI 1.11.2 -->

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      // $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.2 JS -->

    <!-- Morris.js charts -->


    <!-- Sparkline -->
    <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- jvectormap -->
    <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/knob/jquery.knob.js" type="text/javascript"></script>
    <!-- daterangepicker -->

    <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/daterangepicker/moment.js" type="text/javascript"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <!-- datepicker -->
    <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
    <!-- Slimscroll -->
    <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script type="text/javascript" src='<?php echo $this->webconfig['back_base_template']; ?>plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $this->webconfig['back_base_template']; ?>dist/js/app.min.js" type="text/javascript"></script>

    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->

    <!-- AdminLTE for demo purposes -->
    <!-- <script src="<?php echo $this->webconfig['back_base_template']; ?>dist/js/demo.js" type="text/javascript"></script> -->
    <script src="<?php echo $this->webconfig['back_base_template']; ?>js/jquery.autoNumeric.js" type="text/javascript"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>js/jquery.numeric.js" type="text/javascript"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/timepicker/jquery.timepicker.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->webconfig['back_base_template']; ?>dist/js/jquery.Jcrop.min.js"></script>
  </body>
</html>
