<?php if(isset($error_hash) && count($error_hash) > 0){ ?>
    <div id="error_validator_no_border">
    	<img src="<?php echo $base_template;?>images/reject.png" style="vertical-align:middle" align="left" />
        <div id="validator_title">ERROR REPORT :</div>
        <?php foreach($error_hash as $inpname => $inp_err){ ?>
            <ul>
                <li><?php echo $inp_err; ?></li>
            </ul>
        <?php } ?>
    </div>	
<?php } 

if(isset($success)){ ?>
    <script type="text/javascript">
        $('#formData').load('<?php echo base_url().$this->router->class; ?>/LoadForm/'+Math.random()+'/');    
    </script>

    <div id="success_validator">
        <div id="validator_title"><img src="<?php echo $base_template; ?>images/accepted.png" style="vertical-align:middle" />
    	<?php echo $success; ?>
        </div>
    </div>	
<?php } ?>