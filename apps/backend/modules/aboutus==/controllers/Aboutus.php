<?php
class Aboutus extends CI_Controller{
    var $webconfig;
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
        $this->load->library('session');
        $this->load->sharedModel('AboutusModel');
        $this->load->library('pagination');
        $this->load->sharedModel('LoginModel');
        if(!$this->LoginModel->isLoggedIn()){
            redirect(base_url().'login');
        }
        $this->webconfig = $this->config->item('webconfig');
        $this->module_name = $this->lang->line('aboutus');
    }
    function index(){
        $tdata = array();
        $tdata['lists'] = $this->AboutusModel->listData();
        
        require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
        if($_POST)
        {
            
            $validator = new FormValidator();
            if($validator->ValidateForm())
            {
                
                if($this->input->post('id') == ''){
                    $doUpdate = $this->AboutusModel->entriData(array(
                                                                'name'=>$this->input->post('name')
                                                                ,'body'=>$this->input->post('body')
                                                                ,'x1' => $this->input->post('x1')
                                                                ,'y1' => $this->input->post('y1')
                                                                ,'x2' => $this->input->post('x2')
                                                                ,'y2' => $this->input->post('y2')
                                                                ,'width_resize' => $this->webconfig["width_resize"]
                                                          ));
                }else{
                    $doUpdate = $this->AboutusModel->updateData(array(
                                                                'id' =>$this->input->post('id')
                                                                ,'name'=>$this->input->post('name')
                                                                ,'body'=>$this->input->post('body')
                                                                ,'x1' => $this->input->post('x1')
                                                                ,'y1' => $this->input->post('y1')
                                                                ,'x2' => $this->input->post('x2')
                                                                ,'y2' => $this->input->post('y2')
                                                                ,'width_resize' => $this->webconfig["width_resize"]
                                                          ));
                }
                
                
                if($doUpdate == 'empty'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
                }else if($doUpdate == 'failed'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
                }else if($doUpdate == 'empty_file'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_file_image'));
                }else if($doUpdate == 'empty_name'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('error_empty_name_aboutus'));
                }else if($doUpdate == 'empty_body'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('error_empty_body_aboutus'));
                }else if($doUpdate == 'failed_ext'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_extension'));
                }else if($doUpdate == 'success'){
                    $tdata['success'] = $this->lang->line('msg_success_entry');             
                }

                if($doUpdate != 'success'){
                    $tdata['id']       = $this->input->post('id');
                    $tdata['name']     = $this->input->post('name');
                    $tdata['body']     = $this->input->post('body');
                }

                $ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
                $this->load->sharedView('template', $ldata);
                
            }
            else
            {   
                $tdata['id']       = $this->input->post('id');
                $tdata['name']     = $this->input->post('name');
                $tdata['body']     = $this->input->post('body');
                
                $tdata['error_hash'] = $validator->GetErrors();
                $ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
                $this->load->sharedView('template', $ldata);
            }
        }else{
            $ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }
    
}