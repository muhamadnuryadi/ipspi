<script src="<?php echo $this->webconfig['back_base_template']; ?>plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
   tinymce.init({
        selector: "textarea#body",
        height: 250,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools jbimages'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true,
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ],
        relative_urls : false
     });

   
});

<?php if(isset($success)){ ?>
    setTimeout(function() {
        window.location.href = "<?php echo base_url().$this->router->class; ?>";
    }, 1000);
<?php } ?>
</script>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo strtoupper($this->module_name); ?>
			<small><?php echo $this->lang->line('aboutus_teks'); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('/'); ?>"><i class="fa fa-dashboard"></i><?php echo $this->lang->line('dashboard'); ?></a></li>
			<li class="active"><?php echo ucfirst($this->module_name); ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
	                <div class="box-header with-border">
		                 <div class="row">
								<form class="form-horizontal" name="form" method="POST" action="" >
			                    	<input type="hidden" name="id" value='<?php echo isset($lists['id'])?$lists['id']:''; ?>' >
			                        <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
			                            <?php foreach($error_hash as $inp_err){ ?>
			                                <script type="text/javascript">
			                                jQuery(document).ready(function($) {
			                                    toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
			                                    });
			                                </script>
			                            <?php } ?>
			                        <?php } ?>
			                        
			                        <?php if(isset($success)){ ?>
			                            <script type="text/javascript">
			                            jQuery(document).ready(function($) {
			                                toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
			                                });
			                            </script>
			                        <?php } ?>
			                        <div class="form-group">
			                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('title'); ?> <span>*</span></label>
			                            <div class="col-sm-5">
			                                <input type="text" id="name" name="name" value='<?php echo isset($lists[0]['name'])?$lists[0]['name']:(isset($name)?$name:''); ?>' size="50" class="form-control">
			                            </div>
			                        </div>

			                        <div class="form-group">
			                            <label class="col-sm-2 control-label"><?php echo $this->lang->line('description'); ?> <span>*</span></label>
			                            <div class="col-sm-8">
			                                <textarea type="text" id="body" name="body" class="form-control"><?php echo isset($lists[0]['body'])?stripslashes($lists[0]['body']):(isset($body)?$body:''); ?></textarea>
			                                <div id="image_tinymce">
			                                    <?php
			                                        if(isset($image_tinymce) && count($image_tinymce) > 0){
			                                            foreach ($image_tinymce as $key => $value) {
			                                            ?>
			                                                <input type="hidden" name="image_tinymce[]" value='<?php echo $value; ?>' >
			                                            <?php
			                                            }
			                                        }
			                                    ?>
			                                </div>
			                            </div>
			                        </div>
			                        
			                        <div class="form-group footertable">
			                            <label class="col-md-2 control-label">&nbsp;</label>
			                            <div class="col-md-8 text-left">
			                                <input class="btn btn-danger btn-sm mr5" type="submit" value="<?php echo $this->lang->line('navigation_kirim'); ?>">
			                            </div>
			                        </div>
			                    </form>
	                 	</div>
	                </div>
	            </div>
			</div>
		</div>
	</section>
</div>