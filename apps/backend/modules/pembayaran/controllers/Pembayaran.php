<?php
class Pembayaran extends CI_Controller{
    var $webconfig;
  	function __construct(){
  		parent::__construct();
      $this->load->helper('url');
  		$this->load->helper('date');
  		$this->load->library('session');
  		$this->load->library('pagination');
  		$this->load->sharedModel('PaymentModel');
  		$this->load->sharedModel('LoginModel');
  		$this->load->sharedModel('MembersModel');
  		if(!$this->LoginModel->isLoggedIn()){
  			redirect(base_url().'login');
  		}

  		if($this->session->userdata('role') != 3){
  			 redirect(base_url().'members');
  		}

      if ($this->session->userdata('status_access') != '') {
          redirect(base_url().'members');
      }

  		$this->webconfig = $this->config->item('webconfig');
  		$this->module_name = 'Pembayaran';
  	}  
  	function index(){
  		$tdata = array();
  		$ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
  		$this->load->sharedView('template', $ldata);

  	}

	 public function token(){
        if ($this->webconfig['payment_production'] == true) {
            $server_key = $this->webconfig['midtrans_server_key_production'];
        }else{
            $server_key = $this->webconfig['midtrans_server_key_sandbox'];
        }

        $params = array('server_key' => $server_key, 'production' => $this->webconfig['payment_production']);
        $this->load->library('midtrans');
        $this->midtrans->config($params);

        $order_id = strtotime(date('Y-m-d H:i:s'));
        $this->session->set_userdata('order_id',$order_id);
        // Required
        $transaction_details = array(
          'order_id' => $order_id,
          'gross_amount' => $this->webconfig['harga_member'], // no decimal allowed for creditcard
        );

        // Optional
        $item_details = array(
          'id' => 'ipspi-member',
          'price' => $this->webconfig['harga_member'],
          'quantity' => 1,
          'name' => "Ipspi Member Tetap ".$this->webconfig['masa_berlaku']." Tahun"
        );

        // Optional
        $customer_details = array(
          'first_name'    => $this->session->userdata('name'),
          // 'last_name'     => "Litani",
          'email'         => $this->session->userdata('email'),
          // 'phone'         => "081122334455",
        );

        // Fill transaction details
        $transaction = array(
          'transaction_details' => $transaction_details,
          'customer_details' => $customer_details,
          'item_details' => $item_details,
          'custom_field1' => $this->session->userdata('email')
        );

        $insertpayment = $this->MembersModel->insertPayment(array(
            'id_member'=>$this->session->userdata('iduser')
            ,'price' => $this->webconfig['harga_member']
            ,'charge_id' => $order_id
            ));

        if ($insertpayment == true) {
            //error_log(json_encode($transaction));
            $snapToken = $this->midtrans->getSnapToken($transaction);
            
            error_log($snapToken);
            echo $snapToken;
        }else{

        }
    }

    public function invoice_success(){
        $tdata = array();
        $ldata = array();

        $ldata['content'] = $this->load->view($this->router->class . '/invoice_success', $tdata, true);
        $this->load->sharedView('template', $ldata);   
    }

    public function invoice_error(){
        $tdata = array();
        $ldata = array();

        $ldata['content'] = $this->load->view($this->router->class . '/invoice_error', $tdata, true);
        $this->load->sharedView('template', $ldata);   
    }
}
