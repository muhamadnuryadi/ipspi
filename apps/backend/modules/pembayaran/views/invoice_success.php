<script type="text/javascript">
jQuery(document).ready(function($) {
    
});
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo strtoupper($this->module_name); ?>
            <small>Pembayaran</small>
        </h1>
        
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="col-xs-12 text-right">
                            
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                            <h4>Order Pembayaran Anggota Tetap</h4>
                            <hr/>
                            <div>
                                <h4>Terima Kasih telah melakukan pemesanan.Untuk mendapatkan akses sebagai anggota tetap, setelah menyelesaikan pembayaran anda, silakan untuk logout dan login ulang.</h4>
                                <br/>
                                <a href="<?php echo base_url(); ?>login/logout" class="btn btn-danger btn-sm mr5 pull-right">Sign out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>