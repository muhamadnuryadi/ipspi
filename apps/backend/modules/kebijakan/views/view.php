<!DOCTYPE html>
<html>
<head>
   
   <style type="text/css">
   p.white, h1.white, h2.white, h3.white, h4.white, h5.white, h6.white, a.white, i.white, span.white, input.white[type="text"], textarea.white, input.white[type="submit"], select.white {
	    color: #ffffff;
	}
	.nicdark_bg_yellow {
	    background-color: #edbf47;
	}
	h5 {
	    font-size: 15px;
	    line-height: 15px;
	}
	h1, h2, h3, h4, h5, h6, input[type="text"], textarea, select {
	    color: #868585;
	    font-family: "Montserrat",sans-serif;
	}

   /* WENDY */
	.container_img_4{
		margin-bottom: 20px;
	}
	.container_img_4:after{
		content: ""; display: block; height: 0; clear: both; visibility: hidden;
	}
	.container_img_4 .container_item{
		width: 25%;
		margin: 0px;
		padding: 0px;
		float: left;
		position: relative;
	}
	.container_img_4 .container_item h5{
		position: absolute;
		padding: 5px;
		bottom: 3px;
	}
	.container_img_4 img{
		width: 100%;
	}
	/*----*/
	.container_img_1{
		margin-bottom: 20px;
	}
	.container_img_1:after{
		content: ""; display: block; height: 0; clear: both; visibility: hidden;
	}
	.container_img_1 .container_item{
		width: 50%;
		margin: 0px auto;
		padding: 0px;
		position: relative;
	}
	.container_img_1 .container_item h5{
		position: absolute;
		padding: 5px;
		bottom: 3px;
	}
	.container_img_1 img{
		width: 100%;
	}
	/*----*/
	.container_img_side{
		margin-bottom: 20px;
	}
	.container_img_side:after{
		content: ""; display: block; height: 0; clear: both; visibility: hidden;
	}
	.container_img_side .container_item_img{
		width: 20%;
		float: left;
		padding: 0px;
	}
	.container_img_side .container_item_text{
		width: 75%;
		float: left;
		padding: 0px;
		margin-left: 20px;
	}
	.container_img_side .container_item_text h5{
		margin-bottom: 10px;
	}
	.container_img_side img{
		width: 100%;
	}
   </style>
</head>
<div style="min-width:700px;max-width:700px">
	<section class="panel position-relative">
		<div class="panel-body">
			<h2><?php echo isset($lists[0]['name'])?$lists[0]['name']:''; ?></h2>
			<hr>
			<p class="small">
				<?php echo isset($lists[0]['body'])?stripcslashes($lists[0]['body']):''; ?>
			</p>
			<iframe width="100%" height="600" name="iframe_a" src="<?php echo $this->webconfig['media-server-files'].$lists[0]['files'];?>"></iframe>
		</div>
	</section>
</div>
</html>

	