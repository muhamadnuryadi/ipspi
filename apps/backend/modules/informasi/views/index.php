<script type="text/javascript">
	jQuery(document).ready(function($) {
		jQuery("#listData").load('<?php echo base_url().$this->router->class; ?>/getAllData/');
	});
	function searchThis(){
		var frm = document.searchform;
		var name = frm.name.value;
		if(name == ''){ name = '-'; }
		jQuery("#listData").load("<?php echo base_url().$this->router->class; ?>/searchdata/"+name.replace(/\s/g,'%20'));
	}
</script>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo strtoupper($this->module_name); ?>
			<small><?php echo $this->lang->line('informasi_teks'); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('/'); ?>"><i class="fa fa-dashboard"></i><?php echo $this->lang->line('dashboard'); ?></a></li>
			<li class="active"><?php echo ucfirst($this->module_name); ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
	                <div class="box-header with-border">
		                 <div class="row">
	                 		<div class="col-md-4 col-md-12">
	                 			<form id="searchform" name="searchform" method="POST" action="<?php echo base_url().$this->router->class; ?>searchdata/" onsubmit="searchThis(); return false;">
								<div class="input-group">
									<input type="text" name="name" class="form-control input-sm pull-left"  placeholder="<?php echo $this->lang->line('navigation_search'); ?>"/>
									<div class="input-group-btn">
										<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
									</div> 
								</div>
								</form>
							</div>
							<div class="col-md-6"></div>
							<div class="col-md-2 col-md-12 text-right">
								<a class="btn btn-success btn-xs" href="<?php echo base_url().$this->router->class; ?>/add">
									<i class="fa fa-fw fa-plus"></i>
									<?php echo $this->lang->line('navigation_add'); ?>
								</a>
							</div>
	                 	</div>
	                </div>
	                <div id="listData">
	                	<center>
	                		<img src='<?php echo $this->webconfig['back_images']; ?>ajax_loading.gif' align='middle' style="margin:5px;" />
	                	</center>
	                </div>
	            </div>
			</div>
		</div>
	</section>
</div>