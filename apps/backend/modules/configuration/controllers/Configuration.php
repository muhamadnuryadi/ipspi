<?php
class Configuration extends CI_Controller{
    var $webconfig;
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
        $this->load->library('session');
        $this->load->sharedModel('ConfigurationModel');
        $this->load->library('pagination');
        $this->load->sharedModel('LoginModel');
        if(!$this->LoginModel->isLoggedIn()){
            redirect(base_url().'login');
        }
        $this->webconfig = $this->config->item('webconfig');
        $this->module_name = $this->lang->line('configuration');
    }
    function index(){
        $tdata = array();                   
        $webconfig = $this->config->item('webconfig');
        $tdata['base_template'] = $webconfig['base_template'];
        $tdata['lists'] = $this->ConfigurationModel->listData();
        require_once(dirname(__FILE__).'/../../../libraries/formvalidator.php');
        if($_POST)
        {
            
            $validator = new FormValidator();
            // $validator->addValidation("dashboard_text","req",$this->lang->line('error_empty_dashboard_text_configuration'));
            
            if($validator->ValidateForm())
            {
                if($this->input->post('id') == ''){
                    $doUpdate = $this->ConfigurationModel->entriData(array(
                                                                'meta_title'=>$this->input->post('meta_title')
                                                                ,'site_title'=>$this->input->post('site_title')
                                                                ,'tagline_text'=>$this->input->post('tagline_text')
                                                                ,'meta_description'=>$this->input->post('meta_description')
                                                                ,'email_contact'=>$this->input->post('email_contact')
                                                                ,'fb_url'=>$this->input->post('fb_url')
                                                                ,'youtube_url'=>$this->input->post('youtube_url')
                                                                ,'twitter_url'=>$this->input->post('twitter_url')
                                                                ,'gplus_url'=>$this->input->post('gplus_url')
                                                                ,'instagram_url'=>$this->input->post('instagram_url')
                                                                ,'address'=>$this->input->post('address')
                                                                ,'phone'=>$this->input->post('phone')
                                                                ,'phone_2'=>$this->input->post('phone_2')
                                                                ,'fax'=>$this->input->post('fax')
                                                                ,'namaprofil'=>$this->input->post('namaprofil')
                                                                ,'bank'=>$this->input->post('bank')
                                                                ,'norek'=>$this->input->post('norek')
                                                          ));
                }else{
                    $doUpdate = $this->ConfigurationModel->updateData(array(
                                                                'id' =>$this->input->post('id')
                                                                ,'meta_title'=>$this->input->post('meta_title')
                                                                ,'site_title'=>$this->input->post('site_title')
                                                                ,'tagline_text'=>$this->input->post('tagline_text')
                                                                ,'meta_description'=>$this->input->post('meta_description')
                                                                ,'email_contact'=>$this->input->post('email_contact')
                                                                ,'fb_url'=>$this->input->post('fb_url')
                                                                ,'youtube_url'=>$this->input->post('youtube_url')
                                                                ,'twitter_url'=>$this->input->post('twitter_url')
                                                                ,'gplus_url'=>$this->input->post('gplus_url')
                                                                ,'instagram_url'=>$this->input->post('instagram_url')
                                                                ,'address'=>$this->input->post('address')
                                                                ,'phone'=>$this->input->post('phone')
                                                                ,'phone_2'=>$this->input->post('phone_2')
                                                                ,'fax'=>$this->input->post('fax')
                                                                ,'namaprofil'=>$this->input->post('namaprofil')
                                                                ,'bank'=>$this->input->post('bank')
                                                                ,'norek'=>$this->input->post('norek')
                                                          ));
                }
                
                
                if($doUpdate == 'empty'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
                }else if($doUpdate == 'failed'){
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
                }else if($doUpdate == 'success'){
                    $tdata['success'] = $this->lang->line('msg_success_entry');             
                }

                $ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
                $this->load->sharedView('template', $ldata);
                
            }
            else
            {   
                $tdata['id']       = $this->input->post('id');
                $tdata['dashboard_text']       = $this->input->post('dashboard_text');
                
                $tdata['error_hash'] = $validator->GetErrors();
                $ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
                $this->load->sharedView('template', $ldata);
            }
        }else{
            $ldata['content'] = $this->load->view($this->router->class.'/index',$tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }
    
}