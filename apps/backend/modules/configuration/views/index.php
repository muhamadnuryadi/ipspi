<script type="text/javascript">
	jQuery(document).ready(function($) {

	});
	<?php if(isset($success)){ ?>
	    setTimeout(function() {
	        window.location.href = "<?php echo base_url().$this->router->class; ?>";
	    }, 1000);
	<?php } ?>
</script>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo strtoupper($this->module_name); ?>
			<small><?php echo $this->lang->line('configuration_teks'); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('/'); ?>"><i class="fa fa-dashboard"></i><?php echo $this->lang->line('dashboard'); ?></a></li>
			<li class="active"><?php echo ucfirst($this->module_name); ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
	                <div class="box-header with-border">
		                 <div class="row">
								<form class="form-horizontal" name="form" method="POST" action="" >
			                    	<input type="hidden" name="id" value='<?php echo isset($lists['id'])?$lists['id']:''; ?>' >
			                        <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
			                            <?php foreach($error_hash as $inp_err){ ?>
			                                <script type="text/javascript">
			                                jQuery(document).ready(function($) {
			                                    toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
			                                    });
			                                </script>
			                            <?php } ?>
			                        <?php } ?>
			                        
			                        <?php if(isset($success)){ ?>
			                            <script type="text/javascript">
			                            jQuery(document).ready(function($) {
			                                toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
			                                });
			                            </script>
			                        <?php } ?>
			                        <div class="form-group">
			                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('site_title'); ?> <span>*</span></label>
			                            <div class="col-sm-6">
			                                <input type="text" id="site_title" name="site_title" value='<?php echo isset($lists['site_title'])?$lists['site_title']:(isset($site_title)?$site_title:''); ?>' size="50" class="form-control">
			                            </div>
			                        </div>
			                       
			                        <div class="form-group">
			                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('meta_title'); ?> <span>*</span></label>
			                            <div class="col-sm-5">
			                                <input type="text" id="meta_title" name="meta_title" value='<?php echo isset($lists['meta_title'])?$lists['meta_title']:(isset($meta_title)?$meta_title:''); ?>' size="50" class="form-control">
			                                <?php echo $this->lang->line('you_have'); ?> <b><span id="charsLeft"></span></b> <?php echo $this->lang->line('char_left'); ?>
			                            </div>
			                        </div>
			                        <div class="form-group">
			                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('meta_description'); ?> <span>*</span></label>
			                            <div class="col-sm-6">
			                                <textarea type="text" id="meta_description" name="meta_description" class="form-control"><?php echo isset($lists['meta_description'])?$lists['meta_description']:(isset($meta_description)?$meta_description:''); ?></textarea>
			                                <?php echo $this->lang->line('you_have'); ?> <b><span id="charsLeft_textarea"></span></b> <?php echo $this->lang->line('char_left'); ?>
			                            </div>
			                        </div>
			                        <div class="form-group">
			                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('namaprofil'); ?> <span>*</span></label>
			                            <div class="col-sm-6">
			                                <input type="text" id="namaprofil" name="namaprofil" value='<?php echo isset($lists['namaprofil'])?$lists['namaprofil']:(isset($namaprofil)?$namaprofil:''); ?>' size="50" class="form-control">
			                            </div>
			                        </div>
			                        <div class="form-group">
			                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('address'); ?> <span>*</span></label>
			                            <div class="col-sm-6">
			                                <input type="text" id="address" name="address" value='<?php echo isset($lists['address'])?$lists['address']:(isset($address)?$address:''); ?>' size="50" class="form-control">
			                            </div>
			                        </div>
			                        <div class="form-group">
			                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('fax'); ?> <span>*</span></label>
			                            <div class="col-sm-6">
			                                <input type="text" id="fax" name="fax" value='<?php echo isset($lists['fax'])?$lists['fax']:(isset($fax)?$fax:''); ?>' size="50" class="form-control">
			                            </div>
			                        </div>
			                        <div class="form-group">
			                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('phone'); ?> <span>*</span></label>
			                            <div class="col-sm-6">
			                                <input type="text" id="phone" name="phone" value='<?php echo isset($lists['phone'])?$lists['phone']:(isset($phone)?$phone:''); ?>' size="50" class="form-control">
			                            </div>
			                        </div>
			                        <div class="form-group">
			                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('phone'); ?> 2<span>*</span></label>
			                            <div class="col-sm-6">
			                                <input type="text" id="phone_2" name="phone_2" value='<?php echo isset($lists['phone_2'])?$lists['phone_2']:(isset($phone_2)?$phone_2:''); ?>' size="50" class="form-control">
			                            </div>
			                        </div>
			                        <div class="form-group">
			                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('email_contact'); ?> <span>*</span></label>
			                            <div class="col-sm-6">
			                                <input type="text" id="email_contact" name="email_contact" value='<?php echo isset($lists['email_contact'])?$lists['email_contact']:(isset($email_contact)?$email_contact:''); ?>' size="50" class="form-control">
			                            </div>
			                        </div>
			                        <div class="form-group">
			                            <label class="col-sm-3 control-label">Bank <span>*</span></label>
			                            <div class="col-sm-6">
			                                <input type="text" id="bank" name="bank" value='<?php echo isset($lists['bank'])?$lists['bank']:(isset($bank)?$bank:''); ?>' size="50" class="form-control">
			                            </div>
			                        </div>
			                        <div class="form-group">
			                            <label class="col-sm-3 control-label">No Rekening <span>*</span></label>
			                            <div class="col-sm-6">
			                                <input type="text" id="norek" name="norek" value='<?php echo isset($lists['norek'])?$lists['norek']:(isset($norek)?$norek:''); ?>' size="50" class="form-control">
			                            </div>
			                        </div>
			                        <div class="form-group">
			                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('fb_url'); ?></label>
			                            <div class="col-sm-6">
			                                <input type="text" id="fb_url" name="fb_url" value='<?php echo isset($lists['fb_url'])?$lists['fb_url']:(isset($fb_url)?$fb_url:''); ?>' size="50" class="form-control">
			                            </div>
			                        </div>
			                        <div class="form-group">
			                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('youtube_url'); ?></label>
			                            <div class="col-sm-6">
			                                <input type="text" id="youtube_url" name="youtube_url" value='<?php echo isset($lists['youtube_url'])?$lists['youtube_url']:(isset($youtube_url)?$youtube_url:''); ?>' size="50" class="form-control">
			                            </div>
			                        </div>
			                        <div class="form-group">
			                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('twitter_url'); ?></label>
			                            <div class="col-sm-6">
			                                <input type="text" id="twitter_url" name="twitter_url" value='<?php echo isset($lists['twitter_url'])?$lists['twitter_url']:(isset($twitter_url)?$twitter_url:''); ?>' size="50" class="form-control">
			                            </div>
			                        </div>
			                        <div class="form-group">
			                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('gplus_url'); ?></label>
			                            <div class="col-sm-6">
			                                <input type="text" id="gplus_url" name="gplus_url" value='<?php echo isset($lists['gplus_url'])?$lists['gplus_url']:(isset($gplus_url)?$gplus_url:''); ?>' size="50" class="form-control">
			                            </div>
			                        </div>
			                        <div class="form-group">
			                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('instagram_url'); ?></label>
			                            <div class="col-sm-6">
			                                <input type="text" id="instagram_url" name="instagram_url" value='<?php echo isset($lists['instagram_url'])?$lists['instagram_url']:(isset($instagram_url)?$instagram_url:''); ?>' size="50" class="form-control">
			                            </div>
			                        </div>
			                        
			                        <div class="form-group footertable">
			                            <label class="col-md-3 control-label">&nbsp;</label>
			                            <div class="col-md-5 text-left">
			                                <input class="btn btn-danger btn-sm mr5" type="submit" value="<?php echo $this->lang->line('navigation_kirim'); ?>">
			                            </div>
			                        </div>
			                    </form>
	                 	</div>
	                </div>
	            </div>
			</div>
		</div>
	</section>
</div>