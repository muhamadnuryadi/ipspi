<script type="text/javascript">
jQuery(document).ready(function($) {
    
});
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo strtoupper($this->module_name); ?>
            <small>Pembayaran</small>
        </h1>
        
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="col-xs-12 text-right">
                            <a class="btn btn-success btn-xs" href="<?php echo base_url().$this->router->class; ?>"  title="<?php echo $this->lang->line('nav_kembali'); ?>">
                                <i class="fa fa-fw fa-plus"></i>
                                <?php echo $this->lang->line('nav_kembali'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                            <h4>Order Pembayaran Anggota Tetap</h4>
                            <hr/>
                            <div>
                                <h4>Terima Kasih telah melakukan pemesanan. Silakan seleseaikan pembayaran anda</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>