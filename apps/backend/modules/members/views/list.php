<script type="text/javascript">
jQuery(document).ready(function($) {
	$('#checkall').click(function () {
		$('#tabellistdata .check-all').attr('checked', this.checked);
	});
	$(".pagination a").click(function(e){
		// stop normal link click
        jQuery(".table-responsive").html("<center><img src='<?php echo $this->webconfig['back_images']; ?>ajax_loading.gif' align='middle' style='margin:5px;' /></center>");
		e.preventDefault(); 
		var linkUrl = jQuery(this).attr("href");
		if(linkUrl){
			jQuery('#listData').load(linkUrl.replace(/\s/g,'%20'));
		}
	});
	$('.fancybox').fancybox();
});

function submitkie(){
	jQuery().ajaxStart(function($) {
		$('#loading').show();
		$('#result').hide();
	}).ajaxStop(function($) {
		$('#loading').hide();
		$('#result').fadeIn('slow');	
		$("#tabellistdata")[0].reset();
	});
	
	jQuery.ajax({
			type: 'POST',
			url: jQuery('#tabellistdata').attr('action'),
			data: jQuery('#tabellistdata').serialize(),
			success: function(response) {
				if(response == 'success'){
					toastr.success("<?php echo $this->lang->line('msg_success_delete'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
				}else{
					toastr.error("<?php echo $this->lang->line('msg_empty_delete'); ?>", "<?php echo $this->lang->line('error_notif'); ?>");
				}
				
				jQuery("#listData").load('<?php echo $_SERVER["REQUEST_URI"]; ?>');
			}
		})
		return false;
}
function deleteThis(code){
	var txt = "<?php echo $this->lang->line('alert_delete'); ?> <input type='hidden' id='alertName' name='alertName' value='"+code+"' />";
	jQuery.prompt(txt ,{  submit: doCondition, buttons: { <?php echo $this->lang->line('ok'); ?>: true, <?php echo $this->lang->line('cancel'); ?>: false },prefix:'jqismooth' });
}
function doCondition(v,m,f,e){
	console.log(v)
		console.log(m)
		console.log(f)
		console.log(e)
	  if(m){
		var posting = "dataid="+e.alertName;
		jQuery.ajax({
			type: 'POST',
			url: "<?php echo base_url().$this->router->class; ?>/deleteThis",
			data: posting,
			success: function(response) {
				console.log(response)
				if(response == 'success'){
					toastr.success("<?php echo $this->lang->line('msg_success_delete'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
				}else{
					toastr.error("<?php echo $this->lang->line('msg_empty_delete'); ?>", "<?php echo $this->lang->line('error_notif'); ?>");
				}
				jQuery("#listData").load('<?php echo $_SERVER["REQUEST_URI"]; ?>');
			}
		})
	  }else{
	  	jQuery.prompt.close();
	  }
}
function publishThis(code){
	var txt = "<?php echo $this->lang->line('alert_publish'); ?>  <input type='hidden' id='alertName' name='alertName' value='"+code+"' />";
	$.prompt(txt ,{  submit: doPublish, buttons: { Ok: true, Cancel: false },prefix:'jqismooth' });
}
function doPublish(v,m,f,e){
	  if(m){
		var posting = "dataid="+e.alertName;
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url().$this->router->class; ?>/publish/",
			data: posting,
			success: function(response) {
				if(response == 'success'){
					toastr.success('<?php echo $this->lang->line('msg_success_publish'); ?>','<?php echo $this->lang->line('success_notif'); ?>', 3000);
				}else{
					toastr.error('<?php echo $this->lang->line('msg_empty_publish'); ?>', '<?php echo $this->lang->line('error_notif'); ?>',3000);
				}
				$("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
	  }else{
	  	$.prompt.close();
	  }
}
function unpublishThis(code,page){
	var txt = "<?php echo $this->lang->line('alert_unpublish'); ?>  <input type='hidden' id='alertName' name='alertName' value='"+code+"' />";
	$.prompt(txt ,{  submit: doUnpublish, buttons: { Ok: true, Cancel: false },prefix:'jqismooth' });
}
function doUnpublish(v,m,f,e){
	  if(m){
	  	var posting = "dataid="+e.alertName;
		
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url().$this->router->class; ?>/unpublish/",
			data: posting,
			success: function(response) {
				if(response == 'success'){
					toastr.success('<?php echo $this->lang->line('msg_success_unpublish'); ?>','<?php echo $this->lang->line('success_notif'); ?>', 3000);
				}else{
					toastr.error('<?php echo $this->lang->line('msg_empty_unpublish'); ?>','<?php echo $this->lang->line('error_notif'); ?>', 3000);
				}
				$("#listData").load('<?php echo $_SERVER['REQUEST_URI']; ?>');
			}
		})
	  }else{
	  	$.prompt.close();
	  }
}

function generateNoAnggota(id){
	// alert(id);
	$.fancybox.open([{
        type:'iframe',
        width:'600',
        height:'350',
        autoSize : false,
        href:'<?php echo base_url() ?>members/generateAnggota/'+id,
        scrolling : 'no',
    }])
}
</script>
<form id="tabellistdata" method="post" action="<?php echo base_url().$this->router->class; ?>/postProcess" onsubmit="return false;">
<div class="box-body table-responsive no-padding">
	<?php if(isset($lists) && count($lists) > 0) {?>
	<table class="table table-bordered table-striped">
	<tr>
        <th width="10" class="text-center">
        	<input type="checkbox" id="checkall" />
        </th>
        <th width="10" class="text-center">
        	<?php echo $this->lang->line('tablelist_no'); ?>
        </th>
        <th>
        	<?php echo $this->lang->line('name_members'); ?> / Email
        </th>
        <th>
        	Provinsi / DPD
        </th>

        <th>
        	Kota
        </th>

        <th>
        	No. Anggota
        </th>
        <th>
        	Detail
        </th>
        <?php if($this->session->userdata('role') != '3'){?>
        <th>
        </th>
    	<?php } ?>
        <th>
        	<?php echo $this->lang->line('status'); ?>
        </th>
        <th colspan="2" class="text-center" width="30">
        	<?php echo $this->lang->line('tablelist_option'); ?>
        </th>
    </tr>
	<?php $i = $start_no; foreach ($lists as $list) { $i++; ?>
        <tr>
           	<td class="text-center">
            	<input type="checkbox" name="datacek[]" class="check-all" value="<?php echo $list['id']; ?>" />
            </td>
            <td class="text-center">
            	<?php echo $i; ?>
            </td>
            <td>
            	<?php echo $list['nama']; ?>
            	<br/>
            	<small>
            		<?php echo isset($list['email'])?$list['email']:''; ?>		
            	</small>
            </td>
            <td>
            	<?php 
            	echo isset($list['nama_provinsi']['nama'])?$list['nama_provinsi']['nama']:'';
            	?>	
            </td>
            <td>
            	<?php 
            	echo isset($list['nama_kota']['nama'])?$list['nama_kota']['nama']:'';
            	?>	
            </td>
            <td>
            	<?php echo isset($list['no_anggota'])?$list['no_anggota']:''; ?>
            </td>

            <td width='10'>
            	<a class="btn btn-danger btn-xs fancybox fancybox.ajax" href="<?php echo base_url().$this->router->class; ?>/view/<?php echo $list['id']; ?>">
        			Detail
        		</a>
            </td>
            <?php if($this->session->userdata('role') == '1'){?>
            <td width='10'>
            	<a class="btn btn-danger btn-xs" onclick="generateNoAnggota(<?php echo $list['id'] ?>)" href="#">
        			Generate No Anggota
        		</a>
            </td>
        	<?php }else{ ?>
        		<td width='10'>
	            	-
	            </td>
        	<?php } ?>
            <td>
            	<?php if($this->session->userdata('role') != '3'){?>
            	<?php 
					switch($list['status']){
						case 0:
						?>
							<a href="javascript:void(0)" onclick="publishThis(<?php echo $list['id']; ?>)"  title="<?php echo $this->lang->line('navigation_publish'); ?>">
							<button class="btn btn-primary btn-xs" type="button">
								<i class="fa fa-arrow-up"></i> <?php echo $this->lang->line('navigation_publish'); ?>
							</button>
							</a>
                        <?php
							break;
						case 1:
						?>
							<a href="javascript:void(0)" onclick="unpublishThis(<?php echo $list['id']; ?>)" title="<?php echo $this->lang->line('navigation_unpublish'); ?>">
							<button class="btn btn-warning btn-xs" type="button">
								<i class="fa fa-arrow-down"></i> <?php echo $this->lang->line('navigation_unpublish'); ?>
							</button>
							</a>
                        <?php
							break;
					}
				?>
				<?php } ?>
				<?php if($this->session->userdata('role') == '3'){?>
					<?php 
						switch($list['status']){
							case 0:
							?>
								Tidak Aktif 
	                        <?php
								break;
							case 1:
							?>
								Aktif
	                        <?php
								break;
						}
					?>
				<?php } ?>
            </td>
            <td width='10' class="text-center">
            	<a href="<?php echo base_url('/members')."/modif/".$list['id']; ?>" title='<?php echo $this->lang->line('navigation_modif'); ?>'>
            	<button class="btn btn-info btn-xs" type="button">
        			<i class="fa fa-fw fa-edit"></i>
        		</button>
	        	</a>  
            </td>
            <td width='10' class="text-center">
            	<?php if($this->session->userdata('role') != '3'){?>
            	<button class="btn btn-danger btn-xs" type="button" onclick="deleteThis(<?php echo $list['id']; ?>)" title="<?php echo $this->lang->line('navigation_delete'); ?>">
        			<i class="fa fa-fw fa-trash"></i>
        		</button> 
        		<?php } ?>
            </td>
        </tr>
    <?php } ?>
	</table>
</div><!-- /.box-body -->
<div class="box-footer clearfix">
	<?php if($this->session->userdata('role') != '3'){?>
	<button onclick="submitkie()" type="button" class="btn btn-danger btn-xs pull-left">
		<i class="fa fa-trash"></i>Hapus
	</button>
	<?php } ?>
  	<ul class="pagination pagination-sm no-margin pull-right"><?php echo isset($pagination)?$pagination:""; ?></ul>
</div>
<?php } else { ?>
	<p><center><?php echo $this->lang->line('no_data'); ?></center></p>
<?php } ?>
</form>
