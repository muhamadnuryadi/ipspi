<script type="text/javascript">
jQuery(document).ready(function($) {
    $('.datepicker').datepicker({
        format: "dd-mm-yyyy",
        autoclose: true
    });
    <?php  if(isset($lists[0]['id_propinsi']) && $lists[0]['id_propinsi'] !=''){?>
        getkota(<?php echo $lists[0]['id_propinsi'];?>);
    <?php } ?>
    $("#id_propinsi").change(function(){
        $("#id_kota").html('');
        var arr = {prop:$(this).val()};
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>wilayah/getkota",
            data: arr,
            dataType: "json",
            success: function(data) {
                var html = '<option value="">-- Pilih Kota/Kabupaten --</option>';
                $("#id_kota").html(html);
                if(data.length > 0) {
                    $.each(data, function(key, val) {
                        $("#id_kota").append($("<option />").val(val.id).text(val.nama));
                    });   
                }
            }
        });
    });

    // jQuery("#listData").load('<?php echo base_url().$this->router->class; ?>/getProfile/');
});
function getkota(id_propinsi){
    $("#id_kota").html('');
    var arr = {prop:id_propinsi};
    $.ajax({
        type: 'POST',
        url: "<?php echo base_url(); ?>wilayah/getkota",
        data: arr,
        dataType: "json",
        success: function(data) {
            var html = '<option value="">-- Pilih Kota/Kabupaten--</option>';
            $("#id_kota").html(html);
            var citydb = '<?php echo isset($lists[0]['id_kota'])?$lists[0]['id_kota']:''; ?>';
            if(data.length > 0) {
              $.each(data, function(key, val) {
                if(val.id == citydb){
                  $("#id_kota").append($("<option />").val(val.id).text(val.nama).prop('selected', true));
                }else{
                   $("#id_kota").append($("<option />").val(val.id).text(val.nama));
                }
              });
            }
        }
    });
}
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo strtoupper($this->module_name); ?>
            <small><?php echo $this->lang->line('add_members'); ?></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('/'); ?>">
                    <i class="fa fa-dashboard"></i>
                    <?php echo $this->lang->line('dashboard'); ?>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url().$this->router->class; ?>"><?php echo ucfirst($this->module_name); ?></a>
            </li>
            <li class="active"><?php echo $this->lang->line('navigation_add'); ?></li>
        </ol>
    </section>
    <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
        <?php foreach($error_hash as $inp_err){ ?>
            <script type="text/javascript">
            jQuery(document).ready(function($) {
                toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                });
            </script>
        <?php } ?>
    <?php } ?>
    
    <?php if(isset($success)){ ?>
        <script type="text/javascript">
        jQuery(document).ready(function($) {
            toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
            });
        </script>
    <?php } ?>
    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a style="padding: 10px 12px;" href="#profile" data-toggle="tab">Profile Form</a></li>
                <li><a style="padding: 10px 12px;" href="#pendidikan" data-toggle="tab">Pendidikan</a></li>
                <li><a style="padding: 10px 12px;" href="#pekerjaan" data-toggle="tab">Pekerjaan</a></li>
                <li><a style="padding: 10px 12px;" href="#peksos" data-toggle="tab">Praktek Peksos</a></li>
                <li><a style="padding: 10px 12px;" href="#penghargaan" data-toggle="tab">Penghargaan</a></li>
                <li><a style="padding: 10px 12px;" href="#sertifikasi" data-toggle="tab">Sertifikasi</a></li>
                <li><a style="padding: 10px 12px;" href="#referensi" data-toggle="tab">Referensi</a></li>
                <li><a style="padding: 10px 12px;" href="#profesi" data-toggle="tab">Organisasi Profesi</a></li>
                <li><a style="padding: 10px 12px;" href="#komunitas" data-toggle="tab">Komunitas Internet</a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="profile">
                    <form class="form-horizontal" name="form" method="POST" action="<?php echo base_url().$this->router->class; ?>/add" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Nama</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="nama" placeholder="" name="nama" value="<?php echo isset($lists[0]['nama'])?$lists[0]['nama']:''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Username</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="username" placeholder="" name="username" value="<?php echo isset($lists[0]['username'])?$lists[0]['username']:''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="email" placeholder="" name="email" value="<?php echo isset($lists[0]['email'])?$lists[0]['email']:''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Password</label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control" id="password" placeholder="" name="password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Ulangi Password</label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control" id="password_1" placeholder="" name="password_1">
                                </div>
                            </div>
                            <div class="form-group">
                              <label for="inputEmail3" class="col-sm-3 control-label">Tgl Lahir</label>
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <input type="text" id="tgllahir" name="tgllahir" value='<?php echo isset($lists[0]['tgllahir'])?date('d-m-Y', strtotime($lists[0]['tgllahir'])):''; ?>' size="50" class="form-control datepicker">
                                        <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Status Perkawinan</label>
                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label>
                                            <input type="radio" name="statuskawin" value="0" <?php if(!isset($status)){ echo "checked=checked"; } ?> <?php if(isset($lists[0]['statuskawin']) && $lists[0]['statuskawin'] == 0){ echo "checked=checked"; }?> /> Belum Menikah
                                        </label>
                                        <label>
                                            <input type="radio" name="statuskawin" value="1" <?php if(isset($lists[0]['statuskawin']) && $lists[0]['statuskawin'] == 1){ echo "checked=checked"; }?> /> Menikah
                                        </label>
                                        <label>
                                            <input type="radio" name="statuskawin" value="2" <?php if(isset($lists[0]['statuskawin']) && $lists[0]['statuskawin'] == 2){ echo "checked=checked"; }?> /> Duda/janda
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Jumlah Anak</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="jumanak" placeholder="" name="jumanak" value="<?php echo isset($lists[0]['jumanak'])?$lists[0]['jumanak']:''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Alamat kantor</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control" rows="3" placeholder="" name="alamatkantor"><?php echo isset($lists[0]['alamatkantor'])?$lists[0]['alamatkantor']:''; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Telp Kantor</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="telpkantor" placeholder="" name="telpkantor" value="<?php echo isset($lists[0]['telpkantor'])?$lists[0]['telpkantor']:''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Fax Kantor</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="faxkantor" placeholder="" name="faxkantor" value="<?php echo isset($lists[0]['faxkantor'])?$lists[0]['faxkantor']:''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Alamat Rumah</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="alamatrumah" placeholder="" name="alamatrumah" value="<?php echo isset($lists[0]['alamatrumah'])?$lists[0]['alamatrumah']:''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Provinsi</label>
                                <div class="col-sm-6">
                                    <select name="id_propinsi" class="form-control select2" id="id_propinsi" style="padding: 3px;">
                                        <option value="">-- <?php echo $this->lang->line('pilih_propinsi'); ?> --</option>
                                        <?php foreach ($wilayah as $key => $value){ ?>
                                            <option <?php echo (isset($lists[0]['id_propinsi']) && $lists[0]['id_propinsi'] == $value['id'])?"selected='selected'":""; ?> value="<?php echo $value['id']; ?>"><?php echo $value['nama']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Kota</label>
                                <div class="col-sm-6">
                                    <select name="id_kota" class="form-control select2" id="id_kota" style="padding: 3px;">
                                        <option value="">-- <?php echo $this->lang->line('pilih_kota'); ?> --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Telp</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="telp" placeholder="" name="telp" value="<?php echo isset($lists[0]['telp'])?$lists[0]['telp']:''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">HP</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="hp" placeholder="" name="hp" value="<?php echo isset($lists[0]['hp'])?$lists[0]['hp']:''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Status</label>
                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label>
                                            <input type="radio" name="status" value="1" <?php if(!isset($status)){ echo "checked=checked"; } ?>  <?php if(isset($lists[0]['status']) && $lists[0]['status'] == 1){ echo "checked=checked"; }?> /> Aktif
                                        </label>
                                        <label>
                                            <input type="radio" name="status" value="0" <?php if(isset($lists[0]['status']) && $lists[0]['status'] == 0){ echo "checked=checked"; }?> /> Tidak Aktif
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                              <label for="inputEmail3" class="col-sm-3 control-label">Tgl Daftar</label>
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <input type="text" id="created_at" name="created_at" value='<?php echo isset($lists[0]['created_at'])?date('d-m-Y', strtotime($lists[0]['created_at'])):''; ?>' size="50" class="form-control datepicker">
                                        <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">Ijazah</label>
                            <div class="col-sm-6">
                                <input type="file" class="form-control" id="path_ijazah" placeholder="" name="path_ijazah" value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">KTP</label>
                            <div class="col-sm-6">
                                <input type="file" class="form-control" id="path_ktp" placeholder="" name="path_ktp" value="">
                            </div>
                        </div>

                        <div class="box-footer">
                            <!-- <button type="submit" class="btn btn-default">Cancel</button> -->
                            <button type="submit" class="btn btn-info pull-right">Simpan</button>
                        </div>
                    </form>
                </div>

                <div class=" tab-pane" id="pendidikan">
                    <div id="listPendidikan">
                        <center>
                            Silahkan Klik Simpan Terlebih dahulu
                        </center>
                    </div>
                </div>

                <div class="tab-pane" id="pekerjaan">
                    
                </div>

                <div class="tab-pane" id="peksos">
                    
                </div>

                <div class="tab-pane" id="penghargaan">
                    
                </div>

                <div class="tab-pane" id="sertifikasi">
                    
                </div>

                <div class="tab-pane" id="referensi">
                    
                </div>

                <div class="tab-pane" id="profesi">
                    
                </div>

                <div class="tab-pane" id="komunitas">
                    
                </div>
            </div>
        </div>
    </section>
</div>