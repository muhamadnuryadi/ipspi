<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.js"></script>
<link href="https://fonts.googleapis.com/css?family=Poppins:400,600,700,800&display=swap" rel="stylesheet">
<style type="text/css">
body {
    /*font-family: 'Poppins', serif;
    font-size: 18pt;
    color: #000;
    line-height: 30pt;
    background-color: #fff;*/
}
.container-card {
    /*max-height: 300px;
    max-width:  600px;
    margin: 0;
    padding: 0;*/
    max-width: 75vw;
    /*width: 100%;*/
    margin: 0;
    padding: 0;
    font-family: 'Poppins', serif;
    font-size: 3vw;
    color: #000;
    line-height: 2.5vw;
    background-color: #fff;
}
.logo{
    float: right;
    width: 26%;
}
.clear{
    clear: both;
}
.header-card {
    background-image: url(<?php echo $this->webconfig['back_base_template']; ?>images/head.jpg) ;
    /*background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    height: 93px;
    padding: 15px;
    border-top-left-radius: 50px;
    border-top-right-radius: 50px;
    border-left: 1px solid #ddd;
    border-top: 1px solid #ddd;
    border-right: 1px solid #ddd;*/

    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    padding: 1vw 0.5vw;
    border-top-left-radius: 2vw;
    border-top-right-radius: 2vw;
    border-left: 1px solid #ddd;
    border-top: 1px solid #ddd;
    border-right: 1px solid #ddd;
}
.header-card p{
    /*display: inline-block;
    margin: 20px 30px;
    font-size: 25px;
    font-weight:    ;*/

    display: inline-block;
    margin: 2vw 1vw;
    font-size: 3vw;
    font-weight: 800;
}
.divider{
    /*background: #78c400;
    width: 100%;
    height: 15px;*/

    background: #78c400;
    width: 100%;
    height: 2vw;
}
.content-card{
    background-image: url(<?php echo $this->webconfig['back_base_template']; ?>images/bg.jpg) ;
    /*background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    height: 300px;
    padding: 15px 40px 40px 40px;
    border: 1px solid #033A71;
    color: #fff;
    position: relative;
    border-bottom-left-radius: 50px;
    border-bottom-right-radius: 50px;*/

    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    padding: 1vw 2vw 2vw 2vw;
    border: 1px solid #033A71;
    color: #fff;
    position: relative;
    border-bottom-left-radius: 2vw;
    border-bottom-right-radius: 2vw;
}
.content-card p{
    /*display: block;
    margin: 0;
    font-size: 20px;
    font-weight: 600;*/

    display: block;
    margin: 0;
    font-size: 2vw;
    line-height: 3vw;
    font-weight: 600;
}
.content-card span{
    /*display: block;
    margin: 0;
    font-size: 16px;
    font-weight: 400;
    font-style: italic;*/

    display: block;
    margin: 0;
    font-size: 1.3vw;
    font-weight: 400;
    font-style: italic;
}
.row-info{
    width: 100%;
    display: flex;
}
.box-info{
    position: absolute;
    left: 40px;
    bottom: 40px;
}
.dynamic-box{
    /*margin-bottom: 20px;*/
    margin-top: 4vw;
    margin-bottom: 1vw;
}
.dynamic-box h4{
    /*color: #9aec00;
    font-weight: 700;
    font-size: 18px;
    margin: 0;
    line-height: 27px;*/

    color: #9aec00;
    font-weight: 700;
    font-size: 2vw;
    margin: 0;
    line-height: 3vw;
}
.dynamic-box h2{
    /*color: #fff;
    font-weight: 700;
    font-size: 30px;
    margin: 0;
    line-height: 25px;*/

    color: #fff;
    font-weight: 700;
    font-size: 2vw;
    margin: 0;
    line-height: 2vw;
}
.barcode{
    /*width: 70%;*/

    width: 45%;
    float: left;
}
.barcode img{
    width: 100%;
    /*max-width: 100%;*/
}
.info{
    /*width: 50%;
    float: left;
    text-align: right;
    position: absolute;
    right: 40px;
    bottom: 40px;*/

    width: 60%;
    float: left;
    text-align: right;
    display: flex;
    flex-direction: column;
    justify-content: center;
}
.info p{
    /*color: #9aec00;*/

    color: #9aec00;
    font-size: 2vw;
}
</style>
<script type="text/javascript">
jQuery(document).ready(function($) {
    var element = $("#html-content-holder"); // global variable
    var getCanvas; // global variable

    // $("#btn-Preview-Image").on('click', function () {
         html2canvas(element, {
         onrendered: function (canvas) {
                // $("#previewImage").append(canvas);
                getCanvas = canvas;
             }
         });
    // });

    $("#btn-Convert-Html2Image").on('click', function () {
        var imgageData = getCanvas.toDataURL("image/png");
        // Now browser starts downloading it instead of just showing it
        var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
        $("#btn-Convert-Html2Image").attr("download", "<?php echo isset($member[0]['no_anggota'])?$member[0]['no_anggota'].'.png':'no_anggota.png';?>").attr("href", newData);
    });
});
function downloadThis(id){
    var posting = "dataid="+id;
    jQuery.ajax({
        type: 'POST',
        url: "<?php echo base_url().$this->router->class; ?>/modifTglSurat",
        data: posting,
        success: function(response) {
            if(response == 'success'){
                toastr.success("Surat Rekomendasi Berhasil Digenerate", "<?php echo $this->lang->line('success_notif'); ?>");
                window.onload = timedRefresh(3000);
            }else if(response == 'notupdate'){
                toastr.info("Surat Rekomendasi Tidak diupdate", "Informasi");
            }else{
                toastr.error("Surat Rekomendasi Gagal Digenerate", "<?php echo $this->lang->line('error_notif'); ?>");
            }
        }
    })
}
function timedRefresh(timeoutPeriod) {
    // setTimeout("location.reload(true);",timeoutPeriod);
}
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo strtoupper($this->module_name); ?>
            <small>Halaman Kartu Anggota</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('/'); ?>">
                    <i class="fa fa-dashboard"></i>
                    <?php echo $this->lang->line('dashboard'); ?>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url().$this->router->class; ?>"><?php echo ucfirst($this->module_name); ?></a>
            </li>
        </ol>
    </section>
    <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
        <?php foreach($error_hash as $inp_err){ ?>
            <script type="text/javascript">
            jQuery(document).ready(function($) {
                toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                });
            </script>
        <?php } ?>
    <?php } ?>
    
    <?php if(isset($success)){ ?>
        <script type="text/javascript">
        jQuery(document).ready(function($) {
            toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
            });
        </script>
    <?php } ?>
    <section class="content">
        <form class="form-horizontal" name="form" method="POST" action="<?php echo base_url().$this->router->class; ?>/rekomendasi" enctype="multipart/form-data" onsubmit="return false;">
            <div class="box box-default" >
                <div class="box-header with-border">
                  <i class="fa fa-download"></i>
                  <h3 class="box-title">KARTU ANGGOTA</h3>
                </div>
                <div class="box-body" style="" id="html-content-holder" >
                    <div class="col-md-8 col-xs-12">
                        <div class="container-card" >
                            <div class="header-card">
                                <p>KARTU ANGGOTA</p>
                                <img src="<?php echo $this->webconfig['back_base_template']; ?>img/logo.jpg" alt="Logo" class="logo" />
                                <div class="clear"></div>
                            </div>
                            <div class="divider"></div>
                            <div class="content-card">
                                <p>Ikatan Pekerja Sosial Profesional Indonesia </p>
                                <span>Indonesian Association of Social Workers</span>
                                <div class="dynamic-box">
                                    <h4><?php echo $member[0]['no_anggota'];?></h4>
                                    <h2><?php echo ucwords($member[0]['nama']);?></h2>
                                </div>
                                <div class="row-info">                
                                    <div class="barcode"><img src="<?php echo base_url().$this->router->class; ?>/set_barcode/<?php echo $member[0]['no_anggota'];?>" alt="Barcode" /></div>
                                    <div class="info">
                                        <!-- <p>Anggota Biasa</p> -->
                                        <?php if($jenismember2 =='1'){?>
                                            <p>Anggota Biasa</p>
                                        <?php } else { ?>
                                            <p>Anggota Rekan</p>
                                        <?php } ?>
                                        <span>Berlaku sampai <?php echo $masaberlaku;?></span>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <!-- <div id="previewImage"></div> -->
                    </div>
                </div>
                <div class="col-xs-12">
                    <a id="btn-Convert-Html2Image" href="#" class="button">
                        <button class="btn btn-info btn-xs" type="button">
                            <i class="fa fa-fw fa-download"></i> Download
                        </button>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
            </div>



        </form>
    </section>
</div>