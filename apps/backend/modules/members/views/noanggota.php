<link href="<?php echo $this->webconfig['back_base_template']; ?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
<div>
	<section class="panel position-relative">
		<div class="panel-body">
			<form action="" method="post">
				<h3>
					Generate No Anggota
				</h3>
				<hr>
				<div>
					<?php 
					if (isset($error_hash['noanggota_input'])) {
						?>
						<div class="help-block bg-danger" style="padding:10px 5px;">
							<?php echo $error_hash['noanggota_input']; ?>	
						</div>
						<?php
						echo "<br/><br/>";
					}

					if (isset($success) && $success == 'success') {
						?>
						<div class="help-block bg-success" style="padding:10px 5px;">
							Data Berhasil Disimpan
						</div>
						<script type="text/javascript">
						  setTimeout(function(){
						    // window.location.reload(true);
						  },2000)
						</script>
						<?php
						echo "<br/><br/>";
					}

					if (isset($exist) && $exist == 'exist') {
						?>
						<div class="help-block bg-danger" style="padding:10px 5px;">
							No Anggota yang anda masukan sudah digunakan
						</div>
						<?php
						echo "<br/><br/>";
					}

					$noanggota_input = '';
					// $no_anggota = '';
					if (isset($no_anggota_from_db) && $no_anggota_from_db != '') {
						$noanggota = explode('.', $no_anggota_from_db);
						$noanggota_fix = $noanggota[0].'.'.$noanggota[1].'.'.$noanggota[2].'.'.$noanggota[3].'.';
						$noanggota_input = $noanggota[4];
						// $no_anggota = $noanggota_fix;
					}else{
						// $no_anggota = $no_anggota_generate;
					}
					$no_anggota = $no_anggota_generate;
					?>
					No Anggota : <input type="text" readonly="readonly" name="noanggota" value="<?php echo $no_anggota; ?>">
					<input type="text" readonly="readonly" name="noanggota_input" maxlength="5" value="<?php echo $newno_anggota ; ?>">
					<br/><br/>
					<input type="submit">
				</div>

			</form>
		</div>
	</section>
</div>