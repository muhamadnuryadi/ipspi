<script type="text/javascript">
$(document).ready(function() {

});
</script>
<script type="text/javascript">
function openTablePendidikan(){
    var dt = new Date();
    var dstring = dt.getTime();
    var numOfVisibleRows = $('tr').length;
    var counting = numOfVisibleRows+1;
    var blockhtml = '';
    blockhtml += '<tr>';
        blockhtml += '<td>';
            blockhtml +=  '<select name="jenis_pendidikan[]" class="form-control input-sm">';
                <?php 
                foreach ($this->webconfig['pendidikan_jenis'] as $key2 => $value2) {
                    ?>
                    blockhtml += '<option value="<?php echo $key2 ?>"> <?php echo $value2 ?></option>';
                    <?php
                }
                ?>
            blockhtml += '</select>';
        blockhtml +=  '</td>';
        blockhtml += '<td>';
            blockhtml += '<input class="form-control input-sm" type="hidden" name="idmember[]" value="">';
            blockhtml += '<select name="tingkat[]" class="form-control" id="tingkat" style="padding: 3px;">';
            blockhtml += '<option value="">-- Pendidikan --</option>';
            <?php if (isset($tingkatpendidikan) && count($tingkatpendidikan) > 0){ ?>
            <?php foreach ($tingkatpendidikan as $row => $list){ ?>
            blockhtml += '<option <?php echo (isset($value['tingkat']) && $value['tingkat'] == $list['id'])?"selected='selected'":""; ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>';
            <?php } ?>
            <?php } ?>
            blockhtml += '</select>';
        blockhtml += '</td>';
        blockhtml += '<td>';
            blockhtml += '<input class="form-control input-sm" type="text" name="pt[]" value="">';
        blockhtml += '</td>';
        blockhtml += '<td>';
            blockhtml += '<select name="jurusan[]" class="form-control" id="tingkat_<?php echo $key;?>" style="padding: 3px;">';
                <?php foreach ($this->webconfig['jurusan_pendidikan'] as $row => $value3){ ?>
                    blockhtml += '<option  value="<?php echo $row; ?>"><?php echo $value3; ?></option>';
                <?php } ?>
            blockhtml += '</select>';
        blockhtml += '</td>';
        blockhtml += '<td>';
            blockhtml += '<input class="form-control input-sm" type="text" name="gelar[]" value="">';
        blockhtml += '</td>';
        blockhtml += '<td>';
            blockhtml += '<input class="form-control input-sm" type="text" name="thn_lulus[]" value="">';
        blockhtml += '</td>';
        blockhtml += '<td>';
             blockhtml += '<a class="btn btn-danger btn-xs" onclick="deleteDetailPendidikan(this)" href="javascript:void(0)">';
                 blockhtml += '<i class="ti-trash"></i>';
                 blockhtml += 'Hapus';
             blockhtml += '</a>';
        blockhtml += '</td>';
    blockhtml += '</tr>';
    $('#tablependidikan tbody tr:last').after(blockhtml);
}
function deleteDetailPendidikan(obj){
    $(obj).parent().parent().remove();
}
function deleteThisPendidikan(code){
    var txt = "<?php echo $this->lang->line('alert_delete'); ?> <input type='hidden' id='alertName' name='alertName' value='"+code+"' />";
    jQuery.prompt(txt ,{  submit: doConditionPendidikan, buttons: { <?php echo $this->lang->line('ok'); ?>: true, <?php echo $this->lang->line('cancel'); ?>: false },prefix:'jqismooth' });
}
function doConditionPendidikan(v,m,f,e){
    if(m){
        $('#member'+e.alertName).remove();
        var posting = "dataid="+e.alertName;
        jQuery.ajax({
            type: 'POST',
            url: "<?php echo base_url().$this->router->class; ?>/deletePendidikan",
            data: posting,
            success: function(response) {
                if(response == 'success'){
                    toastr.success("<?php echo $this->lang->line('msg_success_delete'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
                }else{
                    toastr.error("<?php echo $this->lang->line('msg_empty_delete'); ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                }
            }
        })
    }else{
        jQuery.prompt.close();
    }
}
</script>
<form class="form-horizontal" name="form" method="POST" action="<?php echo base_url().$this->router->class; ?>/getPendidikan/<?php echo $id;?>" enctype="multipart/form-data">
    <div class="box-body">
        <div class="col-md-12">
            <div class="form-group footertable">
                <label class="col-xs 12 col-sm-4 text-left">
                	<a href="javascript:void(0);" onclick="openTablePendidikan();" class="btn btn-danger btn-sm mr5 openTablePendidikan">Tambah Detail</a>
                </label> 
                    <input type="hidden" name="id" value="<?php echo $id; ?>">
                    <div class="col-sm-12 text-left table-responsive">
                        <table class="table table-bordered table-responsive" id="tablependidikan" style="margin-bottom: 0px;">
                            <tbody id="datatable">
                            <tr>

                                <th>Jenis Pendidikan</th>
            					<th>Tingkat Pendidikan</th>
            					<th>Nama Perguruan TInggi</th>
            					<th>Jurusan</th>
            					<th>Gelar</th>
            					<th>Tahun Lulus</th>
            					<th style="width: 40px">Aksi</th>
                            </tr>

                            <?php if(isset($lists) && count($lists) > 0) { ?>
                        	<?php foreach($lists as $key => $value){ ?>
                            <?php ?>
                            <tr id="member<?php echo $value['id']; ?>">
                                <td>
                                    <select name="jenis_pendidikan[]" class="form-control input-sm">
                                        <?php 
                                        foreach ($this->webconfig['pendidikan_jenis'] as $key2 => $value2) {
                                            ?>
                                            <option value="<?php echo $key2 ?>" <?php if($value['jenis'] == $key2){echo "selected='selected'";} ?> ><?php echo $value2 ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                            	<td>
                                    <input class="form-control input-sm" name="idmember[]" value="<?php echo isset($value['id'])?$value['id']:'';?>" type="hidden">
                            		<!-- <input class="form-control input-sm" name="tingkat[]" value="<?php echo isset($value['tingkat'])?$value['tingkat']:'';?>" type="text"> -->
                                    <select name="tingkat[]" class="form-control" id="tingkat_<?php echo $key;?>" style="padding: 3px;">
                                        <option value="">-- Pendidikan --</option>
                                        <?php if (isset($tingkatpendidikan) && count($tingkatpendidikan) > 0){ ?>
                                        <?php foreach ($tingkatpendidikan as $row => $list){ ?>
                                            <option <?php echo (isset($value['tingkat']) && $value['tingkat'] == $list['id'])?"selected='selected'":""; ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                            	</td>
                            	<td>
                            		<input class="form-control input-sm" name="pt[]" value="<?php echo isset($value['pt'])?$value['pt']:'';?>" type="text">
                            	</td>
                            	<td>
                                    <select name="jurusan[]" class="form-control" id="tingkat_<?php echo $key;?>" style="padding: 3px;">
                                        <option value="">-- Jurusan --</option>
                                        <?php foreach ($this->webconfig['jurusan_pendidikan'] as $row => $value3){ ?>
                                            <option <?php if(isset($value['jurusan_2']) && $value['jurusan_2'] == $row){echo "selected='selected'";} ?> value="<?php echo $row; ?>"><?php echo $value3; ?></option>
                                        <?php } ?>
                                    </select>

                            		<?php /*<input class="form-control input-sm" name="jurusan[]" value="<?php echo isset($value['jurusan'])?$value['jurusan']:'';?>" type="text"> */?>
                            	</td>
                            	<td>
                            		<input class="form-control input-sm" name="gelar[]" value="<?php echo isset($value['gelar'])?$value['gelar']:'';?>" type="text">
                            	</td>
                            	<td>
                            		<input class="form-control input-sm" name="thn_lulus[]" value="<?php echo isset($value['thn_lulus'])?$value['thn_lulus']:'';?>" type="text">
                            	</td>
                            	<td>
                            		<a class="btn btn-danger btn-xs" onclick="deleteThisPendidikan(<?php echo $value['id']; ?>)" href="javascript:void(0)"><i class="ti-trash"></i>Hapus</a>
                            	</td>
                            </tr>
                            <?php } ?>
                            <?php } ?>

                            
                        	</tbody>
                        </table>
                    </div>
                    <div class="col-sm-12 text-left table-responsive">
                        <hr>
                        <div class="form-group footertable">
                            <div class="col-sm-12 text-left">
                                <input class="btn btn-primary btn-sm mr5" type="submit" value="<?php echo $this->lang->line('navigation_save'); ?>">
                            </div>
                        </div>
                    </div>
                
            </div>
            
        </div>
    </div>
</form>