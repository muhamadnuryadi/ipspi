<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Member Card</title>
<link rel="stylesheet" href="main.css">
<link href="https://fonts.googleapis.com/css?family=Poppins:400,600,700,800&display=swap" rel="stylesheet">
</head>

<body>
    <div class="container">
        <div class="header">
            <p>KARTU ANGGOTA</p>
            <img src="<?php echo $this->webconfig['back_base_template']; ?>img/logo.jpg" alt="Logo" class="logo" />
            <div class="clear"></div>
        </div>
        <div class="divider"></div>
        <div class="content">
            <p>Ikatan Pekerja Sosial Profesional Indonesia </p>
            <span>Indonesian Association of Social Workers</span>
            <div class="box-info">
                <div class="dynamic-box">
                    <h4>62.07.0619.2.12223</h4>
                    <h2>ETI DWI RIANTI</h2>
                </div>
                <div class="barcode"><img src="<?php echo $this->webconfig['back_base_template']; ?>images/barcode.png" alt="Barcode" /></div>
                <div class="clear"></div>
            </div>
            <div class="info">
                <p>Anggota Biasa</p>
                <span>Berlaku sampai 12 Agustus 2024</span>
            </div>
        </div>
    </div>
</body>

</html>