<!DOCTYPE html>
<html>
<head>
   
   <style type="text/css">
   p.white, h1.white, h2.white, h3.white, h4.white, h5.white, h6.white, a.white, i.white, span.white, input.white[type="text"], textarea.white, input.white[type="submit"], select.white {
	    color: #ffffff;
	}
	.nicdark_bg_yellow {
	    background-color: #edbf47;
	}
	h5 {
	    font-size: 15px;
	    line-height: 15px;
	}
	h1, h2, h3, h4, h5, h6, input[type="text"], textarea, select {
	    color: #868585;
	    font-family: "Montserrat",sans-serif;
	}

   /* WENDY */
	.container_img_4{
		margin-bottom: 20px;
	}
	.container_img_4:after{
		content: ""; display: block; height: 0; clear: both; visibility: hidden;
	}
	.container_img_4 .container_item{
		width: 25%;
		margin: 0px;
		padding: 0px;
		float: left;
		position: relative;
	}
	.container_img_4 .container_item h5{
		position: absolute;
		padding: 5px;
		bottom: 3px;
	}
	.container_img_4 img{
		width: 100%;
	}
	/*----*/
	.container_img_1{
		margin-bottom: 20px;
	}
	.container_img_1:after{
		content: ""; display: block; height: 0; clear: both; visibility: hidden;
	}
	.container_img_1 .container_item{
		width: 50%;
		margin: 0px auto;
		padding: 0px;
		position: relative;
	}
	.container_img_1 .container_item h5{
		position: absolute;
		padding: 5px;
		bottom: 3px;
	}
	.container_img_1 img{
		width: 100%;
	}
	/*----*/
	.container_img_side{
		margin-bottom: 20px;
	}
	.container_img_side:after{
		content: ""; display: block; height: 0; clear: both; visibility: hidden;
	}
	.container_img_side .container_item_img{
		width: 20%;
		float: left;
		padding: 0px;
	}
	.container_img_side .container_item_text{
		width: 75%;
		float: left;
		padding: 0px;
		margin-left: 20px;
	}
	.container_img_side .container_item_text h5{
		margin-bottom: 10px;
	}
	.container_img_side img{
		width: 100%;
	}
   </style>
</head>
<script type="text/javascript">
$(document).ready(function() {
   
});
</script>
<div style="min-width:1100px;max-width:1100px">
	<section class="panel position-relative">
		<div class="panel-body">
			<div class="nav-tabs-custom">
	            <ul class="nav nav-tabs">
	                <li class="active"><a style="padding: 10px 12px;" href="#profile" data-toggle="tab">Profile Form</a></li>
	                <li><a style="padding: 10px 12px;" href="#pendidikan" data-toggle="tab">Pendidikan</a></li>
	                <li><a style="padding: 10px 12px;" href="#pekerjaan" data-toggle="tab">Pekerjaan</a></li>
	                <li><a style="padding: 10px 12px;" href="#praktek" data-toggle="tab">Praktek Peksos</a></li>
	                <li><a style="padding: 10px 12px;" href="#penghargaan" data-toggle="tab">Penghargaan</a></li>
	                <li><a style="padding: 10px 12px;" href="#sertifikasi" data-toggle="tab">Sertifikasi</a></li>
	                <li><a style="padding: 10px 12px;" href="#referensi" data-toggle="tab">Referensi</a></li>
	                <li><a style="padding: 10px 12px;" href="#profesi" data-toggle="tab">Organisasi Profesi</a></li>
	                <li><a style="padding: 10px 12px;" href="#komunitas" data-toggle="tab">Komunitas Internet</a></li>
	            </ul>
	            <div class="tab-content">
	                <div class="active tab-pane" id="profile">
	                	<div class="box-body">

	                		<div class="col-sm-12 text-left table-responsive">
			                    <table class="table table-bordered table-responsive" id="tablepekerjaan" style="margin-bottom: 0px;">
			                        <tbody id="datatable">
			                            <tr>
			            					<th>No Anggota</th>
			            					<th style="font-weight: 100;"><?php echo isset($lists[0]['no_anggota'])?$lists[0]['no_anggota']:''; ?></th>
			            					<th>No KTP / NIK</th>
			            					<th style="font-weight: 100;"><?php echo isset($lists[0]['ktp'])?$lists[0]['ktp']:''; ?></th>
			            				</tr>
			                            <tr>
			            					<th>Nama</th>
			            					<th style="font-weight: 100;"><?php echo isset($lists[0]['nama'])?$lists[0]['nama']:''; ?></th>
			            					<th>Username</th>
			            					<th style="font-weight: 100;"><?php echo isset($lists[0]['username'])?$lists[0]['username']:''; ?></th>
			            				</tr>
			                            <tr>
			            					<th>Email</th>
			            					<th style="font-weight: 100;"><?php echo isset($lists[0]['email'])?$lists[0]['email']:''; ?></th>
			            					<th>Status Perkawinan</th>
			            					<th style="font-weight: 100;"><?php echo isset($lists[0]['statuskawin'])?perkawinantext($lists[0]['statuskawin']):''; ?></th>
			            				</tr>
			            				<tr>
			            					<th>Tgl Lahir</th>
			            					<th style="font-weight: 100;"><?php echo isset($lists[0]['tgllahir'])?date('d-m-Y', strtotime($lists[0]['tgllahir'])):''; ?></th>
			            					<th>Jumlah Anak</th>
			            					<th style="font-weight: 100;"><?php echo isset($lists[0]['jumanak'])?$lists[0]['jumanak']:''; ?></th>
			            				</tr>
			            				<tr>
			            					<th>Telp Kantor</th>
			            					<th style="font-weight: 100;"><?php echo isset($lists[0]['telpkantor'])?$lists[0]['telpkantor']:''; ?></th>
			            					<th>Fax Kantor</th>
			            					<th style="font-weight: 100;"><?php echo isset($lists[0]['faxkantor'])?$lists[0]['faxkantor']:''; ?></th>
			            				</tr>
			            				<tr>
			            					<th>Alamat Rumah</th>
			            					<th style="font-weight: 100;"><?php echo isset($lists[0]['alamatrumah'])?$lists[0]['alamatrumah']:''; ?></th>
			            					<th>Fax Kantor</th>
			            					<th style="font-weight: 100;"><?php echo isset($lists[0]['faxkantor'])?$lists[0]['faxkantor']:''; ?></th>
			            				</tr>
			            				<tr>
			            					<th>Provinsi</th>
			            					<th style="font-weight: 100;"><?php echo isset($lists[0]['id_propinsi'])?userwilayah($lists[0]['id_propinsi']):''; ?></th>
			            					<th>Kota</th>
			            					<th style="font-weight: 100;"><?php echo isset($lists[0]['id_kota'])?userwilayah($lists[0]['id_kota']):''; ?></th>
			            				</tr>
			            				<tr>
			            					<th>Telp</th>
			            					<th style="font-weight: 100;"><?php echo isset($lists[0]['telp'])?$lists[0]['telp']:''; ?></th>
			            					<th>Hp</th>
			            					<th style="font-weight: 100;"><?php echo isset($lists[0]['hp'])?$lists[0]['hp']:''; ?></th>
			            				</tr>
			                            
			                        </tbody>
			                    </table>
			                    <?php 
			                    if ($lists[0]['id'] == $this->session->userdata('iduser') && $this->session->userdata('role') == 3 ) {
			                    	?>
			                    	<div class="link-bayar" style="margin-top:15px;">
				                    	<a href="<?php echo base_url(); ?>pembayaran">Link Pembayaran</a>
				                    </div>
			                    	<?php
			                    }
			                    ?>
			                </div>

				        </div>
	                </div>

	                <div class="tab-pane" id="pendidikan">
	                    <div class="col-sm-12 text-left table-responsive">
	                        <table class="table table-bordered table-responsive" id="tablependidikan" style="margin-bottom: 0px;">
	                            <tr>
	            					<th>Tingkat Pendidikan</th>
	            					<th>Nama Perguruan TInggi</th>
	            					<th>Jurusan</th>
	            					<th>Gelar</th>
	            					<th>Tahun Lulus</th>
	                            </tr>
	                            <?php if(isset($pendidikan) && count($pendidikan) > 0) { ?>
	                        	<?php foreach($pendidikan as $key => $value){ ?>
		                            <tr id="member<?php echo $value['id']; ?>">
		                            	<td>
		                                    <?php echo isset($value['tingkat'])?usertingkat($value['tingkat']):'-';?>
		                            	</td>
		                            	<td>
		                            		<?php echo isset($value['pt'])?$value['pt']:'-';?>
		                            	</td>
		                            	<td>
		                            		<?php echo isset($value['jurusan_2'])?$this->webconfig['jurusan_pendidikan'][$value['jurusan_2']]:'-';?>
		                            	</td>
		                            	<td>
		                            		<?php echo isset($value['gelar'])?$value['gelar']:'-';?>
		                            	</td>
		                            	<td>
		                            		<?php echo isset($value['thn_lulus'])?$value['thn_lulus']:'-';?>
		                            	</td>
		                            </tr>
		                        <?php } ?>
		                        <?php } ?>
		                    </table>
		                </div>
		                <div class="clearfix"></div>
	                </div>

	                <div class="tab-pane" id="pekerjaan">
	                    <div class="col-sm-12 text-left table-responsive">
	                        <table class="table table-bordered table-responsive" id="tablependidikan" style="margin-bottom: 0px;">
	                            <tr>
	            					<th>Nama Instansi</th>
	            					<th>Jabatan</th>
	            					<th>Mulai Tahun</th>
	            					<th>Berakhir Tahun</th>
	                            </tr>
	                            <?php if(isset($pekerjaan) && count($pekerjaan) > 0) { ?>
	                        	<?php foreach($pekerjaan as $key => $value){ ?>
		                            <tr id="member<?php echo $value['id']; ?>">
		                            	<td>
		                                    <?php echo isset($value['instansi']['name'])?$value['instansi']['name']:'-';?>
		                            	</td>
		                            	<td>
		                            		<?php echo isset($value['jabatan'])?$value['jabatan']:'-';?>
		                            	</td>
		                            	<td>
		                            		<?php echo isset($value['mulai'])?$value['mulai']:'-';?>
		                            	</td>
		                            	<td>
		                            		<?php echo isset($value['akhir'])?$value['akhir']:'-';?>
		                            	</td>
		                            </tr>
		                        <?php } ?>
		                        <?php } ?>
		                    </table>
		                </div>
		                <div class="clearfix"></div>
	                </div>

	                <div class="tab-pane" id="praktek">
	                    <div class="col-sm-12 text-left table-responsive">
	                        <table class="table table-bordered table-responsive" id="tablependidikan" style="margin-bottom: 0px;">
	                            <tr>
	            					<th>Nama Instansi</th>
	            					<th>Jabatan</th>
	            					<th>Mulai Tahun</th>
	            					<th>Berakhir Tahun</th>
	                            </tr>
	                            <?php if(isset($pekerjaan) && count($pekerjaan) > 0) { ?>
	                        	<?php foreach($pekerjaan as $key => $value){ ?>
		                            <tr id="member<?php echo $value['id']; ?>">
		                            	<td>
		                                    <?php echo isset($value['nama'])?$value['nama']:'-';?>
		                            	</td>
		                            	<td>
		                            		<?php echo isset($value['jabatan'])?$value['jabatan']:'-';?>
		                            	</td>
		                            	<td>
		                            		<?php echo isset($value['mulai'])?$value['mulai']:'-';?>
		                            	</td>
		                            	<td>
		                            		<?php echo isset($value['akhir'])?$value['akhir']:'-';?>
		                            	</td>
		                            </tr>
		                        <?php } ?>
		                        <?php } ?>
		                    </table>
		                </div>
		                <div class="clearfix"></div>
	                </div>

	                <div class="tab-pane" id="penghargaan">
	                    <div class="col-sm-12 text-left table-responsive">
	                        <table class="table table-bordered table-responsive" id="tablependidikan" style="margin-bottom: 0px;">
	                            <tr>
	            					<th>Nama </th>
	            					<th>Instansi</th>
	            					<th>Tahun</th>
	                            </tr>
	                            <?php if(isset($penghargaan) && count($penghargaan) > 0) { ?>
	                        	<?php foreach($penghargaan as $key => $value){ ?>
		                            <tr id="member<?php echo $value['id']; ?>">
		                            	<td>
		                                    <?php echo isset($value['nama'])?$value['nama']:'-';?>
		                            	</td>
		                            	<td>
		                            		<?php echo isset($value['instansi'])?$value['instansi']:'-';?>
		                            	</td>
		                            	<td>
		                            		<?php echo isset($value['tahun'])?$value['tahun']:'-';?>
		                            	</td>
		                            </tr>
		                        <?php } ?>
		                        <?php } ?>
		                    </table>
		                </div>
		                <div class="clearfix"></div>
	                </div>

	                <div class="tab-pane" id="sertifikasi">
	                    <div class="col-sm-12 text-left table-responsive">
	                        <table class="table table-bordered table-responsive" id="tablependidikan" style="margin-bottom: 0px;">
	                            <tr>
	            					<th>Nama Sertifikasi</th>
	            					<th>Lembaga yg mengeluarkan</th>
	            					<th>Tahun</th>
	                            </tr>
	                            <?php if(isset($sertifikasi) && count($sertifikasi) > 0) { ?>
	                        	<?php foreach($sertifikasi as $key => $value){ ?>
		                            <tr id="member<?php echo $value['id']; ?>">
		                            	<td>
		                                    <?php echo isset($value['nama'])?$value['nama']:'-';?>
		                            	</td>
		                            	<td>
		                            		<?php echo isset($value['lembaga'])?$value['lembaga']:'-';?>
		                            	</td>
		                            	<td>
		                            		<?php echo isset($value['tahun'])?$value['tahun']:'-';?>
		                            	</td>
		                            </tr>
		                        <?php } ?>
		                        <?php } ?>
		                    </table>
		                </div>
		                <div class="clearfix"></div>
	                </div>

	                <div class="tab-pane" id="referensi">
	                    <div class="col-sm-12 text-left table-responsive">
	                        <table class="table table-bordered table-responsive" id="tablependidikan" style="margin-bottom: 0px;">
	                            <tr>
	            					<th>Nama </th>
	            					<th>Jabatan</th>
	            					<th>Telp</th>
	                            </tr>
	                            <?php if(isset($referensi) && count($referensi) > 0) { ?>
	                        	<?php foreach($referensi as $key => $value){ ?>
		                            <tr id="member<?php echo $value['id']; ?>">
		                            	<td>
		                                    <?php echo isset($value['nama'])?$value['nama']:'-';?>
		                            	</td>
		                            	<td>
		                            		<?php echo isset($value['jabatan'])?$value['jabatan']:'-';?>
		                            	</td>
		                            	<td>
		                            		<?php echo isset($value['telp'])?$value['telp']:'-';?>
		                            	</td>
		                            </tr>
		                        <?php } ?>
		                        <?php } ?>
		                    </table>
		                </div>
		                <div class="clearfix"></div>
	                </div>

	                <div class="tab-pane" id="profesi">
	                    <div class="col-sm-12 text-left table-responsive">
	                        <table class="table table-bordered table-responsive" id="tablependidikan" style="margin-bottom: 0px;">
	                            <tr>
	            					<th>Nama Organisasi Profesi</th>
	            					<th>Tahun</th>
	            					<th>Keterangan</th>
	                            </tr>
	                            <?php if(isset($sertifikasi) && count($sertifikasi) > 0) { ?>
	                        	<?php foreach($sertifikasi as $key => $value){ ?>
		                            <tr id="member<?php echo $value['id']; ?>">
		                            	<td>
		                                    <?php echo isset($value['nama'])?$value['nama']:'-';?>
		                            	</td>
		                            	<td>
		                            		<?php echo isset($value['tahun'])?$value['tahun']:'-';?>
		                            	</td>
		                            	<td>
		                            		<?php echo isset($value['keterangan'])?$value['keterangan']:'-';?>
		                            	</td>
		                            </tr>
		                        <?php } ?>
		                        <?php } ?>
		                    </table>
		                </div>
		                <div class="clearfix"></div>
	                </div>

	                <div class="tab-pane" id="komunitas">
	                    <div class="col-sm-12 text-left table-responsive">
	                        <table class="table table-bordered table-responsive" id="tablependidikan" style="margin-bottom: 0px;">
	                            <tr>
	            					<th>Nama Komunitas</th>
            						<th>Alamat</th>
	                            </tr>
	                            <?php if(isset($sertifikasi) && count($sertifikasi) > 0) { ?>
	                        	<?php foreach($sertifikasi as $key => $value){ ?>
		                            <tr id="member<?php echo $value['id']; ?>">
		                            	<td>
		                                    <?php echo isset($value['nama'])?$value['nama']:'-';?>
		                            	</td>
		                            	<td>
		                            		<?php echo isset($value['alamat'])?$value['alamat']:'-';?>
		                            	</td>
		                            </tr>
		                        <?php } ?>
		                        <?php } ?>
		                    </table>
		                </div>
		                <div class="clearfix"></div>
	                </div>
	            </div>
	        </div>
		</div>
	</section>
</div>
</html>

	