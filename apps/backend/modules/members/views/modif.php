<script type="text/javascript">
$(document).ready(function() {
    jQuery("#listProfile").load('<?php echo base_url().$this->router->class; ?>/getProfile/<?php echo $id;?>');
    jQuery("#listPendidikan").load('<?php echo base_url().$this->router->class; ?>/getPendidikan/<?php echo $id;?>');
    jQuery("#listPekerjaan").load('<?php echo base_url().$this->router->class; ?>/getPekerjaan/<?php echo $id;?>');
    jQuery("#listPraktek").load('<?php echo base_url().$this->router->class; ?>/getPraktek/<?php echo $id;?>');
    jQuery("#listPenghargaan").load('<?php echo base_url().$this->router->class; ?>/getPenghargaan/<?php echo $id;?>');
    jQuery("#listSertifikasi").load('<?php echo base_url().$this->router->class; ?>/getSertifikasi/<?php echo $id;?>');
    jQuery("#listReferensi").load('<?php echo base_url().$this->router->class; ?>/getReferensi/<?php echo $id;?>');
    jQuery("#listProfesi").load('<?php echo base_url().$this->router->class; ?>/getProfesi/<?php echo $id;?>');
    jQuery("#listKomunitas").load('<?php echo base_url().$this->router->class; ?>/getKomunitas/<?php echo $id;?>');
});
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo strtoupper($this->module_name); ?>
            <small><?php echo $this->lang->line('modif_members'); ?></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('/'); ?>">
                    <i class="fa fa-dashboard"></i>
                    <?php echo $this->lang->line('dashboard'); ?>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url().$this->router->class; ?>"><?php echo ucfirst($this->module_name); ?></a>
            </li>
            <li class="active"><?php echo $this->lang->line('modif'); ?></li>
        </ol>
    </section>
    <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
        <?php foreach($error_hash as $inp_err){ ?>
            <script type="text/javascript">
            jQuery(document).ready(function($) {
                toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                });
            </script>
        <?php } ?>
    <?php } ?>
    
    <?php if($this->session->flashdata('success')){ ?>
        <script type="text/javascript">
        jQuery(document).ready(function($) {
            toastr.success("<?php echo $this->session->flashdata('success'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
            });
        </script>
    <?php } ?>
    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a style="padding: 10px 12px;" href="#profile" data-toggle="tab">Profile Form</a></li>
                <li><a style="padding: 10px 12px;" href="#pendidikan" data-toggle="tab">Pendidikan</a></li>
                <li><a style="padding: 10px 12px;" href="#pekerjaan" data-toggle="tab">Pekerjaan</a></li>
                <li><a style="padding: 10px 12px;" href="#praktek" data-toggle="tab">Praktek Peksos</a></li>
                <li><a style="padding: 10px 12px;" href="#penghargaan" data-toggle="tab">Penghargaan</a></li>
                <li><a style="padding: 10px 12px;" href="#sertifikasi" data-toggle="tab">Sertifikasi</a></li>
                <li><a style="padding: 10px 12px;" href="#referensi" data-toggle="tab">Referensi</a></li>
                <li><a style="padding: 10px 12px;" href="#profesi" data-toggle="tab">Organisasi Profesi</a></li>
                <li><a style="padding: 10px 12px;" href="#komunitas" data-toggle="tab">Komunitas Internet</a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="profile">
                    <div id="listProfile">
                        <center>
                            <img src='<?php echo $this->webconfig['back_images']; ?>ajax_loading.gif' align='middle' style="margin:5px;" />
                        </center>
                    </div>
                </div>

                <div class="tab-pane" id="pendidikan">
                    <div id="listPendidikan">
                        <center>
                            <img src='<?php echo $this->webconfig['back_images']; ?>ajax_loading.gif' align='middle' style="margin:5px;" />
                        </center>
                    </div>
                </div>

                <div class="tab-pane" id="pekerjaan">
                    <div id="listPekerjaan">
                        <center>
                            <img src='<?php echo $this->webconfig['back_images']; ?>ajax_loading.gif' align='middle' style="margin:5px;" />
                        </center>
                    </div>
                </div>

                <div class="tab-pane" id="praktek">
                    <div id="listPraktek">
                        <center>
                            <img src='<?php echo $this->webconfig['back_images']; ?>ajax_loading.gif' align='middle' style="margin:5px;" />
                        </center>
                    </div>
                </div>

                <div class="tab-pane" id="penghargaan">
                    <div id="listPenghargaan">
                        <center>
                            <img src='<?php echo $this->webconfig['back_images']; ?>ajax_loading.gif' align='middle' style="margin:5px;" />
                        </center>
                    </div>
                </div>

                <div class="tab-pane" id="sertifikasi">
                    <div id="listSertifikasi">
                        <center>
                            <img src='<?php echo $this->webconfig['back_images']; ?>ajax_loading.gif' align='middle' style="margin:5px;" />
                        </center>
                    </div>
                </div>

                <div class="tab-pane" id="referensi">
                    <div id="listReferensi">
                        <center>
                            <img src='<?php echo $this->webconfig['back_images']; ?>ajax_loading.gif' align='middle' style="margin:5px;" />
                        </center>
                    </div>
                </div>

                <div class="tab-pane" id="profesi">
                    <div id="listProfesi">
                        <center>
                            <img src='<?php echo $this->webconfig['back_images']; ?>ajax_loading.gif' align='middle' style="margin:5px;" />
                        </center>
                    </div>
                </div>

                <div class="tab-pane" id="komunitas">
                    <div id="listKomunitas">
                        <center>
                            <img src='<?php echo $this->webconfig['back_images']; ?>ajax_loading.gif' align='middle' style="margin:5px;" />
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>