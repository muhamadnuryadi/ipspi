<script type="text/javascript">
jQuery(document).ready(function($) {
    $('.datepicker').datepicker({
        format: "dd-mm-yyyy",
        autoclose: true
    });
    <?php  if(isset($lists[0]['id_propinsi']) && $lists[0]['id_propinsi'] !=''){?>
        getkota(<?php echo $lists[0]['id_propinsi'];?>);
    <?php } ?>
    $("#id_propinsi").change(function(){
        $("#id_kota").html('');
        var arr = {prop:$(this).val()};
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>wilayah/getkota",
            data: arr,
            dataType: "json",
            success: function(data) {
                var html = '<option value="">-- Pilih Kota/Kabupaten --</option>';
                $("#id_kota").html(html);
                if(data.length > 0) {
                    $.each(data, function(key, val) {
                        $("#id_kota").append($("<option />").val(val.id).text(val.nama));
                    });   
                }
            }
        });
    });

    // jQuery("#listData").load('<?php echo base_url().$this->router->class; ?>/getProfile/');
});
function getkota(id_propinsi){
    $("#id_kota").html('');
    var arr = {prop:id_propinsi};
    $.ajax({
        type: 'POST',
        url: "<?php echo base_url(); ?>wilayah/getkota",
        data: arr,
        dataType: "json",
        success: function(data) {
            var html = '<option value="">-- Pilih Kota/Kabupaten--</option>';
            $("#id_kota").html(html);
            var citydb = '<?php echo isset($lists[0]['id_kota'])?$lists[0]['id_kota']:''; ?>';
            if(data.length > 0) {
              $.each(data, function(key, val) {
                if(val.id == citydb){
                  $("#id_kota").append($("<option />").val(val.id).text(val.nama).prop('selected', true));
                }else{
                   $("#id_kota").append($("<option />").val(val.id).text(val.nama));
                }
              });
            }
        }
    });
}
</script>
<form class="form-horizontal" name="form" method="POST" action="<?php echo base_url().$this->router->class; ?>/getProfile/<?php echo isset($id)?$id:'';?>" enctype="multipart/form-data">
    <div class="box-body">
        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Nama</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="nama" placeholder="" name="nama" value="<?php echo isset($lists[0]['nama'])?$lists[0]['nama']:''; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Username</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="username" placeholder="" name="username" value="<?php echo isset($lists[0]['username'])?$lists[0]['username']:''; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Email</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="email" placeholder="" name="email" value="<?php echo isset($lists[0]['email'])?$lists[0]['email']:''; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Password</label>
            <div class="col-sm-6">
                <input type="password" class="form-control" id="password" placeholder="" name="password">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Ulangi Password</label>
            <div class="col-sm-6">
                <input type="password" class="form-control" id="password_1" placeholder="" name="password_1">
            </div>
        </div>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-3 control-label">Tgl Lahir</label>
            <div class="col-sm-6">
                <div class="input-group">
                    <input type="text" id="tgllahir" name="tgllahir" value='<?php echo isset($lists[0]['tgllahir'])?date('d-m-Y', strtotime($lists[0]['tgllahir'])):''; ?>' size="50" class="form-control datepicker">
                    <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-3 control-label">Status Perkawinan</label>
            <div class="col-sm-6">
                <div class="checkbox">
                    <label>
                        <input type="radio" name="statuskawin" value="0" <?php if(!isset($status)){ echo "checked=checked"; } ?> <?php if(isset($lists[0]['statuskawin']) && $lists[0]['statuskawin'] == 0){ echo "checked=checked"; }?> /> Belum Menikah
                    </label>
                    <label>
                        <input type="radio" name="statuskawin" value="1" <?php if(isset($lists[0]['statuskawin']) && $lists[0]['statuskawin'] == 1){ echo "checked=checked"; }?> /> Menikah
                    </label>
                    <label>
                        <input type="radio" name="statuskawin" value="2" <?php if(isset($lists[0]['statuskawin']) && $lists[0]['statuskawin'] == 2){ echo "checked=checked"; }?> /> Duda/janda
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Jumlah Anak</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="jumanak" placeholder="" name="jumanak" value="<?php echo isset($lists[0]['jumanak'])?$lists[0]['jumanak']:''; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-3 control-label">No KTP / NIK</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="" placeholder="KTP/NIK" name="ktp" value="<?php echo isset($lists[0]['ktp'])?$lists[0]['ktp']:''; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Alamat kantor</label>
            <div class="col-sm-6">
                <textarea class="form-control" rows="3" placeholder="" name="alamatkantor"><?php echo isset($lists[0]['alamatkantor'])?$lists[0]['alamatkantor']:''; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Telp Kantor</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="telpkantor" placeholder="" name="telpkantor" value="<?php echo isset($lists[0]['telpkantor'])?$lists[0]['telpkantor']:''; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Fax Kantor</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="faxkantor" placeholder="" name="faxkantor" value="<?php echo isset($lists[0]['faxkantor'])?$lists[0]['faxkantor']:''; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Alamat Rumah</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="alamatrumah" placeholder="" name="alamatrumah" value="<?php echo isset($lists[0]['alamatrumah'])?$lists[0]['alamatrumah']:''; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Provinsi</label>
            <div class="col-sm-6">
                <select name="id_propinsi" class="form-control select2" id="id_propinsi" style="padding: 3px;">
                    <option value="">-- <?php echo $this->lang->line('pilih_propinsi'); ?> --</option>
                    <?php foreach ($wilayah as $key => $value){ ?>
                        <option <?php echo (isset($lists[0]['id_propinsi']) && $lists[0]['id_propinsi'] == $value['id'])?"selected='selected'":""; ?> value="<?php echo $value['id']; ?>"><?php echo $value['nama']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Kota</label>
            <div class="col-sm-6">
                <select name="id_kota" class="form-control select2" id="id_kota" style="padding: 3px;">
                    <option value="">-- <?php echo $this->lang->line('pilih_kota'); ?> --</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Telp</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="telp" placeholder="" name="telp" value="<?php echo isset($lists[0]['telp'])?$lists[0]['telp']:''; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-3 control-label">HP</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="hp" placeholder="" name="hp" value="<?php echo isset($lists[0]['hp'])?$lists[0]['hp']:''; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-3 control-label">Status</label>
            <div class="col-sm-6">
                <div class="checkbox">
                    <label>
                        <input type="radio" name="status" value="1" <?php if(!isset($status)){ echo "checked=checked"; } ?>  <?php if(isset($lists[0]['status']) && $lists[0]['status'] == 1){ echo "checked=checked"; }?> /> Aktif
                    </label>
                    <label>
                        <input type="radio" name="status" value="0" <?php if(isset($lists[0]['status']) && $lists[0]['status'] == 0){ echo "checked=checked"; }?> /> Tidak Aktif
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-3 control-label">Tgl Daftar</label>
            <div class="col-sm-6">
                <div class="input-group">
                    <input type="text" id="created_at" name="created_at" value='<?php echo isset($lists[0]['created_at'])?date('d-m-Y', strtotime($lists[0]['created_at'])):''; ?>' size="50" class="form-control datepicker">
                    <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="" class="col-sm-3 control-label">FOTO</label>
            <div class="col-sm-6">
                <input type="file" class="form-control" id="foto" placeholder="" name="foto" value="">
                <input type="hidden" name="foto_old" value="<?php echo $lists[0]['foto'] ?>">
                <div >
                    <?php 
                    if (file_exists($this->webconfig['media-path-files'].$lists[0]['foto'])) {
                        ?>
                        <br/>
                        <img width="300" src="<?php echo $this->webconfig['media-server-files'].$lists[0]['foto'] ;?>">
                        <?php
                    }
                    ?>
                    
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="" class="col-sm-3 control-label">KTP</label>
            <div class="col-sm-6">
                <input type="file" class="form-control" id="path_ktp" placeholder="" name="path_ktp" value="">
                <input type="hidden" name="path_ktp_old" value="<?php echo $lists[0]['path_ktp'] ?>">
                <div >
                    <?php 
                    if (file_exists($this->webconfig['media-path-files'].$lists[0]['path_ktp'])) {
                        ?>
                        <br/>
                        <img width="300" src="<?php echo $this->webconfig['media-server-files'].$lists[0]['path_ktp'] ;?>">
                        <?php
                    }
                    ?>
                    
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Ijazah</label>
            <div class="col-sm-6">
                <input type="file" class="form-control" id="path_ijazah" placeholder="" name="path_ijazah" value="">
                <input type="hidden" name="path_ijazah_old" value="<?php echo $lists[0]['path_ijazah'] ?>">
                <div >
                    <?php
                    if (file_exists($this->webconfig['media-path-files'].$lists[0]['path_ijazah'])) {
                        ?>
                        <br/>
                        <img width="300" src="<?php echo $this->webconfig['media-server-files'].$lists[0]['path_ijazah'] ;?>">
                        <?php
                    }
                    ?>
                    
                </div>
            </div>
        </div>

        
    </div>
    <div class="box-footer">
        <!-- <button type="submit" class="btn btn-default">Cancel</button> -->
        <button type="submit" class="btn btn-info pull-right">Simpan</button>
    </div>
</form>