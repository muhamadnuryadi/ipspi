<script type="text/javascript">
	jQuery(document).ready(function($) {
		jQuery("#listData").load('<?php echo base_url().$this->router->class; ?>/getAllData/');

		$("#id_propinsi").change(function(){
	        $("#id_kota").html('');
	        var arr = {prop:$(this).val()};
	        $.ajax({
	            type: 'POST',
	            url: "<?php echo base_url(); ?>wilayah/getkota",
	            data: arr,
	            dataType: "json",
	            success: function(data) {
	                var html = '<option value="">-- Pilih Kota/Kabupaten --</option>';
	                $("#id_kota").html(html);
	                if(data.length > 0) {
	                    $.each(data, function(key, val) {
	                        $("#id_kota").append($("<option />").val(val.id).text(val.nama));
	                    });   
	                }
	            }
	        });
	    });
	});
	function searchThis(){
		jQuery("#listData").html("<center><img src='<?php echo $this->webconfig['back_images']; ?>ajax_loading.gif' align='middle' style='margin:5px;' /></center>");
		var frm = document.searchform;
		var name = frm.name.value;
		var id_propinsi = frm.id_propinsi.value;
		var id_kota = frm.id_kota.value;
		var tingkat = frm.tingkat.value;
		var id_pekerjaan = frm.id_pekerjaan.value;
		var id_instansi = frm.id_instansi.value;
		var id_sertifikasi = frm.id_sertifikasi.value;
		var no_anggota = frm.no_anggota.value;
		var email = frm.email.value;
		var id_jenis_anggota = frm.id_jenis_anggota.value;
		var jurusan = frm.jurusan.value;

		if(name == ''){ name = '-'; }
		if(id_propinsi == ''){ id_propinsi = '-'; }
		if(id_kota == ''){ id_kota = '-'; }
		if(tingkat == ''){ tingkat = '-'; }
		if(id_pekerjaan == ''){ id_pekerjaan = '-'; }
		if(id_instansi == ''){ id_instansi = '-'; }
		if(id_sertifikasi == ''){ id_sertifikasi = '-'; }
		if(no_anggota == ''){ no_anggota = '-'; }
		if(email == ''){ email = '-'; }
		if(id_jenis_anggota == ''){ id_jenis_anggota = '-'; }
		if(jurusan == ''){ jurusan = '-'; }

		jQuery("#listData").load("<?php echo base_url().$this->router->class; ?>/searchdata/"+name.replace(/\s/g,'%20')+"/"+id_propinsi.replace(/\s/g,'%20')+"/"+id_kota.replace(/\s/g,'%20')+"/"+tingkat.replace(/\s/g,'%20')+"/"+id_pekerjaan.replace(/\s/g,'%20')+"/"+id_instansi.replace(/\s/g,'%20')+"/"+id_sertifikasi.replace(/\s/g,'%20')+"/"+no_anggota.replace(/\s/g,'%20')+"/"+email.replace(/\s/g,'%20')+"/"+id_jenis_anggota.replace(/\s/g,'%20')+"/"+jurusan.replace(/\s/g,'%20') );
	}
</script>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo strtoupper($this->module_name); ?>
			<small><?php echo $this->lang->line('members_teks'); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('/'); ?>"><i class="fa fa-dashboard"></i><?php echo $this->lang->line('dashboard'); ?></a></li>
			<li class="active"><?php echo ucfirst($this->module_name); ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
	                <div class="box-header with-border">
		                 <div class="row">
	                 		<div class="col-md-4 col-md-12">
	                 			
							</div>
							<div class="col-md-6"></div>
							<div class="col-md-2 col-md-12 text-right">
								<?php if($this->session->userdata('role') != '3'){?>
								<a class="btn btn-success btn-xs" href="<?php echo base_url().$this->router->class; ?>/add">
									<i class="fa fa-fw fa-plus"></i>
									<?php echo $this->lang->line('navigation_add'); ?>
								</a>
								<?php } ?>
							</div>
	                 	</div>

	                 	<?php if($this->session->userdata('role') != '3'){?>
	                 	<div class="box-group" >
							<?php 
							/*
							<div class="box-header with-border">
							    <h4 class="box-title">
							      <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
							        Search <i class="fa fa-angle-double-down"></i>
							      </a>
							    </h4>
							  </div>
							*/
							?>
							  
							<?php /*<div id="collapseThree" class="panel-collapse collapse">*/ ?>
							<div>
								<div class="box-body">
									<div class="row">
									<form id="searchform" name="searchform" method="POST" action="<?php echo base_url().$this->router->class; ?>searchdata/" onsubmit="searchThis(); return false;">
										<div class="col-md-3 col-md-12">
											<input type="text" name="name" class="form-control input-sm pull-left"  placeholder="Nama"/>
										</div>
										
										<div class="col-md-3 col-md-12">
											<select name="id_propinsi" class="form-control input-sm" id="id_propinsi" style="padding: 3px;">
									            <option value="">-- <?php echo $this->lang->line('pilih_propinsi'); ?> --</option>
									            <?php foreach ($wilayah as $key => $value){ ?>
									                <option <?php echo (isset($lists[0]['id_propinsi']) && $lists[0]['id_propinsi'] == $value['id'])?"selected='selected'":""; ?> value="<?php echo $value['id']; ?>"><?php echo $value['nama']; ?></option>
									            <?php } ?>
									        </select>
										</div>
										
										<div class="col-md-3 col-md-12">
											<select name="id_kota" class="form-control input-sm" id="id_kota" style="padding: 3px;">
									            <option value="">-- <?php echo $this->lang->line('pilih_kota'); ?> --</option>
									        </select>
										</div>

										<div class="col-md-3 col-md-12">
											<select name="tingkat" class="form-control input-sm" id="tingkat" style="padding: 3px;">
									            <option value="">-- Pendidikan --</option>
									            <?php foreach ($tingkatpendidikan as $key => $value){ ?>
									                <option <?php echo (isset($lists[0]['tingkat']) && $lists[0]['tingkat'] == $value['id'])?"selected='selected'":""; ?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
									            <?php } ?>
									        </select>
										</div>
										
										
										<div class="clearfix"></div>
										<br>

										<div class="col-md-3 col-md-12">
											<select name="id_pekerjaan" class="form-control input-sm" id="id_pekerjaan" style="padding: 3px;">
												<option value="">-- Pekerjaan --</option>
												<?php 
												if (isset($pekerjaan) && count($pekerjaan) > 0) {
													foreach ($pekerjaan as $key => $value) {
														?><option value="<?php echo $value['id'] ?>"><?php echo $value['name'] ?></option><?php
													}
												}
												?>
											</select>
										</div>

										<div class="col-md-3 col-md-12">
											<select name="id_instansi" class="form-control input-sm" id="id_instansi" style="padding: 3px;">
												<option value="">-- Instansi --</option>
												<?php 
												if (isset($instansi) && count($instansi) > 0) {
													foreach ($instansi as $key => $value) {
														?><option value="<?php echo $value['id'] ?>"><?php echo $value['name'] ?></option><?php
													}
												}
												?>
											</select>
										</div>

										<div class="col-md-3 col-md-12">
											<select name="id_sertifikasi" class="form-control input-sm" id="id_sertifikasi" style="padding: 3px;">
												<option value="">-- Sertifikasi --</option>
												<?php 
												foreach ($this->webconfig['sertifikasi_tipe'] as $key => $value) {
													?><option value="<?php echo $key ?>"><?php echo $value ?></option><?php
												}
												?>
											</select>
										</div>

										<div class="col-md-3 col-md-12">
											<input type="text" name="no_anggota" class="form-control input-sm pull-left"  placeholder="No Anggota"/>
										</div>

										<div class="clearfix"></div>
										<br>
										
										<div class="col-md-3 col-md-12">
											<input type="text" name="email" class="form-control input-sm pull-left"  placeholder="Email"/>
										</div>

										<div class="col-md-3 col-md-12">
											<select name="id_jenis_anggota" class="form-control input-sm" id="id_jenis_anggota" style="padding: 3px;">
												<option value="">-- Jenis Anggota --</option>
												<?php 
												foreach ($this->webconfig['jenis_anggota'] as $key => $value) {
													?><option value="<?php echo $key ?>"><?php echo $value ?></option><?php
												}
												?>
											</select>
										</div>

										<div class="col-md-3 col-md-12">
											<select name="jurusan" class="form-control input-sm" id="jurusan" style="padding: 3px;">
												<option value="">-- Jurusan --</option>
												<?php 
												foreach ($this->webconfig['jurusan_pendidikan'] as $key => $value) {
													?><option value="<?php echo $key ?>"><?php echo $value ?></option><?php
												}
												?>
											</select>
										</div>

										<div class="col-md-3 col-md-12">
											<div class="input-group-btn">
												<button class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Cari</button>
											</div> 
										</div>

									</form>
									</div>
								</div>
							</div>
						</div>
						<?php } ?>
	                </div>
	                <div id="listData">
	                	<center>
	                		<img src='<?php echo $this->webconfig['back_images']; ?>ajax_loading.gif' align='middle' style="margin:5px;" />
	                	</center>
	                </div>
	            </div>
			</div>
		</div>
	</section>
</div>