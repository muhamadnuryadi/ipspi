<script type="text/javascript">
$(document).ready(function() {

});
</script>
<script type="text/javascript">
function openTablePraktek(){
    var dt = new Date();
    var dstring = dt.getTime();
    var numOfVisibleRows = $('tr').length;
    var counting = numOfVisibleRows+1;
    var blockhtml = '';
    blockhtml += '<tr>';
        blockhtml += '<td>';
            blockhtml += '<input class="form-control input-sm" type="hidden" name="idmember[]" value="">';
            blockhtml += '<select name="setting[]" class="form-control select2 input-sm">';
            blockhtml += '<option value="">-- Pilih --</option>';
            <?php foreach($this->webconfig['bidang_setting'] as $keys => $values){ ?>
            blockhtml += '<option <?php echo (isset($setting) && $setting == $keys)?"selected='selected'":""; ?> value="<?php echo $keys; ?>"><?php echo $values; ?></option>';
            <?php } ?>
            blockhtml += '</select>';
        blockhtml += '</td>';
        blockhtml += '<td>';
            blockhtml += '<input class="form-control input-sm" type="text" name="tahun[]" value="">';
        blockhtml += '</td>';
        blockhtml += '<td>';
            blockhtml += '<input class="form-control input-sm" type="text" name="jabatan[]" value="">';
        blockhtml += '</td>';

        blockhtml += '<td>';
            blockhtml += '<select name="peran[]" class="form-control select2 input-sm">';
            blockhtml += '<option value="">-- Pilih --</option>';
            <?php foreach($this->webconfig['peran_utama'] as $keys => $values){ ?>
            blockhtml += '<option <?php echo (isset($peran) && $peran == $keys)?"selected='selected'":""; ?> value="<?php echo $keys; ?>"><?php echo $values; ?></option>';
            <?php } ?>
            blockhtml += '</select>';
        blockhtml += '</td>';


        blockhtml += '<td>';
            blockhtml += '<select name="keahlian[]" class="form-control select2 input-sm">';
            blockhtml += '<option value="">-- Pilih --</option>';
            <?php foreach($this->webconfig['jenis_keahlian'] as $keys => $values){ ?>
            blockhtml += '<option <?php echo (isset($keahlian) && $keahlian == $keys)?"selected='selected'":""; ?> value="<?php echo $keys; ?>"><?php echo $values; ?></option>';
            <?php } ?>
            blockhtml += '</select>';
        blockhtml += '</td>';


        
        blockhtml += '<td>';
             blockhtml += '<a class="btn btn-danger btn-xs" onclick="deleteDetailPraktek(this)" href="javascript:void(0)">';
                 blockhtml += '<i class="ti-trash"></i>';
                 blockhtml += 'Hapus';
             blockhtml += '</a>';
        blockhtml += '</td>';
    blockhtml += '</tr>';
    $('#tablepraktek tbody tr:last').after(blockhtml);
}
function deleteDetailPraktek(obj){
    $(obj).parent().parent().remove();
}
function deleteThisPraktek(code){
    var txt = "<?php echo $this->lang->line('alert_delete'); ?> <input type='hidden' id='alertName' name='alertName' value='"+code+"' />";
    jQuery.prompt(txt ,{  submit: doConditionPraktek, buttons: { <?php echo $this->lang->line('ok'); ?>: true, <?php echo $this->lang->line('cancel'); ?>: false },prefix:'jqismooth' });
}
function doConditionPraktek(v,m,f,e){
    if(m){
        $('#member'+e.alertName).remove();
        var posting = "dataid="+e.alertName;
        jQuery.ajax({
            type: 'POST',
            url: "<?php echo base_url().$this->router->class; ?>/deletePraktek",
            data: posting,
            success: function(response) {
                if(response == 'success'){
                    toastr.success("<?php echo $this->lang->line('msg_success_delete'); ?>", "<?php echo $this->lang->line('success_notif'); ?>");
                }else{
                    toastr.error("<?php echo $this->lang->line('msg_empty_delete'); ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                }
            }
        })
    }else{
        jQuery.prompt.close();
    }
}
</script>
<form class="form-horizontal" name="form" method="POST" action="<?php echo base_url().$this->router->class; ?>/getPraktek/<?php echo $id;?>" enctype="multipart/form-data">
    <div class="box-body">
        <div class="col-md-12">
            <div class="form-group footertable">
                <label class="col-xs 12 col-sm-4 text-left">
                	<a href="javascript:void(0);" onclick="openTablePraktek();" class="btn btn-danger btn-sm mr5 openTablePraktek">Tambah Detail</a>
                </label> 
                    <input type="hidden" name="id" value="<?php echo $id; ?>">
                    <div class="col-sm-12 text-left table-responsive">
                        <table class="table table-bordered table-responsive" id="tablepraktek" style="margin-bottom: 0px;">
                            <tbody id="datatable">
                            <tr>
            					<th>Bidang/Setting Praktik</th>
            					<th>Tahun</th>
            					<th>Jabatan/Posisi</th>
            					<th>Peran Utama</th>
            					<th>Jenis Keahlian</th>
            					<th style="width: 40px">Aksi</th>
                            </tr>

                            <?php if(isset($lists) && count($lists) > 0) { ?>
                        	<?php foreach($lists as $key => $value){ ?>
                            <tr id="member<?php echo $value['id']; ?>">
                            	<td>
                                    <input class="form-control input-sm" name="idmember[]" value="<?php echo isset($value['id'])?$value['id']:'';?>" type="hidden">
                                    <select name="setting[]" class="form-control select2 input-sm">
                                        <option value="">-- Pilih --</option>
                                        <?php foreach($this->webconfig['bidang_setting'] as $keys => $values){ ?>
                                        <option <?php echo (isset($value['setting']) && $value['setting'] == $keys)?"selected='selected'":""; ?> value="<?php echo $keys; ?>"><?php echo $values; ?></option>
                                        <?php } ?>
                                    </select>
                            	</td>
                            	<td>
                            		<input class="form-control input-sm" name="tahun[]" value="<?php echo isset($value['tahun'])?$value['tahun']:'';?>" type="text">
                            	</td>
                            	<td>
                            		<input class="form-control input-sm" name="jabatan[]" value="<?php echo isset($value['jabatan'])?$value['jabatan']:'';?>" type="text">
                            	</td>
                            	<td>
                                    <select name="peran[]" class="form-control select2 input-sm">
                                        <option value="">-- Pilih --</option>
                                        <?php foreach($this->webconfig['peran_utama'] as $keys => $values){ ?>
                                        <option <?php echo (isset($value['peran']) && $value['peran'] == $keys)?"selected='selected'":""; ?> value="<?php echo $keys; ?>"><?php echo $values; ?></option>
                                        <?php } ?>
                                    </select>
                            	</td>
                            	<td>
                                    <select name="keahlian[]" class="form-control select2 input-sm">
                                        <option value="">-- Pilih --</option>
                                        <?php foreach($this->webconfig['jenis_keahlian'] as $keys => $values){ ?>
                                        <option <?php echo (isset($value['keahlian']) && $value['keahlian'] == $keys)?"selected='selected'":""; ?> value="<?php echo $keys; ?>"><?php echo $values; ?></option>
                                        <?php } ?>
                                    </select>
                            	</td>
                            	<td>
                            		<a class="btn btn-danger btn-xs" onclick="deleteThisPraktek(<?php echo $value['id']; ?>)" href="javascript:void(0)"><i class="ti-trash"></i>Hapus</a>
                            	</td>
                            </tr>
                            <?php } ?>
                            <?php } ?>

                            
                        	</tbody>
                        </table>
                    </div>
                    <div class="col-sm-12 text-left table-responsive">
                        <hr>
                        <div class="form-group footertable">
                            <div class="col-sm-12 text-left">
                                <input class="btn btn-primary btn-sm mr5" type="submit" value="<?php echo $this->lang->line('navigation_save'); ?>">
                            </div>
                        </div>
                    </div>
                
            </div>
            
        </div>
    </div>
</form>