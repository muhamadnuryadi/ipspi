<script type="text/javascript">
jQuery(document).ready(function($) {
    
});
function downloadThis(id){
    var posting = "dataid="+id;
    jQuery.ajax({
        type: 'POST',
        url: "<?php echo base_url().$this->router->class; ?>/modifTglSurat",
        data: posting,
        success: function(response) {
            if(response == 'success'){
                toastr.success("Surat Rekomendasi Berhasil Digenerate", "<?php echo $this->lang->line('success_notif'); ?>");
                window.onload = timedRefresh(3000);
            }else if(response == 'notupdate'){
                toastr.info("Surat Rekomendasi Tidak diupdate", "Informasi");
            }else{
                toastr.error("Surat Rekomendasi Gagal Digenerate", "<?php echo $this->lang->line('error_notif'); ?>");
            }
        }
    })
}
function timedRefresh(timeoutPeriod) {
    setTimeout("location.reload(true);",timeoutPeriod);
}
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo strtoupper($this->module_name); ?>
            <small><?php echo $this->lang->line('download_rekomendasi'); ?></small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('/'); ?>">
                    <i class="fa fa-dashboard"></i>
                    <?php echo $this->lang->line('dashboard'); ?>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url().$this->router->class; ?>/rekomendasi"><?php echo ucfirst($this->module_name); ?></a>
            </li>
        </ol>
    </section>
    <?php if(isset($error_hash) && count($error_hash) > 0){ ?>
        <?php foreach($error_hash as $inp_err){ ?>
            <script type="text/javascript">
            jQuery(document).ready(function($) {
                toastr.error("<?php echo $inp_err; ?>", "<?php echo $this->lang->line('error_notif'); ?>");
                });
            </script>
        <?php } ?>
    <?php } ?>
    
    <?php if(isset($success)){ ?>
        <script type="text/javascript">
        jQuery(document).ready(function($) {
            toastr.success("<?php echo $success; ?>", "<?php echo $this->lang->line('success_notif'); ?>");
            });
        </script>
    <?php } ?>
    <section class="content">
        <form class="form-horizontal" name="form" method="POST" action="<?php echo base_url().$this->router->class; ?>/rekomendasi" enctype="multipart/form-data" onsubmit="return false;">
            <div class="box box-default">
                <div class="box-header with-border">
                  <i class="fa fa-download"></i>
                  <h3 class="box-title">Generate Surat Rekomendasi</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-12 col-xs-12">
                        
                        <div class="col-md-12 col-xs-12 text-center">
                            <button type="submit" class="btn btn-success btn-lg" onclick="downloadThis(<?php echo $member[0]['id'];?>)"><i class="fa fa-file"></i> Generate Surat</button>
                        </div>
                        <br>
                        <br>
                        <br>
                        <div class="clearfix"></div>
                        <?php if($member[0]['tgl_awal_surat'] != '' && $member[0]['tgl_akhir_surat'] > date('Y-m-d')){?>
                        <object data="<?php echo $this->webconfig['media-server-cache'].$filename; ?>" type="application/pdf" width="100%" height="600px">

                            <iframe width="100%" height="600" name="iframe_a" src="<?php echo $this->webconfig['media-server-cache'].$filename; ?>" type="application/pdf">
                              This browser does not support PDFs. Please download the PDF to view it: <a href="<?php echo $this->webconfig['media-server-cache'].$filename; ?>" type="application/pdf">Download PDF</a>
                            </iframe>
                        </object>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </form>
    </section>
</div>