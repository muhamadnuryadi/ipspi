<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Surat Rekomendasi</title>
<style type="text/css">
body {
    font-family: 'Georgia', serif;
    font-size: 11pt;
    color: #000;
    line-height: 20pt;
    background-color: #fff;
}
.container {
    /*height: 1759px;*/
    width: 1202px;
    margin: auto;
    padding: 0 15pt;
}
.logo{
    float: right;
    width: 35%;
    /*margin-top: 80pt;*/
    margin-bottom: 40pt;
}
.clear{
    clear: both;
}
.signature{
    width: 100%;
    margin-top: 25pt;
}
.info{
    /*float: right;*/
    /*text-align: right;*/
    /*width: 54%;*/
}
p.text-center{
    text-align: center;
    margin: 0 !important;
}
.nomor{
    text-align: center;
    margin-top: 0;
    margin-bottom: 20pt;
}
p.small-info{
    text-align: center;
    font-family: "Arial";
    font-size: 8pt;
    line-height: 16pt;
    margin-top: 40pt;
}
.clearfix{
    clear: both;
    display: block;
}
</style>
</head>

<body>
    <div class="container">
        <img src="<?php echo $this->webconfig['back_base_template']; ?>img/logo.jpg" class="logo" />
        <div class="clear"></div>
        <p class="text-center">SURAT REKOMENDASI</p>
        <p class="nomor">Nomor :<span></span>	<?php echo $member[0]['count_surat'];?>/IPSPI/R/<?php echo date('Y')?></p>
        <p>Dewan Pengurus Pusat Ikatan Pekerja Sosial Profesional Indonesia (DPP IPSPI) <br >menerangkan bahwa :</p>
        <table>
            <tr>
                <td width="300">Nama Lengkap</td> 
                <td width="20">:</td> 
                <td><?php echo ucwords($member[0]['nama']); ?></td>
            </tr>
            <tr>
                <td>Nomor Anggota IPSPI</td>
                <td>:</td> 
                <td><?php echo $member[0]['no_anggota']; ?></td>
            </tr>
        </table>
        <p>Adalah benar anggota IPSPI dan yang bersangkutan berhak untuk memperoleh surat rekomendasi. Surat rekomendasi ini diberikan untuk keperluan mengikuti uji kompetensi sertifikasi / resertifikasi*) pada Lembaga Sertifikasi Pekerja Sosial.</p>
        
        <p>Masa berlaku surat rekomendasi ini selama tiga bulan terhitung sejak tanggal <?php echo short_date($member[0]['tgl_awal_surat'])?> sampai dengan <?php echo short_date(date($member[0]['tgl_akhir_surat'], strtotime('+3 months')));?>.</p>
        <p>Demikian rekomendasi ini diberikan, agar dipergunakan sebagaimana mestinya.</p>
        <div style="float: right; width: 50%;">
            <table class="info">
                <tr>
                    <td width="150">Dikeluarkan di</td> 
                    <td width="20">:</td> 
                    <td>Jakarta</td>
                </tr>
                <tr>
                    <td>Pada Tanggal</td>
                    <td>:</td>
                    <td><?php echo short_date($member[0]['tgl_awal_surat'])?></td>
                </tr>
            </table>
        </div>
        <div class="clearfix"></div>
        <img src="<?php echo $this->webconfig['back_base_template']; ?>img/ttd.png" alt="Signature" class="signature" /> 
        <p class="small-info">d.a Pusbangprof Peksosdan Pensos Kementerian Sosial RI, Jalan Dewi Sartika, No.200 Cawang, Jakarta Timur <br >Website: <a href="#">www.ipspi-indonesia.com</a>, Email: <a href="">sekretariat.ipspi@gmail.com</a></p>
    </div>
</body>

</html>