<?php 
if ($this->webconfig['payment_production'] == true) {
    ?><script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="<?php echo $this->webconfig['midtrans_client_key_production'] ?>"></script><?php
}else{
    ?><script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="<?php echo $this->webconfig['midtrans_client_key_sandbox'] ?>"></script><?php
}
?>

<script type="text/javascript">
jQuery(document).ready(function($) {
    $('#pay-button').click(function (event) {
        event.preventDefault();
        // $(this).attr("disabled", "disabled");
    
        $.ajax({
            url: '<?php echo base_url() ?>members/token', //calling this function
            cache: false,
            success: function(data) {
                //location = data;
                console.log('token = '+data);
                
                var resultType = document.getElementById('result-type');
                var resultData = document.getElementById('result-data');
                function changeResult(type,data){
                  $("#result-type").val(type);
                  $("#result-data").val(JSON.stringify(data));
                  //resultType.innerHTML = type;
                  //resultData.innerHTML = JSON.stringify(data);
                }

                snap.pay(data, {
                    onSuccess: function(result){
                        // console.log('success');console.log(result);
                        window.location.href = "<?php echo base_url() ?>members/invoice_success";
                    },
                    onPending: function(result){
                        console.log('pending');console.log(result);
                        window.location.href = "<?php echo base_url() ?>members/invoice_success";
                    },
                    onError: function(result){
                        // console.log('error');console.log(result);
                        window.location.href = "<?php echo base_url() ?>members/invoice_error";
                    },
                    onClose: function(){
                        // console.log('customer closed the popup without finishing the payment');
                        
                        // $('#pay-button').attr("enabled", "enabled");
                    }
                })
            }
        })
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo strtoupper($this->module_name); ?>
            <small>Pembayaran</small>
        </h1>
        
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="col-xs-12 text-right">
                            <a class="btn btn-success btn-xs" href="<?php echo base_url().$this->router->class; ?>"  title="<?php echo $this->lang->line('nav_kembali'); ?>">
                                <i class="fa fa-fw fa-plus"></i>
                                <?php echo $this->lang->line('nav_kembali'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                            <h4>Order Pembayaran Anggota Tetap</h4>
                            <hr/>
                            <div>
                                <form id="payment-form" class="form-horizontal" name="form" method="POST" action="<?php echo base_url().$this->router->class; ?>/payment_finish" enctype="multipart/form-data" onsubmit="return false;">
                                    <input type="hidden" name="result_type" id="result-type" value="">
                                    <input type="hidden" name="result_data" id="result-data" value="">

                                    <div class="table-responsive">
                                        <input type="hidden" name="id_member" value="<?php echo $this->session->userdata('id_user'); ?>">
                                        <table class="table table-striped">
                                            <tr>
                                                <td><?php echo $this->lang->line('email_users'); ?></td>
                                                <td><?php echo $this->session->userdata('email') ?></td>
                                            </tr>

                                            <tr>
                                                <td><?php echo $this->lang->line('name'); ?></td>
                                                <td><?php echo $this->session->userdata('name') ?></td>
                                            </tr>
                                            <tr>
                                                <td><?php echo $this->lang->line('harga'); ?></td>
                                                <td><?php echo rupiah($this->webconfig['harga_member']); ?></td>
                                            </tr>
                                            <tr>
                                                <td><?php echo $this->lang->line('tanggal_expired'); ?></td>
                                                <td><?php echo date('d-m-Y', strtotime('+5 years')); ?></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <?php 
                                                    /*<input class="btn btn-danger btn-sm mr5 pull-right" type="submit" value="<?php echo $this->lang->line('bayar'); ?>">*/
                                                    ?>
                                                    <button class="btn btn-danger btn-sm mr5 pull-right" id="pay-button"><?php echo $this->lang->line('bayar'); ?></button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>