<?php
class Members extends CI_Controller {
	var $webconfig;
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
        $this->load->helper('image');
        $this->load->library('image_moo');
        $this->load->helper('mpdf');
        $this->load->library('pagination');
		$this->lang->load('global', 'bahasa');
        $this->load->sharedModel('WilayahModel');
		$this->load->sharedModel('ConfigurationModel');
        $this->load->sharedModel('PekerjaanModel');
        $this->load->sharedModel('InstansiModel');
		$this->load->sharedModel('MembersModel');
        $this->load->sharedModel('TingkatModel');
		$this->load->sharedModel('LoginModel');
		
		$this->load->library('session');
		$this->webconfig = $this->config->item('webconfig');
		$this->module_name = $this->lang->line('members');		

		if(!$this->LoginModel->isLoggedIn()){
			redirect(base_url().'login');
		}

        if ($this->session->userdata('role') == 3) {
            if ($this->session->userdata('status_access') != 1) {
                redirect(base_url().'pembayaran');
            }
        }
	}

	function index() {
        redirect(base_url() . $this->router->class . '/listing');
    }

    function listing($page = 0) {
        $tdata = array();
        $tdata['tingkatpendidikan'] = $this->TingkatModel->listData();
        $tdata['wilayah'] = $this->WilayahModel->listpropinsi();
        $tdata['pekerjaan'] = $this->PekerjaanModel->listData();
        $tdata['instansi'] = $this->InstansiModel->listData();
        
        $ldata['content'] = $this->load->view($this->router->class . '/index', $tdata, true);
        $this->load->sharedView('template', $ldata);
    }

    function getAllData($page = 0) {

        $all_data = $this->MembersModel->listDataCount();
        $cfg_pg ['base_url'] = base_url() . $this->uri->segment(1) . '/' . $this->uri->segment(2) . "/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah']) ? $all_data['jumlah'] : 0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 3;

        $cfg_pg['first_link'] = '&laquo;';
        $cfg_pg['first_tag_close'] = '</li>';
        $cfg_pg['first_tag_open'] = '<li>';

        $cfg_pg['last_link'] = '&raquo;';
        $cfg_pg['last_tag_open'] = '<li>';
        $cfg_pg['last_tag_close'] = '</li>';

        $cfg_pg['next_link'] = '&gt;';
        $cfg_pg['next_tag_open'] = '<li>';
        $cfg_pg['next_tag_close'] = '</li>';

        $cfg_pg['prev_link'] = '&lt;';
        $cfg_pg['prev_tag_open'] = '<li>';
        $cfg_pg['prev_tag_close'] = '</li>';

        $cfg_pg['num_tag_open'] = '<li>';
        $cfg_pg['num_tag_close'] = '</li>';

        $cfg_pg['cur_tag_open'] = '<li class="active"><span>';
        $cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
        $listdata["pagination"] = $this->pagination->create_links();

        $start_no = 0;
        if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
            $start_no = $this->uri->segment($cfg_pg ['uri_segment']);
        }

        $listdata["start_no"] = $start_no;

        $start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
        $start_idx = $start_idx < 0 ? 0 : $start_idx;

        $listdata['lists'] = $this->MembersModel->listData(array(
            'start' => $start_idx
            , 'limit' => $cfg_pg['per_page']
        ));

        $this->load->view($this->router->class . '/list', $listdata);
    }

    function searchdata($name = '',$id_propinsi = '',$id_kota = '',$tingkat = '',$id_pekerjaan = '',$id_instansi = '',$id_sertifikasi = '',$no_anggota = '',$email = '',$id_jenis_anggota = '',$jurusan = '') {
        if ($name == '-') {$name = '';}
        if ($id_propinsi == '-') {$id_propinsi = '';}
        if ($id_kota == '-') {$id_kota = '';}
        if ($tingkat == '-') {$tingkat = '';}
        if ($id_pekerjaan == '-') {$id_pekerjaan = '';}
        if ($id_instansi == '-') {$id_instansi = '';}
        if ($id_sertifikasi == '-') {$id_sertifikasi = '';}
        if ($no_anggota == '-') {$no_anggota = '';}
        if ($email == '-') {$email = '';}
        if ($id_jenis_anggota == '-') {$id_jenis_anggota = '';}
        if ($jurusan == '-') {$jurusan = '';}
        
        # PAGINATION #
        $all_data = $this->MembersModel->filterDataCount(array(
                                                                'name' => trim(urldecode($name))
                                                                ,'id_propinsi' => trim(urldecode($id_propinsi))
                                                                ,'id_kota' => trim(urldecode($id_kota))
                                                                ,'tingkat' => trim(urldecode($tingkat))
                                                                ,'id_pekerjaan' => trim(urldecode($id_pekerjaan))
                                                                ,'id_instansi' => trim(urldecode($id_instansi))
                                                                ,'id_sertifikasi' => trim(urldecode($id_sertifikasi))
                                                                ,'no_anggota' => trim(urldecode($no_anggota))
                                                                ,'email' => trim(urldecode($email))
                                                                ,'id_jenis_anggota' => trim(urldecode($id_jenis_anggota))
                                                                ,'jurusan' => trim(urldecode($jurusan))
                                                            ));
        $cfg_pg ['base_url'] = base_url() . $this->uri->segment(1).'/'.$this->uri->segment(2)."/".$this->uri->segment(3)."/".$this->uri->segment(4)."/".$this->uri->segment(5)."/".$this->uri->segment(6)."/".$this->uri->segment(7)."/".$this->uri->segment(8)."/".$this->uri->segment(9)."/".$this->uri->segment(10)."/".$this->uri->segment(11)."/".$this->uri->segment(12)."/"."/".$this->uri->segment(13)."/";
        $cfg_pg ['total_rows'] = isset($all_data['jumlah']) ? $all_data['jumlah'] : 0;
        $cfg_pg ['per_page'] = $this->webconfig['count_data'];
        $cfg_pg ['uri_segment'] = 14;

        $cfg_pg['first_link'] = '&laquo;';
        $cfg_pg['first_tag_close'] = '</li>';
        $cfg_pg['first_tag_open'] = '<li>';

        $cfg_pg['last_link'] = '&raquo;';
        $cfg_pg['last_tag_open'] = '<li>';
        $cfg_pg['last_tag_close'] = '</li>';

        $cfg_pg['next_link'] = '&gt;';
        $cfg_pg['next_tag_open'] = '<li>';
        $cfg_pg['next_tag_close'] = '</li>';

        $cfg_pg['prev_link'] = '&lt;';
        $cfg_pg['prev_tag_open'] = '<li>';
        $cfg_pg['prev_tag_close'] = '</li>';

        $cfg_pg['num_tag_open'] = '<li>';
        $cfg_pg['num_tag_close'] = '</li>';

        $cfg_pg['cur_tag_open'] = '<li class="active"><span>';
        $cfg_pg['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($cfg_pg);
        $listdata["pagination"] = $this->pagination->create_links();

        $start_no = 0;
        if ($this->uri->segment($cfg_pg ['uri_segment']) != '') {
            $start_no = $this->uri->segment($cfg_pg ['uri_segment']);
        }

        $listdata["start_no"] = $start_no;

        $start_idx = $cfg_pg['per_page'] * ($this->pagination->cur_page - 1);
        $start_idx = $start_idx < 0 ? 0 : $start_idx;

        $listdata['lists'] = $this->MembersModel->filterData(array(
                                                                    'start' => $start_idx
                                                                    ,'limit' => $cfg_pg['per_page']
                                                                    ,'name' => trim(urldecode($name))
                                                                    ,'id_propinsi' => trim(urldecode($id_propinsi))
                                                                    ,'id_kota' => trim(urldecode($id_kota))
                                                                    ,'tingkat' => trim(urldecode($tingkat))
                                                                    ,'id_pekerjaan' => trim(urldecode($id_pekerjaan))
                                                                    ,'id_instansi' => trim(urldecode($id_instansi))
                                                                    ,'id_sertifikasi' => trim(urldecode($id_sertifikasi))
                                                                    ,'no_anggota' => trim(urldecode($no_anggota))
                                                                    ,'email' => trim(urldecode($email))
                                                                    ,'id_jenis_anggota' => trim(urldecode($id_jenis_anggota))
                                                                    ,'jurusan' => trim(urldecode($jurusan))
        ));
        $this->load->view($this->router->class . '/list', $listdata);
    }

    function add() {
        $tdata = array();
        $tdata['wilayah'] = $this->WilayahModel->listpropinsi();
        require_once(dirname(__FILE__) . '/../../../libraries/formvalidator.php');

        if ($_POST) {
            
            $validator = new FormValidator();
            $validator->addValidation("id_propinsi", "req", $this->lang->line('error_empty_id_propinsi'));
            $validator->addValidation("password","req",$this->lang->line('error_empty_password_users'));
            $validator->addValidation("password","minlen=5",$this->lang->line('error_empty_password_5_users'));
            $validator->addValidation("password_1","req",$this->lang->line('error_empty_retype_password_users'));
            $validator->addValidation("password_1","eqelmnt=password",$this->lang->line('error_empty_retype_password_true_users'));

            if ($validator->ValidateForm()) {
                $doInsert = $this->MembersModel->entriData(array(
                                                                'nama' => $this->input->post('nama'),
                                                                'username' => $this->input->post('username'),
                                                                'email' => $this->input->post('email'),
                                                                'password' => $this->input->post('password'),
                                                                'password_1' => $this->input->post('password_1'),
                                                                'tgllahir' => $this->input->post('tgllahir'),
                                                                'jumanak' => $this->input->post('jumanak'),
                                                                'alamatkantor' => $this->input->post('alamatkantor'),
                                                                'telpkantor' => $this->input->post('telpkantor'),
                                                                'faxkantor' => $this->input->post('faxkantor'),
                                                                'ktp' => $this->input->post('ktp'),
                                                                'alamatrumah' => $this->input->post('alamatrumah'),
                                                                'id_propinsi' => $this->input->post('id_propinsi'),
                                                                'id_kota' => $this->input->post('id_kota'),
                                                                'telp' => $this->input->post('telp'),
                                                                'hp' => $this->input->post('hp'),
                                                                'status' => $this->input->post('status'),
                                                                'created_at' => $this->input->post('created_at')
                ));
                if ($doInsert == 'empty') {
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
                }else if($doInsert == 'failed') {
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
                }else if ($doInsert == 'exist') {
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_exist_name'));
                }else if($doInsert == 'success') {
                    $tdata['success'] = $this->lang->line('msg_success_entry');
                    return redirect('members/modif/'.$this->session->flashdata('id_insert'), $tdata, true);
                }
                $ldata['content'] = $this->load->view($this->router->class . '/add', $tdata, true);
                $this->load->sharedView('template', $ldata);
            } else {
                $tdata['nama'] = $this->input->post('nama');
                $tdata['username'] = $this->input->post('username');
                $tdata['email'] = $this->input->post('email');
                $tdata['password'] = $this->input->post('password');
                $tdata['password_1'] = $this->input->post('password_1');
                $tdata['tgllahir'] = $this->input->post('tgllahir');
                $tdata['jumanak'] = $this->input->post('jumanak');
                $tdata['ktp'] = $this->input->post('ktp');
                $tdata['alamatkantor'] = $this->input->post('alamatkantor');
                $tdata['telpkantor'] = $this->input->post('telpkantor');
                $tdata['faxkantor'] = $this->input->post('faxkantor');
                $tdata['alamatrumah'] = $this->input->post('alamatrumah');
                $tdata['id_propinsi'] = $this->input->post('id_propinsi');
                $tdata['id_kota'] = $this->input->post('id_kota');
                $tdata['telp'] = $this->input->post('telp');
                $tdata['hp'] = $this->input->post('hp');

                $tdata['error_hash'] = $validator->GetErrors();

                $ldata['content'] = $this->load->view($this->router->class . '/add', $tdata, true);
                $this->load->sharedView('template', $ldata);
            }
        } else {
            $ldata['content'] = $this->load->view($this->router->class . '/add', $tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }

    function modif($id = 0, $tdata = []) {
        if ($id == 0) {
            redirect(base_url() . $this->router->class);
        }

        $tdata['id'] = $id;
        $tdata['lists'] = $this->MembersModel->listData(array('id' => $id));
        
        require_once(dirname(__FILE__) . '/../../../libraries/formvalidator.php');
        
        if ($_POST) {
            $validator = new FormValidator();

            $validator->addValidation("name", "req", $this->lang->line('error_empty_name_provinsi'));
            // $validator->addValidation("kode", "req", $this->lang->line('error_empty_kode_provinsi'));
            // $validator->addValidation("kode", "alnum_b", $this->lang->line('error_alpha_b_kode_provinsi'));

            if ($validator->ValidateForm()) {
                $doUpdate = $this->MembersModel->updateData(array(
                                                                    'id' => $id
                                                                    ,'name' => $this->input->post('name')
                                                                    ,'status' => $this->input->post('status')
                ));

                if ($doUpdate == 'empty') {
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
                } else if ($doUpdate == 'failed') {
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
                } else if ($doUpdate == 'exist') {
                    $tdata['error_hash'] = array('error' => $this->lang->line('msg_exist_name'));
                } else if ($doUpdate == 'success') {
                    $tdata['success'] = $this->lang->line('msg_success_update');
                    $tdata['lists'] = $this->MembersModel->listData(array('id' => $id));
                }
                $ldata['content'] = $this->load->view($this->router->class . '/modif', $tdata, true);
                $this->load->sharedView('template', $ldata);
            } else {
                
                $tdata['error_hash'] = $validator->GetErrors();

                $ldata['content'] = $this->load->view($this->router->class . '/modif', $tdata, true);
                $this->load->sharedView('template', $ldata);
            }
        } else {
            $ldata['content'] = $this->load->view($this->router->class . '/modif', $tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }

    function getProfile($id) {
        $tdata = array();
        $tdata['id'] = $id;
        $tdata['wilayah'] = $this->WilayahModel->listpropinsi();
        $tdata['lists'] = $this->MembersModel->listData(array('id' => $id));

        if($_POST){
            
            $doUpdate = $this->MembersModel->updateDataProfile(array(
                                                                'id' => $id,
                                                                'nama' => $this->input->post('nama'),
                                                                'username' => $this->input->post('username'),
                                                                'email' => $this->input->post('email'),
                                                                'password' => $this->input->post('password'),
                                                                'password_1' => $this->input->post('password_1'),
                                                                'tgllahir' => $this->input->post('tgllahir'),
                                                                'jumanak' => $this->input->post('jumanak'),
                                                                'ktp' => $this->input->post('ktp'),
                                                                'alamatkantor' => $this->input->post('alamatkantor'),
                                                                'telpkantor' => $this->input->post('telpkantor'),
                                                                'faxkantor' => $this->input->post('faxkantor'),
                                                                'alamatrumah' => $this->input->post('alamatrumah'),
                                                                'id_propinsi' => $this->input->post('id_propinsi'),
                                                                'id_kota' => $this->input->post('id_kota'),
                                                                'telp' => $this->input->post('telp'),
                                                                'hp' => $this->input->post('hp'),
                                                                'status' => $this->input->post('status'),
                                                                'created_at' => $this->input->post('created_at'),
                                                                'statuskawin' => $this->input->post('statuskawin'),
                                                                'path_ijazah_old' => $this->input->post('path_ijazah_old'),
                                                                'path_ktp_old' => $this->input->post('path_ktp_old'),
                                                                'foto_old' => $this->input->post('foto_old')
                                                            ));
            if($doUpdate == 'empty'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
            }else if($doUpdate == 'failed'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
            }else if($doUpdate == 'success'){
                $tdata['success'] = $this->lang->line('msg_success_update');
                $tdata['lists'] = $this->MembersModel->listData(array('id'=>$id));
            }
            $this->session->set_flashdata('success', $tdata['success']);
            return redirect('members/modif/'.$id, $tdata, true);
        }else{
            $this->load->view($this->router->class . '/profile', $tdata);
        }
    }

    function getPendidikan($id) {
        $tdata = array();
        $tdata['id'] = $id;
        $tdata['lists'] = $this->MembersModel->listDataPendidikan(array('id' => $id));
        $tdata['tingkatpendidikan'] = $this->TingkatModel->listData();
        if($_POST){
            
            $doUpdate = $this->MembersModel->updateDataPendidikan(array(
                                                                'id' => $id
                                                                ,'idmember' => $this->input->post('idmember')
                                                                ,'tingkat' => $this->input->post('tingkat')
                                                                ,'pt' => $this->input->post('pt')
                                                                ,'jurusan' => $this->input->post('jurusan')
                                                                ,'gelar' => $this->input->post('gelar')
                                                                ,'thn_lulus' => $this->input->post('thn_lulus')
                                                                ,'jenis_pendidikan' => $this->input->post('jenis_pendidikan')
                                                            ));
            if($doUpdate == 'empty'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
            }else if($doUpdate == 'failed'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
            }else if($doUpdate == 'success'){
                $tdata['success'] = $this->lang->line('msg_success_update');
                $tdata['lists'] = $this->MembersModel->listData(array('id'=>$id));
            }
            $this->session->set_flashdata('success', $tdata['success']);
            return redirect('members/modif/'.$id, $tdata, true);
        }else{
            $this->load->view($this->router->class . '/pendidikan', $tdata);
        }
    }

    function getPekerjaan($id) {
        $tdata = array();
        $tdata['id'] = $id;
        $tdata['lists'] = $this->MembersModel->listDataPekerjaan(array('id' => $id));
        $tdata['pekerjaan'] = $this->PekerjaanModel->listData();
        $tdata['instansi'] = $this->InstansiModel->listData();
        
        if($_POST){

            $doUpdate = $this->MembersModel->updateDataPekerjaan(array(
                                                                'id' => $this->input->post('id')
                                                                ,'id_pekerjaan' => $this->input->post('id_pekerjaan')
                                                                ,'idmember' => $this->input->post('idmember')
                                                                ,'nama' => $this->input->post('nama')
                                                                ,'jabatan' => $this->input->post('jabatan')
                                                                ,'mulai' => $this->input->post('mulai')
                                                                ,'akhir' => $this->input->post('akhir')
                                                                ,'id_instansi' => $this->input->post('id_instansi')
                                                            ));
            if($doUpdate == 'empty'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
            }else if($doUpdate == 'failed'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
            }else if($doUpdate == 'success'){
                $tdata['success'] = $this->lang->line('msg_success_update');
                $tdata['lists'] = $this->MembersModel->listData(array('id'=>$id));
            }
            $this->session->set_flashdata('success', $tdata['success']);
            return redirect('members/modif/'.$id, $tdata, true);
        }else{
            $this->load->view($this->router->class . '/pekerjaan', $tdata);
        }
    }

    function getPraktek($id) {
        $tdata = array();
        $tdata['id'] = $id;
        $tdata['lists'] = $this->MembersModel->listDataPraktek(array('id' => $id));
        if($_POST){
            $doUpdate = $this->MembersModel->updateDataPraktek(array(
                                                                'id' => $id
                                                                ,'idmember' => $this->input->post('idmember')
                                                                ,'setting' => $this->input->post('setting')
                                                                ,'tahun' => $this->input->post('tahun')
                                                                ,'jabatan' => $this->input->post('jabatan')
                                                                ,'peran' => $this->input->post('peran')
                                                                ,'keahlian' => $this->input->post('keahlian')
                                                            ));
            if($doUpdate == 'empty'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
            }else if($doUpdate == 'failed'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
            }else if($doUpdate == 'success'){
                $tdata['success'] = $this->lang->line('msg_success_update');
                $tdata['lists'] = $this->MembersModel->listData(array('id'=>$id));
            }
            $this->session->set_flashdata('success', $tdata['success']);
            return redirect('members/modif/'.$id, $tdata, true);
        }else{
            $this->load->view($this->router->class . '/praktek', $tdata);
        }
    }

    function getPenghargaan($id) {
        $tdata = array();
        $tdata['id'] = $id;
        $tdata['lists'] = $this->MembersModel->listDataPenghargaan(array('id' => $id));
        if($_POST){
            $doUpdate = $this->MembersModel->updateDataPenghargaan(array(
                                                                'id' => $id
                                                                ,'idmember' => $this->input->post('idmember')
                                                                ,'nama' => $this->input->post('nama')
                                                                ,'instansi' => $this->input->post('instansi')
                                                                ,'tahun' => $this->input->post('tahun')
                                                            ));
            if($doUpdate == 'empty'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
            }else if($doUpdate == 'failed'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
            }else if($doUpdate == 'success'){
                $tdata['success'] = $this->lang->line('msg_success_update');
                $tdata['lists'] = $this->MembersModel->listData(array('id'=>$id));
            }
            $this->session->set_flashdata('success', $tdata['success']);
            return redirect('members/modif/'.$id, $tdata, true);
        }else{
            $this->load->view($this->router->class . '/penghargaan', $tdata);
        }
    }

    function getSertifikasi($id) {
        $tdata = array();
        $tdata['id'] = $id;
        $tdata['lists'] = $this->MembersModel->listDataSertifikasi(array('id' => $id));
        
        if($_POST){
            $doUpdate = $this->MembersModel->updateDataSertifikasi(array(
                                                                'id' => $this->input->post('id')
                                                                ,'idmember' => $this->input->post('idmember')
                                                                ,'nama' => $this->input->post('nama')
                                                                ,'lembaga' => $this->input->post('lembaga')
                                                                ,'tahun' => $this->input->post('tahun')
                                                                ,'tipe' => $this->input->post('tipe')
                                                            ));
            if($doUpdate == 'empty'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
            }else if($doUpdate == 'failed'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
            }else if($doUpdate == 'success'){
                $tdata['success'] = $this->lang->line('msg_success_update');
                $tdata['lists'] = $this->MembersModel->listData(array('id'=>$id));
            }
            $this->session->set_flashdata('success', $tdata['success']);
            return redirect('members/modif/'.$id, $tdata, true);
        }else{
            $this->load->view($this->router->class . '/sertifikasi', $tdata);
        }
    }

    function getReferensi($id) {
        $tdata = array();
        $tdata['id'] = $id;
        $tdata['lists'] = $this->MembersModel->listDataReferensi(array('id' => $id));
        if($_POST){
            $doUpdate = $this->MembersModel->updateDataReferensi(array(
                                                                'id' => $id
                                                                ,'idmember' => $this->input->post('idmember')
                                                                ,'nama' => $this->input->post('nama')
                                                                ,'jabatan' => $this->input->post('jabatan')
                                                                ,'telp' => $this->input->post('telp')
                                                            ));
            if($doUpdate == 'empty'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
            }else if($doUpdate == 'failed'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
            }else if($doUpdate == 'success'){
                $tdata['success'] = $this->lang->line('msg_success_update');
                $tdata['lists'] = $this->MembersModel->listData(array('id'=>$id));
            }
            $this->session->set_flashdata('success', $tdata['success']);
            return redirect('members/modif/'.$id, $tdata, true);
        }else{
            $this->load->view($this->router->class . '/referensi', $tdata);
        }
    }

    function getProfesi($id) {
        $tdata = array();
        $tdata['id'] = $id;
        $tdata['lists'] = $this->MembersModel->listDataProfesi(array('id' => $id));
        if($_POST){
            $doUpdate = $this->MembersModel->updateDataProfesi(array(
                                                                'id' => $id
                                                                ,'idmember' => $this->input->post('idmember')
                                                                ,'nama' => $this->input->post('nama')
                                                                ,'tahun' => $this->input->post('tahun')
                                                                ,'keterangan' => $this->input->post('keterangan')
                                                            ));
            if($doUpdate == 'empty'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
            }else if($doUpdate == 'failed'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
            }else if($doUpdate == 'success'){
                $tdata['success'] = $this->lang->line('msg_success_update');
                $tdata['lists'] = $this->MembersModel->listData(array('id'=>$id));
            }
            $this->session->set_flashdata('success', $tdata['success']);
            return redirect('members/modif/'.$id, $tdata, true);
        }else{
            $this->load->view($this->router->class . '/profesi', $tdata);
        }
    }

    function getKomunitas($id) {
        $tdata = array();
        $tdata['id'] = $id;
        $tdata['lists'] = $this->MembersModel->listDataKomunitas(array('id' => $id));
        if($_POST){
            $doUpdate = $this->MembersModel->updateDataKomunitas(array(
                                                                'id' => $id
                                                                ,'idmember' => $this->input->post('idmember')
                                                                ,'nama' => $this->input->post('nama')
                                                                ,'tahun' => $this->input->post('tahun')
                                                                ,'keterangan' => $this->input->post('keterangan')
                                                            ));
            if($doUpdate == 'empty'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
            }else if($doUpdate == 'failed'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
            }else if($doUpdate == 'success'){
                $tdata['success'] = $this->lang->line('msg_success_update');
                $tdata['lists'] = $this->MembersModel->listData(array('id'=>$id));
            }
            $this->session->set_flashdata('success', $tdata['success']);
            return redirect('members/modif/'.$id, $tdata, true);
        }else{
            $this->load->view($this->router->class . '/komunitas', $tdata);
        }
    }

    public function deleteThis() {
        $doDelete = $this->__delete($this->input->post('dataid'));

        if ($doDelete == 'failed') {
            echo "nodata";
        } else if ($doDelete == 'success') {
            echo "success";
        }
    }

    public function postProcess() {
        $postvalue = $this->input->post('datacek');
        if ($postvalue != '') {
            foreach ($postvalue as $data) {
                $this->__delete($data);
            }
            echo "success";
        } else {
            echo "nodata";
        }
    }

    private function __delete($id) {
        if ($id == 0) {
            redirect(base_url() . $this->router->class);
        }
        $doDelete = $this->MembersModel->deleteData($id);
        return $doDelete;
    }

    public function deletePendidikan() {
        $doDelete = $this->__deletePendidikan($this->input->post('dataid'));

        if ($doDelete == 'failed') {
            echo "nodata";
        } else if ($doDelete == 'success') {
            echo "success";
        }
    }

    private function __deletePendidikan($id) {
        if ($id == 0) {
            redirect(base_url() . $this->router->class);
        }
        $doDelete = $this->MembersModel->deleteDataPendidikan($id);
        return $doDelete;
    }

    public function deletePekerjaan() {
        $doDelete = $this->__deletePekerjaan($this->input->post('dataid'));

        if ($doDelete == 'failed') {
            echo "nodata";
        } else if ($doDelete == 'success') {
            echo "success";
        }
    }

    private function __deletePekerjaan($id) {
        if ($id == 0) {
            redirect(base_url() . $this->router->class);
        }
        $doDelete = $this->MembersModel->deleteDataPekerjaan($id);
        return $doDelete;
    }

    public function deletePraktek() {
        $doDelete = $this->__deletePraktek($this->input->post('dataid'));

        if ($doDelete == 'failed') {
            echo "nodata";
        } else if ($doDelete == 'success') {
            echo "success";
        }
    }

    private function __deletePraktek($id) {
        if ($id == 0) {
            redirect(base_url() . $this->router->class);
        }
        $doDelete = $this->MembersModel->deleteDataPraktek($id);
        return $doDelete;
    }

    public function deletePenghargaan() {
        $doDelete = $this->__deletePenghargaan($this->input->post('dataid'));

        if ($doDelete == 'failed') {
            echo "nodata";
        } else if ($doDelete == 'success') {
            echo "success";
        }
    }

    private function __deletePenghargaan($id) {
        if ($id == 0) {
            redirect(base_url() . $this->router->class);
        }
        $doDelete = $this->MembersModel->deleteDataPenghargaan($id);
        return $doDelete;
    }

    public function deleteSertifikasi() {
        $doDelete = $this->__deleteSertifikasi($this->input->post('dataid'));

        if ($doDelete == 'failed') {
            echo "nodata";
        } else if ($doDelete == 'success') {
            echo "success";
        }
    }

    private function __deleteSertifikasi($id) {
        if ($id == 0) {
            redirect(base_url() . $this->router->class);
        }
        $doDelete = $this->MembersModel->deleteDataSertifikasi($id);
        return $doDelete;
    }

    public function deleteReferensi() {
        $doDelete = $this->__deleteReferensi($this->input->post('dataid'));

        if ($doDelete == 'failed') {
            echo "nodata";
        } else if ($doDelete == 'success') {
            echo "success";
        }
    }

    private function __deleteReferensi($id) {
        if ($id == 0) {
            redirect(base_url() . $this->router->class);
        }
        $doDelete = $this->MembersModel->deleteDataReferensi($id);
        return $doDelete;
    }

    public function deleteProfesi() {
        $doDelete = $this->__deleteProfesi($this->input->post('dataid'));

        if ($doDelete == 'failed') {
            echo "nodata";
        } else if ($doDelete == 'success') {
            echo "success";
        }
    }

    private function __deleteProfesi($id) {
        if ($id == 0) {
            redirect(base_url() . $this->router->class);
        }
        $doDelete = $this->MembersModel->deleteDataProfesi($id);
        return $doDelete;
    }

    public function deleteKomunitas() {
        $doDelete = $this->__deleteKomunitas($this->input->post('dataid'));

        if ($doDelete == 'failed') {
            echo "nodata";
        } else if ($doDelete == 'success') {
            echo "success";
        }
    }

    private function __deleteKomunitas($id) {
        if ($id == 0) {
            redirect(base_url() . $this->router->class);
        }
        $doDelete = $this->MembersModel->deleteDataKomunitas($id);
        return $doDelete;
    }

    public function view($id = 0){
        if($id == 0){
            redirect(base_url().$this->router->class);
        }
        $tdata = array();
        $tdata['id'] = $id;
        $tdata['lists'] = $this->MembersModel->listData(array('id' => $id));
        $tdata['pendidikan'] = $this->MembersModel->listDataPendidikan(array('id' => $id));
        $tdata['pekerjaan'] = $this->MembersModel->listDataPekerjaan(array('id' => $id));
        $tdata['penghargaan'] = $this->MembersModel->listDataPenghargaan(array('id' => $id));
        $tdata['sertifikasi'] = $this->MembersModel->listDataSertifikasi(array('id' => $id));
        $tdata['referensi'] = $this->MembersModel->listDataReferensi(array('id' => $id));
        $tdata['profesi'] = $this->MembersModel->listDataProfesi(array('id' => $id));
        $tdata['komunitas'] = $this->MembersModel->listDataKomunitas(array('id' => $id));

        $this->load->view($this->router->class.'/view',$tdata);
    }

    public function publish($id = 0){
        $doThis = $this->__doPublish($this->input->post('dataid'));
        
        if($doThis == 'failed'){
            echo "nodata";
        }else if($doThis == 'success'){
            echo "success";     
        }
    }

    public function unpublish($id = 0){
        $doThis = $this->__doUnpublish($this->input->post('dataid'));
        
        if($doThis == 'failed'){
            echo "nodata";
        }else if($doThis == 'success'){
            echo "success";     
        }
    }
    private function __doPublish($kodeid){
        if($kodeid == 0){
            redirect(base_url().$this->router->class."/");
        }
        $doPublish = $this->MembersModel->doPublish($kodeid);
        return $doPublish;
    }
    private function __doUnpublish($kodeid){
        if($kodeid == 0){
            redirect(base_url().$this->router->class."/");
        }
        $doUnublish = $this->MembersModel->doUnublish($kodeid);
        return $doUnublish;
    }

    public function generateAnggota($id){
        require_once(dirname(__FILE__) . '/../../../libraries/formvalidator.php');
        // echo $id;
        $tdata = array();
        $tdata['member'] = $this->MembersModel->listData(array('id'=>$id));
        $id_member = $tdata['member'][0]['id'];
        $id_provinsi = isset($tdata['member'][0]['id_propinsi'])?$tdata['member'][0]['id_propinsi']:'';
        $id_kota = isset($tdata['member'][0]['id_kota'])?$tdata['member'][0]['id_kota']:'';
        $tdata['no_anggota_from_db'] = $tdata['member'][0]['no_anggota'];
        $created_at = isset($tdata['member'][0]['created_at'])?$tdata['member'][0]['created_at']:'';
        $fotomember = isset($tdata['member'][0]['foto'])?$tdata['member'][0]['foto']:'';
        $pathijazahmember = isset($tdata['member'][0]['path_ijazah'])?$tdata['member'][0]['path_ijazah']:'';
        $pathktpmember = isset($tdata['member'][0]['path_ktp'])?$tdata['member'][0]['path_ktp']:'';

        //get date join
        if($created_at != ''){
            $month = date('m',strtotime($created_at));
            $year = substr(date('Y',strtotime($created_at)), 2) ;
        }else{
            if($fotomember != ''){
                $expfoto = explode('/', $fotomember);
                $month = $expfoto[1];
                $year = substr($expfoto[0], 2);
            } else if ($pathijazahmember != '') {
                $expfoto = explode('/', $pathijazahmember);
                $month = $expfoto[1];
                $year = substr($expfoto[0], 2);
            } else if ($pathktpmember != ''){
                $expfoto = explode('/', $pathktpmember);
                $month = $expfoto[1];
                $year = substr($expfoto[0], 2);
            } else {
                $month = date('m');
                $year = date('Y');
            }
        }

        //auto no anggota
        $allmember = $this->MembersModel->getLastNoAnggota();
        $newnumb = max(array_column($allmember, 'lastnumb'));
        $tdata['newno_anggota'] = $newnumb + 1;

        //untuk get kode wilayah
        $getKodeProvinsi = $this->WilayahModel->listData(array('id'=>$id_provinsi));
        $getKodeKota = $this->WilayahModel->listData(array('id'=>$id_kota));

        //untuk get pendidikan sosial dan non sosial/jenis anggota
        $pendidikan = $this->MembersModel->listDataPendidikan(array('id'=>$id_member));
        $pendidikanterakhir = end($pendidikan);
        $jenis_pendidikan = $pendidikanterakhir['jenis'];
        
        if ($jenis_pendidikan == 2) {//jika sosial
            $jenis_anggota = 1;
        }else{
            $jenis_anggota = 2;
        }

        $kodeprovinsi = $tdata['kodeprovinsi'] = isset($getKodeProvinsi[0]['kode'])?$getKodeProvinsi[0]['kode']:'';
        $kodekota = $tdata['kodekota'] = isset($getKodeKota[0]['kode'])?$getKodeKota[0]['kode']:'';

        $tdata['no_anggota_generate'] = $kodeprovinsi.'.'.$kodekota.'.'.$month.$year.'.'.$jenis_anggota.'.';

        if ($_POST) {
            $validator = new FormValidator();
            $validator->addValidation("noanggota_input", "req", 'Silakan melengkapi form');
            
            if ($validator->ValidateForm()) {
                $NoAnggota = $this->MembersModel->InsertNoAnggota(array(
                                                    'id'=>$id,
                                                    'noanggota'=>$this->input->post('noanggota'),
                                                    'noanggota_input'=>$this->input->post('noanggota_input')
                                                    ));

                if ($NoAnggota == 'success') {

                    $tdata['member'] = $this->MembersModel->listData(array('id'=>$id));
                    $id_member = $tdata['member'][0]['id'];
                    $id_provinsi = isset($tdata['member'][0]['id_propinsi'])?$tdata['member'][0]['id_propinsi']:'';
                    $id_kota = isset($tdata['member'][0]['id_kota'])?$tdata['member'][0]['id_kota']:'';
                    $tdata['no_anggota_from_db'] = $tdata['member'][0]['no_anggota'];
                    $created_at = isset($tdata['member'][0]['created_at'])?$tdata['member'][0]['created_at']:'';

                    //get date join
                    $month = date('m',strtotime($created_at));
                    $year = substr(date('Y',strtotime($created_at)), 2) ;

                    //untuk get kode wilayah
                    $getKodeProvinsi = $this->WilayahModel->listData(array('id'=>$id_provinsi));
                    $getKodeKota = $this->WilayahModel->listData(array('id'=>$id_kota));

                    //untuk get pendidikan sosial dan non sosial/jenis anggota
                    $pendidikan = $this->MembersModel->listDataPendidikan(array('id'=>$id_member));
                    $pendidikanterakhir = end($pendidikan);
                    $jenis_pendidikan = $pendidikanterakhir['jenis'];
                    
                    if ($jenis_pendidikan == 2) {//jika non sosial
                        $jenis_anggota = 1;
                    }else{
                        $jenis_anggota = 2;
                    }

                    $kodeprovinsi = $tdata['kodeprovinsi'] = isset($getKodeProvinsi[0]['kode'])?$getKodeProvinsi[0]['kode']:'';
                    $kodekota = $tdata['kodekota'] = isset($getKodeKota[0]['kode'])?$getKodeKota[0]['kode']:'';

                    $tdata['no_anggota_generate'] = $kodeprovinsi.'.'.$kodekota.'.'.$month.$year.'.'.$jenis_anggota.'.';
                    
                    $tdata['success'] = 'success';
                } else if($NoAnggota == 'exist'){
                    $tdata['exist'] = 'exist';
                }
            }else{
                $tdata['error_hash'] = $validator->GetErrors();
            }
        }
        
        $this->load->view($this->router->class.'/noanggota',$tdata);
    }

    public function autodatecreated(){
        $doUpdate = array();
        $member = $this->MembersModel->listData();
        foreach ($member as $key => $value) {
            if($value['foto'] != ''){
                $expfoto = explode('/', $value['foto']);
                $this->db->query("
                UPDATE members
                SET
                    created_at = '".$expfoto[0]."-".$expfoto[1]."-".$expfoto[2]."'
                WHERE
                    id = ".$value['id']."
                ");
                $doUpdate[] = array('id' => $value['id'], 'created_at' => $expfoto[0]."-".$expfoto[1]."-".$expfoto[2]);
            }
        }
        echo '<pre>';
        print_r($doUpdate);
    }

    public function autonoanggota(){
        $doUpdate = array();
        $member = $this->MembersModel->listData();
        foreach ($member as $key => $value) {
            if($value['no_anggota'] != ''){
                $expnoanggota = explode('.', $value['no_anggota']);
                $month = date('m',strtotime($value['created_at']));
                $year = substr(date('Y',strtotime($value['created_at'])), 2);
                if($expnoanggota[3] == 1){
                    $jenis_anggota = '2';
                }else{
                    $jenis_anggota = '1';
                }
                $noanggota = $expnoanggota[0].'.'.$expnoanggota[1].'.'.$month.$year.'.'.$jenis_anggota.'.'.$expnoanggota[4];
                $this->db->query("
                UPDATE members
                SET
                    no_anggota = '".$noanggota."'
                WHERE
                    id = ".$value['id']."
                ");
                $doUpdate[] = array('id' => $value['id'], 'no_anggota' => $noanggota);
            }
        }
        echo '<pre>';
        print_r($doUpdate);
    }
	
	public function rekomendasi() {
        $tdata = array();
        $this->module_name = $this->lang->line('rekomendasi');
        
        
        $tdata['member'] = $this->MembersModel->listData(array('id' => $this->session->userdata('iduser')));
        
        $filename = $this->webconfig['media-path-cache'].'suratrekomendasi-'.$tdata['member'][0]['no_anggota'].'.pdf';
        if (file_exists($filename)) {
            @unlink($filename);
        } else {
        }
        
        /*CREATE PDF*/
        $thepath_create = $this->webconfig['media-path-cache'];
        $exp = explode("/",$thepath_create);
        $way = '';
        foreach($exp as $n){
            $way .= $n.'/';
            @mkdir($way);
        }

        $data_html = $this->load->view($this->router->class.'/template_rekomendasi',$tdata,true);
        
        $mpdf = new mPDF('utf-8', 'A4-L');
        $mpdf->AddPage('P');
        $pdfFilePath = $this->webconfig['media-path-cache'].'suratrekomendasi-'.$tdata['member'][0]['no_anggota'].'.pdf';
        $mpdf->WriteHTML($data_html);
        $mpdf->Output($pdfFilePath, "F");
        
        $tdata['filename'] = 'suratrekomendasi-'.$tdata['member'][0]['no_anggota'].".pdf";

        $ldata['content'] = $this->load->view($this->router->class . '/rekomendasi', $tdata, true);
        $this->load->sharedView('template', $ldata);
    }

    public function modifTglSurat() {
        $id = $this->input->post('dataid');

        $memberdata = $this->MembersModel->listData(array('id' => $id));
        if($memberdata[0]['tgl_akhir_surat'] != '' && $memberdata[0]['tgl_akhir_surat'] < date('Y-m-d')){
            $allmember = $this->MembersModel->allListData();
            if(isset($allmember)){
                if($allmember['maxsurat'] != '' || $allmember['maxsurat'] != '0'){
                    $laststring = $allmember['maxsurat']+1;
                    $urutan = str_pad($laststring, 5, "0", STR_PAD_LEFT);
                }else{
                    $urutan = '00001';
                }
            }else{
                $urutan = '00001';
            }

            $doUpdate = $this->MembersModel->updateTglSurat(array(
                                                            'id' => $id
                                                            ,'count_surat' => $urutan
                                                            ,'tgl_awal_surat' => date('Y-m-d')
                                                            ,'tgl_akhir_surat' => date('Y-m-d', strtotime('+3 months'))
                                                        ));
            echo $doUpdate;
        }else if($memberdata[0]['tgl_awal_surat'] == '' && $memberdata[0]['tgl_akhir_surat'] == ''){
            $allmember = $this->MembersModel->allListData();
            if(isset($allmember)){
                if($allmember['maxsurat'] != '' || $allmember['maxsurat'] != '0'){
                    $laststring = $allmember['maxsurat']+1;
                    $urutan = str_pad($laststring, 5, "0", STR_PAD_LEFT);
                }else{
                    $urutan = '00001';
                }
            }else{
                $urutan = '00001';
            }

            $doUpdate = $this->MembersModel->updateTglSurat(array(
                                                            'id' => $id
                                                            ,'count_surat' => $urutan
                                                            ,'tgl_awal_surat' => date('Y-m-d')
                                                            ,'tgl_akhir_surat' => date('Y-m-d', strtotime('+3 months'))
                                                        ));
            echo $doUpdate;
        }else{
            echo 'notupdate';
        }
        
    }

    public function kartu() {
        $tdata = array();
        $tdata['member'] = $this->MembersModel->listData(array('id' => $this->session->userdata('iduser')));
        $tdata['memberpendidikan'] = $this->MembersModel->listDataPendidikan(array('id' => $this->session->userdata('iduser')));
        $normaldate = date('Y-m-d', strtotime($tdata['member'][0]['created_at']));
        $tdata['masaberlaku'] = short_date(date('Y-m-d', strtotime($normaldate. ' + '.$this->webconfig['masa_berlaku'].' years')));

        $expno = explode('.', $tdata['member'][0]['no_anggota']);
        $tdata['jenismember'] = $this->searchForId('1',$tdata['memberpendidikan']);
        $tdata['jenismember2'] = isset($expno[3])?$expno[3]:'';
        
        $ldata['content'] = $this->load->view($this->router->class . '/kartu', $tdata, true);
        $this->load->sharedView('template', $ldata);
    }

    public function searchForId($id, $array) {
       foreach ($array as $key => $val) {
           if ($val['jenis'] == $id) {
               return $id;
           }
       }
       return null;
    }

    public function set_barcode($code){
        //load library
        $this->load->library('zend');
        //load in folder Zend
        $this->zend->load('Zend/Barcode');
        //generate barcode
        Zend_Barcode::render('code128', 'image', array('text'=>$code), array());
    }

    public function uploadavatar(){
        $tdata = array();
        $tdata['lists'] = $this->MembersModel->listData(array('id'=>$this->session->userdata('iduser')));
        if($_POST)
        {
            $doUpdate = $this->MembersModel->updateAvatar(array(
                                                            'id' => $this->session->userdata('iduser')
                                                            ,'x1' => $this->input->post('x1')
                                                            ,'y1' => $this->input->post('y1')
                                                            ,'x2' => $this->input->post('x2')
                                                            ,'y2' => $this->input->post('y2')
                                                            ,'width_resize' => $this->webconfig["width_resize"]
                                                      ));
            if($doUpdate == 'empty'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_empty_field'));
            }else if($doUpdate == 'failed'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_database'));
            }else if($doUpdate == 'failed_ext'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_extension'));
            }else if($doUpdate == 'failed_dimension'){
                $tdata['error_hash'] = array('error' => $this->lang->line('msg_error_dimension'));
            }else if($doUpdate == 'success'){
                $tdata['success'] = $this->lang->line('msg_success_update');
                $tdata['lists'] = $this->MembersModel->listData(array('id'=>$id));
            }
            $ldata['content'] = $this->load->view($this->router->class.'/uploadavatar',$tdata, true);
                $this->load->sharedView('template', $ldata);
        }else{
            $ldata['content'] = $this->load->view($this->router->class.'/uploadavatar',$tdata, true);
            $this->load->sharedView('template', $ldata);
        }
    }

    public function payment(){
        if ($this->session->userdata('role') != 3 ) {
            redirect(base_url().'members');
        }

        if ($this->webconfig['payment_production'] == true) {
            $server_key = $this->webconfig['midtrans_server_key_production'];
        }else{
            $server_key = $this->webconfig['midtrans_server_key_sandbox'];
        }
        
        $params = array('server_key' => $server_key, 'production' => $this->webconfig['payment_production']);
        $this->load->library('midtrans');
        $this->midtrans->config($params);


        $tdata = array();
        $ldata = array();

        $ldata['content'] = $this->load->view($this->router->class . '/payment', $tdata, true);
        $this->load->sharedView('template', $ldata);
    }

}