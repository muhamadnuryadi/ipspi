<?php
function cutting_words($string, $max_length){  
    if (strlen($string) > $max_length){  
        $string = substr($string, 0, $max_length);  
        $pos = strrpos($string, " ");  
        if($pos === false) {  
                return substr($string, 0, $max_length)."...";  
        }  
            return substr($string, 0, $pos)."...";  
    }else{  
        return $string;  
    }  
} 
function clean_str($string){
    $string = str_replace(array("\\r\\n", "\\r", "\\n"), "", $string);
    $string = stripslashes($string);
    return $string;
}

function clean_str_html($string){
    $string = str_replace(array("\\r\\n", "\\r", "\\n"), "", $string);
    $string = htmlentities(stripslashes($string));
    return $string;
}

function instansitext($id){
    $string = '';
    $CI =& get_instance();
    $webconfig = $CI->config->item('webconfig');
    foreach($webconfig["instansi"] as $key => $value){
        if($key == $id){
            $string = $value;
        }
    }
    return $string;
}

function statustext($id){
    $string = '';
    $CI =& get_instance();
    if($id == 0){
        $string = 'Tidak Aktif';
    }else if($id == 1){
        $string = 'Aktif';
    }
    return $string;
}

function rupiah($number) {
    $rupiah = number_format($number, 0, ',', '.');
    return "Rp ".$rupiah;
}

function numberformat($number) {
    $numberformat = number_format($number, 0, ',', '.');
    return $numberformat;
}

if ( ! function_exists('getChannelChild')) {
    function getChannelChild($result = array(), $selected_id = '', $space = '&nbsp;&nbsp;') {
        $CI =& get_instance(); 
        $CI->load->database();
        $tdata = '';    
        $selected = ''; 

        foreach ($result as $val_child) {
            $selected = "";
            
            if($selected_id != '' && $selected_id == $val_child['id']) { $selected = "selected='selected'"; }
            $tdata .= "<option ".$selected." value='".$val_child['id']."'>".$space."&nbsp;".$val_child['name']." (".$val_child['fincode'].")</option>";
            
            if(isset($val_child['child']) && count($val_child['child']) > 0) {
                $tdata .= getChannelChild($val_child['child'], $selected_id,($space.'&nbsp;&nbsp;'));
            }
        }
        return $tdata;

    }
}

function gendertext($id){
    $string = '';
    $CI =& get_instance();
    if($id == 1){
        $string = 'Laki-laki';
    }else if($id == 2){
        $string = 'Perempuan';
    }
    return $string;
}

function perkawinantext($id){
    $string = '';
    $CI =& get_instance();
    if($id == 0){
        $string = 'Belum Menikah';
    }else if($id == 1){
        $string = 'Sudah Menikah';
    }else if($id == 2){
        $string = 'Duda';
    }else if($id == 3){
        $string = 'Janda';
    }
    return $string;
}