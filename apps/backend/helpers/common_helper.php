<?php
if ( ! function_exists('idParent')) {
	function idParent($id_parent_categories='') {
		$CI =& get_instance();
		$CI->load->sharedModel('ChildkategoriModel');

		$result = $CI->ChildkategoriModel->listData(array('id'=>$id_parent_categories));

		$data = isset($result['0']['id_parent_categories'])?$result['0']['id_parent_categories']:"";

		return $data;
	}
}


if ( ! function_exists('GetImage')) {
	function GetImage($id='') {
		$CI =& get_instance();
		$CI->load->sharedModel('PegawaiModel');

		$result = $CI->GambarModel->listData(array('id'=>$id));

		$data = isset($result['0']['path'])?$result['0']['path']:"";

		return $data;
	}
}


if ( ! function_exists('usersdata')) {
	function usersdata($id='') {
		$CI =& get_instance();
		$CI->load->Model('PegawaiModel');

		$result = $CI->PegawaiModel->listData(array('id'=>$id));

		$data = isset($result['0']['fullname'])?$result['0']['fullname']:"";

		return $data;
	}
}

if ( ! function_exists('userwilayah')) {
	function userwilayah($id='') {
		$CI =& get_instance();
		$CI->load->Model('WilayahModel');

		$result = $CI->WilayahModel->listData(array('id'=>$id));

		$data = isset($result['0']['nama'])?$result['0']['nama']:"";

		return $data;
	}
}

if ( ! function_exists('usertingkat')) {
	function usertingkat($id='') {
		$CI =& get_instance();
		$CI->load->Model('TingkatModel');

		$result = $CI->TingkatModel->listData(array('id'=>$id));

		$data = isset($result['0']['name'])?$result['0']['name']:"";

		return $data;
	}
}


if ( ! function_exists('getPenulis')) {
	function getPenulis($id='',$role='') {
		$CI =& get_instance();
		$CI->load->sharedModel('MembersModel');
		if($role == '3'){
			$result = $CI->MembersModel->listData(array('id'=>$id));
			$data = isset($result['0']['nama'])?$result['0']['nama']:"";
		}else{
			$data = 'Admin';
		}
		return $data;
	}
}