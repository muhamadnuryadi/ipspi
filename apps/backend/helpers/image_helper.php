<?php 
if ( ! function_exists('thumb_image')) {
	function thumb_image($path = "", $size = "", $type = ""){
		$CI =& get_instance();
		$webconfig = $CI->config->item('webconfig');

		## DEFINE THE IMAGE ##
		if($type == 'suratmasuk'){
			$path_original   = $webconfig['media-path-files'];
			$server_original = $webconfig['media-server-files'];
		}else if($type == 'berita'){
			$path_thumbs     = $webconfig['media-path-berita-thumb'];
			$server_thumbs   = $webconfig['media-server-berita-thumb'];
			$path_original   = $webconfig['media-path-berita'];
			$server_original = $webconfig['media-server-berita'];
		}else if($type == 'dpd'){
			$path_thumbs     = $webconfig['media-path-dpd-thumb'];
			$server_thumbs   = $webconfig['media-server-dpd-thumb'];
			$path_original   = $webconfig['media-path-dpd'];
			$server_original = $webconfig['media-server-dpd'];
		}else if($type == 'avatar'){
			$path_thumbs     = $webconfig['media-path-avatar-thumb'];
			$server_thumbs   = $webconfig['media-server-avatar-thumb'];
			$path_original   = $webconfig['media-path-avatar'];
			$server_original = $webconfig['media-server-avatar'];
		}else{
			$path_thumbs     = $webconfig['media-path-files'];
			$server_thumbs   = $webconfig['media-server-files'];
			$path_original   = $webconfig['media-path-files'];
			$server_original = $webconfig['media-server-files'];
		}
		
		if($path != "") {
			$str_path = "";
			$exp_img = explode(".", $path);
			
			
				if(file_exists($path_original.$path)) {
					$str_path = $server_original.$path;
				} else {
					$str_path = $webconfig['back_images']."no_image_news.jpg";
				}
			
			
			return $str_path;
		} else {
			return $webconfig['back_images']."no_image_news.jpg";
		}
	}
}
