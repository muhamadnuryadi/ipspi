<?php 
if ( ! function_exists('writeLog')) {
	function writeLog($params = array()) {
		$CI =& get_instance(); 
		$CI->load->library('session');
		$CI->load->database();	
		
		$module = isset($params['module'])?$params['module']:'';
		$details = isset($params['details'])?$params['details']:'';
		
		$data ['id'] = "Null";
		$data ['date_created'] = date("Y-m-d H:i:s");
		$data ['ip_address'] = $CI->input->ip_address();
		$data ['username'] = $CI->session->userdata('userid');
		$data ['module'] = $CI->db->escape_str($module);
		$data ['details'] = $CI->db->escape_str($details);
		$doInsert = $CI->db->insert('logs', $data);
		if($doInsert){		
			return true;
		}else{
			return false;
		}
	}
}
if ( ! function_exists('writeNotif')) {
	function writeNotif($params = array()) {
		$CI =& get_instance(); 
		$CI->load->library('session');
		$CI->load->database();	
		
		$username = isset($params['username'])?$params['username']:'';
		$details = isset($params['details'])?$params['details']:'';
		$details = isset($params['details'])?$params['details']:'';
		$destination = isset($params['destination'])?$params['destination']:'';
		$status = isset($params['status'])?$params['status']:'';
		$id_surat = isset($params['id_surat'])?$params['id_surat']:'';
		$is_suratmasuk = isset($params['is_suratmasuk'])?$params['is_suratmasuk']:'';
		$is_disposisi = isset($params['is_disposisi'])?$params['is_disposisi']:'';
		$is_notadinas = isset($params['is_notadinas'])?$params['is_notadinas']:'';
		$is_revisinotadinas = isset($params['is_revisinotadinas'])?$params['is_revisinotadinas']:'';
		$is_approvenotadinas = isset($params['is_approvenotadinas'])?$params['is_approvenotadinas']:'';
		$is_suratkeluar = isset($params['is_suratkeluar'])?$params['is_suratkeluar']:'';
		$is_forwardsuratkeluar = isset($params['is_forwardsuratkeluar'])?$params['is_forwardsuratkeluar']:'';
		$is_revisisuratkeluar = isset($params['is_revisisuratkeluar'])?$params['is_revisisuratkeluar']:'';
		$is_approvesuratkeluar = isset($params['is_approvesuratkeluar'])?$params['is_approvesuratkeluar']:'';
		
		$data ['id'] = "Null";
		$data ['datecreated'] = date("Y-m-d H:i:s");
		$data ['username'] = $CI->db->escape_str($username);
		$data ['details'] = $CI->db->escape_str($details);
		$data ['destination'] = $CI->db->escape_str($destination);
		$data ['status'] = $CI->db->escape_str($status);
		$data ['id_surat'] = $CI->db->escape_str($id_surat);
		$data ['is_suratmasuk'] = $CI->db->escape_str($is_suratmasuk);
		$data ['is_disposisi'] = $CI->db->escape_str($is_disposisi);
		$data ['is_notadinas'] = $CI->db->escape_str($is_notadinas);
		$data ['is_revisinotadinas'] = $CI->db->escape_str($is_revisinotadinas);
		$data ['is_approvenotadinas'] = $CI->db->escape_str($is_approvenotadinas);
		$data ['is_suratkeluar'] = $CI->db->escape_str($is_suratkeluar);
		$data ['is_forwardsuratkeluar'] = $CI->db->escape_str($is_forwardsuratkeluar);
		$data ['is_revisisuratkeluar'] = $CI->db->escape_str($is_revisisuratkeluar);
		$data ['is_approvesuratkeluar'] = $CI->db->escape_str($is_approvesuratkeluar);
		$doInsert = $CI->db->insert('notif', $data);
		if($doInsert){		
			return true;
		}else{
			return false;
		}
	}
}