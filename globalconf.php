<?php
date_default_timezone_set("Asia/Jakarta");

## EMAIL CONFIG##
DEFINE("HOST_EMAIL", "smtp.gmail.com");
DEFINE("PORT_EMAIL", 587);
DEFINE("SENDER_EMAIL", "smartkoding@gmail.com");
DEFINE("USER_EMAIL", "smartkoding@gmail.com");
DEFINE("PASS_EMAIL", "moehnoer");

// DEFINE("HOST_EMAIL", "ipspi.nuarypradipta.com");
// DEFINE("PORT_EMAIL", 465);
// DEFINE("SENDER_EMAIL", "noreply@ipspi.nuarypradipta.com");
// DEFINE("USER_EMAIL", "noreply@ipspi.nuarypradipta.com");
// DEFINE("PASS_EMAIL", "ipspi1234");

## DATABASE CONFIGURATION ##
DEFINE("APP_HOST", "localhost");
DEFINE("APP_USERNAME", "root");
DEFINE("APP_PASSWORD", "1234");
DEFINE("APP_DBNAME", "ipspi");

## URL CONFIG ##
DEFINE("APP_DOMAIN", "localhost:10000/ipspi");
DEFINE("APP_DOMAIN_BACK", "localhost:10000/ipspi/admincms");
DEFINE("APP_PATH", "/var/www/html/ipspi/public/");


## CONFIG ##
$servername = "http://".APP_DOMAIN."/";
$pathbase              =  APP_PATH;
$servernamemedia       = $servername.'public/';

$config["webconfig"]["count_data"] = 10;
$config["webconfig"]["count_data_popup"] = 10;
$config["webconfig"]["max_file_size"] = 50 ; //MB
$config["webconfig"]["max_foto_filesize"] = 10; //MB MAX 32MB
$config["webconfig"]["width_resize"] = 600; // 600px
$config["webconfig"]["base_site"] = $servername;
$config["webconfig"]["site_url"] = "www.".APP_DOMAIN;
$config["webconfig"]["base_template"] = $servernamemedia."template/backend_template/";
$config["webconfig"]["images"] = $servernamemedia."template/backend_template/img/";
$config["webconfig"]["back_base_template"] = $servernamemedia."template/backend_template/";
$config["webconfig"]["back_images"] = $servernamemedia."template/backend_template/img/";

$config["webconfig"]["media-server"] = $servernamemedia."media/";
$config["webconfig"]["media-server-embeded"] = $servernamemedia."media/embeded/";

## FRONT END ##
$config["webconfig"]["frontend_template"] = $servernamemedia."template/frontend_template/";

# GENERAL IMAGES #
$config["webconfig"]["media-server-images-original"] = $servernamemedia."media/images/original/";
$config["webconfig"]["media-path-images-original"] = $pathbase."media/images/original/";

# GENERAL FILES #
$config["webconfig"]["media-server-files"] = $servernamemedia."media/files/";
$config["webconfig"]["media-path-files"] = $pathbase."media/files/";

# GENERAL CACHE #
$config["webconfig"]["media-server-cache"] = $servernamemedia."media/cache/";
$config["webconfig"]["media-path-cache"] = $pathbase."media/cache/";

## IMAGE SIZE ##
$config["webconfig"]["image_slider"] = array('1100x600');
$config["webconfig"]["image_berita"] = array('658x496','658x248','350x263','134x100');
$config["webconfig"]["image_jurnal"] = array('600x338','78x43');
$config["webconfig"]["image_programs"] = array('743x557','260x195','65x49');
$config["webconfig"]["image_video"] = array('743x557','260x195','65x49');
$config["webconfig"]["image_gallery"] = array('600x400');

# GENERAL upload #
$config["webconfig"]["media-server-upload"] = $servernamemedia."media/upload/";
$config["webconfig"]["media-path-upload"] = $pathbase."media/upload/";

# GENERAL BERITA #
$config["webconfig"]["media-server-berita"] = $servernamemedia."media/berita/";
$config["webconfig"]["media-path-berita"] = $pathbase."media/berita/";
$config["webconfig"]["media-server-berita-thumb"] = $servernamemedia."media/berita/thumb/";
$config["webconfig"]["media-path-berita-thumb"] = $pathbase."media/berita/thumb/";

# GENERAL JURNAL #
$config["webconfig"]["media-server-jurnal"] = $servernamemedia."media/jurnal/";
$config["webconfig"]["media-path-jurnal"] = $pathbase."media/jurnal/";
$config["webconfig"]["media-server-jurnal-thumb"] = $servernamemedia."media/jurnal/thumb/";
$config["webconfig"]["media-path-jurnal-thumb"] = $pathbase."media/jurnal/thumb/";

$config["webconfig"]["bidang_setting"] = array(
                                            1 => 'Anak',
                                            2 => 'Kemiskinan',
                                            3 => 'Napza',
                                            4 => 'AIDS/HIV',
                                            5 => 'Klinis',
                                            6 => 'Dll'
                                        );
$config["webconfig"]["peran_utama"] = array(
                                            1 => 'Pendamping',
                                            2 => 'Pengajar',
                                            3 => 'Dll'
                                        );
$config["webconfig"]["jenis_keahlian"] = array(
                                            1 => 'Pendampingan',
                                            2 => 'Mediator',
                                            3 => 'Dll'
                                        );

$config["webconfig"]["sertifikasi_tipe"] = array('1'=>'Generalisasi','2'=>'Spesialisasi');
$config["webconfig"]["pendidikan_jenis"] = array('1'=>'Non Sosial','2'=>'Sosial');

$config["webconfig"]["media-server-images"] = $servernamemedia."media/images/";
$config["webconfig"]["media-path-images"] = $pathbase."media/images/";